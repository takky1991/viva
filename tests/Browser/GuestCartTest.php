<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CartPage;
use Tests\Browser\Pages\ProductPage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\GuestCart;
use Viva\Product;

class GuestCartTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testEmptyGuestCartPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new CartPage())
                ->assertSee('Vaša korpa je prazna');
        });
    }

    public function testGuestCartPage(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product1',
            'on_sale' => true,
            'price' => 10.66,
            'discount_price' => 6.96,
        ]);
        $product2 = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product2',
            'on_sale' => false,
            'price' => 60.99,
            'discount_price' => 16.77,
        ]);
        Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product3',
            'on_sale' => true,
            'price' => 60.19,
            'discount_price' => 66.96,
        ]);

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->visit(new ProductPage($product1))
                ->assertSee('Product1')
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Product1')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->visit(new ProductPage($product2))
                ->assertSee('Product2')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Product2')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->click('#header-cart-icon')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->click('._dusk_to_cart');
                })

                ->assertPathIs('/korpa')
                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product2')
                ->assertSeeIn('.cart-page #product-0 .column-3', 60.99)
                ->assertDontSeeIn('.cart-page #product-0 .column-3', 16.77)
                ->assertSeeIn('.cart-page #product-0 .column-4', 1)
                ->assertSeeIn('.cart-page #product-0 .column-5', 1 * 60.99)

                ->assertSeeIn('.cart-page #product-1 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-1 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-1 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-1 .column-4', 2)
                ->assertSeeIn('.cart-page #product-1 .column-5', 2 * 6.96)

                ->assertDontSeeIn('.cart-page', 'Product3')
                ->assertDontSeeIn('.cart-page', 60.19)
                ->assertDontSeeIn('.cart-page', 66.96)

                ->assertSeeIn('.cart-page .total-price', 'Cijena')
                ->assertSeeIn('.cart-page .total-price', 2 * 6.96 + 1 * 60.99)

                // Change quantities
                ->press('#product-0 .wrap-num-product .btn-num-product-up')
                ->press('#product-1 .wrap-num-product .btn-num-product-up')
                ->press('#product-1 .wrap-num-product .btn-num-product-up')
                ->press('#update-cart-button')

                ->assertSeeIn('.cart-page', 'Korpa je uspješno ažurirana.')

                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product2')
                ->assertSeeIn('.cart-page #product-0 .column-3', 60.99)
                ->assertDontSeeIn('.cart-page #product-0 .column-3', 16.77)
                ->assertSeeIn('.cart-page #product-0 .column-4', 2)
                ->assertSeeIn('.cart-page #product-0 .column-5', 2 * 60.99)

                ->assertSeeIn('.cart-page #product-1 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-1 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-1 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-1 .column-4', 4)
                ->assertSeeIn('.cart-page #product-1 .column-5', 4 * 6.96)

                ->assertDontSeeIn('.cart-page', 'Product3')
                ->assertDontSeeIn('.cart-page', 60.19)
                ->assertDontSeeIn('.cart-page', 66.96)

                ->assertSeeIn('.cart-page .total-price', 'Cijena')
                ->assertSeeIn('.cart-page .total-price', 2 * 60.99 + 4 * 6.96)

                // Remove products
                ->press('#delete-product-0-link')
                ->assertSeeIn('.cart-page', 'Proizvod je uspješno uklonjen iz korpe.')
                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-0 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-0 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-0 .column-4', 4)
                ->assertSeeIn('.cart-page #product-0 .column-5', 4 * 6.96)

                ->assertDontSeeIn('.cart-page', 'Product2')
                ->assertDontSeeIn('.cart-page', 60.99)
                ->assertDontSeeIn('.cart-page', 16.77)
                ->assertDontSeeIn('.cart-page', 2 * 60.99)

                ->assertDontSeeIn('.cart-page', 'Product3')
                ->assertDontSeeIn('.cart-page', 60.19)
                ->assertDontSeeIn('.cart-page', 66.96)

                ->press('#delete-product-0-link')
                ->assertDontSeeIn('.cart-page', 'Proizvod je uspješno uklonjen iz korpe.')
                ->assertSeeIn('.cart-page', 'Vaša korpa je prazna')
                ->assertDontSeeIn('.cart-page', 'Product1')
                ->assertDontSeeIn('.cart-page', 10.66)
                ->assertDontSeeIn('.cart-page', 6.96)
                ->assertDontSeeIn('.cart-page', 3 * 6.96)
                ->assertDontSeeIn('.cart-page', 'Product2')
                ->assertDontSeeIn('.cart-page', 60.99)
                ->assertDontSeeIn('.cart-page', 16.77)
                ->assertDontSeeIn('.cart-page', 3 * 60.99)
                ->assertDontSeeIn('.cart-page', 'Product3')
                ->assertDontSeeIn('.cart-page', 60.19)
                ->assertDontSeeIn('.cart-page', 66.96);
        });

        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'product_id' => $product2->id,
        ]);
    }

    public function testGuestCartProductPriceChangedAlert(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['brand_id' => $brand->id]);
        $product2 = Product::factory()->create(['brand_id' => $brand->id]);
        Product::factory()->create(['brand_id' => $brand->id]);

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->visit(new ProductPage($product1))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->visit(new ProductPage($product2))
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                });

            $browser
                ->visit(route('cart.show'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.');

            $guestCart = GuestCart::first();
            $guestCart->product_price_changed = true;
            $guestCart->save();

            $browser
                ->visit(route('home'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.')
                ->visit(route('cart.show'))
                ->assertSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.')
                ->visit(route('checkout.one'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.');

            $this->assertDatabaseHas('guest_carts', [
                'id' => $guestCart->id,
                'product_price_changed' => 0,
            ]);
        });
    }

    public function testGuestCartProductRemovedAlert(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['brand_id' => $brand->id]);
        $product2 = Product::factory()->create(['brand_id' => $brand->id]);
        Product::factory()->create(['brand_id' => $brand->id]);

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->visit(new ProductPage($product1))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->visit(new ProductPage($product2))
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                });

            $browser
                ->visit(route('cart.show'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.');

            $guestCart = GuestCart::first();
            $guestCart->product_removed = true;
            $guestCart->save();

            $browser
                ->visit(route('home'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.')
                ->visit(route('cart.show'))
                ->assertSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.')
                ->visit(route('checkout.one'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.');

            $this->assertDatabaseHas('guest_carts', [
                'id' => $guestCart->id,
                'product_removed' => 0,
            ]);
        });
    }

    public function testFreeShippingTooltipOnCart(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'price' => 50,
            'on_sale' => false,
        ]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser
                ->visit(new ProductPage($product))
                //->assertSeeIn('.shipping-tooltip', 'DOSTAVA ' . config('app.lowest_shipping_cost') . 'KM!')

                // open quick cart
                ->click('#header-cart-icon')
                ->waitFor('.header-cart-content')
                ->pause(500)
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.header-cart-content', 'Vas dijeli do besplatne dostave!')
                ->click('.header-cart .close-icon')
                ->pause(500)

                // add item
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })
                ->assertSeeIn('.shipping-tooltip', formatPrice($product->price))

                // open quick cart
                ->click('#header-cart-icon')
                ->waitFor('.header-cart-content')
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertSeeIn('.header-cart-content', '20.00KM Vas dijeli do besplatne dostave!')
                ->pause(400)
                ->click('.header-cart .close-icon')
                ->pause(500)

                // go to cart page
                ->visit(route('cart.show'))
                ->assertDontSeeIn('.cart-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertSeeIn('.cart-page', '20.00KM Vas dijeli do besplatne dostave!')

                // add item
                ->visit(new ProductPage($product))
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })
                ->assertDontSeeIn('.shipping-tooltip', 'DOSTAVA '.config('app.lowest_shipping_cost').'KM!')

                // go to cart page
                ->visit(route('cart.show'))
                ->assertSeeIn('.cart-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.cart-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.shipping-tooltip', 'DOSTAVA '.config('app.lowest_shipping_cost').'KM!')

                // open quick cart
                ->click('#header-cart-icon')
                ->pause(400)
                ->assertSeeIn('.header-cart-total', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSee('.header-cart-total', 'Vas dijeli do besplatne dostave!')

                // remove product from cart
                ->click('.header-cart-content #remove-product-1')
                ->assertSee('Vaša korpa je prazna')
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.header-cart-content', 'Vas dijeli do besplatne dostave!');
            //->click('.header-cart .close-icon')
            //->pause(500)
            //->assertSeeIn('.shipping-tooltip', 'DOSTAVA ' . config('app.lowest_shipping_cost') . 'KM!');
        });
    }
}
