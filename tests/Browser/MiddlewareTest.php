<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\ArticlePage;
use Tests\Browser\Pages\BrandCategoryPage;
use Tests\Browser\Pages\BrandPage;
use Tests\Browser\Pages\CategoryPage;
use Tests\Browser\Pages\DiscountCollectionPage;
use Tests\Browser\Pages\ProductPage;
use Tests\DuskTestCase;
use Viva\Article;
use Viva\Brand;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\Product;

class MiddlewareTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testRedirectToBrandSlugMiddleware(): void
    {
        $brand = Brand::factory()->create(['name' => 'First name']);
        $category = Category::factory()->create(['title' => 'Category name first']);

        $this->browse(function (Browser $browser) use ($brand, $category) {
            $browser->visit(new BrandPage($brand));
            $browser->visit(new BrandCategoryPage($brand, $category));

            sleep(1);
            $brand->name = 'Second name';
            $brand->save();

            $browser
                ->visit('/brendovi/first-name')
                ->assertPathIs('/brendovi/second-name')

                ->visit('/brendovi/first-name/category-name-first')
                ->assertPathIs('/brendovi/second-name/category-name-first');

            $category->title = 'Category name second';
            $category->save();

            $browser
                ->visit('/brendovi/first-name/category-name-first')
                ->assertPathIs('/brendovi/second-name/category-name-second')

                ->visit('/brendovi/second-name/category-name-first')
                ->assertPathIs('/brendovi/second-name/category-name-second');
        });
    }

    public function testRedirectToProductSlugMiddleware(): void
    {
        $brand = Brand::factory()->create(['name' => 'Brand name']);
        $product = Product::factory()->create([
            'title' => 'First name',
            'package_description' => '40 ml',
            'brand_id' => $brand->id,
        ]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser->visit(new ProductPage($product));

            sleep(1);
            $product->title = 'Second name';
            $product->save();

            $browser
                ->visit('/proizvodi/brand-name-first-name-40-ml')
                ->assertPathIs('/proizvodi/brand-name-second-name-40-ml');
        });
    }

    public function testRedirectToCategorySlugMiddleware(): void
    {
        $category = Category::factory()->create(['title' => 'First name']);

        $this->browse(function (Browser $browser) use ($category) {
            $browser->visit(new CategoryPage($category));

            sleep(1);
            $category->title = 'Second name';
            $category->save();

            $browser
                ->visit('/kategorije/first-name')
                ->assertPathIs('/kategorije/second-name');
        });
    }

    public function testRedirectToArticleSlugMiddleware(): void
    {
        $article = Article::factory()->create(['title' => 'First name']);

        $this->browse(function (Browser $browser) use ($article) {
            $browser->visit(new ArticlePage($article));

            sleep(1);
            $article->title = 'Second name';
            $article->save();

            $browser
                ->visit('/savjeti-novosti/first-name')
                ->assertPathIs('/savjeti-novosti/second-name');
        });
    }

    public function testRedirectToDiscountCollectionSlugMiddleware(): void
    {
        $discountCollection = DiscountCollection::factory()->create(['name' => 'First name', 'active' => true]);

        $this->browse(function (Browser $browser) use ($discountCollection) {
            $browser->visit(new DiscountCollectionPage($discountCollection));

            sleep(1);
            $discountCollection->name = 'Second name';
            $discountCollection->save();

            $browser
                ->visit('/popusti/first-name')
                ->assertPathIs('/popusti/second-name');
        });
    }
}
