<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\BrandPage;
use Tests\DuskTestCase;
use Viva\Brand;

class BrandTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testShowPage(): void
    {
        $brand = Brand::factory()->create();

        $this->browse(function (Browser $browser) use ($brand) {
            $browser
                ->visit(new BrandPage($brand))
                ->assertSeeIn('.bread-crumb', 'Brendovi')
                ->assertSeeIn('.bread-crumb', $brand->name)
                ->assertSee($brand->name);
        });
    }
}
