<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class OrdersPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('user.orders');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser
            ->assertPathIs('/korisnik/narudzbe')
            ->assertSee('Moje narudžbe');
    }

    /**
     * Get the element shortcuts for the page.
     */
    public function elements(): array
    {
        return [
            //
        ];
    }
}
