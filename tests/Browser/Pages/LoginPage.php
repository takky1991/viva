<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Viva\User;

class LoginPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('login');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/prijava')
            ->assertSee('Prijava');
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            '@email' => '#email',
            '@password' => '#password',
            '@remember' => '#remember',
            '@loginButton' => '#login-button',
            '@forgotPassword' => '#forgot-password',
        ];
    }

    public function loginWithForm(Browser $browser, User $user)
    {
        $browser
            ->type('@email', $user->email)
            ->type('@password', 'secret')
            ->press('@loginButton')
            ->assertPathIs('/')
            ->assertSee('Moj račun')
            ->assertDontSee('Prijava');
    }
}
