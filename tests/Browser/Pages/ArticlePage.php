<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Viva\Article;

class ArticlePage extends BasePage
{
    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('articles.show', ['article' => $this->article->getSlug()]);
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/savjeti-novosti/'.$this->article->getSlug());
    }

    /**
     * Get the element shortcuts for the page.
     */
    public function elements(): array
    {
        return [
            //'@element' => '#selector',
        ];
    }
}
