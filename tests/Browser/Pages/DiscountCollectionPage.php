<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Viva\DiscountCollection;

class DiscountCollectionPage extends BasePage
{
    protected $discountCollection;

    public function __construct(DiscountCollection $discountCollection)
    {
        $this->discountCollection = $discountCollection;
    }

    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('discount_collections.show', ['discountCollection' => $this->discountCollection->getSlug()]);
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/popusti/'.$this->discountCollection->getSlug());
    }

    /**
     * Get the element shortcuts for the page.
     */
    public function elements(): array
    {
        return [

        ];
    }
}
