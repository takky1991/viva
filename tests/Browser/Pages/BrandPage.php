<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Viva\Brand;

class BrandPage extends BasePage
{
    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('brands.show', ['brand' => $this->brand->getSlug()]);
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser
            ->assertPathIs('/brendovi/'.$this->brand->getSlug())
            ->assertSee($this->brand->name);
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            //'@element' => '#selector',
        ];
    }
}
