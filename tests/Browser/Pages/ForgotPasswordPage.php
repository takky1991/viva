<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class ForgotPasswordPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('password.request');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/sifra/promjena')
            ->assertSee('Promjena šifre');
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            '@email' => '#email',
            '@sendEmailButton' => '#send-email-button',
        ];
    }

    public function requestPasswordChange(Browser $browser, string $email)
    {
        $browser
            ->type('@email', $email)
            ->press('@sendEmailButton')
            ->assertPathIs('/sifra/promjena')
            ->assertSee('Poslali smo Vam e-mail za kreiranje nove šifre!');
    }
}
