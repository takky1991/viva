<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Viva\Brand;
use Viva\Category;

class BrandCategoryPage extends BasePage
{
    protected $brand;

    protected $category;

    public function __construct(Brand $brand, Category $category)
    {
        $this->brand = $brand;
        $this->category = $category;
    }

    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('brands.show', ['brand' => $this->brand->getSlug(), 'category' => $this->category->getSlug()]);
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser
            ->assertPathIs('/brendovi/'.$this->brand->getSlug().'/'.$this->category->getSlug())
            ->assertSee($this->brand->name);
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            //'@element' => '#selector',
        ];
    }
}
