<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class PasswordPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('user.password');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser
            ->assertPathIs('/korisnik/sifra')
            ->assertSee('Promjena šifre');
    }

    /**
     * Get the element shortcuts for the page.
     */
    public function elements(): array
    {
        return [
            //
        ];
    }
}
