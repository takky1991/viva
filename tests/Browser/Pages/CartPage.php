<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class CartPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('cart.show');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/korpa');
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            //'@element' => '#selector',
        ];
    }
}
