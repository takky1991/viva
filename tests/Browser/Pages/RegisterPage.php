<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class RegisterPage extends BasePage
{
    /**
     * Get the URL for the page.
     */
    public function url(): string
    {
        return route('register');
    }

    /**
     * Assert that the browser is on the page.
     */
    public function assert(Browser $browser): void
    {
        $browser->assertPathIs('/registracija')
            ->assertSee('Registracija');
    }

    /**
     * Get the global element shortcuts for the site.
     */
    public static function siteElements(): array
    {
        return [
            '@firstName' => '#first-name',
            '@lastName' => '#last-name',
            '@email' => '#email',
            '@password' => '#password',
            '@passwordConfirmation' => '#password-confirmation',
            '@registerButton' => '#register-button',
            '@termsAccepted' => '#terms-accepted',
        ];
    }

    public function registerWithForm(Browser $browser, array $user)
    {
        $browser
            ->type('@firstName', $user['first_name'])
            ->type('@lastName', $user['last_name'])
            ->type('@email', $user['email'])
            ->type('@password', $user['password'])
            ->type('@passwordConfirmation', $user['password_confirmation'])
            ->click('#customRadioLabel1')
            ->press('@registerButton')
            ->assertPathIs('/email/potvrda')
            ->assertSee('Moj račun')
            ->assertSee('Uspješno ste se registrovali')
            ->assertSee('kliknite za ponovno slanje');
    }
}
