<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\HomePage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\HeroArticle;
use Viva\Product;

class HomepageTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testHomepage(): void
    {
        $brand = Brand::factory()->create();
        $heroArticle = HeroArticle::factory()->create();
        $product = Product::factory()->notOnSale()->create(['brand_id' => $brand]);
        $featuredProduct = Product::factory()->notOnSale()->create(['featured' => true, 'brand_id' => $brand]);
        $onSaleProduct = Product::factory()->onSale()->create(['brand_id' => $brand]);

        $this->browse(function (Browser $browser) use ($heroArticle) {
            $browser
                ->visit(new HomePage())

                // assert hero article
                ->assertSee($heroArticle->title)
                ->assertSee($heroArticle->subtitle)
                ->assertSee($heroArticle->button_text);

            /* // assert on sale products
            ->assertSeeIn('.on-sale', $onSaleProduct->name())
            ->assertDontSeeIn('.on-sale', $product->name())
            ->assertDontSeeIn('.on-sale', $featuredProduct->name())

            // assert featured products
            ->assertSeeIn('.most-sold', $featuredProduct->name())
            ->assertDontSeeIn('.most-sold', $product->name())
            ->assertDontSeeIn('.most-sold', $onSaleProduct->name()); */
        });
    }
}
