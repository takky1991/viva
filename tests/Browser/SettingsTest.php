<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Illuminate\Support\Facades\DB;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\LoginPage;
use Tests\Browser\Pages\OrdersPage;
use Tests\Browser\Pages\PasswordPage;
use Tests\Browser\Pages\SettingsPage;
use Tests\DuskTestCase;
use Viva\NewsletterContact;
use Viva\Order;
use Viva\ShippingMethod;
use Viva\User;

class SettingsTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testSettingsPageForGuestUsers(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->visit(route('user.settings'))
                ->assertSee('Prijava')
                ->type('#email', $user->email)
                ->type('#password', 'secret')
                ->click('#login-button')
                ->assertPathIs('/korisnik/postavke')
                ->assertSee('Moj račun');
        });
    }

    public function testAvatarLinksToSettingsPage(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit('/')
                ->click('#user-avatar')
                ->assertPathIs('/korisnik/postavke')
                ->assertSee('Moj račun');
        });
    }

    public function testSettingsUpdate(): void
    {
        $user = User::factory()->admin()->create([
            'first_name' => 'Oldname',
            'last_name' => 'Oldlastname',
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Oldname',
            'last_name' => 'Oldlastname',
            'gender' => 0,
            'birth_date' => null,
            'street_address' => null,
            'postalcode' => null,
            'city' => null,
            'country_id' => null,
            'phone' => null,
        ]);

        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => $user->email,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(new SettingsPage)
                ->click('#customRadioLabel1')
                ->type('#first-name', 'Newname')
                ->type('#last-name', 'Newlastname')
                ->select('birth_day', '20')
                ->select('birth_month', '6')
                ->select('birth_year', '1991')
                ->type('#street-address', 'Cazinskih brigada bb')
                ->type('#postalcode', '77220')
                ->type('#city', 'Cazin')
                ->type('#phone-8_phone_number', '061621622')
                ->click('#customCheckboxLabel')
                ->click('#save-settings-button');
        });

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Newname',
            'last_name' => 'Newlastname',
            'gender' => 2,
            'birth_date' => '1991-06-20',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
            'phone' => '+38761621622',
        ]);

        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => $user->email,
            'user_id' => $user->id,
            'first_name' => null,
            'last_name' => null,
        ]);

        $this->assertEquals(0, DB::table('email_log')->count());
    }

    public function testDisableNewsletter(): void
    {
        $user = User::factory()->admin()->create([
            'first_name' => 'Oldname',
            'last_name' => 'Oldlastname',
        ]);

        $user->newsletterContact()->create([
            'email' => $user->email,
            'token' => '123123',
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Oldname',
            'last_name' => 'Oldlastname',
            'gender' => 0,
            'birth_date' => null,
            'street_address' => null,
            'postalcode' => null,
            'city' => null,
            'country_id' => null,
            'phone' => null,
        ]);

        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => $user->email,
            'user_id' => $user->id,
            'token' => '123123',
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(new SettingsPage)
                ->click('#customRadioLabel1')
                ->type('#first-name', 'Newname')
                ->type('#last-name', 'Newlastname')
                ->select('birth_day', '20')
                ->select('birth_month', '6')
                ->select('birth_year', '1991')
                ->type('#street-address', 'Cazinskih brigada bb')
                ->type('#postalcode', '77220')
                ->type('#city', 'Cazin')
                ->type('#phone-8_phone_number', '061621622')
                ->click('#customCheckboxLabel')
                ->click('#save-settings-button');
        });

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Newname',
            'last_name' => 'Newlastname',
            'gender' => 2,
            'birth_date' => '1991-06-20',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
            'phone' => '+38761621622',
        ]);

        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => $user->email,
        ]);

        $this->assertEquals(0, DB::table('email_log')->count());
    }

    public function testChangePassword(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->visit(new LoginPage)
                ->type('#email', $user->email)
                ->type('#password', 'secret')
                ->click('#login-button')
                ->assertPathIs('/')
                ->assertSee('Moj račun')
                ->visit(new PasswordPage)
                ->type('#old-password', 'secret')
                ->type('#password', 'newpassword')
                ->type('#password-confirm', 'newpassword')
                ->click('#save-password-button')
                ->assertSee('Vaša nova šifra je uspješno spremljena.')
                ->logout()
                ->visit(new LoginPage)
                ->type('#email', $user->email)
                ->type('#password', 'secret')
                ->click('#login-button')
                ->assertSee('Ovi podaci ne odgovaraju našima.')
                ->type('#password', 'newpassword')
                ->click('#login-button')
                ->assertPathIs('/')
                ->assertSee('Moj račun');
        });
    }

    public function testMyOrders(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(new OrdersPage)
                ->assertSee('Trenutno nemate narudžbi.');

            $order1 = Order::factory()->create(['user_id' => $user->id]);
            $order2 = Order::factory()->create(['user_id' => $user->id]);

            $browser
                ->visit(new OrdersPage)
                ->assertSee($order1->order_number)
                ->assertSee(Order::$statusWording[$order1->status])
                ->assertSee($order2->order_number);
        });
    }

    public function testMyOrdersWithShipping(): void
    {
        $user = User::factory()->create();
        $shippingMethod = ShippingMethod::find(6);
        $order = Order::factory()->create([
            'user_id' => $user->id,
            'shipping_method_id' => $shippingMethod->id,
            'shipping_key' => $shippingMethod->key,
            'shipping_name' => $shippingMethod->name,
            'shipping_price' => $shippingMethod->price,
            'shipping_pickup' => $shippingMethod->pickup,
        ]);

        $this->browse(function (Browser $browser) use ($user, $order) {
            $browser
                ->loginAs($user)
                ->visit(new OrdersPage)
                ->assertSee('Narudžba '.$order->order_number)
                ->assertSee($order->created_at->format('d.m.Y H:i'))
                ->assertSee('Status narudžbe:')
                ->assertSee(Order::$statusWording[$order->status])
                ->assertSee('Način preuzimanja')
                ->assertSee('Slanje na adresu')
                ->assertSee($order->shipping_name)
                ->assertSee('Podaci o kupcu')
                ->assertSee($order->first_name)
                ->assertSee($order->last_name)
                ->assertSee($order->street_address)
                ->assertSee($order->postalcode)
                ->assertSee($order->city)
                ->assertSee($order->country->name)
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem')
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number);

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->fullName())
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Proizvodi')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));
        });
    }

    public function testMyOrdersWithPickup(): void
    {
        $user = User::factory()->create();
        $shippingMethod = ShippingMethod::find(1);
        $order = Order::factory()->create([
            'user_id' => $user->id,
            'shipping_method_id' => $shippingMethod->id,
            'shipping_key' => $shippingMethod->key,
            'shipping_name' => $shippingMethod->name,
            'shipping_price' => $shippingMethod->price,
            'shipping_pickup' => $shippingMethod->pickup,
        ]);

        $this->browse(function (Browser $browser) use ($user, $order) {
            $browser
                ->loginAs($user)
                ->visit(new OrdersPage)
                ->assertSee('Narudžba '.$order->order_number)
                ->assertSee($order->created_at->format('d.m.Y H:i'))
                ->assertSee('Status narudžbe:')
                ->assertSee(Order::$statusWording[$order->status])
                ->assertSee('Način preuzimanja')
                ->assertSee('Preuzimanje u')
                ->assertSee($order->shipping_name)
                ->assertSee('Podaci o kupcu')
                ->assertSee($order->first_name)
                ->assertSee($order->last_name)
                ->assertDontSee($order->street_address)
                ->assertDontSee($order->postalcode)
                ->assertDontSee($order->city)
                ->assertDontSee($order->country->name)
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem')
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number);

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->fullName())
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Proizvodi')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));
        });
    }

    public function testUnsubscribeNewsletterLink(): void
    {
        NewsletterContact::create([
            'email' => 'takky1991@gmail.com',
            'token' => '123456789',
        ]);

        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => 'takky1991@gmail.com',
            'token' => '123456789',
        ]);

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(route('newsletter_unsubscribe', ['token' => 'asdasd']))
                ->assertSee('OOPS!')
                ->visit(route('newsletter_unsubscribe', ['token' => '123456789']))
                ->assertSee('Uspješno ste se odjavili sa newsletter-a te više nećete primati email-ove u vezi novosti i akcija.');
        });

        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => 'takky1991@gmail.com',
        ]);
    }
}
