<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Spatie\Permission\Models\Role;
use Tests\Browser\Pages\ProductPage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\Cart;
use Viva\CartProduct;
use Viva\GuestCart;
use Viva\GuestCartProduct;
use Viva\NewsletterContact;
use Viva\Order;
use Viva\OrderProduct;
use Viva\Product;
use Viva\ProductVariant;
use Viva\User;

class CheckoutTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testGuestCheckoutWithShippingWithRegister(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');

        $customerRole = Role::where('name', 'customer')->first();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product1',
            'on_sale' => true,
            'price' => 10.66,
            'discount_price' => 6.96,
        ]);

        $productVariant = ProductVariant::factory()->create([
            'product_id' => $product->id,
            'name' => '34',
        ]);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'open_count' => 0,
            'add_to_cart_count' => 0,
            'buy_count' => 0,
        ]);

        $randomToken = random_token();
        // Assume this email is in the newsletter contacts
        NewsletterContact::create([
            'email' => 'tarik@gmail.com',
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'user_id' => null,
            'token' => $randomToken,
        ]);

        // Assert newsletter contact created
        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => 'tarik@gmail.com',
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'user_id' => null,
            'token' => $randomToken,
        ]);

        $this->browse(function (Browser $browser) use ($adminUser, $product, $customerRole, $randomToken) {
            $browser
                ->visit(route('checkout'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                // Test CartHasProducts middleware
                ->visit(route('checkout.one'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.two'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.three'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                // Add product to cart
                ->visit(new ProductPage($product))
                ->assertSee('Product1')
                ->assertMissing('.btn-num-product-up')
                ->assertDontSee('Odaberite veličinu prije dodavanja u korpu!')
                ->assertSee('34')
                ->press('DODAJ U KORPU')
                ->assertSee('Odaberite veličinu prije dodavanja u korpu!')
                ->click('.variant')
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) use ($product) {
                    $cart->assertSee('Proizvod je dodan u korpu')
                        ->assertSee($product->title)
                        ->assertSee('Br. 34')
                        ->press('.close-icon');
                })

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/dostava')

                ->assertSee('Dostava')
                ->assertSee('Informacije')
                ->assertSee('Plaćanje')
                ->assertSee('ODABERITE NAČIN DOSTAVE')
                ->assertSee('Dostava na adresu')
                ->assertSee('X-Express Brza pošta (1-2 dana)')
                ->assertSee('+ 3.00KM')
                ->assertSee('Preuzimanje u apoteci')
                ->assertSee('Centralna Apoteka VIVA, Mala Lisa, Cazin')
                ->assertSee('Apoteka VIVA, TC Robot, Ćoralići, Cazin')
                ->assertSee('Apoteka VIVA, Križ, V.Kladuša')
                ->assertSee('Apoteka VIVA, Gata, Bihać')
                ->assertSee('+ 0.00KM')
                ->assertSee($product->name())
                ->assertSee('Br. 34')
                ->assertSee($product->realPrice())
                ->assertDontSeeIn('.checkout-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')

                // Test validation
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/dostava')
                ->assertSee('Odaberite način dostave.')

                ->click('#x_express_label')
                ->assertSee('+ 3.00KM')
                ->assertSee($product->realPrice() + 3)
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/informacije');

            $this->assertDatabaseHas('guest_carts', [
                'shipping_method_id' => 7,
            ]);

            $this->assertEquals(1, GuestCart::count());
            $this->assertEquals(1, GuestCartProduct::count());

            // Enter checkout info
            $browser
                ->visit(route('checkout.two'))
                ->assertDontSeeIn('.checkout-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->type('first_name', 'Tarik')
                ->type('last_name', 'Coralic')
                ->type('#phone-8_phone_number', '061621622')
                ->type('email', 'tarik@gmail.com')
                ->click('.custom-control-label') // register checkbox
                ->type('password', 'secret123')
                ->type('password_confirmation', 'secret123')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->type('street_address', 'Cazinskih brigada bb')
                ->type('postalcode', '77220')
                ->type('city', 'Cazin')
                ->type('additional_info', 'ovo je dodatni komentar')
                ->press('#checkout-two-submit-button')
                ->assertUrlIs(route('checkout.three'))
                ->assertDontSee('Potrebno je prihvatiti uvjete kupovine.')
                ->click('#cash_on_delivery_label')
                ->press('#checkout-three-submit-button')
                ->assertSee('Potrebno je prihvatiti uvjete kupovine.')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button');

            $this->assertDatabaseHas('users', [
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'email' => 'tarik@gmail.com',
                'phone' => '+38761621622',
                'street_address' => 'Cazinskih brigada bb',
                'postalcode' => '77220',
                'city' => 'Cazin',
                'country_id' => 1, // for Bosnia
                'email_verified_at' => null,
                'terms_accepted' => true,
            ]);

            $this->assertDatabaseHas('model_has_roles', [
                'role_id' => $customerRole->id,
                'model_type' => \Viva\User::class,
                'model_id' => 2,
            ]);

            $user = User::where('email', 'tarik@gmail.com')->first();

            // Assert customer role assigned
            $this->assertTrue($user->hasRole('customer'));

            // Assert last_login is set
            $this->assertDatabaseMissing('users', [
                'id' => $user->id,
                'last_login' => null,
            ]);

            //Assert confirmation email sent
            $this->assertDatabaseHas('email_log', [
                'to' => $user->email,
                'subject' => 'Potvrdite Vašu email adresu | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            $this->assertDatabaseHas('products', [
                'id' => $product->id,
                'open_count' => 1,
                'add_to_cart_count' => 1,
                'buy_count' => 1,
            ]);

            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'email' => 'tarik@gmail.com',
                'shipping_method_id' => 7,
                'shipping_name' => 'X-Express Brza pošta (1-2 dana)',
                'shipping_key' => 'x_express',
                'shipping_pickup' => false,
                'shipping_price' => 3.00,
                'original_shipping_price' => 3.00,
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'phone' => '+38761621622',
                'street_address' => 'Cazinskih brigada bb',
                'postalcode' => '77220',
                'city' => 'Cazin',
                'country_id' => 1,
                'status' => 'created',
                'total_price' => 6.96,
                'total_price_with_shipping' => 9.96,
                'additional_info' => 'ovo je dodatni komentar',
                'payment_method' => 'cash_on_delivery',
            ]);

            $order = Order::first();

            $browser->assertPathIs('/kupovina/uspjesno/'.$order->order_number)->pause(1000);

            $this->assertDatabaseHas('order_products', [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand->name,
                'brand_description' => $product->brand->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'real_price' => $product->discount_price,
                'free_shipping' => $product->free_shipping,
                'quantity' => 1,
                'total_price' => $product->discount_price,
                'variants' => '34',
            ]);

            $this->assertEquals(0, GuestCart::count());
            $this->assertEquals(0, GuestCartProduct::count());

            $browser
                ->assertSee('Vaša narudžba je zaprimljena')
                ->assertSee('U svakom trenutku možete pregledati sve narudžbe i njihov trenutni status, tako što otvorite "Narudžbe" u Vašem računu ili putem ovog linka.')
                ->assertSee('Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona: '.config('app.phone_short').' ili putem maila na adresu '.config('app.email'))
                ->assertSee('INFORMACIJE O NARUDŽBI')
                ->assertSee('Način preuzimanja')
                ->assertSee('Slanje na adresu')
                ->assertSee($order->shipping_name)
                ->assertSee('Podaci o kupcu')
                ->assertSee($order->first_name)
                ->assertSee($order->last_name)
                ->assertSee($order->street_address)
                ->assertSee($order->postalcode)
                ->assertSee($order->city)
                ->assertSee($order->country->name)
                ->assertSee('Napomene uz narudžbu')
                ->assertSee($order->additional_info)
                ->assertSee('Kontakt')
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem (gotovina)');

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->name())
                    ->assertSee('Br. 34')
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Cijena')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));

            // Assert order successful email sent
            $this->assertDatabaseHas('email_log', [
                'to' => $order->email,
                'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            // Assert admin emails sent
            $this->assertDatabaseHas('email_log', [
                'to' => $adminUser->email,
                'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            // Assert newsletter contact still exists with user id
            $this->assertDatabaseHas('newsletter_contacts', [
                'email' => 'tarik@gmail.com',
                'first_name' => null,
                'last_name' => null,
                'user_id' => $user->id,
                'token' => $randomToken,
            ]);
        });
    }

    public function testGuestCheckoutWithPickupWithoutRegister(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');

        $customerRole = Role::where('name', 'customer')->first();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product1',
            'on_sale' => false,
            'price' => 55.00,
            'discount_price' => 6.96,
        ]);

        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => 'tarik@gmail.com',
        ]);

        $this->browse(function (Browser $browser) use ($adminUser, $product, $customerRole) {
            $browser
                ->visit(route('checkout'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                // Test CartHasProducts middleware
                ->visit(route('checkout.one'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.two'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.three'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                // Add product to cart
                ->visit(new ProductPage($product))
                ->assertSee('Product1')
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/dostava')

                ->assertSee('Dostava')
                ->assertSee('Informacije')
                ->assertSee('Plaćanje')
                ->assertSee('ODABERITE NAČIN DOSTAVE')
                ->assertSee('Dostava na adresu')
                ->assertSee('X-Express Brza pošta (1-2 dana)')
                ->assertSee('+ 0.00KM')
                ->assertSee('Preuzimanje u apoteci')
                ->assertSee('Centralna Apoteka VIVA, Mala Lisa, Cazin')
                ->assertSee('Apoteka VIVA, TC Robot, Ćoralići, Cazin')
                ->assertSee('Apoteka VIVA, Križ, V.Kladuša')
                ->assertSee('Apoteka VIVA, Gata, Bihać')
                ->assertSee($product->name())
                ->assertSee($product->realPrice())
                ->assertSee(2 * $product->realPrice())
                ->assertSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.checkout-page', '86.08KM Vas dijeli do besplatne dostave!')

                // Test validation
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/dostava')
                ->assertSee('Odaberite način dostave.')

                ->click('#viva_centrala_label')
                ->assertSee('+ 0.00KM')
                ->assertSee(2 * $product->realPrice())
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/informacije');

            $this->assertDatabaseHas('guest_carts', [
                'shipping_method_id' => 1,
            ]);

            $guestCart = GuestCart::first();

            $this->assertEquals(1, GuestCart::count());
            $this->assertEquals(1, GuestCartProduct::count());

            // Enter checkout info
            $browser
                ->visit(route('checkout.two'))
                ->assertSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.checkout-page', '86.08KM Vas dijeli do besplatne dostave!')
                ->type('first_name', 'Tarik')
                ->type('last_name', 'Coralic')
                ->type('#phone-8_phone_number', '061621622')
                ->type('email', 'tarik@gmail.com')
                ->type('additional_info', 'ovo je dodatni komentar')
                ->press('#checkout-two-submit-button')
                ->assertUrlIs(route('checkout.three'))
                ->click('#cash_on_delivery_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button');

            $this->assertDatabaseMissing('users', [
                'email' => 'tarik@gmail.com',
            ]);

            $this->assertDatabaseMissing('model_has_roles', [
                'role_id' => $customerRole->id,
                'model_type' => \Viva\User::class,
                'model_id' => 2,
            ]);

            //Assert confirmation email sent
            $this->assertDatabaseMissing('email_log', [
                'subject' => 'Potvrdite Vašu email adresu | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'email' => 'tarik@gmail.com',
                'shipping_method_id' => 1,
                'shipping_name' => 'Centralna Apoteka VIVA, Mala Lisa, Cazin',
                'shipping_key' => 'viva_centrala',
                'shipping_pickup' => true,
                'shipping_price' => 0.00,
                'original_shipping_price' => 0.00,
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'phone' => '+38761621622',
                'street_address' => null,
                'postalcode' => null,
                'city' => null,
                'country_id' => null,
                'status' => 'created',
                'total_price' => 110,
                'total_price_with_shipping' => 110,
                'additional_info' => 'ovo je dodatni komentar',
                'payment_method' => 'cash_on_delivery',
            ]);

            $order = Order::first();

            $this->assertDatabaseHas('order_products', [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand->name,
                'brand_description' => $product->brand->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'real_price' => $product->price,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'free_shipping' => $product->free_shipping,
                'quantity' => 2,
                'total_price' => 2 * $product->price,
            ]);

            $this->assertEquals(0, GuestCart::count());
            $this->assertEquals(0, GuestCartProduct::count());

            $browser
                ->assertPathIs('/kupovina/uspjesno/'.$order->order_number)
                ->assertSee('Vaša narudžba je zaprimljena')
                ->assertSee('Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona: '.config('app.phone_short').' ili putem maila na adresu '.config('app.email'))
                ->assertSee('INFORMACIJE O NARUDŽBI')
                ->assertSee('Način preuzimanja')
                ->assertSee('Preuzimanje u')
                ->assertSee($order->shipping_name)
                ->assertSee('Rok za preuzimanje')
                ->assertSee($order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.'))
                ->assertSee('Podaci o kupcu')
                ->assertSee($order->first_name)
                ->assertSee($order->last_name)
                ->assertSee('Kontakt')
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertSee('Napomene uz narudžbu')
                ->assertSee($order->additional_info)
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem (gotovina, kartično)');

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->name())
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Cijena')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));

            // Assert order successful email sent
            $this->assertDatabaseHas('email_log', [
                'to' => $order->email,
                'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            // Assert admin emails sent
            $this->assertDatabaseHas('email_log', [
                'to' => $adminUser->email,
                'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            // Assert newsletter contact created
            $this->assertDatabaseHas('newsletter_contacts', [
                'email' => 'tarik@gmail.com',
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'user_id' => null,
            ]);
        });
    }

    public function testUserCheckoutWithShipping(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');

        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product1',
            'on_sale' => true,
            'price' => 10.66,
            'discount_price' => 6.96,
        ]);

        $this->browse(function (Browser $browser) use ($adminUser, $user, $product) {
            $browser
                ->loginAs($user)

                ->visit(route('checkout'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.one'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.two'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                ->visit(route('checkout.three'))
                ->assertPathIs('/korpa')
                ->assertSee('Vaša korpa je prazna')

                // Add product to cart
                ->visit(new ProductPage($product))
                ->assertSee('Product1')
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/dostava')

                ->assertSee('Dostava')
                ->assertSee('Informacije')
                ->assertSee('Plaćanje')
                ->assertSee('ODABERITE NAČIN DOSTAVE')
                ->assertSee('Dostava na adresu')
                ->assertSee('X-Express Brza pošta (1-2 dana)')
                ->assertSee('+ 3.00KM')
                ->assertSee('Preuzimanje u apoteci')
                ->assertSee('Centralna Apoteka VIVA, Mala Lisa, Cazin')
                ->assertSee('Apoteka VIVA, TC Robot, Ćoralići, Cazin')
                ->assertSee('Apoteka VIVA, Križ, V.Kladuša')
                ->assertSee('Apoteka VIVA, Gata, Bihać')
                ->assertSee('+ 0.00KM')
                ->assertSee($product->name())
                ->assertSee($product->realPrice())
                ->assertSee(2 * $product->realPrice())
                ->assertDontSeeIn('.checkout-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')

                // Test validation
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/dostava')
                ->assertSee('Odaberite način dostave.')

                ->click('#x_express_label')
                ->assertSee('+ 3.00KM')
                ->assertSee(2 * $product->realPrice() + 3)
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.one'))
                ->assertPathIs('/kupovina/dostava')

                ->visit(route('checkout.two'))
                ->assertPathIs('/kupovina/informacije')

                ->visit(route('checkout.three'))
                ->assertPathIs('/kupovina/informacije');

            $this->assertDatabaseHas('carts', [
                'shipping_method_id' => 7,
            ]);

            $this->assertEquals(1, Cart::count());
            $this->assertEquals(1, CartProduct::count());

            // Enter checkout info
            $browser
                ->visit(route('checkout.two'))
                ->assertDontSeeIn('.checkout-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSee('Želim se registrovati')
                ->assertInputValue('first_name', 'Tarik')
                ->assertInputValue('last_name', 'Coralic')
                ->assertInputValue('phone', '+38761621622')
                ->assertInputValue('street_address', 'Cazinskih brigada bb')
                ->assertInputValue('postalcode', '77220')
                ->assertInputValue('city', 'Cazin')
                ->press('#checkout-two-submit-button')
                ->assertUrlIs(route('checkout.three'))
                ->click('#cash_on_delivery_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button');

            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'email' => $user->email,
                'shipping_method_id' => 7,
                'shipping_name' => 'X-Express Brza pošta (1-2 dana)',
                'shipping_key' => 'x_express',
                'shipping_pickup' => false,
                'shipping_price' => 3.00,
                'original_shipping_price' => 3.00,
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'phone' => '+38761621622',
                'street_address' => 'Cazinskih brigada bb',
                'postalcode' => '77220',
                'city' => 'Cazin',
                'country_id' => 1,
                'status' => 'created',
                'total_price' => 13.92,
                'total_price_with_shipping' => 16.92,
                'additional_info' => null,
                'payment_method' => 'cash_on_delivery',
            ]);

            $order = Order::first();

            $browser->assertPathIs('/kupovina/uspjesno/'.$order->order_number);

            $this->assertDatabaseHas('order_products', [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand->name,
                'brand_description' => $product->brand->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'real_price' => $product->discount_price,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'free_shipping' => $product->free_shipping,
                'quantity' => 2,
                'total_price' => 2 * $product->discount_price,
            ]);

            $this->assertEquals(0, Cart::count());
            $this->assertEquals(0, CartProduct::count());

            $browser
                ->assertPathIs('/kupovina/uspjesno/'.$order->order_number)
                ->assertSee('Vaša narudžba je zaprimljena')
                ->assertSee('U svakom trenutku možete pregledati sve narudžbe i njihov trenutni status, tako što otvorite "Narudžbe" u Vašem računu ili putem ovog linka.')
                ->assertSee('Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona: '.config('app.phone_short').' ili putem maila na adresu '.config('app.email'))
                ->assertSee('INFORMACIJE O NARUDŽBI')
                ->assertSee('Način preuzimanja')
                ->assertSee('Slanje na adresu')
                ->assertSee($order->shipping_name)
                ->assertSee('Podaci o kupcu')
                ->assertSee($order->first_name)
                ->assertSee($order->last_name)
                ->assertSee($order->street_address)
                ->assertSee($order->postalcode)
                ->assertSee($order->city)
                ->assertSee($order->country->name)
                ->assertSee('Kontakt')
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertDontSee('Napomene uz narudžbu')
                ->assertDontSee($order->additional_info)
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem (gotovina)');

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->name())
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Cijena')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));

            //Assert order successful email sent
            $this->assertDatabaseHas('email_log', [
                'to' => $order->email,
                'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            $this->assertDatabaseHas('email_log', [
                'to' => $adminUser->email,
                'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);
        });
    }

    public function testUserCheckoutWithPickup(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');
        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Product1',
            'on_sale' => false,
            'price' => 55.00,
            'discount_price' => 6.96,
        ]);
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $cart->products()->attach($product->id, ['quantity' => 2]);

        $this->browse(function (Browser $browser) use ($user, $adminUser, $product) {
            $browser
                ->loginAs($user)
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->assertSee('+ 0.00KM')
                ->assertSee(2 * $product->realPrice())
                ->assertSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.checkout-page', '86.08KM Vas dijeli do besplatne dostave!')
                ->click('#checkout-one-submit-button')
                ->assertPathIs('/kupovina/informacije')
                ->assertDontSee('Želim se registrovati')
                ->assertDontSee('Email')
                ->assertDontSee('Adresa ulice')
                ->assertDontSee('Poštanski broj')
                ->assertDontSee('Grad')
                ->assertDontSee('Država')
                ->assertInputValue('first_name', 'Tarik')
                ->assertInputValue('last_name', 'Coralic')
                ->assertInputValue('phone', '+38761621622')
                ->assertSeeIn('.checkout-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.checkout-page', '86.08KM Vas dijeli do besplatne dostave!')
                ->press('#checkout-two-submit-button')
                ->assertUrlIs(route('checkout.three'))
                ->click('#cash_on_delivery_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button');

            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'email' => $user->email,
                'shipping_method_id' => 1,
                'shipping_name' => 'Centralna Apoteka VIVA, Mala Lisa, Cazin',
                'shipping_key' => 'viva_centrala',
                'shipping_pickup' => true,
                'shipping_price' => 0.00,
                'original_shipping_price' => 0.00,
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'phone' => '+38761621622',
                'street_address' => null,
                'postalcode' => null,
                'city' => null,
                'country_id' => null,
                'status' => 'created',
                'total_price' => 110,
                'total_price_with_shipping' => 110,
                'additional_info' => null,
                'payment_method' => 'cash_on_delivery',
            ]);

            $order = Order::first();

            $browser->assertPathIs('/kupovina/uspjesno/'.$order->order_number);

            $this->assertDatabaseHas('order_products', [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand->name,
                'brand_description' => $product->brand->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'real_price' => $product->price,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'free_shipping' => $product->free_shipping,
                'quantity' => 2,
                'total_price' => 2 * $product->price,
            ]);

            $this->assertEquals(0, Cart::count());
            $this->assertEquals(0, CartProduct::count());

            $browser
                ->assertSeeIn('.alert-success', 'Vaša narudžba je zaprimljena')
                ->assertSeeIn('.alert-success', 'Narudžba će biti spremna u poslovnici '.$order->shipping_name.'. Rok za preuzimanje je '.config('app.pickup_days_limit').' dana od dana naručivanja tj. do '.$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.').' (do kraja radnog vremena poslovnice). Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći ovdje.')
                ->assertSeeIn('.alert-success', 'U svakom trenutku možete pregledati sve narudžbe i njihov trenutni status, tako što otvorite "Narudžbe" u Vašem računu ili putem ovog linka.')
                ->assertSeeIn('.alert-success', 'Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona: '.config('app.phone_short').' ili putem maila na adresu '.config('app.email'))
                ->assertSee('INFORMACIJE O NARUDŽBI')
                ->assertSee('Način preuzimanja')
                ->assertSee('Preuzimanje u '.$order->shipping_name)

                ->assertSee('Podaci o kupcu')
                ->assertSee($user->first_name.' '.$user->last_name)
                ->assertDontSeeIn('.dusk-order-info', $user->street_address)
                ->assertDontSeeIn('.dusk-order-info', $user->postalcode)
                ->assertDontSeeIn('.dusk-order-info', $user->city)
                ->assertDontSeeIn('.dusk-order-info', $user->country->name)
                ->assertSee('Kontakt')
                ->assertSee('Tel: '.$order->phone)
                ->assertSee($order->email)
                ->assertSee('Način preuzimanja')
                ->assertSee('Preuzimanje u '.$order->shipping_name)
                ->assertSee('Rok za preuzimanje')
                ->assertSee($order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.'))
                ->assertSee('Broj narudžbe')
                ->assertSee($order->order_number)
                ->assertDontSee('Napomene uz narudžbu')
                ->assertDontSee($order->additional_info)
                ->assertSee('Način plaćanja')
                ->assertSee('Pouzećem (gotovina, kartično)');

            foreach ($order->orderProducts as $orderProduct) {
                $browser->assertSee($orderProduct->name())
                    ->assertSee($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()))
                    ->assertSee(formatPrice($orderProduct->quantity * $orderProduct->realPrice()));
            }

            $browser->assertSee('Cijena')
                ->assertSee(formatPrice($order->total_price))
                ->assertSee('Dostava')
                ->assertSee(formatPrice($order->shipping_price))
                ->assertSee('Ukupno')
                ->assertSee(formatPrice($order->total_price_with_shipping));

            //Assert order successful email sent
            $this->assertDatabaseHas('email_log', [
                'to' => $order->email,
                'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);

            $this->assertDatabaseHas('email_log', [
                'to' => $adminUser->email,
                'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
                ['date', '>=', $this->testStartedAt],
            ]);
        });
    }

    public function testOnlinePaymentBrowserGoBackAsUser(): void
    {
        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 1]);

        $someOtherOrder = Order::factory()->create();
        OrderProduct::factory()->create(['order_id' => $someOtherOrder->id]);

        $this->assertEquals(1, Order::count());
        $this->assertEquals(1, OrderProduct::count());

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->assertDontSee('Potrebno je prihvatiti uvjete kupovine.')
                ->press('#checkout-three-submit-button')
                ->assertSee('Potrebno je prihvatiti uvjete kupovine.')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);

            $browser
                ->back()
                ->click('#online_payment_label')
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);
        });
    }

    public function testOnlinePaymentBrowserGoBackAsGuest(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();

        $someOtherOrder = Order::factory()->create();
        OrderProduct::factory()->create(['order_id' => $someOtherOrder->id]);

        $this->assertEquals(1, Order::count());
        $this->assertEquals(1, OrderProduct::count());

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->logout()
                ->visit(new ProductPage($product1))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                })
                ->visit(new ProductPage($product2))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                });

            $guestCart = GuestCart::first();

            $browser
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->type('first_name', 'Tarik')
                ->type('last_name', 'Coralic')
                ->type('#phone-8_phone_number', '061621622')
                ->type('email', 'tarik@gmail.com')
                ->type('additional_info', 'ovo je dodatni komentar')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);

            $browser
                ->back()
                ->click('#online_payment_label')
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);
        });
    }

    public function testOnlinePaymentBrowserGoBackAndSelectPaymentOnDeliveryAsUser(): void
    {
        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 1]);

        $someOtherOrder = Order::factory()->create();
        OrderProduct::factory()->create(['order_id' => $someOtherOrder->id]);

        $this->assertEquals(1, Order::count());
        $this->assertEquals(1, OrderProduct::count());

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);

            $browser
                ->back()
                ->click('#cash_on_delivery_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(0, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'status' => 'created',
                'payment_method' => 'cash_on_delivery',
                'card_payment_status' => null,
            ]);
        });
    }

    public function testOnlinePaymentBrowserGoBackAndSelectPaymentOnDeliveryAsGuest(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();

        $someOtherOrder = Order::factory()->create();
        OrderProduct::factory()->create(['order_id' => $someOtherOrder->id]);

        $this->assertEquals(1, Order::count());
        $this->assertEquals(1, OrderProduct::count());

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser->visit(new ProductPage($product1))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                })
                ->visit(new ProductPage($product2))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                });

            $guestCart = GuestCart::first();

            $browser
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->type('first_name', 'Tarik')
                ->type('last_name', 'Coralic')
                ->type('#phone-8_phone_number', '061621622')
                ->type('email', 'tarik@gmail.com')
                ->type('additional_info', 'ovo je dodatni komentar')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(1, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
                'card_payment_status' => null,
            ]);

            $browser
                ->back()
                ->click('#cash_on_delivery_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertEquals(2, Order::count());
            $this->assertEquals(0, Order::where('status', 'processing_payment')->count());
            $this->assertEquals(3, OrderProduct::count());
            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'status' => 'created',
                'payment_method' => 'cash_on_delivery',
                'card_payment_status' => null,
            ]);
        });
    }

    public function testOnlinePymentCancelRedirect(): void
    {
        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $product1 = Product::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $cart->products()->attach($product1->id, ['quantity' => 2]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->press('#checkout-two-submit-button')
                ->assertUrlIs(route('checkout.three'))
                ->visit(route('checkout.cancel_online_payment'))
                ->assertUrlIs(route('checkout.three'));
        });
    }

    public function testOnlinePaymentSuccessUrlAsUser(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');
        $user = User::factory()->create([
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'phone' => '+38761621622',
            'street_address' => 'Cazinskih brigada bb',
            'postalcode' => '77220',
            'city' => 'Cazin',
            'country_id' => 1,
        ]);
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 1]);

        $this->assertEquals(1, Cart::count());
        $this->assertEquals(0, Order::count());
        $this->assertEquals(0, OrderProduct::count());

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertDatabaseHas('orders', [
                'user_id' => $user->id,
                'guest_id' => null,
                'status' => 'processing_payment',
                'card_number_masked' => null,
                'card_payment_status' => null,
                'card_type' => null,
                'payment_method' => 'online_payment',
            ]);
            $this->assertEquals(1, Cart::count());
            $this->assertEquals(1, Order::count());
            $this->assertEquals(2, OrderProduct::count());

            $browser
                ->visit(route('checkout.online_payment_success'))
                ->assertPathIs('/')
                ->visit(route('checkout.online_payment_success', ['asd' => 'asd']))
                ->assertPathIs('/');

            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['asd' => 'asd']));
            $browser
                ->visit(route('checkout.online_payment_success', ['asd' => 'asd', 'digest' => $digest]))
                ->assertPathIs('/');

            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['order_number' => 'LA1234']));
            $browser
                ->visit(route('checkout.online_payment_success', ['order_number' => 'LA1234', 'digest' => $digest]))
                ->assertPathIs('/');

            $orderNumeber = Order::first()->order_number;
            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'masked_pan' => '1234****4321', 'cc_type' => 'visa']));
            $browser
                ->visit(route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'masked_pan' => '1234****4321', 'cc_type' => 'visa', 'digest' => $digest]))
                ->assertUrlIs(route('checkout.success', ['order_number' => $orderNumeber]))
                ->visit(route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'digest' => $digest]))
                ->assertPathIs('/');
        });

        $this->assertEquals(0, Cart::count());
        $this->assertEquals(1, Order::count());
        $this->assertEquals(2, OrderProduct::count());
        $order = Order::first();
        $this->assertDatabaseHas('orders', [
            'user_id' => $user->id,
            'guest_id' => null,
            'order_number' => $order->order_number,
            'card_number_masked' => '1234****4321',
            'card_type' => 'visa',
            'card_payment_status' => 'authorized',
            'status' => 'created',
            'payment_method' => 'online_payment',
        ]);

        // assert products have a hit
        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'buy_count' => 2,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'buy_count' => 1,
        ]);

        // Assert order successful email sent
        $this->assertDatabaseHas('email_log', [
            'to' => $order->email,
            'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);

        // Assert admin emails sent
        $this->assertDatabaseHas('email_log', [
            'to' => $adminUser->email,
            'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);
    }

    public function testOnlinePaymentSuccessUrlAsGuest(): void
    {
        $adminUser = User::factory()->admin()->create();
        $adminUser->assignRole('admin');
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->logout()
                ->visit(new ProductPage($product1))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                })
                ->visit(new ProductPage($product2))
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(200)
                ->whenAvailable('.header-cart', function ($modal) {
                    $modal->press('.close-icon');
                });

            $guestCart = GuestCart::first();
            $this->assertEquals(1, GuestCart::count());
            $this->assertEquals(0, Order::count());
            $this->assertEquals(0, OrderProduct::count());

            $browser->visit(route('checkout.one'))
                ->click('#viva_centrala_label')
                ->click('#checkout-one-submit-button')
                ->type('first_name', 'Tarik')
                ->type('last_name', 'Coralic')
                ->type('#phone-8_phone_number', '061621622')
                ->type('email', 'tarik@gmail.com')
                ->type('additional_info', 'ovo je dodatni komentar')
                ->press('#checkout-two-submit-button')
                ->click('#online_payment_label')
                ->click('#customRadioLabel1') // accept terms checkbox
                ->press('#checkout-three-submit-button')
                ->pause(500);

            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'card_number_masked' => null,
                'card_type' => null,
                'card_payment_status' => null,
                'status' => 'processing_payment',
                'payment_method' => 'online_payment',
            ]);
            $this->assertEquals(1, GuestCart::count());
            $this->assertEquals(1, Order::count());
            $this->assertEquals(2, OrderProduct::count());

            $browser
                ->visit(route('checkout.online_payment_success'))
                ->assertPathIs('/')
                ->visit(route('checkout.online_payment_success', ['asd' => 'asd']))
                ->assertPathIs('/');

            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['asd' => 'asd']));
            $browser
                ->visit(route('checkout.online_payment_success', ['asd' => 'asd', 'digest' => $digest]))
                ->assertPathIs('/');

            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['order_number' => 'LA1234']));
            $browser
                ->visit(route('checkout.online_payment_success', ['order_number' => 'LA1234', 'digest' => $digest]))
                ->assertPathIs('/');

            $orderNumeber = Order::first()->order_number;
            $digest = hash('sha512', config('app.monri_key').route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'masked_pan' => '1234****4321', 'cc_type' => 'visa']));
            $browser
                ->visit(route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'masked_pan' => '1234****4321', 'cc_type' => 'visa', 'digest' => $digest]))
                ->assertUrlIs(route('checkout.success', ['order_number' => $orderNumeber]))
                ->visit(route('checkout.online_payment_success', ['order_number' => $orderNumeber, 'digest' => $digest]))
                ->assertPathIs('/');

            $order = Order::first();
            $this->assertDatabaseHas('orders', [
                'user_id' => null,
                'guest_id' => $guestCart->guest_id,
                'order_number' => $order->order_number,
                'card_number_masked' => '1234****4321',
                'card_type' => 'visa',
                'card_payment_status' => 'authorized',
                'status' => 'created',
                'payment_method' => 'online_payment',
            ]);
        });

        $order = Order::first();
        $this->assertEquals(0, GuestCart::count());
        $this->assertEquals(1, Order::count());
        $this->assertEquals(2, OrderProduct::count());

        // assert products have a hit
        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'buy_count' => 2,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'buy_count' => 2,
        ]);

        // Assert order successful email sent
        $this->assertDatabaseHas('email_log', [
            'to' => $order->email,
            'subject' => 'Vaša Narudžba ('.$order->order_number.') | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);

        // Assert admin emails sent
        $this->assertDatabaseHas('email_log', [
            'to' => $adminUser->email,
            'subject' => 'Nova Narudžba ('.$order->order_number.') | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);

        // Assert newsletter contact created
        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => 'tarik@gmail.com',
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'user_id' => null,
        ]);
    }
}
