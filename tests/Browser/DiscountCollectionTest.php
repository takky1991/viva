<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Viva\DiscountCollection;
use Viva\Product;

class DiscountCollectionTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testDiscountsIndexPage(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true]);
        $product2 = Product::factory()->create(['on_sale' => true]);
        $product3 = Product::factory()->create(['on_sale' => false]);
        $discountCollection1 = DiscountCollection::factory()->create(['active' => false, 'product_ids' => json_encode([$product1->id])]);
        $discountCollection2 = DiscountCollection::factory()->create(['active' => false, 'product_ids' => json_encode([$product2->id])]);
        $discountCollection3 = DiscountCollection::factory()->create(['active' => false]);

        $this->browse(function (Browser $browser) use ($product1, $product2, $product3, $discountCollection1, $discountCollection2, $discountCollection3) {
            $browser
                ->visit('/popusti')
                ->assertSee('Popusti')
                ->assertSee($product1->title)
                ->assertSee($product2->title)
                ->assertDontSee($product3->title)
                ->assertDontSee($discountCollection1->name)
                ->assertDontSee($discountCollection2->name)
                ->assertDontSee($discountCollection3->name);

            $discountCollection1->update(['active' => true]);
            $discountCollection2->update(['active' => true]);

            $browser
                ->visit('/popusti')
                ->assertSee('Popusti')
                ->assertSee($product1->title)
                ->assertSee($product2->title)
                ->assertDontSee($product3->title)
                ->assertSee($discountCollection1->name)
                ->assertSee($discountCollection2->name)
                ->assertDontSee($discountCollection3->name);
        });
    }

    public function testDiscountShowPage(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true]);
        $product2 = Product::factory()->create(['on_sale' => true]);
        $product3 = Product::factory()->create(['on_sale' => true]);
        $discountCollection1 = DiscountCollection::factory()->create(['active' => true, 'product_ids' => json_encode([$product1->id, $product2->id])]);
        $discountCollection2 = DiscountCollection::factory()->create(['active' => true]);
        $discountCollection3 = DiscountCollection::factory()->create(['active' => false]);

        $this->browse(function (Browser $browser) use ($product1, $product2, $product3, $discountCollection1, $discountCollection2, $discountCollection3) {
            $browser
                ->visit(route('discount_collections.show', ['discountCollection' => $discountCollection1->getSlug()]))
                ->assertSee($product1->title)
                ->assertSee($product2->title)
                ->assertDontSee($product3->title)
                ->assertSee($discountCollection1->name)
                ->assertDontSee($discountCollection2->name)
                ->assertDontSee($discountCollection3->name);
        });
    }
}
