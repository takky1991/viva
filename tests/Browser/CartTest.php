<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\BrandPage;
use Tests\Browser\Pages\CartPage;
use Tests\Browser\Pages\ProductPage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\Cart;
use Viva\Product;
use Viva\User;

class CartTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testEmptyCartPage(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(new CartPage())
                ->assertSee('Vaša korpa je prazna');
        });
    }

    public function testCartPage(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $product1 = Product::factory()->create([
            'title' => 'Product1',
            'on_sale' => true,
            'price' => 10.66,
            'discount_price' => 6.96,
            'brand_id' => $brand,
        ]);
        $product2 = Product::factory()->create([
            'title' => 'Product2',
            'on_sale' => false,
            'price' => 60.99,
            'discount_price' => 16.77,
            'brand_id' => $brand,
        ]);
        Product::factory()->create([
            'title' => 'Product3',
            'on_sale' => true,
            'price' => 60.19,
            'discount_price' => 66.96,
            'brand_id' => $brand,
        ]);

        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 3, 'variants' => '34,35,36']);

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->loginAs($user)
                ->visit(new CartPage())
                ->assertDontSeeIn('.cart-page', 'Korpa je uspješno ažurirana.')
                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-0 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-0 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-0 .column-4', 2)
                ->assertSeeIn('.cart-page #product-0 .column-5', 2 * 6.96)
                ->assertSeeIn('.cart-page #product-1 .column-2', 'Product2')
                ->assertSeeIn('.cart-page #product-1 .column-3', 60.99)
                ->assertDontSeeIn('.cart-page #product-1 .column-3', 16.77)
                ->assertSeeIn('.cart-page #product-1 .column-4', 'Br. 34,35,36')
                ->assertSeeIn('.cart-page #product-1 .column-5', 3 * 60.99)
                ->assertMissing('.cart-page #product-2')
                ->assertSeeIn('.cart-page .total-price', 'Cijena')
                ->assertSeeIn('.cart-page .total-price', 2 * 6.96 + 3 * 60.99)

                // Change quantities
                ->press('#product-0 .wrap-num-product .btn-num-product-up')
                ->press('#product-0 .wrap-num-product .btn-num-product-up')
                ->assertMissing('#product-1 .wrap-num-product .btn-num-product-up')
                ->press('#update-cart-button')

                ->assertSeeIn('.cart-page', 'Korpa je uspješno ažurirana.')

                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-0 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-0 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-0 .column-4', 4)
                ->assertSeeIn('.cart-page #product-0 .column-5', 4 * 6.96)
                ->assertSeeIn('.cart-page #product-1 .column-2', 'Product2')
                ->assertSeeIn('.cart-page #product-1 .column-3', 60.99)
                ->assertDontSeeIn('.cart-page #product-1 .column-3', 16.77)
                ->assertSeeIn('.cart-page #product-1 .column-4', 'Br. 34,35,36')
                ->assertSeeIn('.cart-page #product-1 .column-5', 3 * 60.99)
                ->assertMissing('.cart-page #product-2')
                ->assertSeeIn('.cart-page .total-price', 'Cijena')
                ->assertSeeIn('.cart-page .total-price', 4 * 6.96 + 3 * 60.99)

                // Remove products
                ->press('#delete-product-1-link')
                ->assertSeeIn('.cart-page', 'Proizvod je uspješno uklonjen iz korpe.')
                ->assertSeeIn('.cart-page #product-0 .column-2', 'Product1')
                ->assertSeeIn('.cart-page #product-0 .column-3', 10.66)
                ->assertSeeIn('.cart-page #product-0 .column-3', 6.96)
                ->assertSeeIn('.cart-page #product-0 .column-4', 4)
                ->assertSeeIn('.cart-page #product-0 .column-5', 4 * 6.96)
                ->assertMissing('.cart-page #product-1')
                ->assertMissing('.cart-page #product-2')

                ->press('#delete-product-0-link')
                ->assertDontSeeIn('.cart-page', 'Proizvod je uspješno uklonjen iz korpe.')
                ->assertSeeIn('.cart-page', 'Vaša korpa je prazna')
                ->assertMissing('.cart-page #product-0')
                ->assertMissing('.cart-page #product-1')
                ->assertMissing('.cart-page #product-2');
        });

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'user_id' => $user->id,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'product_id' => $product2->id,
        ]);
    }

    public function testCartProductPriceChangedAlert(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $product1 = Product::factory()->create(['brand_id' => $brand]);
        $product2 = Product::factory()->create(['brand_id' => $brand]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);

        $this->browse(function (Browser $browser) use ($user, $cart) {
            $browser
                ->loginAs($user)
                ->visit(route('cart.show'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je promjene cijene jednog od proizvoda u Vašoj korpi.');

            $cart->product_price_changed = true;
            $cart->save();

            $browser
                ->visit(route('home'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.')
                ->visit(route('cart.show'))
                ->assertSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.')
                ->visit(route('checkout.one'))
                ->assertDontSee('Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.');

            $this->assertDatabaseHas('carts', [
                'id' => $cart->id,
                'product_price_changed' => 0,
            ]);
        });
    }

    public function testCartProductRemovedAlert(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $cart = Cart::factory()->create(['user_id' => $user->id]);
        $product1 = Product::factory()->create(['brand_id' => $brand]);
        $product2 = Product::factory()->create(['brand_id' => $brand]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);

        $this->browse(function (Browser $browser) use ($user, $cart) {
            $browser
                ->loginAs($user)
                ->visit(route('cart.show'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.');

            $cart->product_removed = true;
            $cart->save();

            $browser
                ->visit(route('home'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.')
                ->visit(route('cart.show'))
                ->assertSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.')
                ->visit(route('checkout.one'))
                ->assertDontSee('Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.');

            $this->assertDatabaseHas('carts', [
                'id' => $cart->id,
                'product_removed' => 0,
            ]);
        });
    }

    public function testFreeShippingTooltipOnCart(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'brand_id' => $brand->id,
            'price' => 50,
            'on_sale' => false,
        ]);

        $this->browse(function (Browser $browser) use ($user, $brand, $product) {
            $browser
                ->loginAs($user)
                ->visit(new BrandPage($brand))
                //->assertSeeIn('.shipping-tooltip', 'DOSTAVA ' . config('app.lowest_shipping_cost') . 'KM!')

                // open quick cart
                ->click('#header-cart-icon')
                ->waitFor('.header-cart-content')
                ->pause(500)
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.header-cart-content', 'Vas dijeli do besplatne dostave!')
                ->click('.header-cart .close-icon')
                ->pause(500)

                // add item
                ->visit(new ProductPage($product))
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) use ($product) {
                    $cart->assertSee($product->title)
                        ->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })
                ->assertSeeIn('.shipping-tooltip', formatPrice($product->price))

                // open quick cart
                ->click('#header-cart-icon')
                ->waitFor('.header-cart-content')
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertSeeIn('.header-cart-content', '20.00KM Vas dijeli do besplatne dostave!')
                ->pause(400)
                ->click('.header-cart .close-icon')
                ->pause(500)

                // go to cart page
                ->visit(route('cart.show'))
                ->assertDontSeeIn('.cart-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertSeeIn('.cart-page', '20.00KM Vas dijeli do besplatne dostave!')

                // add item
                ->visit(new ProductPage($product))
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) use ($product) {
                    $cart->assertSee($product->title)
                        ->assertSee('Proizvod je dodan u korpu')
                        ->press('.close-icon');
                })
                ->pause(3000)
                ->assertDontSeeIn('.shipping-tooltip', 'DOSTAVA '.config('app.lowest_shipping_cost').'KM!')

                // go to cart page
                ->visit(route('cart.show'))
                ->assertSeeIn('.cart-page', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.cart-page', 'Vas dijeli do besplatne dostave!')
                ->assertDontSeeIn('.shipping-tooltip', 'DOSTAVA '.config('app.lowest_shipping_cost').'KM!')

                // open quick cart
                ->click('#header-cart-icon')
                ->pause(400)
                ->waitFor('.header-cart-content')
                ->assertSeeIn('.header-cart-total', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSee('.header-cart-total', 'Vas dijeli do besplatne dostave!')

                // remove product from cart
                ->click('.header-cart-content #remove-product-1')
                ->assertSee('Vaša korpa je prazna')
                ->assertDontSeeIn('.header-cart-content', 'Ostvarili ste besplatnu dostavu!')
                ->assertDontSeeIn('.header-cart-content', 'Vas dijeli do besplatne dostave!');
            //->click('.header-cart .close-icon')
            //->pause(500)
            //->assertSeeIn('.shipping-tooltip', 'DOSTAVA ' . config('app.lowest_shipping_cost') . 'KM!');
        });
    }
}
