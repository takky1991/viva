<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Laravel\Dusk\Browser;
use Spatie\Permission\Models\Role;
use Tests\Browser\Pages\ForgotPasswordPage;
use Tests\Browser\Pages\LoginPage;
use Tests\Browser\Pages\ProductPage;
use Tests\Browser\Pages\RegisterPage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\GuestCart;
use Viva\Product;
use Viva\User;

class AuthTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testLogin(): void
    {
        $user = User::factory()->create();
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'last_login' => null,
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new LoginPage)
                ->loginWithForm($user);
        });

        $this->assertDatabaseMissing('users', [
            'last_login' => null,
        ]);
    }

    public function testRegister(): void
    {
        $customerRole = Role::where('name', 'customer')->first();

        $this->assertDatabaseMissing('users', [
            'email' => 'takky1991@gmail.com',
        ]);

        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => 'takky1991@gmail.com',
        ]);

        $userData = [
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'email' => 'takky1991@gmail.com',
            'password' => 'secret123',
            'password_confirmation' => 'secret123',
        ];

        $this->browse(function (Browser $browser) use ($userData) {
            $browser->visit(new RegisterPage)
                ->registerWithForm($userData);
        });

        $this->assertDatabaseHas('users', [
            'first_name' => 'Tarik',
            'last_name' => 'Coralic',
            'email' => 'takky1991@gmail.com',
            'email_verified_at' => null,
            'terms_accepted' => true,
        ]);

        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => $customerRole->id,
            'model_type' => \Viva\User::class,
            'model_id' => 1,
        ]);

        $user = User::where('email', 'takky1991@gmail.com')->first();

        // Assert customer role assigned
        $this->assertTrue($user->hasRole('customer'));

        // Assert last_login is set
        $this->assertDatabaseMissing('users', [
            'id' => 1,
            'last_login' => null,
        ]);

        // Assert newsletter contact created
        $this->assertDatabaseHas('newsletter_contacts', [
            'email' => 'takky1991@gmail.com',
            'first_name' => null,
            'last_name' => null,
            'user_id' => $user->id,
        ]);

        //Assert email sent
        $this->assertDatabaseHas('email_log', [
            'to' => $user->email,
            'subject' => 'Potvrdite Vašu email adresu | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);
        $this->assertEquals(1, DB::table('email_log')->count());

        // Test resend confirmation email
        $this->browse(function (Browser $browser) {
            $browser
                ->visit(route('verification.notice'))
                ->click('#resend-confirmation-email-button')
                ->assertSee('Poslali smo novi email na Vašu adresu.');
        });

        //Assert another email sent
        $this->assertEquals(2, DB::table('email_log')->count());

        $confirmEmailLink = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(config()->get('auth.verification.expire', 60)),
            [
                'id' => $user->getKey(),
                'hash' => sha1($user->getEmailForVerification()),
            ]
        );

        $this->browse(function (Browser $browser) use ($confirmEmailLink) {
            $now = now();
            $browser
                ->visit($confirmEmailLink)
                ->assertSee('Moj račun');

            $this->assertDatabaseHas('users', [
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'email' => 'takky1991@gmail.com',
                'email_verified_at' => $now,
            ]);
        });
    }

    public function testEmailConfirmationLinkIsProtected(): void
    {
        $user = User::factory()->create(['email' => 'takky1991@gmail']);

        $confirmEmailLink = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(config()->get('auth.verification.expire', 60)),
            [
                'id' => $user->getKey(),
                'hash' => sha1($user->getEmailForVerification()),
            ]
        );

        $this->browse(function (Browser $browser) use ($user, $confirmEmailLink) {
            $browser
                ->visit($confirmEmailLink)
                ->assertPathIs('/prijava')
                ->assertSee('Prijava');

            $this->assertDatabaseHas('users', [
                'email' => 'takky1991@gmail',
                'email_verified_at' => null,
            ]);

            $browser
                ->type('#email', $user->email)
                ->type('#password', 'secret')
                ->press('#login-button')
                ->assertPathIs('/')
                ->assertSee('Moj račun');

            $this->assertDatabaseMissing('users', [
                'email' => 'takky1991@gmail',
                'email_verified_at' => null,
            ]);
        });
    }

    public function testLogout(): void
    {
        $user = User::factory()->create();
        $user->assignRole('customer');

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new LoginPage)
                ->loginWithForm($user)
                ->press('#logout-desktop')
                ->assertPathIs('/')
                ->assertDontSee('Moj račun')
                ->assertSee('Registracija');
        });
    }

    public function testForgotPassword(): void
    {
        $user = User::factory()->create(['first_name' => 'Edo']);
        $user->assignRole('customer');
        $oldPassword = $user->password;

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new ForgotPasswordPage)
                ->requestPasswordChange($user->email);
        });

        // Assert email sent
        $this->assertDatabaseHas('email_log', [
            'to' => $user->email,
            'subject' => 'Promjena šifre | ApotekaViva24',
            ['date', '>=', $this->testStartedAt],
        ]);

        // Assert change password link
        $token = app('auth.password.broker')->createToken($user);
        $resetLink = url(config('app.url').route('password.reset', ['token' => $token], false));
        $this->browse(function (Browser $browser) use ($user, $resetLink) {
            $browser->visit($resetLink)
                ->assertSee('Promjena šifre')
                ->type('#email', $user->email)
                ->type('#password', 'newPassword')
                ->type('#password-confirm', 'newPassword')
                ->press('#change-password-button')
                ->assertPathIs('/')
                ->assertSee('Moj račun');
        });

        $user->refresh();
        $newPassword = $user->password;
        $this->assertNotEquals($oldPassword, $newPassword);
    }

    public function testSyncGuestCartToNewAuthCartOnLogin(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['title' => 'Product1', 'brand_id' => $brand->id]);
        $product2 = Product::factory()->create(['title' => 'Product2', 'brand_id' => $brand->id]);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $this->browse(function (Browser $browser) use ($user, $product1, $product2) {
            $browser
                ->visit(new ProductPage($product1))
                ->assertSee('Product1')
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Product1')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->click('.close-icon');
                })

                ->visit(new ProductPage($product2))
                ->assertSee('Product2')
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Product2')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->click('.close-icon');
                });

            $guestCart = GuestCart::first();
            $this->assertDatabaseHas('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product1->id,
                'quantity' => 2,
            ]);
            $this->assertDatabaseHas('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product2->id,
                'quantity' => 1,
            ]);

            $browser->visit(new LoginPage)
                ->loginWithForm($user);

            $this->assertDatabaseMissing('guest_carts', [
                'guest_id' => $guestCart->id,
            ]);

            $this->assertDatabaseMissing('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product1->id,
            ]);
            $this->assertDatabaseMissing('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product2->id,
            ]);

            $this->assertDatabaseHas('carts', [
                'user_id' => $user->id,
            ]);
            $this->assertDatabaseHas('cart_product', [
                'cart_id' => $user->cart->id,
                'product_id' => $product1->id,
                'quantity' => 2,
            ]);
            $this->assertDatabaseHas('cart_product', [
                'cart_id' => $user->cart->id,
                'product_id' => $product2->id,
                'quantity' => 1,
            ]);
        });
    }

    public function testSyncGuestCartToNewAuthCartOnRegister(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['title' => 'Product1', 'brand_id' => $brand->id]);
        $product2 = Product::factory()->create(['title' => 'Product2', 'brand_id' => $brand->id]);

        $this->browse(function (Browser $browser) use ($product1, $product2) {
            $browser
                ->visit(new ProductPage($product1))
                ->assertSee('Product1')
                ->press('.btn-num-product-up')
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Product1')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->click('.close-icon');
                })

                ->visit(new ProductPage($product2))
                ->assertSee('Product2')
                ->press('DODAJ U KORPU')
                ->pause(100)
                ->whenAvailable('.header-cart', function ($cart) {
                    $cart->assertSee('Product2')
                        ->assertSee('Proizvod je dodan u korpu')
                        ->click('.close-icon');
                });

            $guestCart = GuestCart::first();
            $this->assertDatabaseHas('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product1->id,
                'quantity' => 2,
            ]);
            $this->assertDatabaseHas('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product2->id,
                'quantity' => 1,
            ]);

            $user = [
                'first_name' => 'Tarik',
                'last_name' => 'Coralic',
                'email' => 'takky1991@gmail',
                'password' => 'secret123',
                'password_confirmation' => 'secret123',
            ];

            $browser->visit(new RegisterPage)
                ->registerWithForm($user);

            $user = User::first();

            $this->assertDatabaseMissing('guest_carts', [
                'guest_id' => $guestCart->id,
            ]);

            $this->assertDatabaseMissing('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product1->id,
            ]);
            $this->assertDatabaseMissing('guest_cart_product', [
                'guest_cart_id' => $guestCart->id,
                'product_id' => $product2->id,
            ]);

            $this->assertDatabaseHas('carts', [
                'user_id' => $user->id,
            ]);
            $this->assertDatabaseHas('cart_product', [
                'cart_id' => $user->cart->id,
                'product_id' => $product1->id,
                'quantity' => 2,
            ]);
            $this->assertDatabaseHas('cart_product', [
                'cart_id' => $user->cart->id,
                'product_id' => $product2->id,
                'quantity' => 1,
            ]);
        });
    }
}
