<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PagesTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testLocationsPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/lokacije')
                ->assertSee('Naše apoteke');
        });
    }

    public function testPayingMethodsPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/nacini-placanja')
                ->assertSee('Način plaćanja');
        });
    }

    public function testDeliveryPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/dostava')
                ->assertSee('Dostava');
        });
    }

    public function testReturnsPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/povrat-robe')
                ->assertSee('Povrat robe');
        });
    }

    public function testCookiePolicyPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/politika-kolacica')
                ->assertSee('Politika kolačića');
        });
    }

    public function testPrivacyPolicyPage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/pravila-zastite-privatnosti')
                ->assertSee('Pravila zaštite privatnosti');
        });
    }

    public function testTermsOfPurchasePage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/uvjeti-kupovine')
                ->assertSee('Uvjeti kupovine');
        });
    }

    public function testLegalNoticePage(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/pravne-napomene')
                ->assertSee('Pravne napomene');
        });
    }
}
