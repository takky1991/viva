<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\BrandPage;
use Tests\Browser\Pages\CategoryPage;
use Tests\Browser\Pages\ProductPage;
use Tests\DuskTestCase;
use Viva\Brand;
use Viva\Category;
use Viva\Order;
use Viva\OrderProduct;
use Viva\Product;
use Viva\Review;
use Viva\User;

class ProductTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testIncrementOpenCountOnProductPageOpen(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'open_count' => 0,
        ]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser
                ->visit($product->url())
                ->assertSee($product->fullName());
        });

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'open_count' => 1,
        ]);
    }

    public function testCantAccessProductIfNotPublished(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser
                ->visit($product->url())
                ->assertSee($product->fullName());

            $product->published = false;
            $product->save();

            $browser
                ->visit($product->url())
                ->assertDontSee($product->fullName());
        });
    }

    public function testSeeProductNotAvailableBadge(): void
    {
        $brand = Brand::factory()->create();
        $productInStock = Product::factory()->create(['brand_id' => $brand->id]);
        $productOutOfStock = Product::factory()->create(['brand_id' => $brand->id, 'in_stock' => false]);

        $this->browse(function (Browser $browser) use ($brand, $productInStock, $productOutOfStock) {
            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($productInStock->name())
                ->assertDontSeeIn('#product-'.$productInStock->id, 'Nije dostupno')
                ->assertSee($productOutOfStock->name())
                ->assertSeeIn('#product-'.$productOutOfStock->id, 'Nije dostupno')
                ->visit(new ProductPage($productInStock))
                ->assertDontSee('Nije dostupno')
                ->visit(new ProductPage($productOutOfStock))
                ->assertSee('Nije dostupno');

            $productOutOfStock->in_stock = true;
            $productOutOfStock->save();

            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($productInStock->name())
                ->assertDontSeeIn('#product-'.$productInStock->id, 'Nije dostupno')
                ->assertSee($productOutOfStock->name())
                ->assertDontSeeIn('#product-'.$productOutOfStock->id, 'Nije dostupno');
        });
    }

    public function testSeeFreeShippingBadgeIfFreeShippingFlagIsOn(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);
        $productFreeShipping = Product::factory()->create(['brand_id' => $brand->id, 'free_shipping' => true]);

        $this->browse(function (Browser $browser) use ($brand, $product, $productFreeShipping) {
            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($product->name())
                ->assertDontSeeIn('#product-'.$product->id, 'Besplatna dostava')
                ->assertSee($productFreeShipping->name())
                ->assertSeeIn('#product-'.$productFreeShipping->id, 'Besplatna dostava')
                ->visit(new ProductPage($product))
                ->assertDontSeeIn('.image-wrap', 'Besplatna dostava')
                ->visit(new ProductPage($productFreeShipping))
                ->assertSeeIn('.image-wrap', 'Besplatna dostava');

            $productFreeShipping->free_shipping = false;
            $productFreeShipping->save();

            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($product->name())
                ->assertDontSeeIn('#product-'.$product->id, 'Besplatna dostava')
                ->assertSee($productFreeShipping->name())
                ->assertDontSeeIn('#product-'.$productFreeShipping->id, 'Besplatna dostava');
        });
    }

    public function testSeeFreeShippingBadgeIfPriceOverTreshold(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id, 'price' => 69, 'on_sale' => false]);
        $productFreeShipping = Product::factory()->create(['brand_id' => $brand->id, 'price' => 71, 'on_sale' => false]);

        $this->browse(function (Browser $browser) use ($brand, $product, $productFreeShipping) {
            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($product->name())
                ->assertDontSeeIn('#product-'.$product->id, 'Besplatna dostava')
                ->assertSee($productFreeShipping->name())
                ->assertSeeIn('#product-'.$productFreeShipping->id, 'Besplatna dostava')
                ->visit(new ProductPage($product))
                ->assertDontSeeIn('.image-wrap', 'Besplatna dostava')
                ->visit(new ProductPage($productFreeShipping))
                ->assertSeeIn('.image-wrap', 'Besplatna dostava');

            $productFreeShipping->price = 66;
            $productFreeShipping->save();

            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($product->name())
                ->assertDontSeeIn('#product-'.$product->id, 'Besplatna dostava')
                ->assertSee($productFreeShipping->name())
                ->assertDontSeeIn('#product-'.$productFreeShipping->id, 'Besplatna dostava');
        });
    }

    public function testSeeRightProductInfo(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Tylol Hot',
            'package_description' => '10ml',
            'description' => 'Ovo je opis',
            'price' => 80,
            'on_sale' => false,
            'discount_price' => 66,
        ]);
        $product2 = Product::factory()->create([
            'brand_id' => $brand->id,
            'title' => 'Krema',
            'package_description' => '20g',
            'description' => 'Ovo drugi opis',
            'price' => 50,
            'on_sale' => true,
            'discount_price' => 30,
        ]);

        $this->browse(function (Browser $browser) use ($brand) {
            $browser
                ->visit(new BrandPage($brand))
                ->assertSee($brand->name)
                ->assertSee('Tylol Hot')
                ->assertSee('Krema')
                ->assertSee('10ml')
                ->assertSee('20g')
                ->assertSee(80)
                ->assertSee(50)
                ->assertSee(30)
                ->assertDontSee(66);
        });
    }

    public function testBreadcrumbs(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['title' => 'Proizvod', 'brand_id' => $brand->id]);

        $categoryMenu1 = Category::factory()->create(['parent_id' => null, 'title' => 'Prva menu kategorija']);
        $categoryMenu2 = Category::factory()->create(['parent_id' => $categoryMenu1->id, 'title' => 'Druga menu kategorija']);

        $category1 = Category::factory()->create(['parent_id' => null, 'title' => 'Prva kategorija']);
        $category2 = Category::factory()->create(['parent_id' => $category1->id, 'title' => 'Druga kategorija']);

        $product->categories()->attach([$category1->id, $category2->id, $categoryMenu1->id, $categoryMenu2->id]);

        $this->browse(function (Browser $browser) use ($product, $categoryMenu1, $categoryMenu2, $category1, $category2) {
            $browser
                ->visit(new ProductPage($product))
                ->assertSee($product->fullName())
                ->assertSeeIn('.bread-crumb', $categoryMenu1->title)
                ->assertSeeIn('.bread-crumb', $categoryMenu2->title)
                ->assertSeeIn('.bread-crumb', $product->fullName())
                ->assertDontSeeIn('.bread-crumb', $category1->title)
                ->assertDontSeeIn('.bread-crumb', $category2->title);
        });
    }

    public function testRelatedProductsOnShowPage(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['title' => 'Proizvod1', 'brand_id' => $brand->id]);
        $product2 = Product::factory()->create(['title' => 'Proizvod2', 'brand_id' => $brand->id]);
        $product3 = Product::factory()->create(['title' => 'Proizvod3', 'brand_id' => $brand->id]);

        $product4 = Product::factory()->create(['title' => 'Proizvod4']);
        $product5 = Product::factory()->create(['title' => 'Proizvod5']);

        $this->browse(function (Browser $browser) use ($product1, $product2, $product3, $product4, $product5, $brand) {
            $browser
                ->visit(new ProductPage($product1))
                ->assertSeeIn('.product-title', $product1->title)
                ->assertSeeIn('.related-by-brand', 'A šta misliš o ovim '.$brand->name.' proizvodima?')
                //->assertSeeLink(route('brands.show', ['brand' => $brand->getSlug()]))
                ->assertDontSeeIn('.related-by-brand', $product1->title)
                ->assertSeeIn('.related-by-brand', $product2->title)
                ->assertSeeIn('.related-by-brand', $product3->title)
                ->assertDontSee('Probaj i druge proizvode iz kategorije');

            $categoryMenu1 = Category::factory()->create(['parent_id' => null, 'title' => 'Prva menu kategorija']);
            $categoryMenu2 = Category::factory()->create(['parent_id' => $categoryMenu1->id, 'title' => 'Druga menu kategorija']);

            $product1->categories()->attach([$categoryMenu1->id, $categoryMenu2->id]);
            $product4->categories()->attach([$categoryMenu1->id, $categoryMenu2->id]);
            $product5->categories()->attach([$categoryMenu1->id, $categoryMenu2->id]);

            $browser
                ->visit(new ProductPage($product1))
                ->assertSeeIn('.product-title', $product1->title)
                ->assertSeeIn('.related-by-brand', 'A šta misliš o ovim '.$brand->name.' proizvodima?')
                ->assertAttribute('.related-by-brand .brand', 'href', route('brands.show', ['brand' => $brand->getSlug()]))
                ->assertDontSeeIn('.related-by-brand', $product1->title)
                ->assertSeeIn('.related-by-brand', $product2->title)
                ->assertSeeIn('.related-by-brand', $product3->title)
                ->assertSeeIn('.related-by-category', 'Probaj i druge proizvode iz kategorije '.$categoryMenu2->title)
                ->assertAttribute('.related-by-category .brand', 'href', route('categories.show', ['category' => $categoryMenu2->getSlug()]))
                ->assertDontSeeIn('.related-by-category', $product1->title)
                ->assertSeeIn('.related-by-category', $product4->title)
                ->assertSeeIn('.related-by-category', $product5->title);
        });
    }

    public function testSeeReviewsOnProductShowPage(): void
    {
        $product = Product::factory()->create();
        $publishedReviews = Review::factory()->count(2)->create([
            'product_id' => $product->id,
            'published' => true,
        ]);
        $unpublishedReviews = Review::factory()->count(2)->create([
            'product_id' => $product->id,
            'published' => false,
        ]);

        $this->browse(function (Browser $browser) use ($product, $publishedReviews, $unpublishedReviews) {
            $browser
                ->visit(new ProductPage($product))
                // FIrst review
                ->assertSeeIn('.review-title', $publishedReviews->get(0)->name)
                ->assertSeeIn('.review', $publishedReviews->get(0)->content)
                ->assertSeeIn('.review-date', $publishedReviews->get(0)->created_at->format('d.m.Y'))

                // second review
                ->assertSeeIn('.review-title', $publishedReviews->get(0)->name)
                ->assertSeeIn('.review', $publishedReviews->get(0)->content)
                ->assertSeeIn('.review-date', $publishedReviews->get(0)->created_at->format('d.m.Y'))

                // FIrst review
                ->assertDontSeeIn('.review-title', $unpublishedReviews->get(0)->name)
                ->assertDontSeeIn('.review', $unpublishedReviews->get(0)->content)

                // second review
                ->assertDontSeeIn('.review-title', $unpublishedReviews->get(0)->name)
                ->assertDontSeeIn('.review', $unpublishedReviews->get(0)->content);
        });
    }

    public function testCreateReview(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();

        $this->assertDatabaseMissing('reviews', [
            'name' => 'Tarik',
        ]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser
                ->visit(new ProductPage($product))
                ->click('#_dusk_write_review_button')
                ->pause(1000)
                ->press('#_dusk_review_rating')
                ->type('name', 'Tarik')
                ->type('content', 'Dusk provides a variety of methods for interacting with forms and input elements.')
                ->click('#_dusk_review_submit')
                ->waitForText('Vaša recenzija je uspješno poslana i biti će vidljiva nakon provjere.');
        });

        $this->assertDatabaseHas('reviews', [
            'order_id' => null,
            'product_id' => $product->id,
            'user_id' => null,
            'name' => 'Tarik',
            'content' => 'Dusk provides a variety of methods for interacting with forms and input elements.',
            'published' => false,
            'verified_buyer' => false,
        ]);

        $this->browse(function (Browser $browser) use ($user, $product) {
            $browser
                ->loginAs($user)
                ->visit(new ProductPage($product))
                ->click('#_dusk_write_review_button')
                ->pause(1000)
                ->press('#_dusk_review_rating')
                ->type('name', 'Tarik2')
                ->type('content', 'Second review')
                ->click('#_dusk_review_submit')
                ->waitForText('Vaša recenzija je uspješno poslana i biti će vidljiva nakon provjere.');
        });

        $this->assertDatabaseHas('reviews', [
            'order_id' => null,
            'product_id' => $product->id,
            'user_id' => $user->id,
            'name' => 'Tarik2',
            'content' => 'Second review',
            'published' => false,
            'verified_buyer' => false,
        ]);
    }

    public function testCreateReviewWithLink(): void
    {
        $product = Product::factory()->create();
        $order = Order::factory()->create();

        $this->assertDatabaseMissing('reviews', [
            'name' => 'Tarik',
        ]);

        // Product not in order
        $this->browse(function (Browser $browser) use ($product, $order) {
            $browser
                ->visit(route('products.review', ['product' => $product->getSlug(), 'order_number' => $order->order_number]))
                ->press('#_dusk_review_rating')
                ->type('name', 'Tarik')
                ->type('content', 'Dusk provides a variety of methods for interacting with forms and input elements.')
                ->click('#_dusk_review_submit')
                ->assertUrlIs(route('products.show', ['product' => $product->getSlug()]))
                ->assertSee('Vaša recenzija je uspješno poslana i biti će vidljiva nakon provjere.');
        });

        $this->assertDatabaseHas('reviews', [
            'order_id' => null,
            'user_id' => null,
            'product_id' => $product->id,
            'name' => 'Tarik',
            'content' => 'Dusk provides a variety of methods for interacting with forms and input elements.',
            'published' => false,
            'verified_buyer' => false,
        ]);

        // Product in order
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'product_id' => $product->id,
        ]);

        $this->browse(function (Browser $browser) use ($product, $order) {
            $browser
                ->visit(route('products.review', ['product' => $product->getSlug(), 'order_number' => $order->order_number]))
                ->press('#_dusk_review_rating')
                ->type('name', 'Tarik')
                ->type('content', 'Review content 2')
                ->click('#_dusk_review_submit')
                ->assertUrlIs(route('products.show', ['product' => $product->getSlug()]))
                ->assertSee('Vaša recenzija je uspješno poslana i biti će vidljiva nakon provjere.');
        });

        $this->assertDatabaseHas('reviews', [
            'order_id' => $order->id,
            'user_id' => null,
            'product_id' => $product->id,
            'name' => 'Tarik',
            'content' => 'Review content 2',
            'published' => false,
            'verified_buyer' => true,
        ]);
    }

    public function testSeePopularBadge(): void
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();

        $product = Product::factory()->create(['brand_id' => $brand->id, 'best_seller_in_brand' => false, 'best_seller_in_category' => false]);
        $popularProductInBrand = Product::factory()->create(['brand_id' => $brand->id, 'best_seller_in_brand' => true, 'best_seller_in_category' => false]);
        $popularProductInCategory = Product::factory()->create(['brand_id' => $brand->id, 'best_seller_in_brand' => false, 'best_seller_in_category' => true]);

        $product->categories()->attach($category->id, ['best_seller_in_category' => false]);
        $popularProductInBrand->categories()->attach($category->id, ['best_seller_in_category' => false]);
        $popularProductInCategory->categories()->attach($category->id, ['best_seller_in_category' => true]);

        $this->browse(function (Browser $browser) use ($brand, $product, $popularProductInBrand, $popularProductInCategory, $category) {
            $browser
                // Product show page
                ->visit(new ProductPage($product))
                ->assertDontSeeIn('.image-wrap', 'Popularno')
                ->visit(new ProductPage($popularProductInCategory))
                ->assertSeeIn('.image-wrap', 'Popularno')
                ->visit(new ProductPage($popularProductInBrand))
                ->assertSeeIn('.image-wrap', 'Popularno')

                // Brand show page
                ->visit(new BrandPage($brand))
                ->assertDontSeeIn('#product-'.$product->id, 'Popularno')
                ->assertDontSeeIn('#product-'.$popularProductInCategory->id, 'Popularno')
                ->assertSeeIn('#product-'.$popularProductInBrand->id, 'Popularno')

                // Category show page
                ->visit(new CategoryPage($category))
                ->assertDontSeeIn('#product-'.$product->id, 'Popularno')
                ->assertDontSeeIn('#product-'.$popularProductInBrand->id, 'Popularno')
                ->assertSeeIn('#product-'.$popularProductInCategory->id, 'Popularno');
        });
    }

    public function testBackToStockAlert(): void
    {
        $product = Product::factory()->create(['in_stock' => true]);

        $this->assertDatabaseMissing('back_to_stock_alerts', [
            'product_id' => $product->id,
        ]);

        $this->browse(function (Browser $browser) use ($product) {
            $browser
                // Product show page
                ->visit(new ProductPage($product))
                ->assertMissing('.back-to-stock-alert-form');

            $product->update(['in_stock' => false]);

            $browser
                // Product show page
                ->visit(new ProductPage($product))
                ->assertVisible('.back-to-stock-alert-form')
                ->press('Javi mi')
                ->assertSeeIn('.back-to-stock-alert-form', 'Unesite validan email')
                ->type('email', 'asd')
                ->press('Javi mi')
                ->pause(1000)
                ->assertSeeIn('.back-to-stock-alert-form', 'Unesite validan email')
                ->type('email', 'asd@gmail.com')
                ->press('Javi mi')
                ->pause(1000)
                ->assertSeeIn('.back-to-stock-alert-form', 'Biti ćete obaviješteni kad proizvod ponovno bude dostupan.');
        });

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);
    }
}
