<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CategoryPage;
use Tests\DuskTestCase;
use Viva\Category;

class CategoryTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testBreadcrumbs(): void
    {
        $category1 = Category::factory()->create(['parent_id' => null, 'title' => 'Prva menu kategorija']);
        $category2 = Category::factory()->create(['parent_id' => $category1->id, 'title' => 'Druga menu kategorija']);

        $this->browse(function (Browser $browser) use ($category1, $category2) {
            $browser
                ->visit(new CategoryPage($category1))
                ->assertSeeIn('.bread-crumb', $category1->title)
                ->assertDontSeeIn('.bread-crumb', $category2->title)
                ->visit(new CategoryPage($category2))
                ->assertSeeIn('.bread-crumb', $category1->title)
                ->assertSeeIn('.bread-crumb', $category2->title);
        });
    }
}
