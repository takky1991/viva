<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Viva\Article;

class ArticleTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testCantAccessArticleIfNotPublished(): void
    {
        $article = Article::factory()->create();

        $this->browse(function (Browser $browser) use ($article) {
            $browser
                ->visit(route('articles.show', ['article' => $article->getSlug()]))
                ->assertSee($article->title);

            $article->published = false;
            $article->save();

            $browser
                ->visit(route('articles.show', ['article' => $article->getSlug()]))
                ->assertDontSee($article->title);
        });
    }
}
