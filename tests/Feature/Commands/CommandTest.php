<?php

namespace Tests\Feature\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\GuestCart;
use Viva\Notifications\OrderProductReviewCtaNotification;
use Viva\Notifications\ThirtyDayNoOrderReminderNotification;
use Viva\Order;
use Viva\OrderProduct;
use Viva\Product;

class CommandTest extends TestCase
{
    use RefreshDatabase;

    public function testDeleteExpiredGuestCarts(): void
    {
        $expiredGuestCart = GuestCart::factory()->create();
        $guestCart = GuestCart::factory()->create();

        $expiredGuestCart->expires_at = now()->subMinutes(10);
        $expiredGuestCart->save();
        $guestCart->expires_at = now()->addMinutes(10);
        $guestCart->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $expiredGuestCart->id,
        ]);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
        ]);

        Artisan::call('viva:delete_expited_guest_carts');

        $this->assertDatabaseMissing('guest_carts', [
            'id' => $expiredGuestCart->id,
        ]);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
        ]);
    }

    public function testMangeDiscountsCommand(): void
    {
        Product::factory()->create(['id' => 1, 'price' => 22.00, 'discount_price' => null, 'on_sale' => false]);
        Product::factory()->create(['id' => 2, 'price' => 32.55, 'discount_price' => null, 'on_sale' => false]);
        Product::factory()->create(['id' => 3, 'price' => 85.25, 'discount_price' => null, 'on_sale' => false]);

        $discoutCollection = DiscountCollection::factory()->create([
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
            'start_at' => now()->addMinute(),
            'end_at' => now()->addMinutes(10),
        ]);

        Artisan::call('viva:manage_discounts');

        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
            'start_at' => now()->addMinute(),
            'end_at' => now()->addMinutes(10),
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);

        $discoutCollection->update([
            'start_at' => now()->subMinutes(5),
            'end_at' => now()->addMinutes(10),
        ]);

        Artisan::call('viva:manage_discounts');

        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'active' => true,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 17.6,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 26.05,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);

        $discoutCollection->update([
            'end_at' => now()->subMinute(),
        ]);

        Artisan::call('viva:manage_discounts');

        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
            'start_at' => null,
            'end_at' => null,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 17.6,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 26.05,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);
    }

    public function testSendReviewCtaNotificationCommand(): void
    {
        Notification::fake();

        // will send - fulfilled | older than 7 days | has products | CTA not sent
        $order1 = Order::factory()->create([
            'status' => 'fulfilled',
            'review_cta_sent' => false,
            'created_at' => now()->subDays(15),
        ]);
        OrderProduct::factory()->create(['order_id' => $order1]);

        // wont send - fulfilled | older than 7 days | has products | CTA sent
        $order2 = Order::factory()->create([
            'status' => 'fulfilled',
            'review_cta_sent' => true,
            'created_at' => now()->subDays(15),
        ]);
        OrderProduct::factory()->create(['order_id' => $order2]);

        // wont send - fulfilled | not older than 10 days | has products | CTA not sent
        $order3 = Order::factory()->create([
            'status' => 'fulfilled',
            'review_cta_sent' => false,
            'created_at' => now()->subDays(10),
        ]);
        OrderProduct::factory()->create(['order_id' => $order3]);

        // wont send - not fulfilled | older than 10 days | has products | CTA not sent
        $order4 = Order::factory()->create([
            'status' => 'created',
            'review_cta_sent' => false,
            'created_at' => now()->subDays(15),
        ]);
        OrderProduct::factory()->create(['order_id' => $order4]);

        $this->assertDatabaseHas('orders', [
            'id' => $order1->id,
            'review_cta_sent' => false,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order2->id,
            'review_cta_sent' => true,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order3->id,
            'review_cta_sent' => false,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order4->id,
            'review_cta_sent' => false,
        ]);

        Artisan::call('viva:send_review_cta_notifications');

        Notification::assertSentTo(
            $order1,
            OrderProductReviewCtaNotification::class,
            function ($notification) use ($order1) {
                $mailData = $notification->toMail($order1)->toArray();

                $this->assertStringContainsString('Da li ste zadovoljni sa naručenim proizvodima? | ApotekaViva24', $mailData['subject']);

                return true;
            }
        );

        $this->assertDatabaseHas('orders', [
            'id' => $order1->id,
            'review_cta_sent' => true,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order2->id,
            'review_cta_sent' => true,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order3->id,
            'review_cta_sent' => false,
        ]);
        $this->assertDatabaseHas('orders', [
            'id' => $order4->id,
            'review_cta_sent' => false,
        ]);
    }

    public function testCalculate30DayProductsStatsCommand(): void
    {
        $product = Product::factory()->create();
        $category = Category::factory()->create();
        $category->products()->attach($product->id);
        $order1 = Order::factory()->create();
        $order2 = Order::factory()->create();
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'buy_count_30_days' => 0,
            'best_seller_in_brand' => false,
            'best_seller_in_category' => false,
        ]);

        Artisan::call('viva:calculate_products_30_days_stats');

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'buy_count_30_days' => 0,
            'best_seller_in_brand' => false,
            'best_seller_in_category' => false,
        ]);

        OrderProduct::factory()->create([
            'order_id' => $order1->id,
            'product_id' => $product->id,
            'quantity' => 1,
            'created_at' => now()->subDays(4),
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order2->id,
            'product_id' => $product->id,
            'quantity' => 5,
            'created_at' => now()->subDays(6),
        ]);

        Artisan::call('viva:calculate_products_30_days_stats');

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'buy_count_30_days' => 6,
            'best_seller_in_brand' => true,
            'best_seller_in_category' => true,
        ]);
    }

    public function testCancelExpiredPickupOrdersCommand(): void
    {
        $order1 = Order::factory()->create(['shipping_pickup' => true, 'status' => 'ready_for_pickup', 'created_at' => now()->subDays(2)]);
        $order2 = Order::factory()->create(['shipping_pickup' => true, 'status' => 'ready_for_pickup', 'created_at' => now()->subDays(4)]);
        $order3 = Order::factory()->create(['shipping_pickup' => true, 'status' => 'ready_for_pickup', 'created_at' => now()->subDays(6)]);
        $order4 = Order::factory()->create(['shipping_pickup' => false, 'status' => 'shipped', 'created_at' => now()->subDays(6)]);

        Artisan::call('viva:cancel_expired_pickup_orders');

        $this->assertDatabaseHas('orders', [
            'id' => $order1->id,
            'shipping_pickup' => true,
            'status' => 'ready_for_pickup',
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order2->id,
            'shipping_pickup' => true,
            'status' => 'canceled',
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order3->id,
            'shipping_pickup' => true,
            'status' => 'canceled',
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order4->id,
            'shipping_pickup' => false,
            'status' => 'shipped',
        ]);
    }

    public function testDeleteExpiredProcessingPaymentOrders(): void
    {
        $order1 = Order::factory()->create(['status' => 'ready_for_pickup', 'created_at' => now()->subDays(2)]);
        $order2 = Order::factory()->create(['status' => 'created', 'created_at' => now()->subDays(4)]);
        $order3 = Order::factory()->create(['status' => 'processing_payment', 'created_at' => now()->subDays(6)]);
        $order4 = Order::factory()->create(['status' => 'processing_payment', 'created_at' => now()->subDays(6)]);
        $order5 = Order::factory()->create(['status' => 'processing_payment', 'created_at' => now()->subHours(20)]);

        OrderProduct::factory()->create(['order_id' => $order1->id]);
        OrderProduct::factory()->create(['order_id' => $order2->id]);
        OrderProduct::factory()->create(['order_id' => $order2->id]);
        OrderProduct::factory()->create(['order_id' => $order3->id]);
        OrderProduct::factory()->create(['order_id' => $order3->id]);
        OrderProduct::factory()->create(['order_id' => $order3->id]);
        OrderProduct::factory()->create(['order_id' => $order4->id]);
        OrderProduct::factory()->create(['order_id' => $order4->id]);
        OrderProduct::factory()->create(['order_id' => $order4->id]);
        OrderProduct::factory()->create(['order_id' => $order4->id]);
        OrderProduct::factory()->create(['order_id' => $order5->id]);

        $this->assertEquals(5, Order::count());
        $this->assertEquals(11, OrderProduct::count());

        Artisan::call('viva:delete_expired_processing_payment_orders');

        $this->assertEquals(3, Order::count());
        $this->assertEquals(4, OrderProduct::count());

        $this->assertDatabaseHas('orders', [
            'id' => $order1->id,
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order2->id,
        ]);

        $this->assertDatabaseMissing('orders', [
            'id' => $order3->id,
        ]);

        $this->assertDatabaseMissing('orders', [
            'id' => $order4->id,
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order5->id,
        ]);
    }

    public function testSendThirtyDayNoOrderReminderEmail(): void
    {
        Notification::fake();

        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $order = Order::factory()->create(['created_at' => now()->subDays(29)]);

        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $order->created_at = now()->subDays(30);
        $order->save();

        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $product = Product::factory()->create(['in_stock' => false, 'published' => false]);
        OrderProduct::factory()->create(['product_id' => $product->id, 'order_id' => $order->id]);

        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $product->update(['in_stock' => true, 'published' => false]);
        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $order->created_at = now()->subDays(32);
        $order->save();
        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        
        $newerOrder = Order::factory()->create(['email' => $order->email]);
        $order->created_at = now()->subDays(30);
        $order->save();
        $product->update(['in_stock' => true, 'published' => true]);
        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertNothingSent();

        $newerOrder->delete();
        Artisan::call('viva:send_30_day_no_order_reminder_email');
        Notification::assertSentTo($order, ThirtyDayNoOrderReminderNotification::class);
    }
}
