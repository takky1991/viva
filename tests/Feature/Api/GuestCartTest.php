<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Brand;
use Viva\GuestCart;
use Viva\Product;
use Viva\ProductVariant;

class GuestCartTest extends TestCase
{
    use RefreshDatabase;

    public function testAddGuestCartItemRequest(): void
    {
        $guid = newGuid();
        // Product id invalid
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => 99999,
            'quantity' => 2,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertDontSee('"gid"', false);

        // Product id missing
        $response = $this->json('post', '/api/guest-cart/item', [
            'quantity' => 2,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertDontSee('"gid"', false);

        // Quantity invalid
        $product = Product::factory()->create();
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 0,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"gid"', false);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 51,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"gid"', false);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 'asd',
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"gid"', false);

        // Quantity missing
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"gid"', false);

        // Gid missing
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"gid"', false)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"quantity"', false);

        // Product not in stock
        $product->in_stock = false;
        $product->save();
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"gid"', false)
            ->assertDontSee('"quantity"', false);

        // Product not published
        $product->in_stock = true;
        $product->published = false;
        $product->save();
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guid,
        ]);
        $response->assertStatus(422)
            ->assertSee('"gid"', false)
            ->assertDontSee('"quantity"', false);

        // Variants validation
        $product = Product::factory()->create();
        $productVariant = ProductVariant::factory()->create([
            'product_id' => $product->id,
            'name' => '34',
            'in_stock' => true,
        ]);

        // Variants missing
        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $response->assertStatus(422)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertSee('"variant_id"', false);

        // Variants not in stock
        $productVariant->in_stock = false;
        $productVariant->save();

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
            'variant_id' => $productVariant->id,
        ]);
        $response->assertStatus(422)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertSee('"variant_id"', false);
    }

    public function testRemoveGuestCartItemRequest(): void
    {
        $guid = newGuid();
        $product = Product::factory()->create();
        $guestCart = GuestCart::factory()->create();
        $guestCart->products()->attach($product->id, ['quantity' => 5]);

        // Gid missing
        $response = $this->json('delete', '/api/guest-cart/item/'.$product->id);
        $response->assertStatus(422)
            ->assertSee('"gid"', false);

        // Gid invalid
        $response = $this->json('delete', '/api/guest-cart/item/'.$product->id, [], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $response->assertStatus(422)
            ->assertSee('"gid"', false);
    }

    public function testGetGuestCartItemRequest(): void
    {
        $guid = newGuid();
        $product = Product::factory()->create();
        $guestCart = GuestCart::factory()->create();
        $guestCart->products()->attach($product->id, ['quantity' => 5]);

        // Gid missing
        $response = $this->json('get', '/api/guest-cart');
        $response->assertStatus(422)
            ->assertSee('"gid"', false);

        // Gid invalid
        $response = $this->json('get', '/api/guest-cart', [], ['X-Anonymous-Customer-Unique-Id' => $guid]);
        $response->assertStatus(422)
            ->assertSee('"gid"', false);
    }

    public function testAddItemWhenNoCart(): void
    {
        $guid = newGuid();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ], ['X-Anonymous-Customer-Unique-Id' => $guid]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id', 'guest_id', 'expires_at', 'products', 'free_shipping',
            ]);
        $exiresAt = now()->addMinutes(config('app.guest_cart_lifetime'));
        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(5, $response->products[0]->quantity);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $response->id,
            'guest_id' => $guid,
            'expires_at' => $exiresAt,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 5,
        ]);
    }

    public function testAddItem(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand]);
        $guestCart = GuestCart::factory()->create();

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'guest_id', 'expires_at', 'products', 'free_shipping',
            ]);

        $exiresAt = now()->addMinutes(config('app.guest_cart_lifetime'));
        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(5, $response->products[0]->quantity);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => $exiresAt,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product->id,
            'quantity' => 5,
        ]);

        $product1 = Product::factory()->create();
        $productVariant = ProductVariant::factory()->create([
            'name' => '34',
            'in_stock' => true,
            'product_id' => $product1->id,
        ]);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product1->id,
            'quantity' => 5,
            'variant_id' => $productVariant->id,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('guest_cart_product', [
            'product_id' => $product1->id,
            'quantity' => 1,
            'variants' => '34',
        ]);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product1->id,
            'quantity' => 5,
            'variant_id' => $productVariant->id,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('guest_cart_product', [
            'product_id' => $product1->id,
            'quantity' => 2,
            'variants' => '34,34',
        ]);
    }

    public function testCartFreeShippingIsReturnedOnAddItem(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'price' => 51,
            'on_sale' => false,
            'brand_id' => $brand,
        ]);
        $guestCart = GuestCart::factory()->create();

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'guest_id', 'expires_at', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(1, $response->products[0]->quantity);
        $this->assertEquals(false, $response->free_shipping);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime')),
            'free_shipping' => false,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product->id,
            'quantity' => 1,
        ]);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'guest_id', 'expires_at', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(2, $response->products[0]->quantity);
        $this->assertEquals(true, $response->free_shipping);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime'))->toDateTimeString(),
            'free_shipping' => true,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product->id,
            'quantity' => 2,
        ]);
    }

    public function testAddExistingItem(): void
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand]);
        $guestCart = GuestCart::factory()->create();
        $guestCart->products()->attach($product->id, ['quantity' => 5]);

        $response = $this->json('post', '/api/guest-cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'guest_id', 'expires_at', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(10, $response->products[0]->quantity);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime'))->toDateTimeString(),
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product->id,
            'quantity' => 10,
        ]);
    }

    public function testRemoveItem(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $guestCart = GuestCart::factory()->create();

        $guestCart->products()->attach($product1->id, ['quantity' => 5]);
        $guestCart->products()->attach($product2->id, ['quantity' => 3]);

        $response = $this->json('delete', '/api/guest-cart/item/'.$product2->id, [], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);
        $response->assertStatus(204);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime')),
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product1->id,
            'quantity' => 5,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product2->id,
        ]);

        $response = $this->json('delete', '/api/guest-cart/item/'.$product2->id, [], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);
        $response->assertStatus(400);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime')),
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product1->id,
            'quantity' => 5,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product2->id,
        ]);

        $response = $this->json('delete', '/api/guest-cart/item/'.$product1->id, [], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);
        $response->assertStatus(204);

        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guestCart->id,
            'guest_id' => $guestCart->guest_id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
            'product_id' => $product2->id,
        ]);
    }

    public function testGet(): void
    {
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['brand_id' => $brand]);
        $product2 = Product::factory()->create(['brand_id' => $brand]);
        $guestCart = GuestCart::factory()->create();

        $guestCart->products()->attach($product1->id, ['quantity' => 5]);
        $guestCart->products()->attach($product2->id, ['quantity' => 3]);

        $response = $this->json('get', '/api/guest-cart', [], ['X-Anonymous-Customer-Unique-Id' => $guestCart->guest_id]);

        $response->assertStatus(200)
            ->assertJson([
                'id' => $guestCart->id,
                'guest_id' => $guestCart->guest_id,
                'products' => [
                    [
                        'id' => $product1->id,
                        'quantity' => 5,
                    ],
                    [
                        'id' => $product2->id,
                        'quantity' => 3,
                    ],
                ],
                'expires_at' => $guestCart->expires_at->toJSON(),
            ]);
    }

    public function testGetNoCart(): void
    {
        $response = $this->json('get', '/api/guest-cart', [], ['X-Anonymous-Customer-Unique-Id' => newGuid()]);

        $response->assertStatus(422);
    }
}
