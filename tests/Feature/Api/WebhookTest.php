<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\NewsletterContact;
use Viva\User;

class WebhookTest extends TestCase
{
    use RefreshDatabase;

    public function testMailgunPermanentFailWebhook(): void
    {
        $user = User::factory()->create(['email' => 'takky1991@gmail.com']);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'email' => 'takky1991@gmail.com',
        ]);

        $contact = $user->newsletterContact()->create(['email' => 'takky1991@gmail.com', 'token' => 'aasdasdads']);
        $this->assertDatabaseHas('newsletter_contacts', [
            'id' => $contact->id,
            'email' => 'takky1991@gmail.com',
        ]);

        $contact2 = NewsletterContact::create(['email' => 'ado@gmail.com', 'token' => 'aasdasdads231432']);
        $this->assertDatabaseHas('newsletter_contacts', [
            'id' => $contact2->id,
            'email' => 'ado@gmail.com',
        ]);

        $response = $this->json('post', '/api/webhooks/mailgun/permanent-fail', ['event-data' => ['recipient' => 'ado@gmail.com']]);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => 'ado@gmail.com',
        ]);

        $response = $this->json('post', '/api/webhooks/mailgun/permanent-fail', ['event-data' => ['recipient' => 'takky1991@gmail.com']]);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('newsletter_contacts', [
            'email' => 'takky1991@gmail.com',
        ]);
        $this->assertDatabaseHas('users', [
            'email' => 'takky1991@gmail.com',
        ]);
    }
}
