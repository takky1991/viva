<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Viva\DiscountCollection;
use Viva\Product;
use Viva\User;

class DiscountCollectionTest extends TestCase
{
    use RefreshDatabase;

    public function testUpdateProducts(): void
    {
        $discoutCollection = DiscountCollection::factory()->create([
            'discount_percentage' => 15,
            'active' => false,
        ]);

        $response = $this->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update');
        $response->assertStatus(401);

        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        $product1 = Product::factory()->create(['id' => 1, 'price' => 22.00, 'discount_price' => null, 'on_sale' => false]);
        $product2 = Product::factory()->create(['id' => 2, 'price' => 32.55, 'discount_price' => null, 'on_sale' => false]);
        $product3 = Product::factory()->create(['id' => 3, 'price' => 85.25, 'discount_price' => null, 'on_sale' => false]);

        // add some products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [1, 2, 3],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[1,2,3]',
        ]);

        // add more products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [2, 3],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[2,3]',
        ]);

        // remove all produts
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[]',
        ]);

        // Activate the discount and remove all products
        $discoutCollection->update([
            'active' => true,
            'product_ids' => null,
        ]);

        // add some products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [1, 2],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[1,2]',
            'active' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 18.7,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 27.65,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);

        // remove some products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [2],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[2]',
            'active' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 18.7,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 27.65,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);

        // change products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [3],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[3]',
            'active' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 18.7,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 27.65,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => 72.45,
            'on_sale' => true,
        ]);

        // remove all products
        $response = $this->actingAs($user, 'api')->json('post', '/api/discount-collections/'.$discoutCollection->id.'/products/update', [
            'products' => [],
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'product_ids' => '[]',
            'active' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 18.7,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 27.65,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => 72.45,
            'on_sale' => false,
        ]);
    }

    public function testEnableDiscountCollection(): void
    {
        $product1 = Product::factory()->create(['id' => 1, 'price' => 22.00, 'discount_price' => null, 'on_sale' => false]);
        $product2 = Product::factory()->create(['id' => 2, 'price' => 32.55, 'discount_price' => null, 'on_sale' => false]);
        $product3 = Product::factory()->create(['id' => 3, 'price' => 85.25, 'discount_price' => null, 'on_sale' => false]);

        $discoutCollection = DiscountCollection::factory()->create([
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);

        $response = $this->json('get', '/api/discount-collections/'.$discoutCollection->id.'/enable');
        $response->assertStatus(401);

        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        $response = $this->actingAs($user, 'api')->json('get', '/api/discount-collections/'.$discoutCollection->id.'/enable');
        $response->assertStatus(200);

        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'active' => true,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 17.6,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 26.05,
            'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);
    }

    public function testDisableDiscountCollection(): void
    {
        $product1 = Product::factory()->create(['id' => 1, 'price' => 22.00, 'discount_price' => 17.6, 'on_sale' => true]);
        $product2 = Product::factory()->create(['id' => 2, 'price' => 32.55, 'discount_price' => 26.05, 'on_sale' => true]);
        $product3 = Product::factory()->create(['id' => 3, 'price' => 85.25, 'discount_price' => null, 'on_sale' => false]);

        $discoutCollection = DiscountCollection::factory()->create([
            'active' => true,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);

        $response = $this->json('get', '/api/discount-collections/'.$discoutCollection->id.'/disable');
        $response->assertStatus(401);

        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        $response = $this->actingAs($user, 'api')->json('get', '/api/discount-collections/'.$discoutCollection->id.'/disable');
        $response->assertStatus(200);

        $this->assertDatabaseHas('discount_collections', [
            'id' => $discoutCollection->id,
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 1,
            'price' => 22.00,
            'discount_price' => 17.6,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2,
            'price' => 32.55,
            'discount_price' => 26.05,
            'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3,
            'price' => 85.25,
            'discount_price' => null,
            'on_sale' => false,
        ]);
    }
}
