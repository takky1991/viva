<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Viva\Brand;
use Viva\User;

class BrandTest extends TestCase
{
    use RefreshDatabase;

    public function testGetBrands(): void
    {
        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        Brand::factory()->count(5)->create();
        Brand::factory()->create(['name' => 'Test brand']);

        $response = $this->json('get', '/api/brands');
        $response->assertStatus(401);

        $response = $this->actingAs($user, 'api')->json('get', '/api/brands');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(6, count($response));

        $response = $this->actingAs($user, 'api')->json('get', '/api/brands?q=test');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(1, count($response));
    }
}
