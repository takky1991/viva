<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Viva\Category;
use Viva\User;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function testGetCategories(): void
    {
        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        Category::factory()->count(5)->create();
        $category = Category::factory()->create(['title' => 'Test category']);

        $response = $this->json('get', '/api/categories');
        $response->assertStatus(401);

        $response = $this->actingAs($user, 'api')->json('get', '/api/categories');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(6, count($response));

        $response = $this->actingAs($user, 'api')->json('get', '/api/categories?q=test');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(1, count($response));
    }
}
