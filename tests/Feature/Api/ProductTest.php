<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Viva\BackToStockAlert;
use Viva\Brand;
use Viva\Notifications\Admin\NewReviewNotification;
use Viva\Product;
use Viva\Review;
use Viva\User;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function testGetProducts(): void
    {
        Role::create(['name' => 'admin']);
        $user = User::factory()->create();
        $user->assignRole('admin');

        $brand = Brand::factory()->create();
        Product::factory()->count(5)->create(['brand_id' => $brand]);
        $product = Product::factory()->create(['title' => 'Test product', 'brand_id' => $brand]);

        $response = $this->json('get', '/api/products');
        $response->assertStatus(401);

        $response = $this->actingAs($user, 'api')->json('get', '/api/products');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(6, count($response));

        $response = $this->actingAs($user, 'api')->json('get', '/api/products?q=test');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(1, count($response));
    }

    public function testCreateReview(): void
    {
        Notification::fake();

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'admin', 'guard_name' => 'api']);
        $adminUser = User::factory()->create();
        $adminUser->assignRole('admin');
        $product = Product::factory()->create();

        $this->assertEmpty($product->reviews);

        $response = $this->json('post', '/api/products/'.$product->id.'/reviews', []);
        $response->assertStatus(422);

        $response = $this->json('post', '/api/products/'.$product->id.'/reviews', [
            'rating' => 3,
            'name' => 'Tarik',
            'content' => 'asdasdsa asdasd',
        ]);
        $response->assertStatus(200);

        Notification::assertSentTo(
            $adminUser,
            NewReviewNotification::class,
            function ($notification) use ($adminUser) {
                $mailData = $notification->toMail($adminUser)->toArray();

                $this->assertStringContainsString('Nova Recenzija | ApotekaViva24', $mailData['subject']);

                return true;
            }
        );

        $product->refresh();

        $this->assertEquals(1, $product->reviews->count());
        $this->assertDatabaseHas('reviews', [
            'product_id' => $product->id,
            'user_id' => null,
            'name' => 'Tarik',
            'content' => 'asdasdsa asdasd',
            'rating' => 3,
            'published' => 0,
        ]);

        $user = User::factory()->create();
        $response = $this->actingAs($user, 'api')
            ->json('post', '/api/products/'.$product->id.'/reviews', [
                'rating' => 4,
                'name' => 'Azra',
                'content' => 'Veoma odlican',
            ]);
        $response->assertStatus(200);

        $product->refresh();

        $this->assertEquals(2, $product->reviews->count());
        $this->assertDatabaseHas('reviews', [
            'product_id' => $product->id,
            'user_id' => $user->id,
            'name' => 'Azra',
            'content' => 'Veoma odlican',
            'rating' => 4,
            'published' => 0,
        ]);
    }

    public function testGetReviews(): void
    {
        $product = Product::factory()->create();
        Review::factory()->count(3)->create(['product_id' => $product->id, 'published' => true]);
        Review::factory()->count(5)->create(['product_id' => $product->id, 'published' => false]);

        $product->refresh();

        $this->assertEquals(8, $product->reviews->count());
        $this->assertEquals(3, $product->publishedReviews->count());

        $response = $this->json('get', '/api/products/'.$product->id.'/reviews');
        $response->assertStatus(200);

        $response = json_decode($response->content());
        $this->assertEquals(3, count($response->data->reviews));
    }

    public function testCreateBackToStockAlert(): void
    {
        $product = Product::factory()->create();
        $this->assertDatabaseMissing('back_to_stock_alerts', [
            'product_id' => $product->id,
        ]);

        $response = $this->json('post', '/api/products/'.$product->id.'/alert', ['email' => 'asd']);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('back_to_stock_alerts', [
            'product_id' => $product->id,
        ]);

        $response = $this->json('post', '/api/products/'.$product->id.'/alert', ['email' => 'asd@gmail.com']);
        $response->assertStatus(200);
        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);

        $response = $this->json('post', '/api/products/'.$product->id.'/alert', ['email' => 'asd@gmail.com']);
        $response->assertStatus(200);
        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);

        $this->assertEquals(1, $product->backToStockAlerts()->where('email', 'asd@gmail.com')->count());

        BackToStockAlert::where('notification_sent', false)->update(['notification_sent' => true]);

        $response = $this->json('post', '/api/products/'.$product->id.'/alert', ['email' => 'asd@gmail.com']);
        $response->assertStatus(200);
        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);
        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => true,
        ]);
        $this->assertEquals(2, $product->backToStockAlerts()->where('email', 'asd@gmail.com')->count());
    }
}
