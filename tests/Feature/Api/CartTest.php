<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Brand;
use Viva\Product;
use Viva\ProductVariant;
use Viva\User;

class CartTest extends TestCase
{
    use RefreshDatabase;

    public function testCartAuth(): void
    {
        $response = $this->json('get', '/api/cart');
        $response->assertStatus(401);

        $response = $this->json('post', '/api/cart/item');
        $response->assertStatus(401);

        $response = $this->json('delete', '/api/cart/item/5');
        $response->assertStatus(401);
    }

    public function testAddCartItemRequest(): void
    {
        $user = User::factory()->create();

        // Product id invalid
        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => 99999,
            'quantity' => 2,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false);

        // Product id missing
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'quantity' => 2,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false);

        // Quantity invalid
        $product = Product::factory()->create();
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 0,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false);

        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 51,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false);

        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 'asd',
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false);

        // Quantity missing
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"quantity"', false)
            ->assertDontSee('"id"', false);

        // Product not in stock
        $product->in_stock = false;
        $product->save();
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false);

        // Product not published
        $product->in_stock = true;
        $product->published = false;
        $product->save();
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $response->assertStatus(422)
            ->assertSee('"id"', false)
            ->assertDontSee('"quantity"', false);

        // Variants validation
        $product = Product::factory()->create();
        $productVariant = ProductVariant::factory()->create([
            'product_id' => $product->id,
            'name' => '34',
            'in_stock' => true,
        ]);

        // Variants missing
        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);
        $response->assertStatus(422)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertSee('"variant_id"', false);

        // Variants not in stock
        $productVariant->in_stock = false;
        $productVariant->save();

        $response = $this->actingAs($user)->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
            'variant_id' => $productVariant->id,
        ]);
        $response->assertStatus(422)
            ->assertDontSee('"id"', false)
            ->assertDontSee('"quantity"', false)
            ->assertSee('"variant_id"', false);
    }

    public function testAddItemWhenNoCart(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'id', 'user_id', 'products', 'free_shipping',
            ]);
        $response = json_decode($response->content());

        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(5, $response->products[0]->quantity);

        $this->assertDatabaseHas('carts', [
            'id' => $response->id,
            'user_id' => $user->id,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 5,
        ]);
    }

    public function testAddItem(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand]);
        $user->cart()->create();

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'user_id', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(5, $response->products[0]->quantity);

        $this->assertDatabaseHas('carts', [
            'id' => $response->id,
            'user_id' => $user->id,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 5,
            'variants' => null,
        ]);

        $product1 = Product::factory()->create();
        $productVariant = ProductVariant::factory()->create([
            'name' => '34',
            'in_stock' => true,
            'product_id' => $product1->id,
        ]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product1->id,
            'quantity' => 5,
            'variant_id' => $productVariant->id,
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('cart_product', [
            'product_id' => $product1->id,
            'quantity' => 1,
            'variants' => '34',
        ]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product1->id,
            'quantity' => 5,
            'variant_id' => $productVariant->id,
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('cart_product', [
            'product_id' => $product1->id,
            'quantity' => 2,
            'variants' => '34,34',
        ]);
    }

    public function testAddExistingItem(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand]);
        $user->cart()->create();
        $user->cart->products()->attach($product->id, ['quantity' => 5]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 5,
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'user_id', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(10, $response->products[0]->quantity);

        $this->assertDatabaseHas('carts', [
            'id' => $response->id,
            'user_id' => $user->id,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 10,
        ]);
    }

    public function testCartFreeShippingIsReturnedOnAddItem(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product = Product::factory()->create([
            'price' => 51,
            'on_sale' => false,
            'brand_id' => $brand,
        ]);
        $user->cart()->create();

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'user_id', 'products', 'free_shipping',
            ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(1, $response->products[0]->quantity);
        $this->assertEquals(false, $response->free_shipping);

        $this->assertDatabaseHas('carts', [
            'id' => $response->id,
            'user_id' => $user->id,
            'free_shipping' => false,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 1,
        ]);

        $response = $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ]);

        $response = json_decode($response->content());
        $this->assertEquals(1, count($response->products));
        $this->assertEquals($product->id, $response->products[0]->id);
        $this->assertEquals(2, $response->products[0]->quantity);
        $this->assertEquals(true, $response->free_shipping);

        $this->assertDatabaseHas('carts', [
            'id' => $response->id,
            'user_id' => $user->id,
            'free_shipping' => true,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $response->id,
            'product_id' => $product->id,
            'quantity' => 2,
        ]);
    }

    public function testRemoveItemWhenNoCart(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user, 'api')->json('delete', '/api/cart/item/'.$product->id);
        $response->assertStatus(400);
    }

    public function testRemoveItem(): void
    {
        $user = User::factory()->create();
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $user->cart()->create();
        $user->cart->products()->attach($product1->id, ['quantity' => 5]);
        $user->cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
            'quantity' => 5,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $response = $this->actingAs($user, 'api')->json('delete', '/api/cart/item/'.$product1->id);
        $response->assertStatus(204);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $response = $this->actingAs($user, 'api')->json('delete', '/api/cart/item/'.$product1->id);
        $response->assertStatus(400);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $response = $this->actingAs($user, 'api')->json('delete', '/api/cart/item/'.$product2->id);
        $response->assertStatus(204);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
        ]);
    }

    public function testGet(): void
    {
        $user = User::factory()->create();
        $brand = Brand::factory()->create();
        $product1 = Product::factory()->create(['brand_id' => $brand]);
        $product2 = Product::factory()->create(['brand_id' => $brand]);
        $user->cart()->create();
        $user->cart->products()->attach($product1->id, ['quantity' => 5]);
        $user->cart->products()->attach($product2->id, ['quantity' => 3]);

        $response = $this->actingAs($user, 'api')->json('get', '/api/cart');

        $response->assertStatus(200)
            ->assertJson([
                'user_id' => $user->id,
                'products' => [
                    [
                        'id' => $product1->id,
                        'quantity' => 5,
                    ],
                    [
                        'id' => $product2->id,
                        'quantity' => 3,
                    ],
                ],
            ]);
    }

    public function testGetNoCart(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user, 'api')->json('get', '/api/cart');

        $response->assertStatus(204);
    }

    public function testSourceWebIsSetOnCartCreation(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create(['published' => true]);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ])->assertStatus(201);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
            'source' => 'web',
        ]);
    }

    public function testSourceAndroidIsSetOnCartCreation(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create(['published' => true]);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ], ['User-Agent' => 'Chrome (15.5.5) w2n/Android'])->assertStatus(201);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
            'source' => 'android',
        ]);
    }

    public function testSourceiOSIsSetOnCartCreation(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create(['published' => true]);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $this->actingAs($user, 'api')->json('post', '/api/cart/item', [
            'id' => $product->id,
            'quantity' => 1,
        ], ['User-Agent' => 'Chrome (15.5.5) w2n/iOS'])->assertStatus(201);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
            'source' => 'ios',
        ]);
    }
}
