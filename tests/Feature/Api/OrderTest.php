<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Order;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    public function testOrderSeenEndpoint(): void
    {
        $order = Order::factory()->create(['confirmation_shown' => false]);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'confirmation_shown' => false,
        ]);

        $response = $this->json('get', '/api/orders/'.$order->order_number.'/seen');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(0, $response);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'confirmation_shown' => true,
        ]);

        $response = $this->json('get', '/api/orders/'.$order->order_number.'/seen');
        $response->assertStatus(200);
        $response = json_decode($response->content());
        $this->assertEquals(1, $response);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'confirmation_shown' => true,
        ]);
    }
}
