<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Viva\BackToStockAlert;
use Viva\Brand;
use Viva\Category;
use Viva\Notifications\BackToStockNotification;
use Viva\Product;
use Viva\Review;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function testGetSlugs(): void
    {
        $brand = Brand::factory()->create(['name' => 'Brand name']);
        $product = Product::factory()->create([
            'title' => 'first name',
            'package_description' => '40 ml',
            'brand_id' => $brand->id,
        ]);

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'brand-name-first-name-40-ml',
        ]);

        sleep(1);

        $product->title = 'second name';
        $product->save();

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'brand-name-second-name-40-ml',
        ]);

        $this->assertEquals(['brand-name-second-name-40-ml', 'brand-name-first-name-40-ml'], $product->slugs()->pluck('slug')->toArray());
    }

    public function testGetSlug(): void
    {
        $brand = Brand::factory()->create(['name' => 'Brand name']);
        $product = Product::factory()->create([
            'title' => 'First name',
            'package_description' => '40 ml',
            'brand_id' => $brand->id,
        ]);
        sleep(1);
        $product->title = 'second name';
        $product->save();

        $this->assertEquals('brand-name-second-name-40-ml', $product->getSlug());
    }

    public function testOnSale(): void
    {
        $product = Product::factory()->create(['on_sale' => false, 'discount_price' => null]);
        $this->assertEquals(false, $product->onSale());

        $product = Product::factory()->create(['on_sale' => true, 'discount_price' => null]);
        $this->assertEquals(false, $product->onSale());

        $product = Product::factory()->create(['on_sale' => false, 'discount_price' => 100]);
        $this->assertEquals(false, $product->onSale());

        $product = Product::factory()->create(['on_sale' => true, 'discount_price' => 100]);
        $this->assertEquals(true, $product->onSale());
    }

    public function testRealPrice(): void
    {
        $product = Product::factory()->create(['on_sale' => false, 'price' => 10, 'discount_price' => null]);
        $this->assertEquals(10, $product->realPrice());

        $product = Product::factory()->create(['on_sale' => false, 'price' => 10, 'discount_price' => 5]);
        $this->assertEquals(10, $product->realPrice());

        $product = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => null]);
        $this->assertEquals(10, $product->realPrice());

        $product = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $this->assertEquals(5, $product->realPrice());
    }

    public function testFormatPrice(): void
    {
        $this->assertEquals('1.00KM', formatPrice(1.00));
        $this->assertEquals('1.05KM', formatPrice(1.05));
        $this->assertEquals('1.09KM', formatPrice(1.09));
        $this->assertEquals('1.50KM', formatPrice(1.50));
        $this->assertEquals('1.70KM', formatPrice(1.70));
        $this->assertEquals('1.99KM', formatPrice(1.99));
    }

    public function testProductSlug(): void
    {
        $brand = Brand::factory()->create(['name' => 'Brand name']);
        $product = Product::factory()->create([
            'title' => 'First product',
            'brand_id' => $brand->id,
            'package_description' => '40 ml',
        ]);

        $this->assertDatabaseHas('products', [
            'title' => 'First product',
        ]);

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'brand-name-first-product-40-ml',
        ]);

        // Change product title
        $product->title = 'Second product';
        $product->save();

        $this->assertDatabaseHas('products', [
            'title' => 'Second product',
        ]);

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'brand-name-second-product-40-ml',
        ]);

        // Change product description
        $product->package_description = '50 ml';
        $product->save();

        $this->assertDatabaseHas('products', [
            'title' => 'Second product',
            'package_description' => '50 ml',
        ]);

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'brand-name-second-product-50-ml',
        ]);

        // Change brand
        $newBrand = Brand::factory()->create(['name' => 'New brand']);
        $product->brand_id = $newBrand->id;
        $product->save();
        $product->refresh();
        $product->save();

        $this->assertDatabaseHas('products', [
            'title' => 'Second product',
            'package_description' => '50 ml',
            'brand_id' => $newBrand->id,
        ]);

        $this->assertDatabaseHas('product_slugs', [
            'product_id' => $product->id,
            'slug' => 'new-brand-second-product-50-ml',
        ]);

        // Delete product
        $product->delete();

        $this->assertDatabaseMissing('products', [
            'id' => $product->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('product_slugs', [
            'product_id' => $product->id,
        ]);
    }

    public function testCalculateDiscout(): void
    {
        // price 100.00, discount 20%, new price 80.00
        $newPrice = calculateDiscout(100.00, 20);
        $this->assertEquals(80.00, $newPrice);

        // price 65.80, discount 15%, new price 55.93 => 55.95
        $newPrice = calculateDiscout(65.80, 15);
        $this->assertEquals(55.95, $newPrice);

        // price 65.75, discount 15%, new price 55.88 => 55.9
        $newPrice = calculateDiscout(65.75, 15);
        $this->assertEquals(55.9, $newPrice);

        // price 65.73, discount 15%, new price 55.87 => 55.85
        $newPrice = calculateDiscout(65.73, 15);
        $this->assertEquals(55.85, $newPrice);
    }

    public function testGetCategoriesHierarchy(): void
    {
        $category1 = Category::factory()->create();
        $category2 = Category::factory()->create(['parent_id' => $category1]);
        $category3 = Category::factory()->create(['parent_id' => $category2]);

        $product = Product::factory()->create();
        $this->assertEquals(
            '',
            $product->generateCategoriesHierarchy()
        );

        $product->categories()->attach([$category1->id, $category2->id, $category3->id]);
        $product->refresh();

        $this->assertEquals(
            $category1->title.'/'.$category2->title.'/'.$category3->title,
            $product->generateCategoriesHierarchy()
        );
    }

    public function testUpdatingCategoryChangesCategoriesHierarchyForProducts(): void
    {
        $category1 = Category::factory()->create(['title' => 'Preparati', 'menu' => true]);
        $category11 = Category::factory()->create(['title' => 'Kozmetika', 'parent_id' => $category1->id, 'menu' => true]);
        $category111 = Category::factory()->create(['title' => 'Masna Koza', 'parent_id' => $category11->id, 'menu' => true]);

        $product = Product::factory()->create();
        $product->categories()->sync([$category1->id, $category11->id, $category111->id]);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'categories_hierarchy' => '',
        ]);

        $product->save();

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);

        $product->categories()->sync([$category1->id, $category11->id]);
        $product->save();

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'categories_hierarchy' => 'Preparati/Kozmetika',
        ]);

        $product->categories()->sync([$category1->id, $category11->id, $category111->id]);
        $product->save();

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);
    }

    public function testGetDiscountPercentage(): void
    {
        $product = Product::factory()->create([
            'price' => 44,
            'discount_price' => 33,
        ]);

        $this->assertEquals(25, $product->getDiscountPercentage());
    }

    public function testCalculateRating(): void
    {
        $product = Product::factory()->create();

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'rating' => null,
            'rating_count' => 0,
        ]);

        Review::factory()->create(['product_id' => $product->id, 'rating' => 4, 'published' => false]);
        Review::factory()->create(['product_id' => $product->id, 'rating' => 4, 'published' => true]);
        Review::factory()->create(['product_id' => $product->id, 'rating' => 2, 'published' => true]);
        Review::factory()->create(['product_id' => $product->id, 'rating' => 5, 'published' => true]);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'rating' => 4,
            'rating_count' => 3,
        ]);

        $review = Review::factory()->create(['product_id' => $product->id, 'rating' => 2, 'published' => true]);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'rating' => 3,
            'rating_count' => 4,
        ]);

        $review->delete();

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'rating' => 4,
            'rating_count' => 3,
        ]);
    }

    public function testNotifyBackToStockAlerts(): void
    {
        Notification::fake();

        $product = Product::factory()->create([
            'in_stock' => true,
        ]);

        $alert1 = BackToStockAlert::create([
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);
        $alert2 = BackToStockAlert::create([
            'product_id' => $product->id,
            'email' => 'asd1@gmail.com',
            'notification_sent' => false,
        ]);
        $alert3 = BackToStockAlert::create([
            'product_id' => $product->id,
            'email' => 'asd2@gmail.com',
            'notification_sent' => true,
        ]);

        $product->update(['in_stock' => false]);

        Notification::assertNothingSent();

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => false,
        ]);

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd1@gmail.com',
            'notification_sent' => false,
        ]);

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd2@gmail.com',
            'notification_sent' => true,
        ]);

        $product->update(['in_stock' => true]);

        Notification::assertSentTo($alert1, BackToStockNotification::class);
        Notification::assertSentTo($alert2, BackToStockNotification::class);
        Notification::assertNotSentTo($alert3, BackToStockNotification::class);

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
            'notification_sent' => true,
        ]);

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd1@gmail.com',
            'notification_sent' => true,
        ]);

        $this->assertDatabaseHas('back_to_stock_alerts', [
            'product_id' => $product->id,
            'email' => 'asd2@gmail.com',
            'notification_sent' => true,
        ]);
    }
}
