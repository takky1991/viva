<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Order;
use Viva\OrderProduct;

class OrderProductTest extends TestCase
{
    use RefreshDatabase;

    public function testOnSale(): void
    {
        $order = Order::factory()->create();

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        $this->assertEquals(false, $orderProduct->onSale());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => null,
        ]);
        $this->assertEquals(false, $orderProduct->onSale());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => 100,
        ]);
        $this->assertEquals(false, $orderProduct->onSale());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 100,
        ]);
        $this->assertEquals(true, $orderProduct->onSale());
    }

    public function testRealPrice(): void
    {
        $order = Order::factory()->create();

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'price' => 10,
            'discount_price' => null,
        ]);
        $this->assertEquals(10, $orderProduct->realPrice());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'price' => 10,
            'discount_price' => 5,
        ]);
        $this->assertEquals(10, $orderProduct->realPrice());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => null,
        ]);
        $this->assertEquals(10, $orderProduct->realPrice());

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => 5,
        ]);
        $this->assertEquals(5, $orderProduct->realPrice());
    }

    public function testRealPriceAndTotalPriceIsSet(): void
    {
        $order = Order::factory()->create();

        $orderProduct = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => 5,
            'quantity' => 2,
        ]);

        $this->assertDatabaseHas('order_products', [
            'id' => $orderProduct->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => 5,
            'real_price' => 5,
            'quantity' => 2,
            'total_price' => 10,
        ]);

        $orderProduct2 = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'price' => 10,
            'discount_price' => 5,
            'quantity' => 3,
        ]);

        $this->assertDatabaseHas('order_products', [
            'id' => $orderProduct2->id,
            'on_sale' => false,
            'price' => 10,
            'discount_price' => 5,
            'real_price' => 10,
            'quantity' => 3,
            'total_price' => 30,
        ]);

        $orderProduct3 = OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'price' => 10,
            'quantity' => 1,
            'discount_price' => null,
        ]);

        $this->assertDatabaseHas('order_products', [
            'id' => $orderProduct3->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => null,
            'real_price' => 10,
            'quantity' => 1,
            'total_price' => 10,
        ]);
    }
}
