<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;
use Viva\BackToStockAlert;
use Viva\Country;
use Viva\Notifications\BackToStockNotification;
use Viva\Notifications\OrderPickupReadyNotification;
use Viva\Notifications\OrderProductReviewCtaNotification;
use Viva\Notifications\OrderShippedNotification;
use Viva\Notifications\OrderSuccessfulNotification;
use Viva\Notifications\ResetPasswordNotification;
use Viva\Notifications\ThirtyDayNoOrderReminderNotification;
use Viva\Notifications\VerifyEmail;
use Viva\Order;
use Viva\OrderProduct;
use Viva\Product;
use Viva\ShippingMethod;
use Viva\User;

class NotificationTest extends TestCase
{
    use RefreshDatabase;

    public function testVerifyEmail(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        $user->sendEmailVerificationNotification();

        $url = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(config()->get('auth.verification.expire', 60)),
            [
                'id' => $user->getKey(),
                'hash' => sha1($user->getEmailForVerification()),
            ]
        );

        Notification::assertSentTo(
            $user,
            VerifyEmail::class,
            function ($notification) use ($user, $url) {
                $mailData = $notification->toMail($user)->toArray();
                $email = $notification->toMail($user)->render();

                $this->assertStringContainsString('Potvrdite Vašu email adresu | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Kliknite na dugme ispod kako bi verifikovali Vašu email adresu.', $email);
                $this->assertStringContainsString('Verifikuj', $email);
                $this->assertStringContainsString(str_replace('&', '&amp;', $url), $email);
                $this->assertStringContainsString('Ako niste Vi kreirali ApotekaViva24 račun, ignorišite ovaj email.', $email);

                return true;
            }
        );
    }

    public function testResetPasswordNotification(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        $user->sendPasswordResetNotification('asdasdasd');

        Notification::assertSentTo(
            $user,
            ResetPasswordNotification::class,
            function ($notification) use ($user) {
                $mailData = $notification->toMail($user)->toArray();
                $email = $notification->toMail($user)->render();

                $this->assertStringContainsString('Promjena šifre | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Zaprimili smo zahtjev za promjenu šifre za Vaš ApotekaViva24 korisnički račun.', $email);
                $this->assertStringContainsString('Promjeni šifru', $email);
                $this->assertStringContainsString(url(config('app.url').route('password.reset', ['token' => 'asdasdasd'], false)), $email);
                $this->assertStringContainsString('Link za promjenu šifre će isteći za '.config('auth.passwords.users.expire').' minuta.', $email);
                $this->assertStringContainsString('Ako niste zatražili promjenu šifre, daljnji koraci nisu potrebni.', $email);

                return true;
            }
        );
    }

    public function testOrderSuccessfulNotificationPickup(): void
    {
        Notification::fake();

        $shippingMethod = ShippingMethod::factory()->create([
            'pickup' => true,
            'key' => 'asdasd',
        ]);

        // Pickup order
        $order = Order::factory()->create([
            'shipping_method_id' => $shippingMethod->id,
            'shipping_key' => $shippingMethod->key,
            'shipping_name' => $shippingMethod->name,
            'shipping_price' => $shippingMethod->price,
            'shipping_pickup' => $shippingMethod->pickup,
            'email' => 'test@gmail.com',
            'additional_info' => 'ovo je dodatni text',
        ]);

        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 1.00,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
            'variants' => '34',
        ]);

        $order->notify(new OrderSuccessfulNotification());

        Notification::assertSentTo(
            $order,
            OrderSuccessfulNotification::class,
            function ($notification) use ($order) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Vaša Narudžba ('.$order->order_number.') | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Pozdrav '.$order->first_name.'.', $email);
                $this->assertStringContainsString('Zaprimili smo Vašu narudžbu.', $email);
                $this->assertStringContainsString('Narudžba će biti spremna u poslovnici', $email);
                $this->assertStringContainsString('Rok za preuzimanje je', $email);
                $this->assertStringContainsString(config('app.pickup_days_limit').' dana', $email);
                $this->assertStringContainsString('od dana naručivanja tj. do', $email);
                $this->assertStringContainsString($order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.'), $email);
                $this->assertStringContainsString('(do kraja radnog vremena poslovnice).', $email);
                $this->assertStringContainsString('Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći', $email);
                $this->assertStringContainsString('Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona', $email);
                $this->assertStringContainsString(config('app.phone_short'), $email);
                $this->assertStringContainsString('ili putem maila na adresu', $email);
                $this->assertStringContainsString(config('app.email'), $email);
                $this->assertStringContainsString('Način preuzimanja:', $email);
                $this->assertStringContainsString('Preuzimanje u', $email);
                $this->assertStringContainsString($order->shipping_name, $email);
                $this->assertStringContainsString('Rok za preuzimanje:', $email);
                $this->assertStringContainsString('Način plaćanja:', $email);
                $this->assertStringContainsString('Pouzećem (gotovina, kartično)', $email);
                $this->assertStringContainsString('Podaci o kupcu:', $email);
                $this->assertStringContainsString($order->first_name.' '.$order->last_name, $email);
                $this->assertStringContainsString('Tel: '.$order->phone, $email);
                $this->assertStringContainsString('Email: '.$order->email, $email);
                $this->assertStringContainsString('Broj narudžbe:', $email);
                $this->assertStringContainsString($order->order_number, $email);
                $this->assertStringContainsString('Napomene uz narudžbu:', $email);
                $this->assertStringContainsString($order->additional_info, $email);

                foreach ($order->orderProducts as $orderProduct) {
                    $this->assertStringContainsString($orderProduct->fullName(), $email);
                    $this->assertStringContainsString($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()), $email);
                    $this->assertStringContainsString(formatPrice($orderProduct->quantity * $orderProduct->realPrice()), $email);

                    if ($orderProduct->variants) {
                        $this->assertStringContainsString('Br. '.$orderProduct->variants, $email);
                    }
                }

                $this->assertStringContainsString('Proizvodi', $email);
                $this->assertStringContainsString(formatPrice($order->total_price), $email);
                $this->assertStringContainsString('Dostava', $email);
                $this->assertStringContainsString(formatPrice($order->shipping_price), $email);
                $this->assertStringContainsString('Ukupno', $email);
                $this->assertStringContainsString(formatPrice($order->total_price_with_shipping), $email);

                $this->assertStringContainsString('Hvala,', $email);
                $this->assertStringContainsString('Vaš ApotekaViva24 tim', $email);

                return true;
            }
        );
    }

    public function testOrderSuccessfulNotificationShipping(): void
    {
        Notification::fake();

        $country = Country::factory()->create();
        $shippingMethod = ShippingMethod::factory()->create([
            'pickup' => false,
            'key' => 'asdasd',
        ]);

        // Pickup order
        $order = Order::factory()->create([
            'shipping_method_id' => $shippingMethod->id,
            'shipping_pickup' => $shippingMethod->pickup,
            'shipping_name' => $shippingMethod->key,
            'shipping_price' => $shippingMethod->price,
            'shipping_key' => $shippingMethod->name,
            'email' => 'test@gmail.com',
            'country_id' => $country->id,
            'additional_info' => 'ovo je dodatni text',
        ]);

        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 1.00,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
            'variants' => '34',
        ]);

        $order->notify(new OrderSuccessfulNotification());

        Notification::assertSentTo(
            $order,
            OrderSuccessfulNotification::class,
            function ($notification) use ($order) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Vaša Narudžba ('.$order->order_number.') | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Pozdrav '.$order->first_name.'.', $email);
                $this->assertStringContainsString('Zaprimili smo Vašu narudžbu. Naručeni proizvodi će biti poslani u najkraćem vremenskom roku na Vašu adresu. Sve narudžbe koje su zaprimljene petkom nakon 14h, subotom ili nedjeljom, šalju se sljedeći radni dan tj. u ponedjeljak.', $email);
                $this->assertStringContainsString('Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona', $email);
                $this->assertStringContainsString(config('app.phone_short'), $email);
                $this->assertStringContainsString('ili putem maila na adresu', $email);
                $this->assertStringContainsString(config('app.email'), $email);
                $this->assertStringContainsString('Način preuzimanja:', $email);
                $this->assertStringContainsString('Slanje na adresu', $email);
                $this->assertStringContainsString($order->shipping_name, $email);
                $this->assertStringContainsString('Način plaćanja:', $email);
                $this->assertStringContainsString('Pouzećem (gotovina)', $email);
                $this->assertStringContainsString('Podaci o kupcu:', $email);
                $this->assertStringContainsString($order->first_name.' '.$order->last_name, $email);
                $this->assertStringContainsString($order->street_address, $email);
                $this->assertStringContainsString($order->postalcode.' '.$order->city, $email);
                $this->assertStringContainsString($order->country->name, $email);
                $this->assertStringContainsString('Tel: '.$order->phone, $email);
                $this->assertStringContainsString('Email: '.$order->email, $email);
                $this->assertStringContainsString('Broj narudžbe:', $email);
                $this->assertStringContainsString($order->order_number, $email);
                $this->assertStringContainsString('Napomene uz narudžbu:', $email);
                $this->assertStringContainsString($order->additional_info, $email);

                foreach ($order->orderProducts as $orderProduct) {
                    $this->assertStringContainsString($orderProduct->fullName(), $email);
                    $this->assertStringContainsString($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()), $email);
                    $this->assertStringContainsString(formatPrice($orderProduct->quantity * $orderProduct->realPrice()), $email);

                    if ($orderProduct->variants) {
                        $this->assertStringContainsString('Br. '.$orderProduct->variants, $email);
                    }
                }

                $this->assertStringContainsString('Proizvodi', $email);
                $this->assertStringContainsString(formatPrice($order->total_price), $email);
                $this->assertStringContainsString('Dostava', $email);
                $this->assertStringContainsString(formatPrice($order->shipping_price), $email);
                $this->assertStringContainsString('Ukupno', $email);
                $this->assertStringContainsString(formatPrice($order->total_price_with_shipping), $email);

                $this->assertStringContainsString('Hvala,', $email);
                $this->assertStringContainsString('Vaš ApotekaViva24 tim', $email);

                return true;
            }
        );
    }

    public function testOrderSuccessfulNotificationShippingWithOnlinePayment(): void
    {
        Notification::fake();

        $country = Country::factory()->create();
        $shippingMethod = ShippingMethod::factory()->create([
            'pickup' => false,
            'key' => 'asdasd',
        ]);

        $order = Order::factory()->create([
            'shipping_method_id' => $shippingMethod->id,
            'shipping_pickup' => $shippingMethod->pickup,
            'shipping_name' => $shippingMethod->key,
            'shipping_price' => $shippingMethod->price,
            'shipping_key' => $shippingMethod->name,
            'email' => 'test@gmail.com',
            'country_id' => $country->id,
            'additional_info' => 'ovo je dodatni text',
            'payment_method' => 'online_payment',
        ]);

        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 1.00,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);

        $order->notify(new OrderSuccessfulNotification());

        Notification::assertSentTo(
            $order,
            OrderSuccessfulNotification::class,
            function ($notification) use ($order) {
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Način plaćanja:', $email);
                $this->assertStringContainsString('Kartično', $email);

                return true;
            }
        );
    }

    public function testOrderShippedNotification(): void
    {
        Notification::fake();
        $order = Order::factory()->create(['shipping_pickup' => false]);
        $order->notify(new OrderShippedNotification());

        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 1.00,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
            'variants' => '34',
        ]);

        Notification::assertSentTo(
            $order,
            OrderShippedNotification::class,
            function ($notification) use ($order) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Vaša narudžba je poslana ('.$order->order_number.')', $mailData['subject']);
                $this->assertStringContainsString('Pozdrav '.$order->first_name.'.', $email);
                $this->assertStringContainsString('Dobre vijesti!', $email);
                $this->assertStringContainsString('Vaša narudžba je krenula prema Vama i možete je očekivati u najkraćem vremenskom roku na vratima', $email);

                foreach ($order->orderProducts as $orderProduct) {
                    $this->assertStringContainsString($orderProduct->fullName(), $email);
                    $this->assertStringContainsString($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()), $email);
                    $this->assertStringContainsString(formatPrice($orderProduct->quantity * $orderProduct->realPrice()), $email);

                    if ($orderProduct->variants) {
                        $this->assertStringContainsString('Br. '.$orderProduct->variants, $email);
                    }
                }

                return true;
            }
        );
    }

    public function testOrderPickupReadyNotification(): void
    {
        Notification::fake();
        $order = Order::factory()->create(['shipping_pickup' => true]);
        $order->notify(new OrderPickupReadyNotification());

        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => true,
            'discount_price' => 1.00,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
        ]);
        OrderProduct::factory()->create([
            'order_id' => $order->id,
            'on_sale' => false,
            'discount_price' => null,
            'variants' => '34',
        ]);

        Notification::assertSentTo(
            $order,
            OrderPickupReadyNotification::class,
            function ($notification) use ($order) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Vaša narudžba je spremna za preuzimanje ('.$order->order_number.')', $mailData['subject']);
                $this->assertStringContainsString('Pozdrav '.$order->first_name.'.', $email);
                $this->assertStringContainsString('Dobre vijesti!', $email);
                $this->assertStringContainsString('Vaša narudžba je spremna za preuzimanje u apoteci', $email);
                $this->assertStringContainsString('Rok za preuzimanje je', $email);
                $this->assertStringContainsString('Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći', $email);

                foreach ($order->orderProducts as $orderProduct) {
                    $this->assertStringContainsString($orderProduct->fullName(), $email);
                    $this->assertStringContainsString($orderProduct->quantity.' x '.formatPrice($orderProduct->realPrice()), $email);
                    $this->assertStringContainsString(formatPrice($orderProduct->quantity * $orderProduct->realPrice()), $email);

                    if ($orderProduct->variants) {
                        $this->assertStringContainsString('Br. '.$orderProduct->variants, $email);
                    }
                }

                return true;
            }
        );
    }

    public function testOrderProductReviewCtaNotification(): void
    {
        Notification::fake();
        $order = Order::factory()->create(['status' => 'fulfilled', 'created_at' => now()->subDays(8)]);
        $orderProduct1 = OrderProduct::factory()->create(['order_id' => $order]);
        $orderProduct2 = OrderProduct::factory()->create(['order_id' => $order]);
        $order->notify(new OrderProductReviewCtaNotification());

        Notification::assertSentTo(
            $order,
            OrderProductReviewCtaNotification::class,
            function ($notification) use ($order, $orderProduct1, $orderProduct2) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Da li ste zadovoljni sa naručenim proizvodima? | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Pozdrav '.$order->first_name.'.', $email);
                $this->assertStringContainsString('Da li ste zadovoljni sa naručenim proizvodima?', $email);
                $this->assertStringContainsString('Podijelite Vaša iskustva i ujedno pomozite drugima u odabiru.', $email);
                $this->assertStringContainsString('Vaše mišljenje nam je jako bitno!', $email);
                $this->assertStringContainsString('Dodaj Recenziju', $email);

                // products
                $this->assertStringContainsString(asset('/images/logo_270_contrast.png'), $email);
                $this->assertStringContainsString($orderProduct1->fullName(), $email);
                $this->assertStringContainsString(route('products.review', ['product' => $orderProduct1->product->getSlug()]), $email);
                $this->assertStringContainsString(asset('/images/logo_270_contrast.png'), $email);
                $this->assertStringContainsString($orderProduct2->fullName(), $email);
                $this->assertStringContainsString(route('products.review', ['product' => $orderProduct2->product->getSlug()]), $email);

                return true;
            }
        );
    }

    public function testBackToStockNotification(): void
    {
        Notification::fake();
        $product = Product::factory()->create();
        $backToStockAlert = BackToStockAlert::create([
            'product_id' => $product->id,
            'email' => 'asd@gmail.com',
        ]);

        $backToStockAlert->notify(new BackToStockNotification());

        Notification::assertSentTo(
            $backToStockAlert,
            BackToStockNotification::class,
            function ($notification) use ($backToStockAlert) {
                $mailData = $notification->toMail($backToStockAlert)->toArray();
                $email = $notification->toMail($backToStockAlert)->render();

                $this->assertStringContainsString('Proizvod ponovno dostupan | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Donosimo Vam dobre vijesti.', $email);
                $this->assertStringContainsString('Proizvod koji ste tražili je ponovno dostupan.', $email);
                $this->assertStringContainsString($backToStockAlert->product->fullName(), $email);
                $this->assertStringContainsString('Idi na proizvod', $email);
                $this->assertStringContainsString(route('products.show', ['product' => $backToStockAlert->product->getSlug()]), $email);

                return true;
            }
        );
    }

    public function testThirtyDayNoOrderReminderNotification(): void
    {
        Notification::fake();
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $order = Order::factory()->create();

        $order->notify(new ThirtyDayNoOrderReminderNotification([$product1, $product2]));

        Notification::assertSentTo(
            $order,
            ThirtyDayNoOrderReminderNotification::class,
            function ($notification) use ($product1, $product2, $order) {
                $mailData = $notification->toMail($order)->toArray();
                $email = $notification->toMail($order)->render();

                $this->assertStringContainsString('Ne zaboravite dopuniti Vaše zalihe | ApotekaViva24', $mailData['subject']);
                $this->assertStringContainsString('Prošlo je dosta vremena od Vaše posljednje narudžbe. Vjerovatno ste pri kraju ili ste već potrošili Vaše proizvode te Vas podsjećamo da nadopunite zalihe.', $email);
                $this->assertStringContainsString($product1->brand->name, $email);
                $this->assertStringContainsString($product1->name(), $email);
                $this->assertStringContainsString($product1->package_description, $email);
                $this->assertStringContainsString(route('products.show', ['product' => $product1->getSlug()]), $email);
                $this->assertStringContainsString($product2->brand->name, $email);
                $this->assertStringContainsString($product2->name(), $email);
                $this->assertStringContainsString($product2->package_description, $email);
                $this->assertStringContainsString(route('products.show', ['product' => $product2->getSlug()]), $email);
                $this->assertStringContainsString('Idi na proizvod', $email);

                return true;
            }
        );
    }
}
