<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Cart;
use Viva\GuestCart;
use Viva\Product;
use Viva\User;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testSyncFromGuestCartWhenNoCart(): void
    {
        $user = User::factory()->create();
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $guestCart = GuestCart::factory()->create([
            'shipping_method_id' => 1,
            'product_removed' => 1,
            'product_price_changed' => 1,
            'free_shipping' => 1,
        ]);

        $this->assertDatabaseMissing('carts', [
            'user_id' => $user->id,
        ]);

        $guestCart->products()->attach($product1->id, ['quantity' => 1]);
        $guestCart->products()->attach($product2->id, ['quantity' => 3, 'variants' => '34,35,36']);

        $user->syncFromGuestCart($guestCart->guest_id);
        $user->refresh();

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
            'shipping_method_id' => 1,
            'product_removed' => 1,
            'product_price_changed' => 1,
            'free_shipping' => 1,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
            'quantity' => 1,
            'variants' => null,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
            'variants' => '34,35,36',
        ]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guestCart->id,
        ]);
    }

    public function testSyncFromGuestCartWhenCartExists(): void
    {
        $user = User::factory()->create();
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $product3 = Product::factory()->create();
        $guestCart = GuestCart::factory()->create([
            'shipping_method_id' => 1,
            'product_removed' => 1,
            'product_price_changed' => 1,
            'free_shipping' => 1,
        ]);
        $cart = Cart::factory()->create(['user_id' => $user->id]);

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
        ]);

        $cart->products()->attach($product1->id, ['quantity' => 2, 'variants' => '34,35']);
        $cart->products()->attach($product2->id, ['quantity' => 2]);
        $guestCart->products()->attach($product1->id, ['quantity' => 1, 'variants' => '36']);
        $guestCart->products()->attach($product3->id, ['quantity' => 4]);

        $user->syncFromGuestCart($guestCart->guest_id);
        $user->refresh();

        $this->assertDatabaseHas('carts', [
            'user_id' => $user->id,
            'shipping_method_id' => 1,
            'product_removed' => 1,
            'product_price_changed' => 1,
            'free_shipping' => 1,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product1->id,
            'quantity' => 3,
            'variants' => '34,35,36',
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product2->id,
            'quantity' => 2,
            'variants' => null,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $user->cart->id,
            'product_id' => $product3->id,
            'quantity' => 4,
            'variants' => null,
        ]);
        $this->assertDatabaseMissing('guest_carts', [
            'id' => $guestCart->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $guestCart->id,
        ]);
    }
}
