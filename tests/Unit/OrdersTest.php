<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Viva\Brand;
use Viva\Notifications\OrderPickupReadyNotification;
use Viva\Notifications\OrderShippedNotification;
use Viva\Order;
use Viva\Product;

class OrdersTest extends TestCase
{
    use RefreshDatabase;

    public function testOrderNumberIsCreated(): void
    {
        $order = Order::factory()->create();

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'order_number' => $order->order_number,
        ]);
    }

    public function testTotalPrice(): void
    {
        $brand1 = Brand::factory()->create();
        $brand2 = Brand::factory()->create();
        $product1 = Product::factory()->create([
            'brand_id' => $brand1->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => 5,
        ]);
        $product2 = Product::factory()->create([
            'brand_id' => $brand2->id,
            'on_sale' => false,
            'price' => 20,
            'discount_price' => 10,
        ]);
        $order = Order::factory()->create();

        $order->orderProducts()->create([
            'product_id' => $product1->id,
            'brand_id' => $product1->brand_id,
            'brand_name' => $product1->brand->name,
            'brand_description' => $product1->brand->description,
            'title' => $product1->title,
            'package_description' => $product1->package_description,
            'description' => $product1->description,
            'price' => $product1->price,
            'discount_price' => $product1->discount_price,
            'on_sale' => $product1->on_sale,
            'published' => $product1->published,
            'in_stock' => $product1->in_stock,
            'free_shipping' => $product1->free_shipping,
            'quantity' => 2,
        ]);
        $order->orderProducts()->create([
            'product_id' => $product2->id,
            'brand_id' => $product2->brand_id,
            'brand_name' => $product2->brand->name,
            'brand_description' => $product2->brand->description,
            'title' => $product2->title,
            'package_description' => $product2->package_description,
            'description' => $product2->description,
            'price' => $product2->price,
            'discount_price' => $product2->discount_price,
            'on_sale' => $product2->on_sale,
            'published' => $product2->published,
            'in_stock' => $product2->in_stock,
            'free_shipping' => $product2->free_shipping,
            'quantity' => 3,
        ]);

        $this->assertEquals(70, $order->totalPrice());
    }

    public function testTotalPriceWithShipping(): void
    {
        $brand1 = Brand::factory()->create();
        $brand2 = Brand::factory()->create();
        $product1 = Product::factory()->create([
            'brand_id' => $brand1->id,
            'on_sale' => true,
            'price' => 10,
            'discount_price' => 5,
        ]);
        $product2 = Product::factory()->create([
            'brand_id' => $brand2->id,
            'on_sale' => false,
            'price' => 20,
            'discount_price' => 10,
        ]);
        $order = Order::factory()->create();

        $order->orderProducts()->create([
            'product_id' => $product1->id,
            'brand_id' => $product1->brand_id,
            'brand_name' => $product1->brand->name,
            'brand_description' => $product1->brand->description,
            'title' => $product1->title,
            'package_description' => $product1->package_description,
            'description' => $product1->description,
            'price' => $product1->price,
            'discount_price' => $product1->discount_price,
            'on_sale' => $product1->on_sale,
            'published' => $product1->published,
            'in_stock' => $product1->in_stock,
            'free_shipping' => $product1->free_shipping,
            'quantity' => 2,
        ]);
        $order->orderProducts()->create([
            'product_id' => $product2->id,
            'brand_id' => $product2->brand_id,
            'brand_name' => $product2->brand->name,
            'brand_description' => $product2->brand->description,
            'title' => $product2->title,
            'package_description' => $product2->package_description,
            'description' => $product2->description,
            'price' => $product2->price,
            'discount_price' => $product2->discount_price,
            'on_sale' => $product2->on_sale,
            'published' => $product2->published,
            'in_stock' => $product2->in_stock,
            'free_shipping' => $product2->free_shipping,
            'quantity' => 3,
        ]);

        $this->assertEquals(75, $order->totalPriceWithShipping());

        $order->shipping_price = 8;
        $order->save();
        $order->refresh();

        $this->assertEquals(78, $order->totalPriceWithShipping());
    }

    public function testUserReceivesNotificationWhenStatusIsChangedToShipped(): void
    {
        Notification::fake();
        $order = Order::factory()->create([
            'shipping_pickup' => false,
            'status' => 'created',
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => false,
            'pickup_notification_sent' => false,
        ]);

        $order->status = 'shipped';
        $order->save();

        Notification::assertSentTo($order, OrderShippedNotification::class);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => true,
            'pickup_notification_sent' => false,
        ]);

        $order->status = 'created';
        $order->save();
        $order->status = 'shipped';
        $order->save();

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => true,
            'pickup_notification_sent' => false,
        ]);
    }

    public function testUserReceivesNotificationWhenStatusIsChangedToReady(): void
    {
        Notification::fake();
        $order = Order::factory()->create([
            'shipping_pickup' => true,
            'status' => 'created',
        ]);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => false,
            'pickup_notification_sent' => false,
        ]);

        $order->status = 'ready_for_pickup';
        $order->save();

        Notification::assertSentTo($order, OrderPickupReadyNotification::class);

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => false,
            'pickup_notification_sent' => true,
        ]);

        $order->status = 'created';
        $order->save();
        $order->status = 'ready_for_pickup';
        $order->save();

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'shipped_notification_sent' => false,
            'pickup_notification_sent' => true,
        ]);
    }
}
