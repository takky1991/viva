<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Article;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    public function testGetSlugs(): void
    {
        $article = Article::factory()->create(['title' => 'first name']);

        $this->assertDatabaseHas('article_slugs', [
            'article_id' => $article->id,
            'slug' => 'first-name',
        ]);

        sleep(1);

        $article->title = 'second name';
        $article->save();

        $this->assertDatabaseHas('article_slugs', [
            'article_id' => $article->id,
            'slug' => 'second-name',
        ]);

        $this->assertEquals(['second-name', 'first-name'], $article->slugs()->pluck('slug')->toArray());
    }

    public function testGetSlug(): void
    {
        $article = Article::factory()->create(['title' => 'first name']);
        sleep(1);
        $article->title = 'second name';
        $article->save();

        $this->assertEquals('second-name', $article->getSlug());
    }
}
