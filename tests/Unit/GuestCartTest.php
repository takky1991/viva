<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\GuestCart;
use Viva\Product;
use Viva\ShippingMethod;

class GuestCartTest extends TestCase
{
    use RefreshDatabase;

    public function testProductsRelationship(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart = GuestCart::factory()->create();

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product1->id,
            'quantity' => 1,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $cart->products()->detach($product1->id);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product1->id,
        ]);

        $cart->products()->detach($product2->id);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product2->id,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product1->id,
            'quantity' => 1,
        ]);

        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $cart->delete();
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart->id,
            'product_id' => $product2->id,
        ]);
    }

    public function testSetExiresAt(): void
    {
        $gustCart = GuestCart::factory()->create();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $gustCart->id,
            'guest_id' => $gustCart->guest_id,
            'expires_at' => now()->addMinutes(config('app.guest_cart_lifetime')),
        ]);
    }

    public function testTotalPrice(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart = GuestCart::factory()->create();

        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertEquals(70, $cart->totalPrice());
    }

    public function testTotalPriceWithShipping(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 15, 'discount_price' => 10]);
        $cart = GuestCart::factory()->create();

        $cart->products()->attach($product1->id, ['quantity' => 2]); // 10
        $cart->products()->attach($product2->id, ['quantity' => 3]); // 45

        $this->assertEquals(55, $cart->totalPriceWithShipping());

        $shippingMethod = ShippingMethod::factory()->create();
        $shippingPrice = $shippingMethod->price;

        $cart->shipping_method_id = $shippingMethod->id;
        $cart->save();
        $cart->refresh();

        $this->assertEquals(55 + $shippingPrice, $cart->totalPriceWithShipping());
    }

    public function testProductRemovedOnStockChange(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart1 = GuestCart::factory()->create();
        $cart2 = GuestCart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);

        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);

        $product1->in_stock = false;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('guest_cart_product', [
            'guest_cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseMissing('guest_cart_product', [
            'guest_cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);
    }

    public function testProductPriceChangedOnSaleChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = GuestCart::factory()->create();
        $cart2 = GuestCart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->on_sale = false;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 1,
        ]);

        // Reset product_price_changed for another test
        $cart1->product_price_changed = false;
        $cart1->save();
        $cart2->product_price_changed = false;
        $cart2->save();
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->on_sale = true;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);
    }

    public function testProductPriceChangedOnSalePriceChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = GuestCart::factory()->create();
        $cart2 = GuestCart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->discount_price = 9;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->discount_price = 4;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 1,
        ]);
    }

    public function testProductPriceChangedOnPriceChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = GuestCart::factory()->create();
        $cart2 = GuestCart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->price = 9;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->price = 19;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);
    }

    public function testGuestCartFreeShippingOnProductAddWithFreeShipping(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product = Product::factory()->create(['price' => 5, 'on_sale' => false]);
        $freeShippingProduct = Product::factory()->create(['free_shipping' => true]);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->attach($product->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->attach($freeShippingProduct->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);
    }

    public function testGuestCartFreeShippingOnProductRemoveWithFreeShipping(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create(['price' => 5, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 5, 'on_sale' => false]);
        $freeShippingProduct = Product::factory()->create(['free_shipping' => true]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($freeShippingProduct->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $guestCart->products()->detach($product1->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $guestCart->products()->detach($freeShippingProduct->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);
    }

    public function testGuestCartFreeShippingOnTotalPrice(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create(['price' => 10, 'discount_price' => 5, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 50, 'discount_price' => 40, 'on_sale' => true]);
        $product3 = Product::factory()->create(['price' => 25, 'discount_price' => null, 'on_sale' => false]);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->attach($product1->id);
        $this->assertDatabaseHas('guest_carts', [ // 10
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->updateExistingPivot($product1->id, ['quantity' => 11]);
        $this->assertDatabaseHas('guest_carts', [ // 110
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $guestCart->products()->updateExistingPivot($product1->id, ['quantity' => 1]);
        $this->assertDatabaseHas('guest_carts', [ // 10
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->attach($product2->id);
        $this->assertDatabaseHas('guest_carts', [ // 50
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart->products()->attach($product3->id);
        $this->assertDatabaseHas('guest_carts', [ // 75
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $guestCart->products()->detach($product1->id);
        $this->assertDatabaseHas('guest_carts', [ // 65
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);
    }

    public function testGuestCartFreeShippingOnProductFreeShippingChangeAndNoThreshold(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $product3 = Product::factory()->create(['price' => 10, 'on_sale' => false]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($product3->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $product1->free_shipping = true;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = true;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = false;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = false;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnProductFreeShippingChangeAndWithThreshold(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create(['price' => 20, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 40, 'on_sale' => false]);
        $product3 = Product::factory()->create(['price' => 80, 'on_sale' => false]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($product3->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = true;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = true;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $guestCart->products()->detach($product3->id);
        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = false;
        $product1->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = false;
        $product2->save();

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);
    }

    public function testGuestCartFreeShippingOnProductInStockChange(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create(['free_shipping' => true]);
        $product2 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $product3 = Product::factory()->create(['price' => 10, 'on_sale' => false]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($product3->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->in_stock = false;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product1->in_stock = false;
        $product1->save();
        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 20,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 40,
            'discount_price' => 20,
            'on_sale' => true,
        ]);
        $product3 = Product::factory()->create([
            'price' => 30,
            'discount_price' => 10,
            'on_sale' => false,
        ]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($product3->id);

        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->in_stock = false;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);
    }

    public function testGuestCartFreeShippingOnProductPriceChange(): void
    {
        $guestCart = GuestCart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 20,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 40,
            'discount_price' => 20,
            'on_sale' => true,
        ]);
        $product3 = Product::factory()->create([
            'price' => 30,
            'discount_price' => 10,
            'on_sale' => false,
        ]);

        $guestCart->products()->attach($product1->id);
        $guestCart->products()->attach($product2->id);
        $guestCart->products()->attach($product3->id);

        $this->assertDatabaseHas('guest_carts', [ // 70
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product3->on_sale = true;
        $product3->save();
        $this->assertDatabaseHas('guest_carts', [ // 50
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $product2->price = 100;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [ // 50
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $product2->on_sale = false;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [ // 70
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);

        $product2->price = 25;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [ // 55
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $product2->price = 50;
        $product2->on_sale = true;
        $product2->discount_price = 10;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [ // 65
            'id' => $guestCart->id,
            'free_shipping' => false,
        ]);

        $product2->discount_price = 40;
        $product2->save();
        $this->assertDatabaseHas('guest_carts', [ // 105
            'id' => $guestCart->id,
            'free_shipping' => true,
        ]);
    }
}
