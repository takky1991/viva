<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\DiscountCollection;
use Viva\Product;

class DiscountCollectionTest extends TestCase
{
    use RefreshDatabase;

    public function testDisableDiscountOnProductsWhenDiscountCollectionIsDeleted(): void
    {
        $product1 = Product::factory()->create(['id' => 1, 'price' => 22.00, 'discount_price' => 17.6, 'on_sale' => true]);
        $product2 = Product::factory()->create(['id' => 2, 'price' => 32.55, 'discount_price' => 26.05, 'on_sale' => true]);
        $product3 = Product::factory()->create(['id' => 3, 'price' => 85.25, 'discount_price' => 50, 'on_sale' => true]);

        $discoutCollection = DiscountCollection::factory()->create([
            'active' => false,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);

        $discoutCollection->delete();

        $this->assertDatabaseHas('products', [
            'id' => 1, 'price' => 22.00, 'discount_price' => 17.6, 'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2, 'price' => 32.55, 'discount_price' => 26.05, 'on_sale' => true,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3, 'price' => 85.25, 'discount_price' => 50, 'on_sale' => true,
        ]);

        $discoutCollection = DiscountCollection::factory()->create([
            'active' => true,
            'discount_percentage' => 20,
            'product_ids' => '[1,2]',
        ]);

        $discoutCollection->delete();

        $this->assertDatabaseHas('products', [
            'id' => 1, 'price' => 22.00, 'discount_price' => 17.6, 'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 2, 'price' => 32.55, 'discount_price' => 26.05, 'on_sale' => false,
        ]);
        $this->assertDatabaseHas('products', [
            'id' => 3, 'price' => 85.25, 'discount_price' => 50, 'on_sale' => true,
        ]);
    }

    public function testGetSlugs(): void
    {
        $discoutCollection = DiscountCollection::factory()->create(['name' => 'first name']);

        $this->assertDatabaseHas('discount_collection_slugs', [
            'discount_collection_id' => $discoutCollection->id,
            'slug' => 'first-name',
        ]);

        sleep(1);

        $discoutCollection->name = 'second name';
        $discoutCollection->save();

        $this->assertDatabaseHas('discount_collection_slugs', [
            'discount_collection_id' => $discoutCollection->id,
            'slug' => 'second-name',
        ]);

        $this->assertEquals(['second-name', 'first-name'], $discoutCollection->slugs()->pluck('slug')->toArray());
    }

    public function testGetSlug(): void
    {
        $discoutCollection = DiscountCollection::factory()->create(['name' => 'first name']);
        sleep(1);
        $discoutCollection->name = 'second name';
        $discoutCollection->save();

        $this->assertEquals('second-name', $discoutCollection->getSlug());
    }
}
