<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Category;
use Viva\Product;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function testGetParent(): void
    {
        $parentCategory = Category::factory()->create();
        $category = Category::factory()->create(['parent_id' => $parentCategory->id]);

        $this->assertEquals($parentCategory->id, $category->parent->id);
    }

    public function testGetChildren(): void
    {
        $parentCategory = Category::factory()->create();
        $category1 = Category::factory()->create(['parent_id' => $parentCategory->id]);
        $category2 = Category::factory()->create(['parent_id' => $parentCategory->id]);
        $category3 = Category::factory()->create(['parent_id' => $parentCategory->id]);

        $this->assertEquals([$category1->id, $category2->id, $category3->id], $parentCategory->children()->pluck('id')->toArray());
    }

    public function testGetProducts(): void
    {
        $category = Category::factory()->create();

        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $product3 = Product::factory()->create();
        $product4 = Product::factory()->create(['deleted_at' => now()]);

        $category->products()->attach([$product1->id, $product2->id, $product3->id, $product4->id]);

        $this->assertEquals([$product1->id, $product2->id, $product3->id], $category->products->pluck('id')->toArray());
    }

    public function testGetArchivedProducts(): void
    {
        $category = Category::factory()->create();
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $product3 = Product::factory()->create();
        $product4 = Product::factory()->create(['deleted_at' => now()]);

        $category->products()->attach([$product1->id, $product2->id, $product3->id, $product4->id]);

        $this->assertEquals([$product1->id, $product2->id, $product3->id, $product4->id], $category->productsWithArchived->pluck('id')->toArray());
    }

    public function testPublishedProducts(): void
    {
        $category = Category::factory()->create();
        $product1 = Product::factory()->create(['published' => true]);
        $product2 = Product::factory()->create(['published' => false]);
        $product3 = Product::factory()->create(['published' => true]);
        $product4 = Product::factory()->create(['deleted_at' => now()]);

        $category->products()->attach([$product1->id, $product2->id, $product3->id, $product4->id]);

        $this->assertEquals([$product1->id, $product3->id], $category->publishedProducts->pluck('id')->toArray());
    }

    public function testUnpublishedProducts(): void
    {
        $category = Category::factory()->create();
        $product1 = Product::factory()->create(['published' => true]);
        $product2 = Product::factory()->create(['published' => false]);
        $product3 = Product::factory()->create(['published' => true]);
        $product4 = Product::factory()->create(['deleted_at' => now()]);

        $category->products()->attach([$product1->id, $product2->id, $product3->id, $product4->id]);

        $this->assertEquals([$product2->id], $category->unpublishedProducts->pluck('id')->toArray());
    }

    public function testGetSlugs(): void
    {
        $category = Category::factory()->create(['title' => 'first name']);

        $this->assertDatabaseHas('category_slugs', [
            'category_id' => $category->id,
            'slug' => 'first-name',
        ]);

        sleep(1);

        $category->title = 'second name';
        $category->save();

        $this->assertDatabaseHas('category_slugs', [
            'category_id' => $category->id,
            'slug' => 'second-name',
        ]);

        $this->assertEquals(['second-name', 'first-name'], $category->slugs()->pluck('slug')->toArray());
    }

    public function testGetSlug(): void
    {
        $category = Category::factory()->create(['title' => 'first name']);
        sleep(1);
        $category->title = 'second name';
        $category->save();

        $this->assertEquals('second-name', $category->getSlug());
    }

    public function testWhereNoParentScope(): void
    {
        $parentCategory = Category::factory()->create();
        $category1 = Category::factory()->create(['parent_id' => $parentCategory->id]);
        $category2 = Category::factory()->create(['parent_id' => $parentCategory->id]);

        $this->assertEquals([$parentCategory->id], Category::whereNoParent()->get()->pluck('id')->toArray());
    }

    public function testUpdateChildrenIds(): void
    {
        $category1 = Category::factory()->create();
        $category11 = Category::factory()->create(['parent_id' => $category1->id]);
        $category111 = Category::factory()->create(['parent_id' => $category11->id]);

        $category1->refresh();
        $this->assertEquals([$category11->id, $category111->id], $category1->getAllNestedChildrenIds());

        $category12 = Category::factory()->create(['parent_id' => $category1->id]);
        $category121 = Category::factory()->create(['parent_id' => $category12->id]);

        $category1->refresh();
        $this->assertEquals([$category11->id, $category111->id, $category12->id, $category121->id], $category1->getAllNestedChildrenIds());

        $category111->delete();
        $category1->refresh();
        $this->assertEquals([$category11->id, $category12->id, $category121->id], $category1->getAllNestedChildrenIds());
    }

    public function testUpdatingCategoryChangesCategoriesHierarchyForProducts(): void
    {
        $category1 = Category::factory()->create(['title' => 'Preparati', 'menu' => true]);
        $category11 = Category::factory()->create(['title' => 'Kozmetika', 'parent_id' => $category1->id, 'menu' => true]);
        $category111 = Category::factory()->create(['title' => 'Masna Koza', 'parent_id' => $category11->id, 'menu' => true]);

        $product1 = Product::factory()->create();
        $product1->categories()->sync([$category1->id, $category11->id, $category111->id]);

        $product2 = Product::factory()->create();
        $product2->categories()->sync([$category1->id, $category11->id, $category111->id]);

        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'categories_hierarchy' => '',
        ]);

        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'categories_hierarchy' => '',
        ]);

        $product1->save();
        $product2->save();

        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);

        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);

        $category111->parent_id = null;
        $category111->save();

        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'categories_hierarchy' => 'Preparati/Kozmetika',
        ]);

        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'categories_hierarchy' => 'Preparati/Kozmetika',
        ]);

        $category111->parent_id = $category11->id;
        $category111->save();

        $this->assertDatabaseHas('products', [
            'id' => $product1->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);

        $this->assertDatabaseHas('products', [
            'id' => $product2->id,
            'categories_hierarchy' => 'Preparati/Kozmetika/Masna Koza',
        ]);
    }
}
