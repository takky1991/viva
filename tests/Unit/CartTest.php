<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Cart;
use Viva\Product;
use Viva\ShippingMethod;

class CartTest extends TestCase
{
    use RefreshDatabase;

    public function testProductsRelationship(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart = Cart::factory()->create();

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product1->id,
            'quantity' => 1,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $cart->products()->detach($product1->id);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product1->id,
        ]);

        $cart->products()->detach($product2->id);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product2->id,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product1->id,
            'quantity' => 1,
        ]);

        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product2->id,
            'quantity' => 3,
        ]);

        $cart->delete();
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart->id,
            'product_id' => $product2->id,
        ]);
    }

    public function testTotalPrice(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart = Cart::factory()->create();

        $cart->products()->attach($product1->id, ['quantity' => 2]);
        $cart->products()->attach($product2->id, ['quantity' => 3]);

        $this->assertEquals(70, $cart->totalPrice());
    }

    public function testTotalPriceWithShipping(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 15, 'discount_price' => 10]);
        $cart = Cart::factory()->create();

        $cart->products()->attach($product1->id, ['quantity' => 2]); // 10
        $cart->products()->attach($product2->id, ['quantity' => 3]); // 45

        $this->assertEquals(55, $cart->totalPriceWithShipping());

        $shippingMethod = ShippingMethod::factory()->create();
        $shippingPrice = $shippingMethod->price;

        $cart->shipping_method_id = $shippingMethod->id;
        $cart->save();
        $cart->refresh();

        $this->assertEquals(55 + $shippingPrice, $cart->totalPriceWithShipping());
    }

    public function testProductRemovedOnStockChange(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart1 = Cart::factory()->create();
        $cart2 = Cart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);

        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);

        $product1->in_stock = false;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);
    }

    public function testProductPriceChangedOnSaleChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = Cart::factory()->create();
        $cart2 = Cart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->on_sale = false;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 1,
        ]);

        // Reset product_price_changed for another test
        $cart1->product_price_changed = false;
        $cart1->save();
        $cart2->product_price_changed = false;
        $cart2->save();
        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->on_sale = true;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);
    }

    public function testCartProductPriceChangedOnSalePriceChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = Cart::factory()->create();
        $cart2 = Cart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->discount_price = 9;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->discount_price = 4;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 1,
        ]);
    }

    public function testCartProductPriceChangedOnPriceChange(): void
    {
        $product1 = Product::factory()->create(['on_sale' => true, 'price' => 10, 'discount_price' => 5]);
        $product2 = Product::factory()->create(['on_sale' => false, 'price' => 20, 'discount_price' => 10]);
        $cart1 = Cart::factory()->create();
        $cart2 = Cart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product1->price = 9;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);

        $product2->price = 19;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_price_changed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_price_changed' => 0,
        ]);
    }

    public function testCartFreeShippingOnProductAddWithFreeShipping(): void
    {
        $cart = Cart::factory()->create();
        $product = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $freeShippingProduct = Product::factory()->create(['free_shipping' => true]);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($product->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($freeShippingProduct->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);
    }

    public function testCartFreeShippingOnProductRemoveWithFreeShipping(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $freeShippingProduct = Product::factory()->create(['free_shipping' => true]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($freeShippingProduct->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $cart->products()->detach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $cart->products()->detach($freeShippingProduct->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnTotalPrice(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create(['price' => 10, 'discount_price' => 5, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 50, 'discount_price' => 40, 'on_sale' => true]);
        $product3 = Product::factory()->create(['price' => 24, 'discount_price' => null, 'on_sale' => false]);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($product1->id);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->updateExistingPivot($product1->id, ['quantity' => 11]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $cart->products()->updateExistingPivot($product1->id, ['quantity' => 1]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($product2->id);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($product3->id);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $cart->products()->detach($product1->id);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnProductFreeShippingChangeAndNoThreshold(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 10,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 10,
            'on_sale' => false,
        ]);
        $product3 = Product::factory()->create([
            'price' => 10,
            'on_sale' => false,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $product1->free_shipping = true;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = true;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = false;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = false;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnProductFreeShippingChangeAndWithThreshold(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create(['price' => 20, 'on_sale' => false]);
        $product2 = Product::factory()->create(['price' => 40, 'on_sale' => false]);
        $product3 = Product::factory()->create(['price' => 80, 'on_sale' => false]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = true;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = true;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $cart->products()->detach($product3->id);
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product1->free_shipping = false;
        $product1->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->free_shipping = false;
        $product2->save();

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnProductInStockChange(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create(['free_shipping' => true]);
        $product2 = Product::factory()->create(['price' => 10, 'on_sale' => false]);
        $product3 = Product::factory()->create(['price' => 10, 'on_sale' => false]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->in_stock = false;
        $product2->save();
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product1->in_stock = false;
        $product1->save();
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 30,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 40,
            'discount_price' => 31,
            'on_sale' => true,
        ]);
        $product3 = Product::factory()->create([
            'price' => 30,
            'discount_price' => 10,
            'on_sale' => false,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->in_stock = false;
        $product2->save();
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testCartFreeShippingOnProductPriceChange(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 20,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 40,
            'discount_price' => 20,
            'on_sale' => true,
        ]);
        $product3 = Product::factory()->create([
            'price' => 30,
            'discount_price' => 10,
            'on_sale' => false,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [ // 70
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product3->on_sale = true;
        $product3->save();
        $this->assertDatabaseHas('carts', [ // 50
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $product2->price = 100;
        $product2->save();
        $this->assertDatabaseHas('carts', [ // 50
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $product2->on_sale = false;
        $product2->save();
        $this->assertDatabaseHas('carts', [ // 150
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->price = 15;
        $product2->save();
        $this->assertDatabaseHas('carts', [ // 65
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $product2->price = 50;
        $product2->on_sale = true;
        $product2->discount_price = 10;
        $product2->save();
        $this->assertDatabaseHas('carts', [ // 55
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $product2->discount_price = 40;
        $product2->save();
        $this->assertDatabaseHas('carts', [ // 95
            'id' => $cart->id,
            'free_shipping' => true,
        ]);
    }

    public function testCartFreeShippingOnProductDelete(): void
    {
        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 5,
            'discount_price' => null,
            'on_sale' => false,
            'free_shipping' => true,
        ]);
        $product2 = Product::factory()->create([
            'price' => 5,
            'discount_price' => null,
            'on_sale' => false,
            'free_shipping' => false,
        ]);
        $product3 = Product::factory()->create([
            'price' => 5,
            'discount_price' => null,
            'on_sale' => false,
            'free_shipping' => false,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [ // 15
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->delete();
        $this->assertDatabaseHas('carts', [ // 10
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product1->delete();
        $this->assertDatabaseHas('carts', [ // 5
            'id' => $cart->id,
            'free_shipping' => false,
        ]);

        $cart = Cart::factory()->create();
        $product1 = Product::factory()->create([
            'price' => 20,
            'discount_price' => null,
            'on_sale' => false,
        ]);
        $product2 = Product::factory()->create([
            'price' => 40,
            'discount_price' => 20,
            'on_sale' => true,
        ]);
        $product3 = Product::factory()->create([
            'price' => 30,
            'discount_price' => 10,
            'on_sale' => false,
        ]);

        $cart->products()->attach($product1->id);
        $cart->products()->attach($product2->id);
        $cart->products()->attach($product3->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => true,
        ]);

        $product2->delete();
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'free_shipping' => false,
        ]);
    }

    public function testProductRemovedOnProductDelete(): void
    {
        $product1 = Product::factory()->create();
        $product2 = Product::factory()->create();
        $cart1 = Cart::factory()->create();
        $cart2 = Cart::factory()->create();

        $cart1->products()->attach($product1->id);
        $cart1->products()->attach($product2->id);
        $cart2->products()->attach($product1->id);

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_removed' => 0,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);

        $product1->delete();

        $this->assertDatabaseHas('carts', [
            'id' => $cart1->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseHas('carts', [
            'id' => $cart2->id,
            'product_removed' => 1,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product1->id,
        ]);
        $this->assertDatabaseHas('cart_product', [
            'cart_id' => $cart1->id,
            'product_id' => $product2->id,
        ]);
        $this->assertDatabaseMissing('cart_product', [
            'cart_id' => $cart2->id,
            'product_id' => $product1->id,
        ]);
    }
}
