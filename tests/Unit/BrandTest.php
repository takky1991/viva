<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Viva\Brand;

class BrandTest extends TestCase
{
    use RefreshDatabase;

    public function testGetSlugs(): void
    {
        $brand = Brand::factory()->create(['name' => 'first name']);

        $this->assertDatabaseHas('brand_slugs', [
            'brand_id' => $brand->id,
            'slug' => 'first-name',
        ]);

        sleep(1);

        $brand->name = 'second name';
        $brand->save();

        $this->assertDatabaseHas('brand_slugs', [
            'brand_id' => $brand->id,
            'slug' => 'second-name',
        ]);

        $this->assertEquals(['second-name', 'first-name'], $brand->slugs()->pluck('slug')->toArray());
    }

    public function testGetSlug(): void
    {
        $brand = Brand::factory()->create(['name' => 'first name']);
        sleep(1);
        $brand->name = 'second name';
        $brand->save();

        $this->assertEquals('second-name', $brand->getSlug());
    }
}
