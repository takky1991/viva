<?php

namespace Viva\Listeners;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Mime\Part\DataPart;
use Illuminate\Mail\Events\MessageSending;

class LogSendingMessage
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MessageSending $event): void
    {
        $message = $event->message;

		DB::table('email_log')->insert([
			'date' => Carbon::now()->format('Y-m-d H:i:s'),
			'from' => $this->formatAddressField($message, 'From'),
			'to' => $this->formatAddressField($message, 'To'),
			'cc' => $this->formatAddressField($message, 'Cc'),
			'bcc' => $this->formatAddressField($message, 'Bcc'),
			'subject' => $message->getSubject(),
			'body' => $message->getBody()->bodyToString(),
			'headers' => $message->getHeaders()->toString(),
			'attachments' => $this->saveAttachments($message),
		]);
    }

    /**
	 * Format address strings for sender, to, cc, bcc.
	 */
	function formatAddressField($message, string $field): ?string
	{
		$headers = $message->getHeaders();

		return $headers->get($field)?->getBodyAsString();
	}

    /**
	 * Collect all attachments and format them as strings.
	 */
	protected function saveAttachments($message): ?string
	{
		if (empty($message->getAttachments())) {
			return null;
		}

		return collect($message->getAttachments())
			->map(fn(DataPart $part) => $part->toString())
			->implode("\n\n");
	}
}
