<?php

namespace Viva\Listeners;

use Illuminate\Support\Facades\Notification;
use Viva\Notifications\Admin\NewReviewNotification;
use Viva\User;

class ReviewSubmittedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $review = $event->review;

        // Notification to admins
        $admins = User::role('admin')->get();
        Notification::send($admins, new NewReviewNotification($review));
    }
}
