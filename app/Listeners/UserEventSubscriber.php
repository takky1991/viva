<?php

namespace Viva\Listeners;

use Illuminate\Events\Dispatcher;
use Viva\NewsletterContact;

class UserEventSubscriber
{
    /**
     * Handle user register events.
     */
    public function handleUserRegister($event)
    {
        // Assign role
        $event->user->assignRole('customer');

        // Sync guest cart
        if (session()->has('guest_id')) {
            $event->user->syncFromGuestCart(session('guest_id'));
        }

        // Add user to newsletter contacts
        $newsletterContant = NewsletterContact::where('email', $event->user->email)->first();
        if ($newsletterContant) {
            $newsletterContant->update([
                'user_id' => $event->user->id,
                'first_name' => null,
                'last_name' => null,
            ]);
        } else {
            NewsletterContact::create([
                'user_id' => $event->user->id,
                'email' => $event->user->email,
                'token' => random_token(),
            ]);
        }
    }

    /**
     * Handle user login events.
     */
    public function handleUserLogin($event)
    {
        // Update last login
        $event->user->last_login = now();
        $event->user->save();

        // Sync gurst art
        if (session()->has('guest_id')) {
            $event->user->syncFromGuestCart(session('guest_id'));
        }
    }

    /**
     * Handle user logout events.
     */
    public function handleUserLogout($event)
    {
        //
    }

    /**
     * Register the listeners for the subscriber.
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'Viva\Listeners\UserEventSubscriber@handleUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'Viva\Listeners\UserEventSubscriber@handleUserLogout'
        );

        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'Viva\Listeners\UserEventSubscriber@handleUserRegister'
        );
    }
}
