<?php

namespace Viva\Listeners;

use Illuminate\Support\Facades\Notification;
use Viva\Notifications\Admin\NewOrderNotification;
use Viva\Notifications\OrderSuccessfulNotification;
use Viva\User;

class OrderSuccessfulListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $order = $event->order;

        if (! empty($order->email)) {
            // Notification to email
            $order->notify(new OrderSuccessfulNotification);
        }

        // Notification to admins
        $admins = User::role('admin')->get();
        Notification::send($admins, new NewOrderNotification($order));
    }
}
