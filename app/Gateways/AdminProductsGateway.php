<?php

namespace Viva\Gateways;

use Viva\Interfaces\GatewayInterface;
use Viva\Product;

class AdminProductsGateway implements GatewayInterface
{
    protected $products;

    protected $publicFilters = [
        'onSale',
        'searchTerm',
        'archived',
        'brand',
        'inStock',
        'published',
        'category',
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 20;

    public function __construct()
    {
        $this->products = Product::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $strictMode = false)
    {
        // If strinctMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method.
        if ($strictMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->products;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        //
    }

    public function onSale($input)
    {
        if ($input == 'true') {
            $this->products->where('on_sale', true);
        } elseif ($input == 'false') {
            $this->products->where('on_sale', false);
        }
    }

    public function searchTerm($input)
    {
        if (! empty($input)) {
            $this->products->where('title', 'like', '%'.$input.'%');
        }
    }

    public function archived($input)
    {
        if ($input == 'true') {
            $this->products->onlyTrashed();
        }
    }

    public function inStock($input)
    {
        if ($input == 'true') {
            $this->products->where('in_stock', true);
        } elseif ($input == 'false') {
            $this->products->where('in_stock', false);
        }
    }

    public function published($input)
    {
        if ($input == 'true') {
            $this->products->where('published', true);
        } elseif ($input == 'false') {
            $this->products->where('published', false);
        }
    }

    public function brand($input)
    {
        if (! empty($input)) {
            $this->products->where('brand_id', $input);
        }
    }

    public function category($input)
    {
        if (! empty($input)) {
            $this->products->whereHas('categories', function ($q) use ($input) {
                return $q->where('categories.id', $input);
            });
        }
    }
}
