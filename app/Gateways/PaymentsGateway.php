<?php

namespace Viva\Gateways;

use Carbon\Carbon;
use Viva\Interfaces\GatewayInterface;
use Viva\Payment;

class PaymentsGateway implements GatewayInterface
{
    protected $payments;

    protected $publicFilters = [
        'supplier',
        'paid',
        'payUntil',
        'deliveredAtFrom',
        'deliveredAtTo',
        'invoiceId',
        'discount'
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 30;

    public function __construct()
    {
        $this->payments = Payment::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $strictMode = false)
    {
        // If strinctMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method.
        if ($strictMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->payments;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        // Sort payments by "created_at" by default.
        if (! array_key_exists('sortBy', $input)) {
            $this->payments->orderBy('created_at', 'desc');
        }
    }

    public function supplier($value)
    {
        if ($value != null && $value != '') {
            return $this->payments->where('supplier_id', $value);
        }
    }

    public function unpaid($input)
    {
        if ($input) {
            $this->payments->where('paid', false);
        }
    }

    public function paid($input)
    {
        if ($input == 'true') {
            $this->payments->where('paid', true);
        } elseif ($input == 'false') {
            $this->payments->where('paid', false);
        }
    }

    public function payUntil($input)
    {
        if (! empty($input)) {
            $date = Carbon::parse($input);
            $this->payments->where('pay_until', '<=', $date);
        }
    }

    public function deliveredAtFrom($input)
    {
        if (! empty($input)) {
            $date = Carbon::parse($input);
            $this->payments->where('delivered_at', '>=', $date);
        }
    }

    public function deliveredAtTo($input)
    {
        if (! empty($input)) {
            $date = Carbon::parse($input);
            $this->payments->where('delivered_at', '<=', $date);
        }
    }

    public function invoiceId($input)
    {
        $this->payments->where('invoice_id', 'like', $input . '%');
    }

    public function discount($input)
    {
        if($input === 'true') {
            $this->payments->whereNotNull('discount');
        } else {
            $this->payments->whereNull('discount');
        }
    }
}
