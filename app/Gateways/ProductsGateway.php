<?php

namespace Viva\Gateways;

use Viva\Interfaces\GatewayInterface;
use Viva\Product;

class ProductsGateway implements GatewayInterface
{
    protected $products;

    protected $publicFilters = [
        'onSale',
        'inStock',
        'searchTerm',
        'category',
        'bestSellerInCategory',
        'bestSellerInBrand',
        'bestSeller',
        'brandIds',
        'productIds',
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 20;

    public function __construct()
    {
        $this->products = Product::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $publicMode = false)
    {
        // If publicMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method without validating.
        if ($publicMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->products;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        // Only published products
        $this->products->where('published', true);

        // Sort products by "buy_count" and "in_stock" by default.
        if (! array_key_exists('sortBy', $input)) {
            $this->products
                ->orderBy('in_stock', 'desc')
                ->orderBy('rating_count', 'desc')
                ->orderBy('rating', 'desc')
                ->orderBy('buy_count', 'desc');
        }
    }

    public function onSale($input)
    {
        if ($input == 'true') {
            $this->products->where('on_sale', true);
        } elseif ($input == 'false') {
            $this->products->where('on_sale', false);
        }
    }

    public function searchTerm($input)
    {
        if (! empty($input)) {
            $this->products->where('title', 'like', $input.'%');
        }
    }

    public function inStock($input)
    {
        if ($input == 'true') {
            $this->products->where('in_stock', true);
        } elseif ($input == 'false') {
            $this->products->where('in_stock', false);
        }
    }

    public function category($input)
    {
        if (! empty($input)) {
            $this->products
                ->whereHas('categories', function ($query) use ($input) {
                    $query->where('categories.id', $input);
                });
        }
    }

    public function bestSellerInCategory($input)
    {
        if ($input == 'true') {
            $this->products->where('best_seller_in_category', true);
        } elseif ($input == 'false') {
            $this->products->where('best_seller_in_category', false);
        }
    }

    public function bestSellerInBrand($input)
    {
        if ($input == 'true') {
            $this->products->where('best_seller_in_brand', true);
        } elseif ($input == 'false') {
            $this->products->where('best_seller_in_brand', false);
        }
    }

    public function bestSeller($input)
    {
        if ($input == 'true') {
            $this->products->where(function ($query) {
                $query->where('best_seller_in_brand', true)
                    ->orWhere('best_seller_in_category', true);
            });
        } elseif ($input == 'false') {
            $this->products
                ->where('best_seller_in_brand', false)
                ->where('best_seller_in_category', false);
        }
    }

    public function brandIds($input)
    {
        $brandIds = explode(',', $input);

        $this->products->whereIn('brand_id', $brandIds);
    }

    public function productIds($input)
    {
        $productIds = explode(',', $input);

        $this->products->whereIn('id', $productIds);
    }
}
