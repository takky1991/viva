<?php

namespace Viva\Gateways;

use Carbon\Carbon;
use Viva\Interfaces\GatewayInterface;
use Viva\Order;

class OrdersGateway implements GatewayInterface
{
    protected $orders;

    protected $publicFilters = [
        'sq',
        'email',
        'status',
        'ordersFromDate',
        'ordersToDate',
        'shippingMethodId',
        'cardPaymentStatus',
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 30;

    public function __construct()
    {
        $this->orders = Order::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $strictMode = false)
    {
        // If strinctMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method.
        if ($strictMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->orders;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        $this->orders->where('status', '!=', 'processing_payment');
        // Sort orders by "created_at" by default.
        if (! array_key_exists('sortBy', $input)) {
            $this->orders->orderBy('created_at', 'desc');
        }
    }

    public function sq($value)
    {
        if ($value) {
            $this->orders
                ->where(function ($query) use ($value) {
                    return $query
                        ->where('first_name', 'like', $value.'%')
                        ->orWhere('last_name', 'like', $value.'%')
                        ->orWhere('email', 'like', $value.'%')
                        ->orWhere('order_number', 'like', $value.'%');
                });
        }
    }

    public function status($value)
    {
        if ($value) {
            $this->orders->where('status', $value);
        }
    }

    public function cardPaymentStatus($value)
    {
        if ($value == 'all') {
            $this->orders->whereNotNull('card_payment_status');
        } elseif ($value) {
            $this->orders->where('card_payment_status', $value);
        }
    }

    public function email($value)
    {
        if ($value) {
            $this->orders->where('email', $value);
        }
    }

    public function ordersFromDate($value)
    {
        if ($value) {
            $date = Carbon::parse($value);

            return $this->orders->where('created_at', '>=', $date);
        }
    }

    public function ordersToDate($value)
    {
        if ($value) {
            $date = Carbon::parse($value)->endOfDay();

            return $this->orders->where('created_at', '<=', $date);
        }
    }

    public function shippingMethodId($value)
    {
        if ($value) {
            return $this->orders->where('shipping_method_id', $value);
        }
    }
}
