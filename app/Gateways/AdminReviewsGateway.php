<?php

namespace Viva\Gateways;

use Viva\Interfaces\GatewayInterface;
use Viva\Review;

class AdminReviewsGateway implements GatewayInterface
{
    protected $reviews;

    protected $publicFilters = [
        'published',
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 20;

    public function __construct()
    {
        $this->reviews = Review::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $strictMode = false)
    {
        // If strinctMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method.
        if ($strictMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->reviews;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        // Sort reviews by "created_at" by default.
        if (! array_key_exists('sortBy', $input)) {
            $this->reviews->orderBy('created_at', 'desc');
        }
    }

    public function published($input)
    {
        if ($input == 'true') {
            $this->reviews->where('published', true);
        } elseif ($input == 'false') {
            $this->reviews->where('published', false);
        }
    }
}
