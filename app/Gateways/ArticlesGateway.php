<?php

namespace Viva\Gateways;

use Viva\Article;
use Viva\Interfaces\GatewayInterface;

class ArticlesGateway implements GatewayInterface
{
    protected $articles;

    protected $publicFilters = [
        //
    ]; // Array of available fiters for the public

    public $defaultPaginationSize = 6;

    public function __construct()
    {
        $this->articles = Article::query();
    }

    // Accepts an array and performs filtering based on its data.
    public function getItems(array $input = [], $publicMode = false)
    {
        // If publicMode true, remove all filter options that are not in publicFilters array.
        // Use this option when directly passing user input into the method without validating.
        if ($publicMode) {
            $input = array_filter($input, function ($key) {
                return in_array($key, $this->publicFilters);
            }, ARRAY_FILTER_USE_KEY);
        }

        $this->setDefaultConstraints($input);

        foreach ($input as $key => $value) {
            if (method_exists($this, $key)) {
                $this->$key($value);
            }
        }

        return $this->articles;
    }

    // Here can be defined some default constraints for the getItems() method.
    public function setDefaultConstraints($input)
    {
        // Only published articles
        $this->articles->where('published', true);

        // Sort articles by "created_at" by default.
        if (! array_key_exists('sortBy', $input)) {
            $this->articles->orderBy('created_at', 'desc');
        }
    }
}
