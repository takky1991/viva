<?php

namespace Viva\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RemarketingEmailListExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        return DB::table('orders')->select(DB::raw('email, MIN(first_name) AS first_name, MIN(last_name) AS last_name, MIN(phone) as phone'))->groupBy('email')->orderBy('email');
    }

    public function headings(): array
    {
        return [
            'email',
            'first_name',
            'last_name',
            'phone',
        ];
    }
}
