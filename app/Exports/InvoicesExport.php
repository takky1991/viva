<?php

namespace Viva\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;

class InvoicesExport implements FromView
{
    protected $payments;

    protected $total;

    public function __construct(Collection $payments, int $total, float $totalAmount)
    {
        $this->payments = $payments;
        $this->total = $total;
        $this->totalAmount = $totalAmount;
    }

    public function view(): View
    {
        return view('backend/payments/pdf', [
            'payments' => $this->payments,
            'total' => $this->total,
            'totalAmount' => $this->totalAmount,
        ]);
    }
}
