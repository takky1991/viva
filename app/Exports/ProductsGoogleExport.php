<?php

namespace Viva\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Viva\Gateways\ProductsGateway;

class ProductsGoogleExport implements FromCollection, WithMapping, WithHeadings
{
    public function collection(): Collection
    {
        $productsGateway = new ProductsGateway();
        $products = $productsGateway->getItems(['inStock' => 'true'])->get();

        return $products;
    }

    public function headings(): array
    {
        return [
            'ID',
            'ID2',
            'Item title',
            'Final URL',
            'Image URL',
            'Item subtitle',
            'Item description',
            'Item category',
            'Price',
            'Sale price',
            'Contextual keywords',
            'Item address',
            'Tracking template',
            'Custom parameter',
            'Final mobile URL',
            'Android app link',
            'iOS app link',
            'iOS app store ID',
        ];
    }

    public function map($product): array
    {
        $category = $product->menuCategories()->whereNull('categories.parent_id')->first();
        $categoryIds = $product->categories->pluck('id')->toArray();
        $lastCategory = null;

        while ($category) {
            $lastCategory = $category;
            $category = $category->menuChildren()->whereIn('id', $categoryIds)->first();
        }

        return [
            $product->id, // ID
            '', // ID2
            $product->fullName(), // Item title
            $product->url(), // Final URL
            $product->mainPhoto() ? $product->mainPhoto()->url('product_show') : asset('/images/logo_270_contrast.png'), // Image URL
            $product->package_description, // Item subtitle
            '', // Item description
            $lastCategory?->title, // Item category
            number_format($product->price, 2, '.', '').' BAM', // Price
            $product->onSale() ? number_format($product->discount_price, 2, '.', '').' BAM' : '', // Sale price
            '', // Contextual keywords
            '', // Item address
            '', // Tracking template
            '', // Custom parameter
            $product->url(), // Final mobile URL
            '', // Android app link
            '', // iOS app link
            '', // iOS app store ID
        ];
    }
}
