<?php

namespace Viva\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;

class ProductsSalesExport implements FromView
{
    protected $productsFromDate;

    protected $productsToDate;

    protected $products;

    protected $productsOrdersCount;

    protected $productsOrdersTotalPrice;

    public function __construct(string $productsFromDate, string $productsToDate, Collection $products, int $productsOrdersCount, int $productsOrdersTotalPrice)
    {
        $this->productsFromDate = $productsFromDate;
        $this->productsToDate = $productsToDate;
        $this->products = $products;
        $this->productsOrdersCount = $productsOrdersCount;
        $this->productsOrdersTotalPrice = $productsOrdersTotalPrice;
    }

    public function view(): View
    {
        return view('backend/shop/products/pdf', [
            'from' => $this->productsFromDate,
            'to' => $this->productsToDate,
            'products' => $this->products,
            'productsOrdersCount' => $this->productsOrdersCount,
            'productsOrdersTotalPrice' => $this->productsOrdersTotalPrice,
        ]);
    }
}
