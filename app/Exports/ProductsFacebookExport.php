<?php

namespace Viva\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Viva\Gateways\ProductsGateway;

class ProductsFacebookExport implements FromCollection, WithMapping, WithHeadings
{
    public function collection(): Collection
    {
        $productsGateway = new ProductsGateway();
        $products = $productsGateway->getItems(['inStock' => 'true'])->get();

        return $products;
    }

    public function headings(): array
    {
        return [
            'id',
            'title',
            'description',
            'availability',
            'condition',
            'price',
            'link',
            'image_link',
            'brand',

            // optional
            'inventory',
            'google_product_category',
            'fb_product_category',
            'sale_price',
            'sale_price_effective_date',
            'item_group_id',
            'gender',
            'color',
            'size',
            'age_group',
            'material',
            'pattern',
            'shipping',
            'shipping_weight',
        ];
    }

    public function map($product): array
    {
        return [
            $product->id,
            $product->fullName(),
            $product->package_description,
            'in stock',
            'new',
            number_format($product->price, 2, '.', '').' BAM',
            $product->url(),
            $product->mainPhoto() ? $product->mainPhoto()->url('product_show') : asset('/images/logo_270_contrast.png'),
            $product->brand->name,

            // optional
            '',
            '',
            '',
            $product->onSale() ? number_format($product->discount_price, 2, '.', '').' BAM' : '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ];
    }
}
