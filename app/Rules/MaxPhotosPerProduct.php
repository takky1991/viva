<?php

namespace Viva\Rules;

use Closure;
use Viva\Product;
use Illuminate\Contracts\Validation\ValidationRule;

class MaxPhotosPerProduct implements ValidationRule
{
    private $productId;

    private $maxValue;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($productId, $maxValue)
    {
        $this->productId = $productId;
        $this->maxValue = $maxValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  mixed  $value
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $product = Product::where('id', $this->productId)->with('photos')->first();
        $productPhotosCount = $product->photos->count();

        $photosCount = count($value);
        $totalPhotosCount = $productPhotosCount + $photosCount;

        if ($totalPhotosCount >= $this->maxValue) {
            $fail('Dosegli ste maksimalan broj fotografija za proizvod.');
        }
    }
}
