<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BackToStockAlert extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'email',
        'notification_sent',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }
}
