<?php

namespace Viva\Observers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Viva\Cart;
use Viva\GuestCart;
use Viva\Product;

class ProductObserver
{
    public function creating(Product $product): void
    {
        do {
            $sku = randomCode(7);
        } while (Product::where('sku', $sku)->exists());

        $product->sku = $sku;
    }

    public function saved(Product $product)
    {
        $product->createSlug();

        DB::table('products')->where('id', $product->id)->update(['categories_hierarchy' => $product->generateCategoriesHierarchy()]);

        Cache::forget('homeProductsOnSale');
        Cache::forget('homeProductsPopular');
    }

    public function deleting(Product $product)
    {
        // remove the product from all carts and show message to cart owner
        Cart::whereHas('products', function (Builder $query) use ($product) {
            $query->where('product_id', $product->id);
        })->update(['product_removed' => true]);

        GuestCart::whereHas('products', function (Builder $query) use ($product) {
            $query->where('product_id', $product->id);
        })->update(['product_removed' => true]);

        // Let the CartProductObserver handle the free_shipping flag of the carts
        $carts = Cart::whereHas('products', function (Builder $query) use ($product) {
            $query->where('product_id', $product->id);
        })->get();

        foreach ($carts as $cart) {
            $cart->products()->detach($product->id);
        }

        // Let the GuestCartProductObserver handle the free_shipping flag of the carts
        $guestCarts = GuestCart::whereHas('products', function (Builder $query) use ($product) {
            $query->where('product_id', $product->id);
        })->get();

        foreach ($guestCarts as $guestCart) {
            $guestCart->products()->detach($product->id);
        }
    }

    public function deleted(Product $product): void
    {
        $product->slugs()->delete();
        Cache::forget('homeProductsOnSale');
        Cache::forget('homeProductsPopular');
    }

    public function updated(Product $product): void
    {
        // if changed from out of stock to in_stock
        if ((bool) $product->in_stock == true && (bool) $product->getOriginal()['in_stock'] == false) {
            $product->notifyBackToStockAlerts();
        }

        // if changed from in_stock to out_of stock, remove the product from all carts and show message to cart owner
        if ((bool) $product->in_stock == false && (bool) $product->getOriginal()['in_stock'] == true) {
            Cart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->update(['product_removed' => true]);

            GuestCart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->update(['product_removed' => true]);

            // Let the CartProductObserver handle the free_shipping flag of the carts
            $carts = Cart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->get();

            foreach ($carts as $cart) {
                $cart->products()->detach($product->id);
            }

            // Let the GuestCartProductObserver handle the free_shipping flag of the carts
            $guestCarts = GuestCart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->get();

            foreach ($guestCarts as $guestCart) {
                $guestCart->products()->detach($product->id);
            }
        }

        // if realPrice has changed then show a message to he users that have the product in the cart and update cart free_shipping flag
        if (
            ((bool) $product->on_sale != (bool) $product->getOriginal()['on_sale']) ||
            ($product->on_sale && $product->discount_price != $product->getOriginal()['discount_price']) ||
            (! $product->on_sale && $product->price != $product->getOriginal()['price'])
        ) {
            Cart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->update(['product_price_changed' => true]);

            GuestCart::whereHas('products', function (Builder $query) use ($product) {
                $query->where('product_id', $product->id);
            })->update(['product_price_changed' => true]);

            if (! $product->free_shipping) {
                $carts = Cart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->whereDoesntHave('products', function (Builder $query) {
                    $query->where('products.free_shipping', true);
                })->get();

                foreach ($carts as $cart) {
                    $cart->free_shipping = $cart->totalPrice() >= (float) config('app.free_shipping_threshold');
                    $cart->save();
                }

                $guestCarts = GuestCart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->whereDoesntHave('products', function (Builder $query) {
                    $query->where('products.free_shipping', true);
                })->get();

                foreach ($guestCarts as $guestCart) {
                    $guestCart->free_shipping = $guestCart->totalPrice() >= (float) config('app.free_shipping_threshold');
                    $guestCart->save();
                }
            }
        }

        // if free_shipping is changed we have to update cart free_shipping flag
        if ((bool) $product->free_shipping != (bool) $product->getOriginal()['free_shipping']) {
            if ($product->free_shipping) {
                Cart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->update(['free_shipping' => true]);

                GuestCart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->update(['free_shipping' => true]);
            } else {
                $carts = Cart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->whereDoesntHave('products', function (Builder $query) {
                    $query->where('products.free_shipping', true);
                })->get();

                foreach ($carts as $cart) {
                    $cart->free_shipping = $cart->totalPrice() >= (float) config('app.free_shipping_threshold');
                    $cart->save();
                }

                $guestCarts = GuestCart::whereHas('products', function (Builder $query) use ($product) {
                    $query->where('product_id', $product->id);
                })->whereDoesntHave('products', function (Builder $query) {
                    $query->where('products.free_shipping', true);
                })->get();

                foreach ($guestCarts as $guestCart) {
                    $guestCart->free_shipping = $guestCart->totalPrice() >= (float) config('app.free_shipping_threshold');
                    $guestCart->save();
                }
            }
        }
    }
}
