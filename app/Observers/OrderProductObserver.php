<?php

namespace Viva\Observers;

use Viva\OrderProduct;

class OrderProductObserver
{
    public function saving(OrderProduct $orderProduct)
    {
        $realPrice = ($orderProduct->discount_price && $orderProduct->on_sale) ? $orderProduct->discount_price : $orderProduct->price;
        $orderProduct->real_price = $realPrice;
        $orderProduct->total_price = $realPrice * $orderProduct->quantity;
    }
}
