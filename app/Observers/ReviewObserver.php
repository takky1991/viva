<?php

namespace Viva\Observers;

use Viva\Review;

class ReviewObserver
{
    public function saved(Review $review)
    {
        $review->product()->first()->calculateRating();
    }

    public function deleted(Review $review): void
    {
        $review->product()->first()->calculateRating();
    }
}
