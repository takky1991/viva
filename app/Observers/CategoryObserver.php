<?php

namespace Viva\Observers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Viva\Category;
use Viva\Product;

class CategoryObserver
{
    public function updated(Category $category): void
    {
        $original = $category->getOriginal();

        // If we had a parent category and removed it or changed it
        if (array_key_exists('parent_id', $original) && ! empty($original['parent_id']) && ($original['parent_id'] != $category->parent_id)) {
            $parentCategory = Category::find($original['parent_id']);
            // Update children_ids for old parent category
            $parentCategory->updateChildrenIds();

            $parentCategory->refresh();

            // Update products for old parent category
            $parentCategory->updateProducts();
        }

        // If we added a new parent category and previously we didn't have one
        elseif ((! array_key_exists('parent_id', $original) || empty($original['parent_id'])) && ! empty($category->parent_id)) {
            $parentCategory = $category->parent;
            // Update children_ids for old parent category
            $parentCategory->updateChildrenIds();

            $parentCategory->refresh();

            // Update products for old parent category
            $parentCategory->updateProducts();
        }
    }

    public function created(Category $category): void
    {
        // If there is a parent category for the first time do updates
        if ($category->parent) {
            $category->parent->updateChildrenIds();
        }
    }

    public function saved(Category $category)
    {
        $category->createSlug();

        Cache::forget('mainMenuCategories');

        // Generate categories_hierarchy for products
        Product::withTrashed()->chunk(100, function ($products) {
            foreach ($products as $product) {

                // we don't want other observers to fire
                DB::table('products')->where('id', $product->id)->update(['categories_hierarchy' => $product->generateCategoriesHierarchy()]);
            }
        });
    }

    public function deleted(Category $category): void
    {
        $category->slugs()->delete();
        $category->photo()->delete();
    }
}
