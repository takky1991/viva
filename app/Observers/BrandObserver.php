<?php

namespace Viva\Observers;

use Illuminate\Support\Facades\Cache;
use Viva\Brand;

class BrandObserver
{
    public function saved(Brand $brand)
    {
        $brand->createSlug();

        Cache::forget('featuredBrands');
    }

    public function deleted(Brand $brand): void
    {
        $brand->slugs()->delete();
    }
}
