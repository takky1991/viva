<?php

namespace Viva\Observers;

use Viva\GuestCartProduct;

class GuestCartProductObserver
{
    public function created(GuestCartProduct $guestCartProduct): void
    {
        $product = $guestCartProduct->product;
        $guestCart = $guestCartProduct->guestCart;

        if (
            $product->free_shipping ||
            $guestCart->totalPrice() >= (float) config('app.free_shipping_threshold')
        ) {
            $guestCart->free_shipping = true;
            $guestCart->save();
        }
    }

    public function updated(GuestCartProduct $guestCartProduct): void
    {
        $product = $guestCartProduct->product;
        $guestCart = $guestCartProduct->guestCart;

        $guestCart->free_shipping =
            $product->free_shipping ||
            $guestCart->totalPrice() >= (float) config('app.free_shipping_threshold');
        $guestCart->save();
    }

    public function deleted(GuestCartProduct $guestCartProduct): void
    {
        $guestCart = $guestCartProduct->guestCart;
        $freeShippingProduct = $guestCart->products()->where('free_shipping', true)->first();

        $guestCart->free_shipping =
            ! empty($freeShippingProduct) ||
            $guestCart->totalPrice() >= (float) config('app.free_shipping_threshold');
        $guestCart->save();
    }
}
