<?php

namespace Viva\Observers;

use Carbon\Carbon;
use Viva\GuestCart;

class GuestCartObserver
{
    public function creating(GuestCart $guestCart)
    {
        $guestCart->expires_at = Carbon::now()->addMinutes(config('app.guest_cart_lifetime'));
        $guestCart->source = app()->make('DeviceHandler')->getDevice()['source'];
    }
}
