<?php

namespace Viva\Observers;

use Viva\Article;

class ArticleObserver
{
    public function saved(Article $article)
    {
        $article->createSlug();
    }

    public function deleted(Article $article): void
    {
        $article->slugs()->delete();
        $article->photo()->delete();
    }
}
