<?php

namespace Viva\Observers;

use Viva\Cart;

class CartObserver
{
    public function creating(Cart $cart)
    {
        $cart->source = app()->make('DeviceHandler')->getDevice()['source'];
    }
}
