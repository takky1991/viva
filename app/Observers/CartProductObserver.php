<?php

namespace Viva\Observers;

use Viva\CartProduct;

class CartProductObserver
{
    public function created(CartProduct $cartProduct): void
    {
        $product = $cartProduct->product;
        $cart = $cartProduct->cart;

        if (
            $product->free_shipping ||
            $cart->totalPrice() >= (float) config('app.free_shipping_threshold')
        ) {
            $cart->free_shipping = true;
            $cart->save();
        }
    }

    public function updated(CartProduct $cartProduct): void
    {
        $product = $cartProduct->product;
        $cart = $cartProduct->cart;

        $cart->free_shipping =
            $product->free_shipping ||
            $cart->totalPrice() >= (float) config('app.free_shipping_threshold');
        $cart->save();
    }

    public function deleted(CartProduct $cartProduct): void
    {
        $cart = $cartProduct->cart;
        $freeShippingProduct = $cart->products()->where('free_shipping', true)->first();

        $cart->free_shipping =
            ! empty($freeShippingProduct) ||
            $cart->totalPrice() >= (float) config('app.free_shipping_threshold');
        $cart->save();
    }
}
