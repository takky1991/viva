<?php

namespace Viva\Observers;

use Illuminate\Support\Facades\Cache;
use Viva\DiscountCollection;
use Viva\Product;

class DiscountCollectionObserver
{
    public function saved(DiscountCollection $discountCollection)
    {
        $discountCollection->createSlug();
        Cache::forget('mainMenuDiscounts');
    }

    public function deleted(DiscountCollection $discountCollection): void
    {
        $discountCollection->slugs()->delete();

        if ($discountCollection->active) {
            $productIds = empty($discountCollection->product_ids) ? [] : json_decode($discountCollection->product_ids);
            $products = Product::whereIn('id', $productIds)->get();

            foreach ($products as $product) {
                $product->update([
                    'on_sale' => false,
                ]);
            }
        }

        Cache::forget('mainMenuDiscounts');
    }
}
