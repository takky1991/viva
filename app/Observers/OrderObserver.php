<?php

namespace Viva\Observers;

use Viva\Notifications\OrderPickupReadyNotification;
use Viva\Notifications\OrderShippedNotification;
use Viva\Order;

class OrderObserver
{
    public function creating(Order $order)
    {
        do {
            $orderNumber = randomCode(6);
        } while (Order::where('order_number', $orderNumber)->exists());

        $order->order_number = $orderNumber;
        $order->source = app()->make('DeviceHandler')->getDevice()['source'];
    }

    public function updated(Order $order): void
    {
        if ($order->status == 'shipped' && $order->getOriginal()['status'] != 'shipped' && ! $order->shipped_notification_sent) {
            $order->notify(new OrderShippedNotification());
            $order->shipped_notification_sent = true;
            $order->save();
        }

        if ($order->status == 'ready_for_pickup' && $order->getOriginal()['status'] != 'ready_for_pickup' && ! $order->pickup_notification_sent) {
            $order->notify(new OrderPickupReadyNotification());
            $order->pickup_notification_sent = true;
            $order->save();
        }
    }
}
