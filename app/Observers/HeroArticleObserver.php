<?php

namespace Viva\Observers;

use Illuminate\Support\Facades\Cache;
use Viva\HeroArticle;

class HeroArticleObserver
{
    public function saved()
    {
        Cache::forget('homeHeroArticles');
    }

    public function deleted(HeroArticle $heroArticle): void
    {
        $heroArticle->photo()->delete();
        $heroArticle->mobilePhoto()->delete();
        Cache::forget('homeHeroArticles');
    }
}
