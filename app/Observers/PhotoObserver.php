<?php

namespace Viva\Observers;

use Illuminate\Support\Facades\Storage;
use Viva\Photo;

class PhotoObserver
{
    public function deleted(Photo $photo): void
    {
        // Delete file from all discs
        $extension = $photo->extension;
        if (empty($extension)) {
            $extension = 'jpg';
        }
        $path = str_replace('/original.'.$extension, '', $photo->getPath());

        Storage::disk('s3')->deleteDirectory($path);
        Storage::disk('public')->deleteDirectory($path);
    }
}
