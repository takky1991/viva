<?php

namespace Viva\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Viva\Order::class => \Viva\Policies\OrderPolicy::class,
        \Viva\Category::class => \Viva\Policies\CategoryPolicy::class,
        \Viva\User::class => \Viva\Policies\UserPolicy::class,
        \Viva\Supplier::class => \Viva\Policies\SupplierPolicy::class,
        \Viva\Product::class => \Viva\Policies\ProductPolicy::class,
        \Viva\Payment::class => \Viva\Policies\PaymentPolicy::class,
        \Viva\SmsNumber::class => \Viva\Policies\SmsNumberPolicy::class,
        \Viva\Brand::class => \Viva\Policies\BrandPolicy::class,
        \Viva\Article::class => \Viva\Policies\ArticlePolicy::class,
        \Viva\DiscountCollection::class => \Viva\Policies\DiscountCollectionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
