<?php

namespace Viva\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrap();
        JsonResource::withoutWrapping();
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }
}
