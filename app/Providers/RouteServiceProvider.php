<?php

namespace Viva\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Viva\Article;
use Viva\Brand;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\Order;
use Viva\Product;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    public const VERIFY_EMAIL = '/email/potvrda';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            $this->mapApiRoutes();
            $this->mapWebRoutes();
            $this->mapAdminRoutes();

            //
        });
        Route::bind('order_number', function ($value, $route) {
            if ($route->getName() == 'checkout.success') {
                if (auth()->check()) {
                    return Order::where('user_id', auth()->id())->where('order_number', $value)->firstOrFail();
                } else {
                    return Order::where('guest_id', session('guest_id'))->where('order_number', $value)->firstOrFail();
                }
            }

            return Order::where('order_number', $value)->firstOrFail();
        });

        Route::bind('product', function ($value, $route) {
            if (
                $route->getName() == 'backend.shop.products.show' ||
                $route->getName() == 'backend.shop.products.restore'
            ) {
                return Product::onlyTrashed()->findOrFail($value);
            } elseif (
                $route->getName() == 'products.show' ||
                $route->getName() == 'products.review' ||
                $route->getName() == 'products.review.save'
            ) {
                return Product::where('published', true)
                    ->whereHas('slugs', function ($q) use ($value) {
                        return $q->where('slug', $value);
                    })
                    ->firstOrFail();
            }

            return Product::findOrFail($value);
        });

        Route::bind('article', function ($value, $route) {
            if ($route->getName() == 'articles.show') {
                return Article::where('published', true)
                    ->whereHas('slugs', function ($q) use ($value) {
                        return $q->where('slug', $value);
                    })
                    ->firstOrFail();
            }

            return Article::findOrFail($value);
        });

        Route::bind('discountCollection', function ($value, $route) {
            if ($route->getName() == 'discount_collections.show') {
                return DiscountCollection::where('active', true)
                    ->whereHas('slugs', function ($q) use ($value) {
                        return $q->where('slug', $value);
                    })
                    ->firstOrFail();
            }

            return DiscountCollection::findOrFail($value);
        });

        Route::bind('category', function ($value, $route) {
            if (in_array($route->getName(), [
                'categories.show',
                'brands.show',
            ])) {
                return Category::whereHas('slugs', function ($q) use ($value) {
                    return $q->where('slug', $value);
                })->firstOrFail();
            }

            return Category::findOrFail($value);
        });

        Route::bind('brand', function ($value, $route) {
            if ($route->getName() == 'brands.show') {
                return Brand::whereHas('slugs', function ($q) use ($value) {
                    return $q->where('slug', $value);
                })->firstOrFail();
            }

            return Brand::findOrFail($value);
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
             ->middleware('api')
             ->group(base_path('routes/api.php'));

        Route::prefix('api')
             ->middleware(['api', 'admin'])
             ->group(base_path('routes/api_admin.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
             ->middleware(['web', 'admin'])
             ->group(base_path('routes/admin.php'));
    }

    /**
     * Configure the rate limiters for the application.
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
