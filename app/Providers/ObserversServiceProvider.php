<?php

namespace Viva\Providers;

use Illuminate\Support\ServiceProvider;
use Viva\Article;
use Viva\Brand;
use Viva\Cart;
use Viva\CartProduct;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\GuestCart;
use Viva\GuestCartProduct;
use Viva\HeroArticle;
use Viva\Observers\ArticleObserver;
use Viva\Observers\BrandObserver;
use Viva\Observers\CartObserver;
use Viva\Observers\CartProductObserver;
use Viva\Observers\CategoryObserver;
use Viva\Observers\DiscountCollectionObserver;
use Viva\Observers\GuestCartObserver;
use Viva\Observers\GuestCartProductObserver;
use Viva\Observers\HeroArticleObserver;
use Viva\Observers\OrderObserver;
use Viva\Observers\OrderProductObserver;
use Viva\Observers\PhotoObserver;
use Viva\Observers\ProductObserver;
use Viva\Observers\ReviewObserver;
use Viva\Order;
use Viva\OrderProduct;
use Viva\Photo;
use Viva\Product;
use Viva\Review;

class ObserversServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        Cart::observe(CartObserver::class);
        Photo::observe(PhotoObserver::class);
        Order::observe(OrderObserver::class);
        Brand::observe(BrandObserver::class);
        Review::observe(ReviewObserver::class);
        Article::observe(ArticleObserver::class);
        Product::observe(ProductObserver::class);
        Category::observe(CategoryObserver::class);
        GuestCart::observe(GuestCartObserver::class);
        CartProduct::observe(CartProductObserver::class);
        HeroArticle::observe(HeroArticleObserver::class);
        OrderProduct::observe(OrderProductObserver::class);
        GuestCartProduct::observe(GuestCartProductObserver::class);
        DiscountCollection::observe(DiscountCollectionObserver::class);
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        //
    }
}
