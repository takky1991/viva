<?php

namespace Viva\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Viva\Events\OrderSuccessful;
use Viva\Events\ReviewSubmitted;
use Viva\Listeners\LogSendingMessage;
use Illuminate\Mail\Events\MessageSending;
use Viva\Listeners\OrderSuccessfulListener;
use Viva\Listeners\ReviewSubmittedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        OrderSuccessful::class => [
            OrderSuccessfulListener::class,
        ],

        ReviewSubmitted::class => [
            ReviewSubmittedListener::class,
        ],

        MessageSending::class => [
            LogSendingMessage::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        \Viva\Listeners\UserEventSubscriber::class,
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {

        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
