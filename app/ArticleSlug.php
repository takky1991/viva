<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class ArticleSlug extends Model
{
    protected $fillable = [
        'slug',
    ];

    public function article()
    {
        return $this->belongsTo(\Viva\Article::class);
    }
}
