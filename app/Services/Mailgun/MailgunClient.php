<?php

namespace Viva\Services\Mailgun;

use Exception;
use Mailgun\Mailgun;
use Mailgun\Message\BatchMessage;
use Mailgun\Message\MessageBuilder;

class MailgunClient
{
    /**
     * Undocumented variable
     *
     * @var Mailgun
     */
    private Mailgun $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->client = Mailgun::create(config('services.mailgun.secret'), 'https://api.eu.mailgun.net');
    }

    /**
     * Get the message builder object
     *
     * @return MessageBuilder
     */
    public function getMessageBuilder(): MessageBuilder
    {
        $builder = new MessageBuilder();
        $builder->setFromAddress(config('app.email'), ['first' => config('app.name')]);

        return $builder;
    }

    /**
     * Undocumented function
     *
     * @param MessageBuilder $builder
     * @return void
     * @throws MailgunClientException
     */
    public function sendMessage(MessageBuilder $builder): void
    {
        try {
            $this->client->messages()->send(config('services.mailgun.domain'), $builder->getMessage());
        } catch (Exception $e) {
            throw new MailgunClientException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get a batch message builder object
     *
     * @return BatchMessage
     */
    public function getBatchMessageBuilder(): BatchMessage
    {
        $batchMessage = $this->client->messages()->getBatchMessage(config('services.mailgun.domain'));
        $batchMessage->setFromAddress(config('app.email'), ['first' => config('app.name')]);

        return $batchMessage;
    }
}
