<?php

namespace Viva\Services\Csp\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Basic;
use Spatie\Csp\Value;

class VivaPolicy extends Basic
{
    public function configure()
    {
        $this
            ->addDirective(Directive::BASE, Keyword::SELF)
            ->addDirective(Directive::BLOCK_ALL_MIXED_CONTENT, Value::NO_VALUE)
            ->addDirective(Directive::CONNECT, [
                Keyword::SELF,
                'https://www.google-analytics.com',
                'https://sentry.io',
                'https://www.facebook.com',
                'https://o112325.ingest.sentry.io',
                'https://*.algolianet.com',
                'https://*.algolia.net',
                'https://stats.g.doubleclick.net',
                '*.doubleclick.net',
                '*.google.com',
                'www.googletagmanager.com',
                'translate.googleapis.com',
                'translate.google.com',
                'www.gstatic.com',
                '*.googlesyndication.com',
                'www.googletagservices.com',
                'about:',
            ])
            ->addDirective(Directive::DEFAULT, Keyword::SELF)
            ->addDirective(Directive::FONT, [
                Keyword::SELF,
                'data:',
                'https://fonts.gstatic.com',
                'https://fonts.googleapis.com',
                'https://use.fontawesome.com',
            ])
            ->addDirective(Directive::FORM_ACTION, [
                Keyword::SELF,
                'https://www.facebook.com',
                '*.google.com',
                '*.monri.com',
            ])
            ->addDirective(Directive::FRAME, [
                Keyword::SELF,
                '*.google.com',
                'https://www.facebook.com',
                'https://saltcdn2.googleapis.com',
                'https://www.googletagmanager.com',
                '*.doubleclick.net',
                '*.googlesyndication.com',
            ])
            ->addDirective(Directive::IMG, [
                Keyword::SELF,
                'https://cdn.apotekaviva24.ba',
                'data:',
                'https://www.google.com',
                'https://www.google.de',
                'https://www.google.co.in',
                'https://www.google.rs/',
                'https://www.google.ba',
                'https://www.google.hr',
                'https://'.config('filesystems.disks.s3.bucket').'.s3.eu-central-1.amazonaws.com',
                'https://www.google-analytics.com',
                'https://ssl.gstatic.com',
                'https://www.gstatic.com',
                'https://maps.googleapis.com',
                'https://www.facebook.com',
                'https://www.googletagmanager.com',
                'https://static.xx.fbcdn.net',
                'https://connect.facebook.net',
                'https://googleads.g.doubleclick.net',
            ])
            ->addDirective(Directive::MANIFEST, Keyword::SELF)
            ->addDirective(Directive::MEDIA, [
                Keyword::SELF,
                'dai.google.com',
            ])
            ->addDirective(Directive::OBJECT, [
                Keyword::SELF,
                '*.googlesyndication.com',
            ])
            ->addDirective(Directive::SCRIPT, [
                Keyword::SELF,
                'unsafe-eval',
                'unsafe-inline',
                'https://*.google.com',
                'https://www.google-analytics.com',
                'http://tagmanager.google.com',
                'https://use.fontawesome.com',
                'https://connect.facebook.net',
                'https://saltcdn2.googleapis.com',
                'https://*.doubleclick.net',
                'https://*.googleadservices.com',
                'https://*.googlesyndication.com',
                'https://*.googletagservices.com',
                'https://*.googletagmanager.com',
                'https://translate.googleapis.com',
                'https://translate.google.com',
                'www.googletagmanager.com',
            ])
            ->addDirective(Directive::STYLE, [
                Keyword::SELF,
                'unsafe-inline',
                '*.google.com',
                'https://fonts.googleapis.com',
                'https://use.fontawesome.com',
                'https://*.googletagmanager.com',
                'translate.googleapis.com',
            ]);
    }
}
