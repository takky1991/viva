<?php

namespace Viva\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Viva\Photo;
use Viva\Product;

class UploadPhotoToS3 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $photo;

    protected $style;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo, $style)
    {
        $this->photo = $photo;
        $this->style = $style;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        logger()->info('Processing Job - UploadPhotoToS3');

        $file = $this->photo->getPath($this->style);

        if (Storage::disk('public')->exists($file)) {
            $fileContents = Storage::disk('public')->get($file);
            if (Storage::disk('s3')->put($file, $fileContents, 'public')) {

                //Delete the local file
                Storage::disk('public')->delete($file);

                //Update the photo process information
                $this->photo->markStyleAsProcessed($this->style);

                // Update algolia
                Product::whereHas('photos', function ($q) {
                    $q->where('photos.id', $this->photo->id);
                })
                ->searchable();
            } else {
                //TODO: throw custom exception to make the job try again
                Log::error('Could not put file on S3');
            }
        } else {
            logger()->info('Image not found on local storage: '.$file);
        }
    }
}
