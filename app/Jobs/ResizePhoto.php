<?php

namespace Viva\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Viva\Photo;

class ResizePhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $photo;

    protected $style;

    protected $dimensions;

    protected $processSynchronous;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo, $style, $processSynchronous = false)
    {
        $this->photo = $photo;
        $this->style = $style;
        $this->processSynchronous = $processSynchronous;
        $this->dimensions = Photo::getStyles($photo->target)[$style];
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        logger()->info('Processing Job - ResizePhoto');

        $uri = $this->photo->uri;

        //Check if local file exists
        if (Storage::disk('public')->exists($uri)) {
            $file = $this->processUsingTinyPNG($uri);

            //Delay the uploading to S3 for 1 min
            $job = (new UploadPhotoToS3($this->photo, $this->style))->delay(Carbon::now()->addMinutes(1));

            //When the image is resized, upload to S3
            if ($this->processSynchronous) {
                $job->handle();
            } else {
                dispatch($job);
            }
        } else {
            echo "{$uri} Does not exist<br>";
        }
    }

    protected function processUsingTinyPNG($uri)
    {
        $path = storage_path('app/public').'/'.$uri;
        [$basename, $extension] = explode('.', basename($path));

        //All versions of the photo will be stored in a folder with the name of the photo without the extension
        $newPathForResizedFiles = str_replace('/'.basename($path), '', $path);
        $relativePath = str_replace('/'.basename($path), '', $uri);
        $dimensions = explode('x', $this->dimensions);
        $filename = $newPathForResizedFiles.'/'.$this->style.'.'.$extension;
        $relativeFilename = $relativePath.'/'.$this->style.'.'.$extension;

        //If there is no width, continue
        if (! isset($dimensions[0])) {
            return;
        }

        $options = [
            'width' => (int) $dimensions[0],
            'method' => 'scale',
        ];

        if (isset($dimensions[1])) {
            $options['height'] = (int) $dimensions[1];
            $options['method'] = 'cover';
        }

        \Tinify\setKey(config('services.tinyPng.key'));
        $source = \Tinify\fromFile($path);
        $resized = $source->resize($options);
        $resized->toFile($filename);

        return $relativeFilename;
    }
}
