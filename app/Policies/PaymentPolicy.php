<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\Payment;
use Viva\User;

class PaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view payments index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasRole(['admin', 'employee']);
    }

    /**
     * Determine whether the user can create payments.
     */
    public function create(User $user): bool
    {
        return $user->hasRole(['admin', 'employee']);
    }

    /**
     * Determine whether the user can update the payment.
     */
    public function update(User $user, Payment $payment): bool
    {
        return $user->hasRole('admin') || ($user->hasRole('employee') && $user->id == $payment->user_id && ! $payment->locked);
    }

    /**
     * Determine whether the user can delete the payment.
     */
    public function delete(User $user, Payment $payment): bool
    {
        return $user->hasRole('admin') || ($user->hasRole('employee') && $user->id == $payment->user_id && ! $payment->locked);
    }

    /**
     * Determine whether the user can pay the payment.
     *
     * @return mixed
     */
    public function pay(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can get pdf
     *
     * @return mixed
     */
    public function pdf(User $user)
    {
        return $user->hasRole('admin');
    }
}
