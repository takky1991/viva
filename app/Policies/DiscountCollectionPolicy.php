<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\DiscountCollection;
use Viva\User;

class DiscountCollectionPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view categories index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the discountCollection.
     */
    public function view(User $user, DiscountCollection $discountCollection): bool
    {
        //
    }

    /**
     * Determine whether the user can create discountCollection.
     */
    public function create(User $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the discountCollection.
     */
    public function update(User $user, DiscountCollection $discountCollection): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the discountCollection.
     */
    public function delete(User $user, DiscountCollection $discountCollection): bool
    {
        //
    }
}
