<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\SmsNumber;
use Viva\User;

class SmsNumberPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view sms number index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can create sms number.
     */
    public function create(User $user): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the sms number.
     */
    public function delete(User $user, SmsNumber $smsNumber): bool
    {
        //
    }
}
