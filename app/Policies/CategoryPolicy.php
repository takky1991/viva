<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\Category;
use Viva\User;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view categories index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the category.
     */
    public function view(User $user, Category $category): bool
    {
        //
    }

    /**
     * Determine whether the user can create category.
     */
    public function create(User $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the category.
     */
    public function update(User $user, Category $category): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the category.
     */
    public function delete(User $user, Category $category): bool
    {
        //
    }
}
