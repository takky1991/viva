<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\HeroArticle;
use Viva\User;

class HeroArticlePolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view hero articles index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the hero article.
     */
    public function view(User $user, HeroArticle $heroArticle): bool
    {
        //
    }

    /**
     * Determine whether the user can create hero article.
     */
    public function create(User $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the hero article.
     */
    public function update(User $user, HeroArticle $heroArticle): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the hero article.
     */
    public function delete(User $user, HeroArticle $heroArticle): bool
    {
        //
    }
}
