<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\Review;
use Viva\User;

class ReviewPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view reviews index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can delete the review.
     */
    public function delete(User $user, Review $review): bool
    {
        //
    }

    /**
     * Determine whether the user can show the review.
     *
     * @return mixed
     */
    public function show(User $user, Review $review)
    {
        //
    }

    /**
     * Determine whether the user can publish the review.
     *
     * @return mixed
     */
    public function publish(User $user, Review $review)
    {
        //
    }
}
