<?php

namespace Viva\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Viva\Article;
use Viva\User;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        return $user->hasAnyRole('admin', 'writer');
    }

    /**
     * Determine whether the user can view articles index page.
     *
     * @return mixed
     */
    public function index(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the article.
     */
    public function view(User $user, Article $article): bool
    {
        //
    }

    /**
     * Determine whether the user can create article.
     */
    public function create(User $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the article.
     */
    public function update(User $user, Article $article): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the article.
     */
    public function delete(User $user, Article $article): bool
    {
        //
    }

    /**
     * Determine whether the user can preview the article.
     *
     * @return mixed
     */
    public function preview(User $user, Article $article)
    {
        //
    }
}
