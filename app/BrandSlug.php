<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class BrandSlug extends Model
{
    protected $fillable = [
        'slug',
    ];

    public function brand()
    {
        return $this->belongsTo(\Viva\Brand::class);
    }
}
