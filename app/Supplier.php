<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function payments()
    {
        return $this->hasMany(\Viva\Payment::class);
    }

    public function paidPayments()
    {
        return $this->payments()->where('paid', true);
    }

    public function unpaidPayments()
    {
        return $this->payments()->where('paid', false);
    }

    public function duePayments()
    {
        return $this->unpaidPayments()->where('pay_until', '<=', now());
    }
}
