<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Viva\Http\Resources\OrderResource;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'guest_id',
        'email',
        'shipping_method_id',
        'shipping_key',
        'shipping_name',
        'shipping_price',
        'original_shipping_price',
        'shipping_pickup',
        'first_name',
        'last_name',
        'phone',
        'street_address',
        'postalcode',
        'city',
        'country_id',
        'additional_info',
        'payment_method',
        'card_number_masked',
        'card_type',
        'card_payment_status',
        'total_price',
        'total_price_with_shipping',
        'status',
        'comment',
        'source',
    ];

    public static $statuses = [
        'created',
        'ready_for_pickup',
        'ready_for_sending',
        'shipped',
        'fulfilled',
        'canceled',
        'returned',
        'processing_payment',
    ];

    public static $pickupStatuses = [
        'created',
        'ready_for_pickup',
        'fulfilled',
        'canceled',
        'processing_payment',
    ];

    public static $shippingStatuses = [
        'created',
        'ready_for_sending',
        'shipped',
        'fulfilled',
        'canceled',
        'returned',
        'processing_payment',
    ];

    public static $cardPaymentStatuses = [
        'authorized',
        'voided',
        'captured',
        'refund',
        'processed',
    ];

    public static $statusWording = [
        'created' => 'Narudžba kreirana',
        'ready_for_pickup' => 'Spremno za preuzimanje',
        'ready_for_sending' => 'Spremno za slanje',
        'shipped' => 'Narudžba poslana',
        'fulfilled' => 'Narudžba izvršena',
        'canceled' => 'Narudžba otkazana',
        'returned' => 'Narudžba vraćena',
        'processing_payment' => 'Plaćanje u procesu',
    ];

    public static $cardPaymentStatuseWording = [
        'authorized' => 'Autorizirano',
        'voided' => 'Poništeno',
        'captured' => 'Prihvaćeno',
        'refund' => 'Povrat',
        'processed' => 'Procesirano',
    ];

    public function orderProducts()
    {
        return $this->hasMany(\Viva\OrderProduct::class);
    }

    public function country()
    {
        return $this->belongsTo(\Viva\Country::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(\Viva\ShippingMethod::class);
    }

    public function totalPrice(): float
    {
        $totalPrice = 0.00;

        foreach ($this->orderProducts as $orderProduct) {
            $totalPrice += $orderProduct->quantity * $orderProduct->realPrice();
        }

        return $totalPrice;
    }

    public function totalPriceWithShipping(): float
    {
        return $this->totalPrice() + $this->shipping_price;
    }

    public function toSearchableJson()
    {
        return json_encode(new OrderResource($this));
    }
}
