<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Viva\Http\Resources\PhotoResource;

class HeroArticle extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'subtitle',
        'brand_id',
        'button_text',
        'button_url',
        'background_color',
        'text_color',
        'published',
    ];

    public function brand()
    {
        return $this->belongsTo(\Viva\Brand::class);
    }

    public function photo()
    {
        return $this->belongsTo(\Viva\Photo::class);
    }

    public function mobilePhoto()
    {
        return $this->belongsTo(\Viva\Photo::class, 'mobile_photo_id');
    }

    public function getPhotosJson()
    {
        return json_encode(PhotoResource::collection(collect([$this->photo])));
    }

    public function getMobilePhotosJson()
    {
        return json_encode(PhotoResource::collection(collect([$this->mobilePhoto])));
    }
}
