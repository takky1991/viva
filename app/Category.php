<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;
use Viva\Http\Resources\PhotoResource;
use Viva\Traits\SluggableTrait;

class Category extends Model
{
    use HasFactory;
    use Searchable,
        SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'title',
        'description',
        'children_ids',
        'menu',
        'photo_id',
        'featured',
    ];

    protected $sluggableColumn = 'title';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'children_ids' => 'array',
    ];

    public function parent()
    {
        return $this->belongsTo(\Viva\Category::class);
    }

    public function children()
    {
        return $this->hasMany(\Viva\Category::class, 'parent_id');
    }

    public function menuChildren()
    {
        return $this->children()->where('menu', true);
    }

    public function products()
    {
        return $this->belongsToMany(\Viva\Product::class)->withPivot('best_seller_in_category');
    }

    public function productsWithArchived()
    {
        return $this->products()->withTrashed();
    }

    public function publishedProducts()
    {
        return $this->products()->where('published', true);
    }

    public function unpublishedProducts()
    {
        return $this->products()->where('published', false);
    }

    public function photo()
    {
        return $this->belongsTo(\Viva\Photo::class);
    }

    public function slugs()
    {
        return $this->hasMany(\Viva\CategorySlug::class)->orderBy('created_at', 'desc');
    }

    public function slug()
    {
        return $this->slugs?->first();
    }

    public function getSlug()
    {
        return $this->slug()?->slug;
    }

    public function scopeWhereNoParent()
    {
        return $this->where('parent_id', null);
    }

    public function getPhotosJson()
    {
        return json_encode(PhotoResource::collection(collect([$this->photo])));
    }

    public function getAllNestedChildrenIds()
    {
        $children = [];

        foreach ($this->children as $child) {
            $children[] = $child->id;
            $children = array_merge($children, $child->getAllNestedChildrenIds());
        }

        return $children;
    }

    public function updateChildrenIds()
    {
        // We don't want to trigger the observer
        DB::table('categories')->where('id', $this->id)->update(['children_ids' => json_encode($this->getAllNestedChildrenIds())]);

        if ($this->parent) {
            $this->parent->updateChildrenIds();
        }
    }

    public function updateProducts()
    {
        $productIds = $this->products->pluck('id')->toArray();

        $nestedCategories = Category::whereIn('id', $this->children_ids)->get();

        foreach ($nestedCategories as $category) {
            $productIds = array_merge($productIds, $category->products->pluck('id')->toArray());
        }

        $productIds = array_unique($productIds);
        $productIds = array_values($productIds);

        $this->products()->sync($productIds);
    }

    /**
     * Get the indexable data array for the model.
     */
    public function toSearchableArray(): array
    {
        if (! $this->menu) {
            $this->unsearchable();

            return [];
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'url' => route('categories.show', ['category' => $this->getSlug()]),
        ];
    }
}
