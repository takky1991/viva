<?php

namespace Viva\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Blank extends Mailable
{
    use Queueable;
    use ShouldQueue;
    use SerializesModels;

    public $subject;

    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $content)
    {
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        return $this->view('mail.blank')
            ->from(config('app.email'), config('app.name'))
            ->subject($this->subject)
            ->withSymfonyMessage(function ($message) {
                $headers = $message->getHeaders();
                $headers->addTextHeader('X-Mailgun-Tag', 'blank');
                $headers->addTextHeader('o:tag', 'blank');
            });
    }
}
