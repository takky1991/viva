<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Viva\Http\Resources\PhotoResource;
use Viva\Traits\SluggableTrait;

class Article extends Model
{
    use HasFactory;
    use SluggableTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'subtitle',
        'content',
        'photo_id',
        'product_ids',
        'category_ids',
        'brand_ids',
        'published',
    ];

    protected $sluggableColumn = 'title';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'product_ids' => 'array',
        'category_ids' => 'array',
        'brand_ids' => 'array',
    ];

    public function slugs()
    {
        return $this->hasMany(\Viva\ArticleSlug::class)->orderBy('created_at', 'desc');
    }

    public function slug()
    {
        return $this->slugs?->first();
    }

    public function getSlug()
    {
        return $this->slug()?->slug;
    }

    public function photo()
    {
        return $this->belongsTo(\Viva\Photo::class);
    }

    public function getPhotosJson()
    {
        return json_encode(PhotoResource::collection(collect([$this->photo])));
    }

    public function url(): string
    {
        return route('articles.show', ['article' => $this->getSlug()]);
    }
}
