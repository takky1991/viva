<?php

namespace Viva\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ThirtyDayNoOrderReminderNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $products;

    /**
     * Create a new notification instance.
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage())
            ->from(config('app.email'), config('app.name'))
            ->subject('Ne zaboravite dopuniti Vaše zalihe | '.config('app.name'))
            ->markdown('mail.order.thirty_day_no_order_reminder', [
                'notifiable' => $notifiable,
                'products' => $this->products,
            ])
            ->withSymfonyMessage(function ($message) {
                $headers = $message->getHeaders();
                $headers->addTextHeader('X-Mailgun-Tag', 'thirty-day-no-order-reminder');
                $headers->addTextHeader('o:tag', 'thirty-day-no-order-reminder');
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
