<?php

namespace Viva\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderStatsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $stats;

    /**
     * Create a new notification instance.
     */
    public function __construct(array $stats)
    {
        $this->stats = $stats;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $period = $this->stats['period'];
        $date = $this->stats['date'];

        return (new MailMessage())
            ->from(config('app.email'), config('app.name'))
            ->subject($period.' izvještaj za '.$date.' | '.config('app.name'))
            ->markdown('mail.admin.order_stats', [
                'notifiable' => $notifiable,
                'stats' => $this->stats
            ])
            ->withSymfonyMessage(function ($message) {
                $headers = $message->getHeaders();
                $headers->addTextHeader('X-Mailgun-Tag', 'admin:order-stats');
                $headers->addTextHeader('o:tag', 'admin:order-stats');
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
