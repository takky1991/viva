<?php

namespace Viva\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderPickupReadyNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage())
            ->from(config('app.email'), config('app.name'))
            ->subject('Vaša narudžba je spremna za preuzimanje '.'('.$notifiable->order_number.') | '.config('app.name'))
            ->markdown('mail.order.pickup_ready', [
                'order' => $notifiable,
                'orderProducts' => $notifiable->orderProducts->load('product'),
            ])
            ->withSymfonyMessage(function ($message) {
                $headers = $message->getHeaders();
                $headers->addTextHeader('X-Mailgun-Tag', 'order-pickup-ready');
                $headers->addTextHeader('o:tag', 'order-pickup-ready');
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     */
    public function toArray($notifiable): array
    {
        return [
            //
        ];
    }
}
