<?php

namespace Viva\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Viva\Product;

trait SluggableTrait
{
    public function slugsTableName()
    {
        return Str::singular($this->getTable()).'_slugs';
    }

    public function getMaxSlugNumber($slug)
    {
        $maxSlugNumer = DB::table($this->slugsTableName())
            ->selectRaw('MAX(CAST(SUBSTRING(slug, (CHAR_LENGTH(slug)-CHAR_LENGTH(?)) * -1) as UNSIGNED)) as max_slug_number', [$slug.'-'])
            ->where('slug', 'like', $slug.'-%')->value('max_slug_number');

        return (int) $maxSlugNumer;
    }

    public function createSlug()
    {
        if (get_class($this) == Product::class) {
            $slug = Str::slug($this->brand->name.' '.$this->{$this->sluggableColumn}.' '.$this->package_description, '-');
        } else {
            $slug = Str::slug($this->{$this->sluggableColumn}, '-');
        }

        $existingSlug = DB::table($this->slugsTableName())->where('slug', $slug)->first();

        if (empty($existingSlug)) {
            $this->slugs()->create(['slug' => $slug]);
        } else {
            if ($this->id == $existingSlug->{Str::singular($this->getTable()).'_id'}) {
                DB::table($this->slugsTableName())
                    ->where('id', $existingSlug->id)
                    ->update([
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
            } else {
                $suffix = $this->getMaxSlugNumber($slug);
                $this->slugs()->create(['slug' => $slug.'-'.($suffix + 1)]);
            }
        }
    }
}
