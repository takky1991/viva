<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Viva\Http\Resources\GuestCartResource;
use Viva\Interfaces\CartInterface;

class GuestCart extends Model implements CartInterface
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guest_id',
        'free_shipping',
        'first_name',
        'last_name',
        'phone',
        'email',
        'street_address',
        'postalcode',
        'city',
        'country_id',
        'additional_info',
        'source',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'expires_at' => 'datetime',
    ];

    public function products()
    {
        return $this->belongsToMany(\Viva\Product::class)->using(\Viva\GuestCartProduct::class)->withPivot(['quantity', 'variants'])->withTimestamps()->orderBy('guest_cart_product.created_at', 'desc');
    }

    public function shippingMethod()
    {
        return $this->belongsTo(\Viva\ShippingMethod::class);
    }

    public function toSearchableJson()
    {
        return json_encode(new GuestCartResource($this));
    }

    public function totalPrice(): float
    {
        $totalPrice = 0.00;

        foreach ($this->products as $product) {
            $totalPrice += $product->pivot->quantity * $product->realPrice();
        }

        return $totalPrice;
    }

    public function totalPriceWithShipping(): float
    {
        if ($this->shippingMethod && ! $this->free_shipping) {
            return $this->totalPrice() + $this->shippingMethod->price;
        }

        return $this->totalPrice();
    }
}
