<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Viva\Http\Resources\DiscountCollectionResource;
use Viva\Traits\SluggableTrait;

class DiscountCollection extends Model
{
    use HasFactory;
    use SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'discount_percentage',
        'product_ids',
        'active',
        'start_at',
        'end_at',
    ];

    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];

    protected $sluggableColumn = 'name';

    public function slugs()
    {
        return $this->hasMany(\Viva\DiscountCollectionSlug::class)->orderBy('created_at', 'desc');
    }

    public function slug()
    {
        return $this->slugs?->first();
    }

    public function getSlug()
    {
        return $this->slug()?->slug;
    }

    public function toSearchableJson()
    {
        return json_encode(new DiscountCollectionResource($this));
    }

    public function enable()
    {
        $this->update(['active' => true]);

        $productIds = empty($this->product_ids) ? [] : json_decode($this->product_ids);
        Product::whereIn('id', $productIds)->chunk(10, function ($products) {
            foreach ($products as $product) {
                $newPrice = calculateDiscout($product->price, $this->discount_percentage);
                $product->update([
                    'discount_price' => $newPrice,
                    'on_sale' => true,
                ]);
            }
        });
    }

    public function disable()
    {
        $this->update([
            'active' => false,
            'start_at' => null,
            'end_at' => null,
        ]);

        $productIds = empty($this->product_ids) ? [] : json_decode($this->product_ids);
        Product::whereIn('id', $productIds)->chunk(10, function ($products) {
            foreach ($products as $product) {
                $product->update([
                    'on_sale' => false,
                ]);
            }
        });
    }
}
