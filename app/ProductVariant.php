<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'in_stock',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }
}
