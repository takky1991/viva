<?php

namespace Viva\Console\Commands;

use Viva\Order;
use Illuminate\Console\Command;
use Viva\Notifications\ThirtyDayNoOrderReminderNotification;

class SendThirtyDayNoOrderReminderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:send_30_day_no_order_reminder_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'If an email address has no new ordereds in 30 days and more, send a reminder.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $orders = Order::where('created_at', '>=', now()->subDays(30)->startOfDay())
            ->where('created_at', '<=', now()->subDays(30)->endOfDay())
            ->get();

        foreach ($orders as $order) {
            $newerOrder = Order::where('email', $order->email)
                ->where('created_at', '>', now()->subDays(30)->endOfDay())
                ->first();

            if (!$newerOrder) {
                $products = [];

                foreach($order->orderProducts as $orderProduct) {
                    if($orderProduct->product && $orderProduct->product->published && $orderProduct->product->in_stock) {
                        $products[] = $orderProduct->product;
                    }
                }

                if(sizeof($products)) {
                    $order->notify(new ThirtyDayNoOrderReminderNotification($products));
                }
            }
        }
    }
}
