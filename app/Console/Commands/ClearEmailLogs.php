<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearEmailLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:clear_email_logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears email_log table periodically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        DB::table('email_log')->where('date', '<', now()->subMonths(1))->delete();
    }
}
