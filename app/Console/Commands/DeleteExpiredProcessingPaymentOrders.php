<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Order;

class DeleteExpiredProcessingPaymentOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:delete_expired_processing_payment_orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes orders with status processing_payment that expired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        Order::where('status', 'processing_payment')->where('created_at', '<', now()->subDay())->forceDelete();
    }
}
