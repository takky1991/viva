<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Order;

class CancelExpiredPickupOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:cancel_expired_pickup_orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel expired pickup orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $pickupOrders = Order::where('shipping_pickup', true)->where('status', 'ready_for_pickup')->get();
        foreach ($pickupOrders as $order) {
            $expDate = $order->created_at->addDays(config('app.pickup_days_limit'))->endOfDay();
            if ($expDate->lessThan(now())) {
                $order->status = 'canceled';
                $order->save();
            }
        }
    }
}
