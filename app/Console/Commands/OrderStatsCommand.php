<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Gateways\OrdersGateway;
use Viva\Notifications\Admin\OrderStatsNotification;
use Viva\User;

class OrderStatsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:order_stats {period}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates stats for orders and sends notification';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $period = $this->argument('period');

        if (!in_array($period, ['daily', 'monthly'])) {
            $this->error('Unsupported period');

            return;
        }

        if($period == 'daily') {
            $ordersQuery = (new OrdersGateway())->getItems([
                'ordersFromDate' => now()->yesterday()->startOfDay(),
                'ordersToDate' => now()->yesterday()->endOfDay()
            ]);
    
            $ordersQuery = $ordersQuery->with(['orderProducts']);
            $totalPrice = $ordersQuery->sum('total_price');
            $totalShippingPrice = $ordersQuery->sum('shipping_price');
            $totalOrders = $ordersQuery->count();
    
            $users = User::role('admin')->get();

            foreach($users as $user) {
                $user->notify(new OrderStatsNotification([
                    'period' => 'Dnevni',
                    'date' => now()->yesterday()->format('d.m.Y'),
                    'totalOrders' => (int) $totalOrders,
                    'totalPrice' => (float) $totalPrice,
                    'totalShippingPrice' => (float) $totalShippingPrice
                ]));
            }
        } else if($period == 'monthly') {
            $ordersQuery = (new OrdersGateway())->getItems([
                'ordersFromDate' => now()->subMonth()->startOfMonth()->startOfDay(),
                'ordersToDate' => now()->subMonth()->endOfMonth()->endOfDay()
            ]);
    
            $ordersQuery = $ordersQuery->with(['orderProducts']);
            $totalPrice = $ordersQuery->sum('total_price');
            $totalShippingPrice = $ordersQuery->sum('shipping_price');
            $totalOrders = $ordersQuery->count();
    
            $users = User::role('admin')->get();

            foreach($users as $user) {
                $user->notify(new OrderStatsNotification([
                    'period' => 'Mjesečni',
                    'date' => now()->subMonth()->format('M Y'),
                    'totalOrders' => (int) $totalOrders,
                    'totalPrice' => (float) $totalPrice,
                    'totalShippingPrice' => (float) $totalShippingPrice
                ]));
            }
        }
    }
}
