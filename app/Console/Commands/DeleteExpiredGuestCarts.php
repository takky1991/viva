<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\GuestCart;

class DeleteExpiredGuestCarts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:delete_expited_guest_carts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes expired guest carts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        GuestCart::where('expires_at', '<=', now())->delete();
    }
}
