<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Viva\Jobs\UploadPhotoToS3;
use Viva\Photo;

class RemoveUploadedImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:clear_uploaded_photos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that runs as cron to remove all uploaded photos based on if all styles have been processed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        foreach (Photo::$targets as $target) {
            $photosToRemove = Photo::where('processed', false);

            foreach (Photo::getStyles($target) as $style => $size) {
                $photosToRemove->where('processed_'.$style, true);
            }

            $photosToRemove = $photosToRemove->get();

            foreach ($photosToRemove as $photo) {
                logger()->info('Found Photo #'.$photo->id.' - Uploading original');
                $this->info('Found Photo #'.$photo->id.' - Uploading original');

                //Delete the local version of the folder and the original
                $extension = $photo->extension;
                if (empty($extension)) {
                    $extension = 'jpg';
                }
                $path = str_replace('/original.'.$extension, '', $photo->getPath());

                //Check if both the containing folder still exists
                if (Storage::disk('public')->exists($path)) {
                    //This checks for if the original.ext file actually still exists, only try to upload if it exists
                    if (Storage::disk('public')->exists($photo->getPath())) {
                        $job = (new UploadPhotoToS3($photo, 'original'));

                        //Handle the job right away
                        $job->handle();
                    }

                    //After uploading the original to S3, also delete the folder in locale storage to keep things nice and clean
                    Storage::disk('public')->deleteDirectory($path);

                    logger()->info('Photo #'.$photo->id.' - Deleted original');
                    $this->info('Photo #'.$photo->id.' - Deleted original');
                }

                $photo->update(['processed' => true]);
            }
        }
    }
}
