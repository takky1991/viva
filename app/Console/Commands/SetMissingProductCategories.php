<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Product;

class SetMissingProductCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:set_missing_product_categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates missing categories for every product by looking for the parent category.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        Product::chunk(100, function ($products) {
            foreach ($products as $product) {
                $categoryIds = [];
                $categories = $product->categories;

                foreach ($categories as $category) {
                    $categoryIds[] = $category->id;
                    $parentCategory = $category->parent;

                    while (! empty($parentCategory)) {
                        $categoryIds[] = $parentCategory->id;
                        $parentCategory = $parentCategory->parent;
                    }
                }

                $categoryIds = array_unique($categoryIds);
                $categoryIds = array_values($categoryIds);
                $product->categories()->sync($categoryIds);
            }
        });
    }
}
