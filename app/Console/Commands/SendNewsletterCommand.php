<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Services\Mailgun\MailgunClient;
use Viva\NewsletterContact;

class SendNewsletterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:send_newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send newsletter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        NewsletterContact::chunk(500, function ($contacts) {
            $client = new MailgunClient();
            $batchMessage = $client->getBatchMessageBuilder();
            $batchMessage->setSubject('-30% NOVELIUS hydrocollagen - ZADNJA ŠANSA  | ' . config('app.name'));
            $batchMessage->setTemplate('novelius-signup');

            foreach ($contacts as $contact) {
                $batchMessage->addToRecipient($contact->email, [
                    'unsubscribe_link' => route('newsletter_unsubscribe', ['token' => $contact->token]),
                ]);
            }

            $batchMessage->finalize();
        });
    }
}
