<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Viva\Product;

class GenerateProductCategoriesHierarchy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:generate_products_categories_hierarchy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates categories_hierarchy for every product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        Product::withTrashed()->chunk(100, function ($products) {
            foreach ($products as $product) {

                // we don't want obervers to fire
                DB::table('products')->where('id', $product->id)->update(['categories_hierarchy' => $product->generateCategoriesHierarchy()]);
            }
        });
    }
}
