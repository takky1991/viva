<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Viva\Brand;
use Viva\Category;

class CalculateProducts30DaysStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:calculate_products_30_days_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates stats for products in the last 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        DB::table('products')
        ->update([
            'buy_count_30_days' => DB::raw('(select IFNULL(sum(order_products.quantity), 0) from order_products where order_products.product_id = products.id AND order_products.created_at > NOW() - INTERVAL 30 DAY)'),
        ]);

        // best_seller_in_brand
        DB::table('products')->update(['best_seller_in_brand' => false]);
        Brand::chunk(10, function ($brands) {
            foreach ($brands as $brand) {
                $limit = 3;
                $productsCount = $brand->publishedProducts()->count();

                if ($productsCount > 30) {
                    $limit = ceil($productsCount / 10);
                }

                $products = $brand->publishedProducts()
                    ->where('in_stock', true)
                    ->where('buy_count_30_days', '>', 0)
                    ->orderBy('buy_count_30_days', 'DESC')
                    ->limit($limit)
                    ->get();

                DB::table('products')
                    ->whereIn('id', $products->pluck('id')->toArray())
                    ->update(['best_seller_in_brand' => true]);
            }
        });

        // best_seller_in_category
        DB::table('products')->update(['best_seller_in_category' => false]);
        DB::table('category_product')->update(['best_seller_in_category' => false]);
        Category::chunk(10, function ($categories) {
            foreach ($categories as $category) {
                $limit = 3;
                $productsCount = $category->publishedProducts()->count();

                if ($productsCount > 30) {
                    $limit = ceil($productsCount / 10);
                }

                $products = $category->publishedProducts()
                    ->where('in_stock', true)
                    ->where('buy_count_30_days', '>', 0)
                    ->orderBy('buy_count_30_days', 'DESC')
                    ->limit($limit)
                    ->get();

                $productIds = $products->pluck('id')->toArray();

                DB::table('category_product')
                    ->where('category_id', $category->id)
                    ->whereIn('product_id', $productIds)
                    ->update(['best_seller_in_category' => true]);

                DB::table('products')
                    ->whereIn('id', $productIds)
                    ->update(['best_seller_in_category' => true]);
            }
        });
    }
}
