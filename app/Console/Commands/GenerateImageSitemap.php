<?php

namespace Viva\Console\Commands;

use DOMDocument;
use Illuminate\Console\Command;
use SimpleXMLElement;
use Viva\Gateways\ArticlesGateway;
use Viva\Gateways\ProductsGateway;

class GenerateImageSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:generate_image_sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate image sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        //create your XML document, using the namespaces
        $urlset = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" /><!--?xml version="1.0" encoding="UTF-8"?-->');

        (new ProductsGateway)
            ->getItems()
            ->with('slugs', 'photos')
            ->chunk(100, function ($products) use ($urlset) {
                foreach ($products as $product) {
                    if ($product->photos->isNotEmpty()) {
                        //add the page URL to the XML urlset
                        $url = $urlset->addChild('url');
                        $url->addChild('loc', $product->url());

                        foreach ($product->photos as $photo) {
                            if ($photo->processed) {
                                //add an image
                                $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
                                $image->addChild('image:loc', $photo->url(), 'http://www.google.com/schemas/sitemap-image/1.1');
                                $image->addChild('image:caption', htmlspecialchars($product->fullName()), 'http://www.google.com/schemas/sitemap-image/1.1');
                                $image->addChild('image:title', htmlspecialchars($product->fullName()), 'http://www.google.com/schemas/sitemap-image/1.1');
                            }
                        }
                    }
                }
            });

        (new ArticlesGateway)
            ->getItems()
            ->with('slugs', 'photo')
            ->chunk(100, function ($articles) use ($urlset) {
                foreach ($articles as $article) {
                    if ($article->photo) {
                        //add the page URL to the XML urlset
                        $url = $urlset->addChild('url');
                        $url->addChild('loc', $article->url());
                        if ($article->photo->processed) {
                            //add an image
                            $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
                            $image->addChild('image:loc', $article->photo->url(), 'http://www.google.com/schemas/sitemap-image/1.1');
                            $image->addChild('image:caption', htmlspecialchars($article->title), 'http://www.google.com/schemas/sitemap-image/1.1');
                            $image->addChild('image:title', htmlspecialchars($article->title), 'http://www.google.com/schemas/sitemap-image/1.1');
                        }
                    }
                }
            });

        //add whitespaces to xml output (optional, of course)
        $dom = new DomDocument();
        $dom->loadXML($urlset->asXML());
        $dom->formatOutput = true;

        //output xml
        $dom->save(public_path().'/sitemapimages.xml');
    }
}
