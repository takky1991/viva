<?php

namespace Viva\Console\Commands;

use Viva\Order;
use Illuminate\Console\Command;
use Viva\Notifications\OrderProductReviewCtaNotification;

class SendReviewCtaNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:send_review_cta_notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for orders to send product review CTA notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $orders = Order::where('review_cta_sent', false)
            ->where('status', 'fulfilled')
            ->where('created_at', '<', now()->subDays(14))
            ->get();

        foreach ($orders as $order) {
            $order->notify(new OrderProductReviewCtaNotification());
            $order->review_cta_sent = true;
            $order->save();
        }
    }
}
