<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\DiscountCollection;

class ManageDiscounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:manage_discounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for discounts to enable / disable';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Enable discounts
        $discountCollections =
            DiscountCollection::where('start_at', '<=', now())
                ->where('end_at', '>', now())
                ->where('active', false)
                ->get();

        foreach ($discountCollections as $discountCollection) {
            $discountCollection->enable();
        }

        // Disable discounts
        $discountCollections =
            DiscountCollection::where('start_at', '<', now())
                ->where('end_at', '<', now())
                ->where('active', true)
                ->get();

        foreach ($discountCollections as $discountCollection) {
            $discountCollection->disable();
        }
    }
}
