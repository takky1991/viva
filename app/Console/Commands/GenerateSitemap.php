<?php

namespace Viva\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Spatie\Sitemap\Sitemap;
use Viva\Brand;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\Gateways\ArticlesGateway;
use Viva\Gateways\ProductsGateway;
use Viva\Mail\Blank;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:generate_sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $path = public_path('sitemap.xml');

        // Pages
        $sitemap = Sitemap::create()
            ->add(route('home'))
            ->add(route('brands.index'))
            ->add(route('articles.index'))
            ->add(route('products.index'))
            ->add(route('discount_collections.index'))
            ->add(route('login'))
            ->add(route('register'))
            ->add(route('password.request'))
            ->add(route('locations'))
            ->add(route('paying'))
            ->add(route('delivery'))
            ->add(route('returns'))
            ->add(route('cookie_policy'))
            ->add(route('privacy_policy'))
            ->add(route('terms_of_purchase'))
            ->add(route('legal_notice'));

        // Category pages
        Category::with('slugs')->chunk(100, function ($categories) use ($sitemap) {
            foreach ($categories as $category) {
                $sitemap->add(route('categories.show', ['category' => $category->getSlug()]));
            }
        });

        // Brand pages
        Brand::with('slugs')->chunk(100, function ($brands) use ($sitemap) {
            foreach ($brands as $brand) {
                $sitemap->add(route('brands.show', ['brand' => $brand->getSlug()]));

                $availableCategories = Category::whereNotNull('parent_id')
                ->whereHas('publishedProducts', function ($query) use ($brand) {
                    $query->where('brand_id', $brand->id);
                })->get();

                foreach ($availableCategories as $availableCategory) {
                    $sitemap->add(route('brands.show', ['brand' => $brand->getSlug(), 'category' => $availableCategory->getSlug()]));
                }
            }
        });

        // Product pages
        (new ProductsGateway)
            ->getItems()
            ->with('slugs')
            ->chunk(200, function ($products) use ($sitemap) {
                foreach ($products as $product) {
                    $sitemap->add($product->url());
                }
            });

        // Article pages
        (new ArticlesGateway)
            ->getItems()
            ->with('slugs')
            ->chunk(200, function ($articles) use ($sitemap) {
                foreach ($articles as $article) {
                    $sitemap->add(route('articles.show', ['article' => $article->getSlug()]));
                }
            });

        // Discount pages
        DiscountCollection::where('active', true)
            ->with('slugs')
            ->chunk(200, function ($discounts) use ($sitemap) {
                foreach ($discounts as $discount) {
                    $sitemap->add(route('discount_collections.show', ['discountCollection' => $discount->getSlug()]));
                }
            });

        $sitemap->writeToFile($path);

        $this->pingSearchEngines();
    }

    /**
     * Important: When adding a new url to a sitemap, you can alert the search engines through so-called “pinging”.
     * They will know about the changes immediately, and they can then index the new content.
     */
    private function pingSearchEngines()
    {
        if (app()->environment() == 'production') {
            /**
             * Ping Google
             * Examlpe: $this->ping('http://www.google.com/ping?sitemap=http://example.com/sitemap-path.xml');
             */
            $this->ping('http://www.google.com/ping?sitemap='.url('/sitemap.xml'));

        /**
         * Ping Bing/MSN
         * Examlpe: $this->ping('http://www.bing.com/ping?sitemap=http://example.com/sitemap-path.xml');
         */
            //$this->ping('http://www.bing.com/ping?sitemap=' . url('/sitemap.xml'));
        } else {
            $this->warn('Cannot ping search engines while working locally or testing...');
        }
    }

    /**
     * Ping a given url so it can re-index the sitemap
     */
    private function ping(string $url): void
    {
        $client = new Client([
            'timeout' => 5,
            'allow_redirects' => false,
        ]);

        try {
            $response = $client->request('GET', $url);

            $this->info('Successfully pinged - '.$url);
        } catch (ClientException $e) {
            $this->error('URL('.$url.') '.$e->getMessage());
            $this->sendFailEmail([
                'code' => $e->getCode(),
                'exception' => 'ClientException',
                'url' => $url,
                'message' => $e->getMessage(),
            ]);
        } catch (ConnectException $e) {
            $this->error('URL('.$url.') '.$e->getMessage());
            $this->sendFailEmail([
                'code' => $e->getCode(),
                'exception' => 'ConnectException',
                'url' => $url,
                'message' => $e->getMessage(),
            ]);
        } catch (ServerException $e) {
            $this->error('URL('.$url.') '.$e->getMessage());
            $this->sendFailEmail([
                'code' => $e->getCode(),
                'exception' => 'ServerException',
                'url' => $url,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Send email if a ping to a search engine fails.
     */
    private function sendFailEmail(array $options): void
    {
        $content =
            '<p><strong>Code:</strong> '.$options['code'].'</p>'.
            '<p><strong>Exception:</strong> '.(! empty($options['exception']) ? $options['exception'] : '').'</p>'.
            '<p><strong>URL:</strong> '.$options['url'].'</p>'.
            '<p><strong>Message:</strong> '.$options['message'].'</p>';

        Mail::to('takky1991@gmail.com')->send(new Blank('Error: GenerateSitemap - failed search engine ping.', $content));
    }
}
