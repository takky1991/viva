<?php

namespace Viva\Console\Commands;

use Illuminate\Console\Command;
use Viva\Product;

class GenerateSKUForProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viva:generate_products_sku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates SKU for products where empty.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $count = Product::whereNull('sku')->count();
        $bar = $this->output->createProgressBar($count);
        $bar->start();

        Product::whereNull('sku')->chunk(100, function ($products) use ($bar) {
            foreach ($products as $product) {
                do {
                    $sku = randomCode(7);
                } while (Product::where('sku', $sku)->exists());

                $product->sku = $sku;
                $product->save();
            }

            $bar->advance(100);
        });

        $bar->finish();
    }
}
