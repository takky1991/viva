<?php

namespace Viva\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // Minutely
        $schedule->command('viva:manage_discounts')->everyFiveMinutes();

        // Daily
        //$schedule->command('viva:cancel_expired_pickup_orders')->daily()->at('00:10');
        $schedule->command('viva:delete_expited_guest_carts')->daily()->at('00:20');
        $schedule->command('viva:delete_expired_processing_payment_orders')->daily()->at('00:30');
        $schedule->command('viva:clear_uploaded_photos')->daily()->at('00:40');
        $schedule->command('viva:calculate_products_30_days_stats')->daily()->at('00:50');
        $schedule->command('viva:generate_sitemap')->daily()->at('01:00');
        $schedule->command('viva:generate_image_sitemap')->daily()->at('01:10');
        $schedule->command('viva:order_stats daily')->daily()->at('08:00');
        $schedule->command('viva:send_30_day_no_order_reminder_email')->daily()->at('10:00');
        $schedule->command('viva:send_review_cta_notifications')->daily()->at('18:00');

        // Monthly
        $schedule->command('viva:order_stats monthly')->monthlyOn(1, '08:00');
        $schedule->command('viva:clear_email_logs')->monthly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
