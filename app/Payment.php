<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'invoice_id',
        'delivered_at',
        'pay_until',
        'amount',
        'discount',
        'paid',
        'discount_paid',
        'locked',
    ];

    protected $casts = [
        'delivered_at' => 'datetime',
        'pay_until' => 'datetime',
    ];

    public function supplier()
    {
        return $this->belongsTo(\Viva\Supplier::class);
    }

    public function user()
    {
        return $this->belongsTo(\Viva\User::class);
    }
}
