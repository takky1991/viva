<?php

namespace Viva;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PhotoProduct extends Pivot
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    protected $fillable = [
        'product_id',
        'photo_id',
        'order_id',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class)->withTrashed();
    }
}
