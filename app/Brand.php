<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Viva\Traits\SluggableTrait;

class Brand extends Model
{
    use HasFactory;
    use Searchable;
    use SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'featured',
    ];

    protected $sluggableColumn = 'name';

    public function products()
    {
        return $this->hasMany(\Viva\Product::class);
    }

    public function publishedProducts()
    {
        return $this->products()->where('published', true);
    }

    public function unpublishedProducts()
    {
        return $this->products()->where('published', false);
    }

    public function slugs()
    {
        return $this->hasMany(\Viva\BrandSlug::class)->orderBy('created_at', 'desc');
    }

    public function slug()
    {
        return $this->slugs?->first();
    }

    public function getSlug()
    {
        return $this->slug()?->slug;
    }

    /**
     * Get the indexable data array for the model.
     */
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => route('brands.show', ['brand' => $this->getSlug()]),
        ];
    }
}
