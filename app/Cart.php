<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Viva\Http\Resources\CartResource;
use Viva\Interfaces\CartInterface;

class Cart extends Model implements CartInterface
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipping_method_id',
        'product_removed',
        'product_price_changed',
        'free_shipping',
        'first_name',
        'last_name',
        'phone',
        'street_address',
        'postalcode',
        'city',
        'country_id',
        'additional_info',
        'source',
    ];

    public function products()
    {
        return $this->belongsToMany(\Viva\Product::class)->using(\Viva\CartProduct::class)->withPivot(['quantity', 'variants'])->withTimestamps()->orderBy('cart_product.created_at', 'desc');
    }

    public function user()
    {
        return $this->belongsTo(\Viva\User::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(\Viva\ShippingMethod::class);
    }

    public function toSearchableJson()
    {
        return json_encode(new CartResource($this));
    }

    public function totalPrice(): float
    {
        $totalPrice = 0.00;

        foreach ($this->products as $product) {
            $totalPrice += $product->pivot->quantity * $product->realPrice();
        }

        return $totalPrice;
    }

    public function totalPriceWithShipping(): float
    {
        if ($this->shippingMethod && ! $this->free_shipping) {
            return $this->totalPrice() + $this->shippingMethod->price;
        }

        return $this->totalPrice();
    }
}
