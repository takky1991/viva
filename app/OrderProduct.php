<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Viva\Interfaces\ProductInterface;

class OrderProduct extends Model implements ProductInterface
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'brand_id',
        'brand_name',
        'brand_description',
        'title',
        'package_description',
        'description',
        'price',
        'discount_price',
        'on_sale',
        'real_price',
        'published',
        'in_stock',
        'free_shipping',
        'quantity',
        'variants',
        'total_price',
        'categories_hierarchy',
        'added_to_cart_at',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }

    public function brand()
    {
        return $this->belongsTo(\Viva\Brand::class);
    }

    public function onSale(): bool
    {
        return $this->on_sale && ! empty($this->discount_price);
    }

    public function realPrice(): float
    {
        return $this->onSale() ? $this->discount_price : $this->price;
    }

    public function name(): string
    {
        return strlen($this->title) < 5 ? $this->nameWithBrand() : $this->title;
    }

    public function nameWithBrand(): string
    {
        return $this->brand->name.' '.$this->title;
    }

    public function fullName(): string
    {
        return $this->brand_name.' '.$this->title.' '.$this->package_description;
    }
}
