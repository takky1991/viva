<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'content',
        'rating',
        'verified_buyer',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }
}
