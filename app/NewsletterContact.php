<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class NewsletterContact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'token',
        'campaign'
    ];
}
