<?php

namespace Viva;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Viva\Notifications\ResetPasswordNotification;
use Viva\Notifications\VerifyEmail;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;
    use HasRoles;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'street_address',
        'postalcode',
        'city',
        'country_id',
        'terms_accepted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'birth_date' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function canAccessBackend()
    {
        return $this->hasRole(['admin', 'employee', 'writer']);
    }

    public function payments()
    {
        return $this->hasMany(\Viva\Payment::class);
    }

    public function cart()
    {
        return $this->hasOne(\Viva\Cart::class);
    }

    public function newsletterContact()
    {
        return $this->hasOne(\Viva\NewsletterContact::class);
    }

    public function country()
    {
        return $this->belongsTo(\Viva\Country::class);
    }

    public function orders()
    {
        return $this->hasMany(\Viva\Order::class)->orderBy('created_at', 'DESC');
    }

    public function syncFromGuestCart($guestId)
    {
        $guestCart = GuestCart::where('guest_id', $guestId)->first();

        if (! empty($guestCart) && $guestCart->products->isNotEmpty()) {
            $cart = $this->cart ?? $this->cart()->create();
            $cart->update(collect($guestCart->toArray())->except(['id', 'guest_id', 'expired_at', 'created_at', 'updated_at'])->toArray());

            foreach ($guestCart->products as $product) {
                $existingProduct = $cart->products()->find($product->id);

                if (empty($existingProduct)) {
                    $cart->products()->attach($product->id, [
                        'quantity' => $product->pivot->quantity,
                        'variants' => $product->pivot->variants,
                    ]);
                } else {
                    $cart->products()->updateExistingPivot($product->id, [
                        'quantity' => $product->pivot->quantity + $existingProduct->pivot->quantity,
                        'variants' => $existingProduct->pivot->variants.','.$product->pivot->variants,
                    ]);
                }
            }

            // Delete guest cart
            $guestCart->delete();
        }
    }
}
