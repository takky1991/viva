<?php

namespace Viva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;
use Viva\Http\Resources\PhotoResource;
use Viva\Http\Resources\ProductResource;
use Viva\Interfaces\ProductInterface;
use Viva\Notifications\BackToStockNotification;
use Viva\Traits\SluggableTrait;

class Product extends Model implements ProductInterface
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use SluggableTrait;

    protected $sluggableColumn = 'title';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'title',
        'package_description',
        'description',
        'price',
        'discount_price',
        'on_sale',
        'published',
        'in_stock',
        'free_shipping',
        'featured',
        'featured_text'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'float',
        'discount_price' => 'float',
    ];

    public function categories()
    {
        return $this->belongsToMany(\Viva\Category::class)->withPivot('best_seller_in_category');
    }

    public function menuCategories()
    {
        return $this->categories()->where('categories.menu', true);
    }

    public function carts()
    {
        return $this->belongsToMany(\Viva\Cart::class)->using(\Viva\CartProduct::class)->withPivot(['quantity', 'variants']);
    }

    public function guestCarts()
    {
        return $this->belongsToMany(\Viva\GuestCart::class)->using(\Viva\GuestCartProduct::class)->withPivot(['quantity', 'variants']);
    }

    public function orderProducts()
    {
        return $this->hasMany(\Viva\OrderProduct::class);
    }

    public function cart(int $cartId)
    {
        return $this->carts->where('id', $cartId)->first();
    }

    public function brand()
    {
        return $this->belongsTo(\Viva\Brand::class);
    }

    public function guestCart(int $cartId)
    {
        return $this->guestCarts->where('id', $cartId)->first();
    }

    public function photos()
    {
        return $this->belongsToMany(\Viva\Photo::class)->with('photoProduct')->orderBy('photo_product.order_id');
    }

    public function backToStockAlerts()
    {
        return $this->hasMany(\Viva\BackToStockAlert::class);
    }

    public function notSentBackToStockAlerts()
    {
        return $this->backToStockAlerts()->where('notification_sent', false);
    }

    public function hasPhotos()
    {
        return $this->photos->isNotEmpty();
    }

    public function photosForSlickGallery()
    {
        $photos = [];

        if ($this->hasPhotos()) {
            foreach ($this->photos as $photo) {
                $photos[] = [
                    'urls' => [
                        'large' => $photo->url('product_show'),
                        'profile' => $photo->url('product_profile'),
                    ],
                ];
            }
        }

        return $photos;
    }

    public function mainPhoto()
    {
        return $this->photos->first();
    }

    public function slugs()
    {
        return $this->hasMany(\Viva\ProductSlug::class)->orderBy('created_at', 'desc');
    }

    public function reviews()
    {
        return $this->hasMany(\Viva\Review::class)->orderBy('created_at', 'desc');
    }

    public function variants()
    {
        return $this->hasMany(\Viva\ProductVariant::class)->orderBy('name', 'asc');
    }

    public function publishedReviews()
    {
        return $this->reviews()->where('published', true);
    }

    public function slug()
    {
        return $this->slugs?->first();
    }

    public function getSlug()
    {
        return $this->slug()?->slug;
    }

    public function getPhotosJson()
    {
        return json_encode(PhotoResource::collection($this->photos));
    }

    public function toSearchableJson()
    {
        return json_encode(new ProductResource($this));
    }

    public function onSale(): bool
    {
        return $this->on_sale && ! empty($this->discount_price);
    }

    public function realPrice(): float
    {
        return $this->onSale() ? $this->discount_price : $this->price;
    }

    public function url(): string
    {
        return route('products.show', ['product' => $this->getSlug()]);
    }

    public function name(): string
    {
        return strlen($this->title) < 5 ? $this->nameWithBrand() : $this->title;
    }

    public function nameWithBrand(): string
    {
        return $this->brand->name.' '.$this->title;
    }

    public function fullName(): string
    {
        return $this->brand->name.' '.$this->title.' '.$this->package_description;
    }

    public function generateCategoriesHierarchy(): string
    {
        $categories = [];
        $category = $this->menuCategories()->where('parent_id', null)->first();
        $categoryIds = $this->menuCategories()->pluck('categories.id')->toArray();

        while ($category) {
            $categories[] = $category->title;
            $category = $category->menuChildren()->whereIn('id', $categoryIds)->first();
        }

        return implode('/', $categories);
    }

    /**
     * Get the indexable data array for the model.
     */
    public function toSearchableArray(): array
    {
        if (! $this->published) {
            $this->unsearchable();

            return [];
        }

        return [
            'id' => $this->id,
            'full_name' => $this->fullName(),
            'brand' => $this->brand?->name,
            'url' => $this->url(),
            'photo' => $this->hasPhotos() ? $this->mainPhoto()->url('product_profile') : null,
            'in_stock' => $this->in_stock,
            'rating_count' => $this->rating_count,
            'rating' => $this->rating,
            'buy_count' => $this->buy_count,
        ];
    }

    public function hasFreeShipping()
    {
        return $this->free_shipping || $this->realPrice() >= (float) config('app.free_shipping_threshold');
    }

    public function openHit()
    {
        DB::table('products')->where('id', $this->id)->increment('open_count');
    }

    public function addToCartHit()
    {
        DB::table('products')->where('id', $this->id)->increment('add_to_cart_count');
    }

    public function buyHit($quantity)
    {
        DB::table('products')->where('id', $this->id)->increment('buy_count', $quantity);
    }

    public function getDiscountPercentage()
    {
        return round((($this->price - $this->discount_price) / $this->price) * 100);
    }

    public function calculateRating()
    {
        $this->rating_count = $this->publishedReviews->count();

        if ($this->rating_count == 0) {
            $this->rating = null;
            $this->save();

            return;
        }

        $rating = 0;
        $ratingSum = $this->publishedReviews->sum('rating');
        $rating = round($ratingSum / $this->rating_count);

        $this->rating = $rating;
        $this->save();
    }

    public function notifyBackToStockAlerts()
    {
        foreach ($this->notSentBackToStockAlerts as $alert) {
            $alert->notify(new BackToStockNotification());
            $alert->update(['notification_sent' => true]);
        }
    }

    public function categoriesHierarchyAsArray()
    {
        return explode('/', $this->categories_hierarchy);
    }
}
