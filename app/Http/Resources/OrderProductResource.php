<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'full_name' => $this->fullName(),
            'real_price' => (float) $this->realPrice(),
            'brand_name' => $this->brand_name,
            'quantity' => $this->quantity,
            'variants' => $this->variants,
            'categories_hierarchy' => $this->categories_hierarchy,
        ];
    }
}
