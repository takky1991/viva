<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Viva\Photo;

class PhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        $urls = [];

        foreach (Photo::getStyles() as $style => $dimensions) {
            $urls[$style] = $this->url($style);
        }

        return [
            'id' => (int) $this->id,
            'user_id' => (int) $this->user_id,
            'title' => $this->title,
            'filename' => $this->file_name,
            'filesize' => $this->file_size,
            'urls' => $urls,
            'order_id' => $this->when($this->relationLoaded('photoProduct'),
                function () {
                    return (int) $this->photoProduct->order_id;
                }
            ),
        ];
    }
}
