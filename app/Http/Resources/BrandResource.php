<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'name' => $this->name,
            'url' => $this->when($this->relationLoaded('slugs'), function () {
                return route('brands.show', ['brand' => $this->getSlug()]);
            }),
        ];
    }
}
