<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'user_id' => $this->user_id,
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'free_shipping' => (bool) $this->free_shipping,
        ];
    }
}
