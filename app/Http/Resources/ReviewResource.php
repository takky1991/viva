<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'name' => $this->name,
            'content' => $this->content,
            'rating' => (int) $this->rating,
            'verified_buyer' => (bool) $this->verified_buyer,
            'created_at' => $this->created_at->format('d.m.Y'),
        ];
    }
}
