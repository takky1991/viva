<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'title' => $this->title,
            'name' => $this->name(),
            'full_name' => $this->fullName(),
            'name_with_brand' => $this->nameWithBrand(),
            'package_description' => $this->package_description,
            'description' => $this->description,
            'price' => (float) $this->price,
            'discount_price' => (float) $this->discount_price,
            'on_sale' => (bool) $this->on_sale,
            'in_stock' => (bool) $this->in_stock,
            'sku' => $this->sku,
            'free_shipping' => (bool) $this->free_shipping,
            'real_price' => (float) $this->realPrice(),
            'photos' => PhotoResource::collection($this->whenLoaded('photos')),
            'categories_hierarchy' => $this->categories_hierarchy,
            'quantity' => $this->when($this->relationLoaded('pivot'), function () {
                return (int) $this->pivot->quantity;
            }),
            'variants_string' => $this->when($this->relationLoaded('pivot'), function () {
                return $this->pivot->variants;
            }),
            'total_price' => $this->when($this->relationLoaded('pivot'), function () {
                return (float) ($this->pivot->quantity * $this->realPrice());
            }),
            'slug' => $this->when($this->relationLoaded('slugs'), function () {
                return $this->getSlug();
            }),
            'brand' => $this->when($this->relationLoaded('brand'), function () {
                return new BrandResource($this->brand);
            }),
            'variants' => ProductVariantResource::collection($this->whenLoaded('variants')),
        ];
    }
}
