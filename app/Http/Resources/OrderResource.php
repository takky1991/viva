<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'order_number' => $this->order_number,
            'total_price' => (float) $this->total_price,
            'shipping_price' => (float) $this->shipping_price,
            'orderProducts' => OrderProductResource::collection($this->whenLoaded('orderProducts')),
        ];
    }
}
