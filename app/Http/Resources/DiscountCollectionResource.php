<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'name' => $this->name,
            'discount_pecentage' => (int) $this->discount_pecentage,
            'active' => (bool) $this->active,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
        ];
    }
}
