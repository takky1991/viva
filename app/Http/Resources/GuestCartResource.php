<?php

namespace Viva\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GuestCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => (int) $this->id,
            'guest_id' => $this->guest_id,
            'expires_at' => $this->expires_at->toJSON(),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'free_shipping' => (bool) $this->free_shipping,
        ];
    }
}
