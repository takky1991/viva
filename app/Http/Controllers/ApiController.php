<?php

namespace Viva\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    public function respondWithDeleted(): JsonResponse
    {
        return response()->json([], 204);
    }

    public function respondWithErrors($errors = []): JsonResponse
    {
        return response()->json(['errors' => $errors], 400);
    }

    public function respondWithOk($message = 'OK'): JsonResponse
    {
        return response()->json(['message' => $message], 200);
    }

    public function respondWithNotFound($message): JsonResponse
    {
        return response()->json(['message' => $message], 404);
    }
}
