<?php

namespace Viva\Http\Controllers;

use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\GuestCart;
use Viva\Http\Requests\UpdateCartRequest;
use Viva\Product;

class CartController extends Controller
{
    /**
     * Show the application dashboard.
     */
    public function index(): View
    {
        $this->setTitle('Korpa');

        $cart = null;

        if (auth()->check() && auth()->user()->cart) {
            $cart = auth()->user()->cart->load('products.slugs', 'products.photos', 'products.brand');
        } elseif (session('guest_id')) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->with('products.slugs', 'products.photos', 'products.brand')->first();
        }

        return view('checkout/cart', [
            'cart' => $cart,
        ]);
    }

    public function update(UpdateCartRequest $request): RedirectResponse
    {
        $cart = null;
        $products = $request->products;

        if (auth()->check()) {
            $cart = auth()->user()->cart;
        } elseif (session('guest_id')) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->with('products')->first();
        }

        if (empty($cart)) {
            throw new Exception('No cart found');
        }

        foreach ($products as $product) {
            if (empty($product['quantity'])) {
                continue;
            }

            $prod = Product::find($product['id']);

            if ($prod->variants->isNotEmpty()) {
                continue;
            }

            $existingProduct = $cart->products->find($product['id']);

            if ($existingProduct) {
                $cart->products()->updateExistingPivot($product['id'], ['quantity' => $product['quantity']]);
            } else {
                $cart->products()->attach($product['id'], ['quantity' => $product['quantity']]);
            }
        }

        session()->flash('success', 'Korpa je uspješno ažurirana.');

        return redirect()->route('cart.show');
    }

    public function deleteProduct(Product $product): RedirectResponse
    {
        $cart = null;

        if (auth()->check()) {
            $cart = auth()->user()->cart;
        } elseif (session('guest_id')) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->with('products')->first();
        }

        if (empty($cart)) {
            throw new Exception('No cart found');
        }

        $cart->products()->detach($product->id);

        session()->flash('success', 'Proizvod je uspješno uklonjen iz korpe.');

        return redirect()->route('cart.show');
    }
}
