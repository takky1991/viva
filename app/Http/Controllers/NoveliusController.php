<?php

namespace Viva\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\NewsletterContact;
use Viva\Services\Mailgun\MailgunClient;
use Viva\Services\Mailgun\MailgunClientException;
use Viva\Http\Requests\NoveliusSignupRequest;

use function Sentry\captureException;

class NoveliusController extends Controller
{
    /**
     * Get view
     *
     * @return View
     */
    public function getNoveliusBlackFriday(): View
    {
        return view('pages/novelius');
    }

    /**
     * Creates a new newsletter contact and sends a welcome email
     *
     * @param NoveliusSignupRequest $request
     * @return RedirectResponse
     */
    public function postNoveliusBlackFriday(NoveliusSignupRequest $request): RedirectResponse
    {
        $newsletterContact = NewsletterContact::where('email', $request->email)->first();

        if($newsletterContact) {
            $newsletterContact->update(['campaign' => 'novelius-black-friday-signup']);
        } else {
            $newsletterContact = NewsletterContact::create([
                'first_name' => $request->first_name,
                'email' => $request->email,
                'campaign' => 'novelius-black-friday-signup',
                'token' => random_token()
            ]);
        }

        $client = new MailgunClient();
        $builder = $client->getMessageBuilder();
        $builder->addToRecipient($newsletterContact->email, ['first' => $newsletterContact->first_name]);
        $builder->setSubject("Najveći BLACK FRIDAY POPUST na NOVELIUS MEDICAL 🔥");
        $builder->setTemplate('novelius-signup');

        try {
            $client->sendMessage($builder);
        } catch(MailgunClientException $e) {
            captureException($e);
        }

        return redirect()->route('novelius_black_friday_success');
    }

    /**
     * Returns a view
     *
     * @return View
     */
    public function successNoveliusBlackFriday(): View
    {
        return view('pages.novelius_success');
    }
}
