<?php

namespace Viva\Http\Controllers;

use Illuminate\View\View;
use Viva\ShippingMethod;

class PagesController extends Controller
{
    public $openGraph;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
    }

    public function getLocations(): View
    {
        $this->setTitle('Naše apoteke');
        $this->setDescription('Potražite naše apoteke. Pronađite informacije o radnim vremenima apoteka, njihovim lokacijama i kontakt informacije.');

        $this->openGraph
            ->set('og:title', 'Naše apoteke')
            ->set('og:description', 'Potražite naše apoteke. Pronađite informacije o radnim vremenima apoteka, njihovim lokacijama i kontakt informacije.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Naše apoteke',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/locations');
    }

    public function getPaying(): View
    {
        $this->setTitle('Način plaćanja');
        $this->setDescription('Informiši se kako možeš plaćati na ' . config('app.name') . ' web shopu i naruči svoje omiljene proizvode uz dostavu od samo ' . config('app.lowest_shipping_cost') . 'KM.');

        $this->openGraph
            ->set('og:title', 'Način plaćanja')
            ->set('og:description', 'Informiši se kako možeš plaćati na ' . config('app.name') . ' web shopu i naruči svoje omiljene proizvode uz dostavu od samo ' . config('app.lowest_shipping_cost') . 'KM.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Način plaćanja',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/paying');
    }

    public function getDelivery(): View
    {
        $this->setTitle('Dostava');
        $this->setDescription('Informiši se kako dostavljamo Vaše narudžbe na ' . config('app.name') . ' web shopu i naruči svoje omiljene proizvode uz dostavu od samo ' . config('app.lowest_shipping_cost') . 'KM.');

        $this->openGraph
            ->set('og:title', 'Dostava')
            ->set('og:description', 'Informiši se kako dostavljamo Vaše narudžbe na ' . config('app.name') . ' web shopu i naruči svoje omiljene proizvode uz dostavu od samo ' . config('app.lowest_shipping_cost') . 'KM.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Dostava',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $shippingMethods = ShippingMethod::where('pickup', false)->orderBy('order_id')->get();

        return view('pages/delivery', [
            'shippingMethods' => $shippingMethods,
        ]);
    }

    public function getReturns(): View
    {
        $this->setTitle('Povrat robe');
        $this->openGraph->set('og:title', 'Povrat robe');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Povrat robe',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/returns');
    }

    public function getCookiePolicy(): View
    {
        $this->setTitle('Politika kolačića');
        $this->openGraph->set('og:title', 'Politika kolačića');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Politika kolačića',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/cookie_policy');
    }

    public function getPrivacyPolicy(): View
    {
        $this->setTitle('Pravila zaštite privatnosti');
        $this->openGraph->set('og:title', 'Pravila zaštite privatnosti');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Pravila zaštite privatnosti',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/privacy_policy');
    }

    public function getTermsOfPurchase(): View
    {
        $this->setTitle('Uvjeti kupovine');
        $this->openGraph->set('og:title', 'Uvjeti kupovine');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Uvjeti kupovine',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/terms_of_purchase');
    }

    public function getLegalNotice(): View
    {
        $this->setTitle('Pravne napomene');
        $this->openGraph->set('og:title', 'Pravne napomene');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Pravne napomene',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('pages/legal_notice');
    }
}
