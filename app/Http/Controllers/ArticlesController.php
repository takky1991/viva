<?php

namespace Viva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Viva\Article;
use Viva\Brand;
use Viva\Category;
use Viva\Gateways\ArticlesGateway;
use Viva\Product;

class ArticlesController extends Controller
{
    public $openGraph;

    public $articlesGateway;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
        $this->articlesGateway = new ArticlesGateway();
    }

    public function index(Request $request): View
    {
        $this->setTitle('Savjeti i Novosti');
        $this->setDescription('Pronađi mnoge savjete i novosti vezane za kozmetiku i zdravlje. Iskoristi dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $this->openGraph
            ->set('og:title', 'Savjeti i Novosti')
            ->set('og:description', 'Pronađi mnoge savjete i novosti vezane za kozmetiku i zdravlje. Iskoristi dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Savjeti i Novosti',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        if ($request->filled('sq')) {
            $articles = Article::search($request->sq)->paginate($this->articlesGateway->defaultPaginationSize);

            // Lazy load relationships
            $articles->load([
                'photo',
                'slugs',
            ]);
        } else {
            $articles = $this->articlesGateway
                ->getItems($request->all(), true);

            // Eager load relationships
            $articles->with([
                'photo',
                'slugs',
            ]);

            // Paginate
            $articles = $articles->paginate($this->articlesGateway->defaultPaginationSize);
        }

        return view('articles/index', [
            'articles' => $articles,
        ]);
    }

    public function show(Article $article): View
    {
        $this->setTitle($article->title);
        $this->setDescription($article->subtitle);

        $this->openGraph
            ->set('og:title', $article->title)
            ->set('og:description', $article->subtitle);

        if ($article->photo) {
            $this->openGraph->set('og:image', $article->photo->url('article'));
        }

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Savjeti i novosti',
                'link' => route('articles.index'),
            ],
            [
                'label' => $article->title,
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $products = null;
        if (! empty($article->product_ids)) {
            $products = Product::whereIn('id', $article->product_ids)->with('slugs', 'brand.slugs', 'photos')->get();
        }

        $brands = null;
        if (! empty($article->brand_ids)) {
            $brands = Brand::whereIn('id', $article->brand_ids)->with('slugs')->get();
        }

        $categories = null;
        if (! empty($article->category_ids)) {
            $categories = Category::whereIn('id', $article->category_ids)->with('slugs')->get();
        }

        return view('articles/show', [
            'article' => $article,
            'brands' => $brands,
            'categories' => $categories,
            'products' => $products,
        ]);
    }
}
