<?php

namespace Viva\Http\Controllers;

use Illuminate\View\View;
use Viva\Brand;
use Viva\Category;
use Viva\Gateways\ProductsGateway;

class BrandsController extends Controller
{
    public $openGraph;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
        $this->productsGateway = new ProductsGateway();
    }

    public function index(): View
    {
        $this->setTitle('Brendovi');
        $this->setDescription('Otkrij sve naše brendove na '.config('app.name').' i naruči online uz dostavu od samo '.config('app.lowest_shipping_cost').'KM.');

        $this->openGraph
            ->set('og:title', 'Brendovi')
            ->set('og:description', 'Otkrij sve naše brendove na '.config('app.name').' i naruči online uz dostavu od samo '.config('app.lowest_shipping_cost').'KM.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Brendovi',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $brands = Brand::with('slugs')->whereHas('publishedProducts')->orderBy('name', 'ASC')->get();
        $letter = ucfirst(mb_substr($brands->get(0)->name, 0, 1));
        $alphabetGroups = [];

        foreach ($brands as $brand) {
            if ($letter == ucfirst(mb_substr($brand->name, 0, 1))) {
                $alphabetGroups[$letter][] = $brand;
            } else {
                $letter = ucfirst(mb_substr($brand->name, 0, 1));
                $alphabetGroups[$letter][] = $brand;
            }
        }

        return view('brands/index', [
            'alphabetGroups' => $alphabetGroups,
        ]);
    }

    public function show(Brand $brand, Category $category = null): View
    {
        if ($category) {
            $this->setTitle($brand->name.' '.$category->title);
            $this->setDescription('Otkrij sve proizvode brenda '.$brand->name.' u kategoriji '.$category->title.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

            $this->openGraph
                ->set('og:title', $brand->name.' '.$category->title)
                ->set('og:description', 'Otkrij sve proizvode brenda '.$brand->name.' u kategoriji '.$category->title.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');
        } else {
            $this->setTitle($brand->name);
            $this->setDescription('Otkrij sve proizvode brenda '.$brand->name.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

            $this->openGraph
                ->set('og:title', $brand->name)
                ->set('og:description', 'Otkrij sve proizvode brenda '.$brand->name.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');
        }

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Brendovi',
                'link' => route('brands.index'),
            ],
            [
                'label' => $category ? $brand->name.' '.$category->title : $brand->name,
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $filters = [];

        if ($category) {
            $filters['category'] = $category->id;
        }

        $products = $this->productsGateway
            ->getItems($filters)
            ->where('brand_id', $brand->id)
            ->with('photos', 'slugs', 'brand.slugs')
            ->paginate(20);

        $availableCategories = Category::select('id')
            ->whereHas('publishedProducts', function ($query) use ($brand) {
                $query->where('brand_id', $brand->id);
            })
            ->get()
            ->pluck('id')
            ->toArray();

        return view('brands/show', [
            'brand' => $brand,
            'products' => $products,
            'category' => $category,
            'availableCategories' => $availableCategories,
        ]);
    }
}
