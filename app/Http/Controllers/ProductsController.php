<?php

namespace Viva\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Viva\Events\ReviewSubmitted;
use Viva\Exports\ProductsFacebookExport;
use Viva\Exports\ProductsGoogleExport;
use Viva\Gateways\ProductsGateway;
use Viva\Http\Requests\CreateReviewRequest;
use Viva\Order;
use Viva\Product;
use Viva\ShippingMethod;

class ProductsController extends Controller
{
    public $openGraph;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
    }

    public function show(Product $product)
    {
        $product->openHit();
        $product->load('variants');

        // Add free shipping if hasFreeShipping()
        $shippingSegment = $product->hasFreeShipping() ? ' uz BESPLATNU DOSTAVU' : ' uz dostavu '.config('app.lowest_shipping_cost').'KM';
        $this->setTitle($product->fullName());
        $this->setDescription($product->fullName().' za '.formatPrice($product->realPrice()).$shippingSegment.', samo na '.config('app.name').'. Naruči ONLINE.');

        $this->openGraph
            ->set('og:title', $product->fullName())
            ->set('og:description', $product->fullName().' za '.formatPrice($product->realPrice()).$shippingSegment.', samo na '.config('app.name').'. Naruči ONLINE.')
            ->set('og:type', 'product');

        if ($product->mainPhoto()) {
            $this->openGraph
                ->set('og:image', $product->mainPhoto()->url('product_show'))
                ->set('og:image:width', 500)
                ->set('og:image:height', 500)
                ->set('og:image:alt', $product->fullName());
        }

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
        ];

        $category = $product->menuCategories()->whereNull('categories.parent_id')->first();
        $categoryIds = $product->categories->pluck('id')->toArray();
        $lastCategory = null;

        while ($category) {
            $breadcrumbs[] = [
                'label' => $category->title,
                'link' => route('categories.show', ['category' => $category->getSlug()]),
            ];
            $lastCategory = $category;

            $category = $category->menuChildren()->whereIn('id', $categoryIds)->first();
        }

        $breadcrumbs[] = ['label' => $product->fullName()];

        $this->setBreadcrumbs($breadcrumbs);

        // Get random related products by brand
        $brand = $product->brand;
        $relatedProductsByBrand = (new ProductsGateway())
            ->getItems(['inStock' => true, 'sortBy' => false])
            ->where('brand_id', $brand->id)
            ->where('id', '!=', $product->id)
            ->with([
                'photos',
                'slugs',
                'brand.slugs',
            ])
            ->inRandomOrder()
            ->limit(12)
            ->get();

        $relatedProductsByBrandIds = $relatedProductsByBrand->pluck('id')->toArray();
        $relatedProductsByBrandIds[] = $product->id;

        // Get random related products by category
        $relatedProductsByCategory = null;
        if ($lastCategory) {
            $relatedProductsByCategory = (new ProductsGateway())
                ->getItems(['inStock' => true, 'sortBy' => false])
                ->whereNotIn('id', $relatedProductsByBrandIds)
                ->whereHas('menuCategories', function ($q) use ($lastCategory) {
                    return $q->where('categories.id', $lastCategory->id);
                })
                ->with([
                    'photos',
                    'slugs',
                    'brand.slugs',
                ])
                ->inRandomOrder()
                ->limit(12)
                ->get();
        }

        $shippingMethods = Cache::remember('shippingMethodsPickup', 2592000, function () { //cache for a month
            return ShippingMethod::where('pickup', false)->orderBy('order_id')->get();
        });

        return view('products/show', [
            'product' => $product,
            'lastCategory' => $lastCategory,
            'shippingMethods' => $shippingMethods,
            'relatedProductsByBrand' => $relatedProductsByBrand,
            'relatedProductsByCategory' => $relatedProductsByCategory,
        ]);
    }

    public function exportFacebookFeed()
    {
        return Excel::download(new ProductsFacebookExport(), 'products.csv');
    }

    public function exportGoogleFeed()
    {
        return Excel::download(new ProductsGoogleExport(), 'products.csv');
    }

    public function index(Request $request): View
    {
        $this->setTitle('Proizvodi');
        $this->openGraph->set('og:title', 'Proizvodi');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Proizvodi'.(! empty($request->sq) ? (' za "'.$request->sq.'"') : ''),
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        if ($request->filled('sq')) {
            $products = Product::search($request->sq)->paginate((new ProductsGateway())->defaultPaginationSize);

            // Lazy load relationships
            $products->load([
                'photos',
                'slugs',
                'brand.slugs',
            ]);
        } else {
            $products = (new ProductsGateway())
                ->getItems($request->all(), true);

            // Eager load relationships
            $products->with([
                'photos',
                'slugs',
                'brand.slugs',
            ]);

            // Paginate
            $products = $products->paginate((new ProductsGateway())->defaultPaginationSize);
        }

        return view('products.index', [
            'products' => $products,
        ]);
    }

    public function createReview(Product $product): View
    {
        $this->setTitle('Ostavi recenziju');
        $this->openGraph->set('og:title', 'Ostavi recenziju');

        return view('products.review', [
            'product' => $product,
        ]);
    }

    public function saveReview(CreateReviewRequest $request, Product $product): RedirectResponse
    {
        $review = $product->reviews()->create($request->all());

        $needSaving = false;

        if (auth()->check()) {
            $review->user_id = auth()->id();
            $needSaving = true;
        }

        if ($request->filled('order_number')) {
            $order = Order::where('order_number', $request->order_number)
                ->whereHas('orderProducts', function ($query) use ($product) {
                    $query->where('order_products.product_id', $product->id);
                })
                ->first();

            if ($order) {
                $review->order_id = $order->id;
                $review->verified_buyer = true;
                $needSaving = true;
            }
        }

        if ($needSaving) {
            $review->save();
        }

        event(new ReviewSubmitted($review));

        session()->flash('review-success', 'Vaša recenzija je uspješno poslana i biti će vidljiva nakon provjere.');

        return redirect()->route('products.show', ['product' => $product->getSlug()]);
    }
}
