<?php

namespace Viva\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Viva\Country;
use Viva\Http\Requests\UpdatePasswordRequest;
use Viva\Http\Requests\UpdateSettingsRequest;
use Viva\NewsletterContact;

class UserController extends Controller
{
    public function getSettings(): View
    {
        $this->setTitle('Informacije o računu');

        $country = Country::find(1);

        return view('user.settings', [
            'country' => $country,
            'user' => auth()->user(),
        ]);
    }

    public function postSettings(UpdateSettingsRequest $request): RedirectResponse
    {
        $user = auth()->user();
        $user->gender = $request->gender ?: $user->gender;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->birth_date = $request->has('birth_date') ? $request->birth_date : $user->birth_date;
        $user->phone = $request->filled('phone') ? $request->phone : null;

        if ($request->filled('street_address') && $request->filled('postalcode') && $request->filled('city')) {
            $user->street_address = $request->street_address;
            $user->postalcode = $request->postalcode;
            $user->city = $request->city;
            $user->country_id = 1; // Hardcoded to Bosnia for now
        } else {
            $user->street_address = null;
            $user->postalcode = null;
            $user->city = null;
            $user->country_id = null;
        }

        $user->save();

        if ($request->newsletter && ! $user->newsletterContact) {
            $user->newsletterContact()->create([
                'email' => $user->email,
                'token' => random_token(),
            ]);
        } elseif ($request->filled('newsletter') && ! $request->newsletter && $user->newsletterContact) {
            $user->newsletterContact->delete();
        }

        session()->flash('success', 'Vaše informacije o računu su uspješno spremljene.');

        return redirect()->route('user.settings');
    }

    public function getPassword(): View
    {
        $this->setTitle('Promjena šifre');

        return view('user.password', [
            'user' => auth()->user(),
        ]);
    }

    public function postPassword(UpdatePasswordRequest $request): RedirectResponse
    {
        $user = auth()->user();

        $user->password = Hash::make($request->password);
        $user->save();

        session()->flash('success', 'Vaša nova šifra je uspješno spremljena.');

        return redirect()->route('user.password');
    }

    public function getOrders(): View
    {
        $this->setTitle('Moje narudžbe');

        return view('user.orders', [
            'orders' => auth()->user()->orders()->with(['orderProducts.product.photos', 'orderProducts.product.slugs', 'country'])->get(),
        ]);
    }

    public function newsletterUnsubscribe(string $token): View
    {
        $newsletterContact = NewsletterContact::where('token', $token)->first();

        if ($newsletterContact) {
            $newsletterContact->delete();

            return view('user.unsubscrine_successful');
        }

        abort(404);
    }
}
