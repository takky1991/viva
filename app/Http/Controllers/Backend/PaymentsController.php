<?php

namespace Viva\Http\Controllers\Backend;

use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PDF;
use Viva\Gateways\PaymentsGateway;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdatePaymentRequest;
use Viva\Payment;
use Viva\Supplier;

class PaymentsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->paymentsGateway = new PaymentsGateway();
    }

    public function index(Request $request): View
    {
        $this->authorize('index', Payment::class);

        $payments = $this->paymentsGateway->getItems($request->all(), true);

        if (! auth()->user()->hasRole('admin')) {
            $payments = $payments->where('user_id', auth()->id());
        }

        $infoPayments = $payments;
        $infoPayments = $infoPayments->get();
        $paymentsPaid = $infoPayments->where('paid', true)->count();
        $paymentsUnpaid = $infoPayments->where('paid', false)->count();
        $paymentsTotalPaid = $infoPayments->where('paid', true)->sum('amount');
        $paymentsTotalUnpaid = $infoPayments->where('paid', false)->sum('amount');

        //Eager loading
        $payments = $payments->with(['supplier', 'user']);

        $payments = $payments->paginate($this->paymentsGateway->defaultPaginationSize);
        $paymentsCount = $payments->total();
        $suppliers = Supplier::orderBy('name')->get();

        return view('backend/payments/index', [
            'payments' => $payments,
            'paymentsCount' => $paymentsCount,
            'paymentsPaid' => $paymentsPaid,
            'paymentsUnpaid' => $paymentsUnpaid,
            'paymentsTotalPaid' => $paymentsTotalPaid,
            'paymentsTotalUnpaid' => $paymentsTotalUnpaid,
            'suppliers' => $suppliers,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Payment::class);

        $suppliers = Supplier::orderBy('name')->get();

        return view('backend/payments/create-update', [
            'suppliers' => $suppliers,
        ]);
    }

    public function store(CreateUpdatePaymentRequest $request): RedirectResponse
    {
        $this->authorize('create', Payment::class);

        auth()->user()->payments()->create([
            'supplier_id' => $request->supplier_id,
            'invoice_id' => $request->invoice_id,
            'delivered_at' => Carbon::parse($request->delivered_at),
            'pay_until' => Carbon::parse($request->pay_until),
            'amount' => $request->amount,
            'discount' => $request->discount,
            'paid' => auth()->user()->hasRole('admin') ? ($request->filled('paid') ? $request->paid : false) : false,
            'discount_paid' => auth()->user()->hasRole('admin') ? ($request->filled('discount_paid') ? $request->discount_paid : false) : false,
            'locked' => (auth()->user()->hasRole('admin') && $request->filled('paid') && $request->paid) ? true : false,
        ]);

        flash('success', 'Faktura je kreirano.', 'Uspješno!');

        return redirect()->route('backend.payments.index', ['supplier' => $request->supplier_id]);
    }

    public function edit(Payment $payment): View
    {
        $this->authorize('update', $payment);

        $suppliers = Supplier::orderBy('name')->get();

        return view('backend/payments/create-update', [
            'payment' => $payment,
            'suppliers' => $suppliers,
        ]);
    }

    public function update(CreateUpdatePaymentRequest $request, Payment $payment): RedirectResponse
    {
        $this->authorize('update', $payment);

        $payment->update([
            'supplier_id' => $request->supplier_id,
            'invoice_id' => $request->invoice_id,
            'delivered_at' => Carbon::parse($request->delivered_at),
            'pay_until' => Carbon::parse($request->pay_until),
            'amount' => $request->amount,
            'discount' => $request->discount,
            'paid' => auth()->user()->hasRole('admin') ? ($request->filled('paid') ? $request->paid : false) : false,
            'discount_paid' => auth()->user()->hasRole('admin') ? ($request->filled('discount_paid') ? $request->discount_paid : false) : false,
            'locked' => (auth()->user()->hasRole('admin') && $request->filled('paid') && $request->paid) ? true : false,
        ]);

        flash('success', 'Faktura je ažurirana.', 'Uspješno!');

        return redirect()->route('backend.payments.index', ['supplier' => $request->supplier_id]);
    }

    public function destroy(Payment $payment): RedirectResponse
    {
        $this->authorize('delete', $payment);

        $payment->delete();
        flash('success', 'Faktura je izbrisana.', 'Uspješno!');

        return redirect()->route('backend.payments.index');
    }

    public function search(Request $request): RedirectResponse
    {
        $this->authorize('index', Payment::class);

        $parameters = $request->except('_token');
        $redirectUrl = route('backend.payments.index');

        $query = '';
        $i = 0;
        $len = count($parameters);
        foreach ($parameters as $key => $value) {
            // last
            if ($i == $len - 1) {
                $query = $query.$key.'='.$value;
            } else {
                $query = $query.$key.'='.$value.'&';
            }
            $i++;
        }

        $redirectUrl = $redirectUrl.'?'.$query;

        return redirect($redirectUrl);
    }

    public function suppliers(): View
    {
        $this->authorize('index', Payment::class);

        $suppliers = Supplier::orderBy('name')->with('payments')->get();

        return view('backend/payments/suppliers', [
            'suppliers' => $suppliers,
        ]);
    }

    public function pay(Request $request)
    {
        $this->authorize('pay', Payment::class);

        $this->paymentsGateway->getItems($request->all(), true)->where('paid', false)->update(['paid' => true]);

        flash('success', 'Uspjesno placeno');

        return back();
    }

    public function getPdf(Request $request)
    {
        $this->authorize('pdf', Payment::class);

        $payments = $this->paymentsGateway->getItems($request->all(), true)->with(['supplier']);
        $infoPayments = $payments;
        $paymentsTotalPaid = $infoPayments->sum('amount');
        $payments = $payments->get();

        $pdf = PDF::loadView('backend/payments/pdf', [
            'payments' => $payments,
            'total' => $payments->count(),
            'totalAmount' => $paymentsTotalPaid,
        ]);

        return $pdf->stream('fakture.pdf')->header('Content-Type', 'application/pdf');
    }
}
