<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\Brand;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateBrandRequest;

class BrandsController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', Brand::class);

        $brands = Brand::orderBy('name', 'ASC')->get();

        return view('backend/shop/brands/index', [
            'brands' => $brands,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Brand::class);

        return view('backend/shop/brands/create-update');
    }

    public function store(CreateUpdateBrandRequest $request): RedirectResponse
    {
        $this->authorize('create', Brand::class);

        Brand::create($request->all());

        flash('success', 'Brend je kreirana.', 'Uspješno!');

        return redirect()->route('backend.shop.brands.index');
    }

    public function edit(Brand $brand): View
    {
        $this->authorize('update', $brand);

        return view('backend/shop/brands/create-update', [
            'brand' => $brand,
        ]);
    }

    public function update(CreateUpdateBrandRequest $request, Brand $brand): RedirectResponse
    {
        $this->authorize('update', $brand);

        $brand->update($request->all());

        flash('success', 'Brend je ažurirana.', 'Uspješno!');

        return redirect()->route('backend.shop.brands.index');
    }
}
