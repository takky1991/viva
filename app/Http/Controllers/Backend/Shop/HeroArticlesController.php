<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\Brand;
use Viva\HeroArticle;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateHeroArticleRequest;

class HeroArticlesController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', HeroArticle::class);

        $heroArticles = HeroArticle::with('brand')->orderBy('created_at', 'DESC')->paginate(20);

        return view('backend/shop/hero-articles/index', [
            'heroArticles' => $heroArticles,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', HeroArticle::class);

        $brands = Brand::orderBy('name')->get();

        return view('backend/shop/hero-articles/create-update', [
            'brands' => $brands,
        ]);
    }

    public function store(CreateUpdateHeroArticleRequest $request): RedirectResponse
    {
        $this->authorize('create', HeroArticle::class);

        $heroArticle = HeroArticle::create($request->all());

        flash('success', 'Hero članak je kreiran.', 'Uspješno!');

        return redirect()->route('backend.shop.hero_articles.edit', [
            'heroArticle' => $heroArticle,
        ]);
    }

    public function edit(HeroArticle $heroArticle): View
    {
        $this->authorize('update', $heroArticle);

        $brands = Brand::orderBy('name')->get();

        return view('backend/shop/hero-articles/create-update', [
            'heroArticle' => $heroArticle,
            'brands' => $brands,
        ]);
    }

    public function update(CreateUpdateHeroArticleRequest $request, HeroArticle $heroArticle): RedirectResponse
    {
        $this->authorize('update', $heroArticle);

        $heroArticle->update($request->all());

        flash('success', 'Hero članak je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.shop.hero_articles.index');
    }

    public function destroy(HeroArticle $heroArticle): RedirectResponse
    {
        $this->authorize('delete', $heroArticle);

        $heroArticle->delete();

        flash('success', 'Hero članak je uklonjen.', 'Uspješno!');

        return redirect()->route('backend.shop.hero_articles.index');
    }

    public function togglePublish(HeroArticle $heroArticle): RedirectResponse
    {
        $heroArticle->published = ! $heroArticle->published;
        $heroArticle->published_at = $heroArticle->published ? now() : null;
        $heroArticle->save();

        flash('success', 'Hero članak je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.shop.hero_articles.index');
    }
}
