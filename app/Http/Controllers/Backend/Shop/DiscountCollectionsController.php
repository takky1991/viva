<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\DiscountCollection;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateDiscountCollectionRequest;
use Viva\Http\Resources\ProductResource;
use Viva\Product;

class DiscountCollectionsController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', DiscountCollection::class);

        $discountCollections = DiscountCollection::orderBy('created_at')->get();

        return view('backend/shop/discount-collections/index', [
            'discountCollections' => $discountCollections,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', DiscountCollection::class);

        return view('backend/shop/discount-collections/create-update');
    }

    public function store(CreateUpdateDiscountCollectionRequest $request): RedirectResponse
    {
        $this->authorize('create', DiscountCollection::class);

        $discountCollection = DiscountCollection::create($request->all());

        return redirect()->route('backend.shop.discount_collections.edit', ['discountCollection' => $discountCollection]);
    }

    public function edit(DiscountCollection $discountCollection): View
    {
        $this->authorize('update', DiscountCollection::class);

        $selectedProducts = null;

        if ($discountCollection->product_ids) {
            $selectedProducts = Product::whereIn('id', json_decode($discountCollection->product_ids))->orderBy('title')->get();
            $selectedProducts = ProductResource::collection($selectedProducts)->jsonSerialize();
        }

        return view('backend/shop/discount-collections/create-update', [
            'discountCollection' => $discountCollection,
            'selectedProducts' => $selectedProducts,
        ]);
    }

    public function update(CreateUpdateDiscountCollectionRequest $request, DiscountCollection $discountCollection): RedirectResponse
    {
        $this->authorize('update', DiscountCollection::class);

        $discountCollection->update($request->all());

        return redirect()->route('backend.shop.discount_collections.edit', ['discountCollection' => $discountCollection]);
    }

    public function destroy(DiscountCollection $discountCollection): RedirectResponse
    {
        $this->authorize('delete', DiscountCollection::class);

        $discountCollection->delete();
        flash('success', 'Popust je izbrisan.', 'Uspješno!');

        return redirect()->route('backend.shop.discount_collections.index');
    }
}
