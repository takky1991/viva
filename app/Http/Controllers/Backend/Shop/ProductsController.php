<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Viva\Brand;
use Viva\Category;
use Viva\Exports\ProductsSalesExport;
use Viva\Gateways\AdminProductsGateway;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateProductRequest;
use Viva\Product;
use Viva\ShippingMethod;

class ProductsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->productsGateway = new AdminProductsGateway();
    }

    public function index(Request $request)
    {
        $this->authorize('index', Product::class);

        $productsQuery = $this->productsGateway->getItems($request->all(), true);

        //Eager loading
        $productsQuery = $productsQuery->with(['photos', 'brand']);

        if ($request->filled('productsFromDate') && $request->filled('productsToDate')) {
            $productsQuery->withCount(['orderProducts' => function ($query) use ($request) {
                $query->select(DB::raw('sum(order_products.quantity)'));

                if ($request->filled('productsFromDate')) {
                    $productsFromDate = Carbon::parse($request->productsFromDate);
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $productsToDate = Carbon::parse($request->productsToDate);
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }])
            ->withCount(['orderProducts as totalPrice' => function ($query) use ($request) {
                $query->select(DB::raw('sum(order_products.total_price)'));

                if ($request->filled('productsFromDate')) {
                    $productsFromDate = Carbon::parse($request->productsFromDate);
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $productsToDate = Carbon::parse($request->productsToDate);
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }]);
        }

        if ($request->filled('productsFromDate') && $request->filled('productsToDate')) {
            $products = $productsQuery
                ->orderBy('order_products_count', 'desc')
                ->paginate($this->productsGateway->defaultPaginationSize);
        } else {
            $products = $productsQuery
                ->orderBy('buy_count', 'desc')
                ->paginate($this->productsGateway->defaultPaginationSize);
        }

        $productsCount = $products->total();
        $categories = Category::orderBy('title', 'ASC')->get();
        $brands = Brand::orderBy('name', 'ASC')->get();

        return view('backend/shop/products/index', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'productsCount' => $productsCount,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Product::class);

        $categories = Category::orderBy('title', 'asc')->get();
        $brands = Brand::orderBy('name', 'asc')->get();

        return view('backend/shop/products/create-update', [
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    public function store(CreateUpdateProductRequest $request): RedirectResponse
    {
        $this->authorize('create', Product::class);

        $product = Product::create($request->all());

        $categoryIds = [];

        if ($request->filled('categories')) {
            $categories = Category::whereIn('id', $request->categories)->get();
            foreach ($categories as $category) {
                $categoryIds[] = $category->id;
                $parentCategory = $category->parent;

                while (! empty($parentCategory)) {
                    $categoryIds[] = $parentCategory->id;
                    $parentCategory = $parentCategory->parent;
                }
            }
            $categoryIds = array_unique($categoryIds);
            $categoryIds = array_values($categoryIds);
        }

        $product->categories()->sync($categoryIds);

        // Save again so the observer can set the categories_hierarchy
        $product->save();

        flash('success', 'Proizvod je kreiran.', 'Uspješno!');

        return redirect()->route('backend.shop.products.edit', ['product' => $product]);
    }

    public function edit(Product $product): View
    {
        $this->authorize('update', Product::class);

        $categories = Category::orderBy('title', 'asc')->get();
        $brands = Brand::orderBy('name', 'asc')->get();

        return view('backend/shop/products/create-update', [
            'product' => $product,
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    public function update(CreateUpdateProductRequest $request, Product $product): RedirectResponse
    {
        $this->authorize('update', Product::class);

        $product->update($request->all());

        $categoryIds = [];

        if ($request->filled('categories')) {
            $categories = Category::whereIn('id', $request->categories)->get();
            foreach ($categories as $category) {
                $categoryIds[] = $category->id;
                $parentCategory = $category->parent;

                while (! empty($parentCategory)) {
                    $categoryIds[] = $parentCategory->id;
                    $parentCategory = $parentCategory->parent;
                }
            }
            $categoryIds = array_unique($categoryIds);
            $categoryIds = array_values($categoryIds);
        }

        $product->categories()->sync($categoryIds);

        // Save again so the observer can set the categories_hierarchy
        $product->save();

        flash('success', 'Proizvod je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.shop.products.edit', ['product' => $product]);
    }

    public function destroy(Product $product): RedirectResponse
    {
        $this->authorize('delete', Product::class);

        $product->delete();

        flash('success', 'Proizvod je arhiviran.', 'Uspješno!');

        return redirect()->route('backend.shop.products.index');
    }

    public function search(Request $request): RedirectResponse
    {
        $this->authorize('index', Product::class);

        $parameters = $request->except('_token');
        $redirectUrl = route('backend.shop.products.index');

        $query = '';
        $i = 0;
        $len = count($parameters);
        foreach ($parameters as $key => $value) {
            // last
            if ($i == $len - 1) {
                $query = $query.$key.'='.$value;
            } else {
                $query = $query.$key.'='.$value.'&';
            }
            $i++;
        }

        $redirectUrl = $redirectUrl.'?'.$query;

        return redirect($redirectUrl);
    }

    public function show(Product $product): View
    {
        $this->authorize('show', Product::class);

        return view('backend/shop/products/show', [
            'product' => $product,
        ]);
    }

    public function preview(Product $product)
    {
        $this->authorize('preview', Product::class);

        $this->setTitle($product->fullName());

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
        ];

        $product->load('variants');
        $category = $product->menuCategories()->whereNull('categories.parent_id')->first();
        $categoryIds = $product->categories->pluck('id')->toArray();

        while ($category) {
            $breadcrumbs[] = [
                'label' => $category->title,
                'link' => route('categories.show', ['category' => $category->getSlug()]),
            ];

            $category = $category->menuChildren()->whereIn('id', $categoryIds)->first();
        }

        $breadcrumbs[] = ['label' => $product->fullName()];

        $this->setBreadcrumbs($breadcrumbs);

        $shippingMethods = Cache::remember('shippingMethodsPickup', 2592000, function () { //cache for a month
            return ShippingMethod::where('pickup', false)->orderBy('order_id')->get();
        });

        return view('products/show', [
            'product' => $product,
            'shippingMethods' => $shippingMethods,
        ]);
    }

    public function restore(Product $product): RedirectResponse
    {
        $this->authorize('restore', Product::class);

        $product->restore();

        return redirect()->route('backend.shop.products.edit', [
            'product' => $product,
        ]);
    }

    public function getPdf(Request $request)
    {
        $brand = null;

        if ($request->filled('brand')) {
            $brand = Brand::find($request->brand);
        }

        $productsFromDate = Carbon::parse($request->productsFromDate);
        $productsToDate = Carbon::parse($request->productsToDate);

        $productsQuery = $this->productsGateway
            ->getItems($request->all(), true)
            ->withCount(['orderProducts' => function ($query) use ($request, $productsFromDate, $productsToDate) {
                $query->select(DB::raw('sum(order_products.quantity)'));

                if ($request->filled('productsFromDate')) {
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }])
            ->withCount(['orderProducts as totalPrice' => function ($query) use ($request, $productsFromDate, $productsToDate) {
                $query->select(DB::raw('sum(order_products.total_price)'));

                if ($request->filled('productsFromDate')) {
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }])
            ->orderBy('order_products_count', 'desc');

        $products = $productsQuery->get();

        $pdf = PDF::loadView('backend/shop/products/pdf', [
            'from' => $productsFromDate->format('d.m.Y'),
            'to' => $productsToDate->format('d.m.Y'),
            'products' => $products,
            'productsOrdersCount' => $products->sum('order_products_count'),
            'productsOrdersTotalPrice' => $products->sum('totalPrice'),
        ]);

        $filename = [
            $brand?->name,
            $productsFromDate->format('d.m.Y'),
            $productsToDate->format('d.m.Y'),
        ];

        $filename = array_filter($filename);

        return $pdf->stream(implode('-', $filename).'.pdf')->header('Content-Type', 'application/pdf');
    }

    public function getCSV(Request $request)
    {
        $brand = null;

        if ($request->filled('brand')) {
            $brand = Brand::find($request->brand);
        }

        $productsFromDate = Carbon::parse($request->productsFromDate);
        $productsToDate = Carbon::parse($request->productsToDate);

        $productsQuery = $this->productsGateway
            ->getItems($request->all(), true)
            ->withCount(['orderProducts' => function ($query) use ($request, $productsFromDate, $productsToDate) {
                $query->select(DB::raw('sum(order_products.quantity)'));

                if ($request->filled('productsFromDate')) {
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }])
            ->withCount(['orderProducts as totalPrice' => function ($query) use ($request, $productsFromDate, $productsToDate) {
                $query->select(DB::raw('sum(order_products.total_price)'));

                if ($request->filled('productsFromDate')) {
                    $query->where('order_products.created_at', '>=', $productsFromDate);
                }

                if ($request->filled('productsToDate')) {
                    $query->where('order_products.created_at', '<=', $productsToDate);
                }

                return $query;
            }])
            ->orderBy('order_products_count', 'desc');

        $products = $productsQuery->get();

        $export = new ProductsSalesExport(
            $productsFromDate->format('d.m.Y'),
            $productsToDate->format('d.m.Y'),
            $products,
            $products->sum('order_products_count'),
            $products->sum('totalPrice')
        );

        $filename = [
            $brand?->name,
            $productsFromDate->format('d.m.Y'),
            $productsToDate->format('d.m.Y'),
        ];

        $filename = array_filter($filename);

        return Excel::download($export, implode('-', $filename).'.xlsx');
    }
}
