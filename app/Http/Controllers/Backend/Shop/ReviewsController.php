<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Viva\Gateways\AdminReviewsGateway;
use Viva\Http\Controllers\Controller;
use Viva\Review;

class ReviewsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->reviewsGateway = new AdminReviewsGateway;
    }

    public function index(Request $request): View
    {
        $this->authorize('index', Review::class);

        $reviews = $this->reviewsGateway->getItems($request->all(), true);
        $reviews = $reviews->with(['product']);
        $reviews = $reviews->paginate($this->reviewsGateway->defaultPaginationSize);

        return view('backend/shop/reviews/index', [
            'reviews' => $reviews,
        ]);
    }

    public function show(Review $review): View
    {
        $this->authorize('show', Review::class);

        return view('backend/shop/reviews/show', [
            'review' => $review,
        ]);
    }

    public function destroy(Review $review): RedirectResponse
    {
        $this->authorize('delete', Review::class);

        $review->delete();

        flash('success', 'Recenzija je uklonjena.', 'Uspješno!');

        return redirect()->route('backend.shop.reviews.index');
    }

    public function search(Request $request): RedirectResponse
    {
        $this->authorize('index', Review::class);

        $parameters = $request->except('_token');
        $redirectUrl = route('backend.shop.reviews.index');

        $query = '';
        $i = 0;
        $len = count($parameters);
        foreach ($parameters as $key => $value) {
            // last
            if ($i == $len - 1) {
                $query = $query.$key.'='.$value;
            } else {
                $query = $query.$key.'='.$value.'&';
            }
            $i++;
        }

        $redirectUrl = $redirectUrl.'?'.$query;

        return redirect($redirectUrl);
    }

    public function togglePublished(Review $review): RedirectResponse
    {
        $this->authorize('publish', Review::class);

        if ($review->published) {
            $review->published = false;
            $review->save();
            flash('success', 'Recenzija je sakrivena.', 'Uspješno!');
        } else {
            $review->published = true;
            $review->save();
            flash('success', 'Recenzija je objavljena.', 'Uspješno!');
        }

        return redirect()->back();
    }
}
