<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\Article;
use Viva\Brand;
use Viva\Category;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateArticleRequest;
use Viva\Http\Resources\BrandResource;
use Viva\Http\Resources\CategoryResource;
use Viva\Http\Resources\ProductResource;
use Viva\Product;

class ArticlesController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', Article::class);

        $articles = Article::orderBy('created_at')->get();

        return view('backend/shop/articles/index', [
            'articles' => $articles,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Article::class);

        return view('backend/shop/articles/create-update');
    }

    public function store(CreateUpdateArticleRequest $request): RedirectResponse
    {
        $this->authorize('create', Article::class);

        $article = Article::create($request->all());

        flash('success', 'Blog članak je kreiran.', 'Uspješno!');

        return redirect()->route('backend.shop.articles.edit', ['article' => $article]);
    }

    public function edit(Article $article)
    {
        $this->authorize('update', $article);

        if (! empty($article->product_ids)) {
            $relatedProducts = Product::whereIn('id', $article->product_ids)->get()
                ->map(function ($product) {
                    return new ProductResource($product);
                });
        }

        if (! empty($article->category_ids)) {
            $relatedCategories = Category::whereIn('id', $article->category_ids)->get()
                ->map(function ($category) {
                    return new CategoryResource($category);
                });
        }

        if (! empty($article->brand_ids)) {
            $relatedBrands = Brand::whereIn('id', $article->brand_ids)->get()
                ->map(function ($brand) {
                    return new BrandResource($brand);
                });
        }

        return view('backend/shop/articles/create-update', [
            'article' => $article,
            'relatedProducts' => $relatedProducts ?? null,
            'relatedCategories' => $relatedCategories ?? null,
            'relatedBrands' => $relatedBrands ?? null,
        ]);
    }

    public function update(CreateUpdateArticleRequest $request, Article $article): RedirectResponse
    {
        $this->authorize('update', $article);

        $article->update($request->all());

        flash('success', 'Blog članak je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.shop.articles.index');
    }

    public function destroy(Article $article): RedirectResponse
    {
        $this->authorize('delete', $article);

        $article->delete();
        flash('success', 'Blog članak je izbrisan.', 'Uspješno!');

        return redirect()->route('backend.shop.articles.index');
    }

    public function preview(Article $article): View
    {
        $this->authorize('preview', Article::class);

        $this->setTitle($article->title);

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Savjeti i Novosti',
                'link' => route('articles.index'),
            ],
            [
                'label' => $article->title,
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $products = null;
        if (! empty($article->product_ids)) {
            $products = Product::whereIn('id', $article->product_ids)->with('slugs', 'brand.slugs', 'photos')->get();
        }

        $brands = null;
        if (! empty($article->brand_ids)) {
            $brands = Brand::whereIn('id', $article->brand_ids)->with('slugs')->get();
        }

        $categories = null;
        if (! empty($article->category_ids)) {
            $categories = Category::whereIn('id', $article->category_ids)->with('slugs')->get();
        }

        return view('articles/show', [
            'article' => $article,
            'brands' => $brands,
            'categories' => $categories,
            'products' => $products,
        ]);
    }
}
