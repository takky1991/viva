<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\Category;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateCategoryRequest;

class CategoriesController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', Category::class);

        $categories = Category::whereNull('parent_id')->where('menu', true)->with('menuChildren.menuChildren.menuChildren.menuChildren')->orderBy('created_at', 'DESC')->get();

        return view('backend/shop/categories/index', [
            'categories' => $categories,
        ]);
    }

    public function indexOther(): View
    {
        $this->authorize('index', Category::class);

        $categories = Category::whereNull('parent_id')->with('children.children.children.children')->orderBy('created_at', 'DESC')->get();

        return view('backend/shop/categories/index', [
            'categories' => $categories,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Category::class);

        $categories = Category::orderBy('title', 'asc')->get();

        return view('backend/shop/categories/create-update', [
            'categories' => $categories,
        ]);
    }

    public function store(CreateUpdateCategoryRequest $request): RedirectResponse
    {
        $this->authorize('create', Category::class);

        Category::create($request->all());

        flash('success', 'Kategorija je kreirana.', 'Uspješno!');

        return redirect()->route('backend.shop.categories.index');
    }

    public function edit(Category $category)
    {
        $this->authorize('update', $category);

        $categories = Category::orderBy('created_at', 'DESC')->get();
        $categories = $categories->reject(function ($item) use ($category) {
            return $item->id == $category->id;
        });

        return view('backend/shop/categories/create-update', [
            'category' => $category,
            'categories' => $categories,
        ]);
    }

    public function update(CreateUpdateCategoryRequest $request, Category $category): RedirectResponse
    {
        $this->authorize('update', $category);

        $category->update($request->all());

        flash('success', 'Kategorija je ažurirana.', 'Uspješno!');

        return redirect()->route('backend.shop.categories.index');
    }

    public function destroy(Category $category)
    {
        // $this->authorize('delete', $category);

        // if($category->children->isEmpty() && $category->productsWithArchived->isEmpty()) {
        //     $category->delete();
        //     flash('success', 'Kategorija je izbrisana.', 'Uspješno!');
        //     return redirect()->route('backend.shop.categories.index');
        // } else {
        //     flash('warning', 'Nije moguće obrisati kategoriju jer posjeduje druge kategorije ili proizvode.', 'Upozorenje!');
        //     return back();
        // }
    }
}
