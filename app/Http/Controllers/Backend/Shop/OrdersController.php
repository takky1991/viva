<?php

namespace Viva\Http\Controllers\Backend\Shop;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PDF;
use Viva\Gateways\OrdersGateway;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\UpdateOrderRequest;
use Viva\Order;
use Viva\ShippingMethod;

class OrdersController extends Controller
{
    protected $ordersGateway;

    public function __construct()
    {
        parent::__construct();

        $this->ordersGateway = new OrdersGateway();
    }

    public function index(Request $request): View
    {
        $this->authorize('index', Order::class);

        $ordersQuery = $this->ordersGateway->getItems($request->all(), true);
        $ordersQuery = $ordersQuery->with(['orderProducts']);

        $totalPrice = $ordersQuery->sum('total_price');
        $totalShippingPrice = $ordersQuery->sum('shipping_price');
        $orders = $ordersQuery->paginate($this->ordersGateway->defaultPaginationSize);

        $shippingMethods = ShippingMethod::orderBy('order_id')->get();

        return view('backend/shop/orders/index', [
            'orders' => $orders,
            'totalPrice' => $totalPrice,
            'totalShippingPrice' => $totalShippingPrice,
            'shippingMethods' => $shippingMethods,
        ]);
    }

    public function edit(Order $order): View
    {
        $this->authorize('update', $order);

        return view('backend/shop/orders/update', [
            'order' => $order,
        ]);
    }

    public function update(UpdateOrderRequest $request, Order $order): RedirectResponse
    {
        $this->authorize('update', $order);

        $order->status = $request->status;
        $order->comment = $request->comment;

        if ($request->filled('card_payment_status')) {
            $order->card_payment_status = $request->card_payment_status;
        }

        $order->save();

        flash('success', 'Narudžba je ažurirana.', 'Uspješno!');

        return redirect()->route('backend.shop.orders.index');
    }

    public function search(Request $request): RedirectResponse
    {
        $this->authorize('index', Order::class);

        $parameters = $request->except('_token');
        $redirectUrl = route('backend.shop.orders.index');

        $query = '';
        $i = 0;
        $len = count($parameters);
        foreach ($parameters as $key => $value) {
            // last
            if ($i == $len - 1) {
                $query = $query.$key.'='.$value;
            } else {
                $query = $query.$key.'='.$value.'&';
            }
            $i++;
        }

        $redirectUrl = $redirectUrl.'?'.$query;

        return redirect($redirectUrl);
    }

    public function invoice(Order $order)
    {
        $this->authorize('invoice', $order);

        $pdf = PDF::loadView('backend/shop/orders/invoice', compact('order'));

        return $pdf->stream('invoice.pdf')->header('Content-Type', 'application/pdf');
    }
}
