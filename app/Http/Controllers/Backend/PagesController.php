<?php

namespace Viva\Http\Controllers\Backend;

use Illuminate\View\View;
use Viva\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function home(): View
    {
        return view('backend/home');
    }

    public function shopHome(): View
    {
        return view('backend/shop/home');
    }
}
