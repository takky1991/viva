<?php

namespace Viva\Http\Controllers\Backend;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Propaganistas\LaravelPhone\PhoneNumber;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateSmsNumberRequest;
use Viva\SmsNumber;

class SmsNumberController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', SmsNumber::class);

        $smsNumbers = SmsNumber::orderBy('created_at', 'DESC')->paginate(50);
        $smsNumbersCount = SmsNumber::all()->count();

        return view('backend/sms/index', [
            'smsNumbers' => $smsNumbers,
            'smsNumbersCount' => $smsNumbersCount,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', SmsNumber::class);

        return view('backend/sms/create');
    }

    public function store(CreateSmsNumberRequest $request): RedirectResponse
    {
        $this->authorize('create', SmsNumber::class);

        SmsNumber::create([
            'number' => (string) PhoneNumber::make($request->number, 'BA'),
        ]);

        flash('success', 'Broj je kreiran.', 'Uspješno!');

        return redirect()->route('backend.sms_numbers.index');
    }

    public function destroy(SmsNumber $smsNumber): RedirectResponse
    {
        $this->authorize('delete', $smsNumber);

        $smsNumber->delete();

        flash('success', 'Broj je izbrisan.', 'Uspješno!');

        return redirect()->route('backend.sms_numbers.index');
    }
}
