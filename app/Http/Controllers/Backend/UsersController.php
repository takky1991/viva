<?php

namespace Viva\Http\Controllers\Backend;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Viva\Exports\RemarketingEmailListExport;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateUserRequest;
use Viva\User;

class UsersController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', User::class);

        $users = User::where('id', '!=', auth()->id())->paginate(20);

        return view('backend/users/index', [
            'users' => $users,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', User::class);

        $roles = Role::all();

        return view('backend/users/create-update', [
            'roles' => $roles,
        ]);
    }

    public function store(CreateUpdateUserRequest $request): RedirectResponse
    {
        $this->authorize('create', User::class);

        $request->merge(['password' => bcrypt('secret')]);
        $user = User::create($request->all());
        $user->assignRole($request->role_name);

        flash('success', 'Korisnik je kreiran.', 'Uspješno!');

        return redirect()->route('backend.users.index');
    }

    public function edit(User $user): View
    {
        $this->authorize('update', $user);

        $roles = Role::all();

        return view('backend/users/create-update', [
            'user' => $user,
            'roles' => $roles,
        ]);
    }

    public function update(CreateUpdateUserRequest $request, User $user): RedirectResponse
    {
        $this->authorize('update', $user);

        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
        ]);
        $user->syncRoles($request->role_name);

        flash('success', 'Korisnik je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.users.index');
    }

    public function destroy(User $user): RedirectResponse
    {
        $this->authorize('delete', $user);

        $user->delete();

        flash('success', 'Korisnik je izbrisan.', 'Uspješno!');

        return redirect()->route('backend.users.index');
    }

    public function exportRemarketingEmailList()
    {
        return (new RemarketingEmailListExport)->download('remarketingEmailList.xlsx');
    }
}
