<?php

namespace Viva\Http\Controllers\Backend;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Viva\Http\Controllers\Controller;
use Viva\Http\Requests\Backend\CreateUpdateSupplierRequest;
use Viva\Supplier;

class SuppliersController extends Controller
{
    public function index(): View
    {
        $this->authorize('index', Supplier::class);

        $suppliers = Supplier::orderBy('name')->get();

        return view('backend/suppliers/index', [
            'suppliers' => $suppliers,
        ]);
    }

    public function create(): View
    {
        $this->authorize('create', Supplier::class);

        return view('backend/suppliers/create-update');
    }

    public function store(CreateUpdateSupplierRequest $request): RedirectResponse
    {
        $this->authorize('create', Supplier::class);

        Supplier::create($request->all());

        flash('success', 'Dobavljač je kreiran.', 'Uspješno!');

        return redirect()->route('backend.suppliers.index');
    }

    public function edit(Supplier $supplier): View
    {
        $this->authorize('update', $supplier);

        return view('backend/suppliers/create-update', [
            'supplier' => $supplier,
        ]);
    }

    public function update(CreateUpdateSupplierRequest $request, Supplier $supplier): RedirectResponse
    {
        $this->authorize('update', $supplier);

        $supplier->update($request->all());

        flash('success', 'Dobavljač je ažuriran.', 'Uspješno!');

        return redirect()->route('backend.suppliers.index');
    }

    public function destroy(Supplier $supplier)
    {
        $this->authorize('delete', $supplier);

        if ($supplier->payments->isEmpty()) {
            $supplier->delete();
            flash('success', 'Dobavljač je izbrisan.', 'Uspješno!');

            return redirect()->route('backend.suppliers.index');
        } else {
            flash('warning', 'Nije moguće obrisati dobavljača jer postoje fakture.', 'Upozorenje!');

            return back();
        }
    }
}
