<?php

namespace Viva\Http\Controllers\Backend\Api;

use Illuminate\Http\Request;
use Viva\Category;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Resources\CategoryResource;

class CategoriesController extends ApiController
{
    public function get(Request $request)
    {
        $categories = Category::query();

        if ($request->filled('q')) {
            $categories = $categories->where('title', 'like', '%'.$request->q.'%');
        }

        $categories = $categories->orderBy('title')->get();

        return CategoryResource::collection($categories);
    }
}
