<?php

namespace Viva\Http\Controllers\Backend\Api;

use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\PayPaymentRequest;
use Viva\Payment;

class PaymentsController extends ApiController
{
    public function pay(PayPaymentRequest $request, Payment $payment)
    {
        $payment->paid = true;
        $payment->save();

        $this->respondWithOk();
    }
}
