<?php

namespace Viva\Http\Controllers\Backend\Api;

use Illuminate\Http\Request;
use Viva\DiscountCollection;
use Viva\Http\Controllers\ApiController;
use Viva\Product;

class DiscountCollectionsController extends ApiController
{
    public function updateProducts(Request $request, DiscountCollection $discountCollection)
    {
        $oldProductIds = empty($discountCollection->product_ids) ? [] : json_decode($discountCollection->product_ids);
        $newProductIds = $request->products;
        $removedIds = array_diff($oldProductIds, $newProductIds);
        $addedIds = array_diff($newProductIds, $oldProductIds);

        $discountCollection->update([
            'product_ids' => json_encode($request->products),
        ]);

        if ($discountCollection->active && count($removedIds)) {
            $products = Product::whereIn('id', $removedIds)->get();

            foreach ($products as $product) {
                $product->update(['on_sale' => false]);
            }
        }

        if ($discountCollection->active && count($addedIds)) {
            $products = Product::whereIn('id', $addedIds)->get();

            foreach ($products as $product) {
                $newPrice = calculateDiscout($product->price, $discountCollection->discount_percentage);
                $product->update([
                    'discount_price' => $newPrice,
                    'on_sale' => true,
                ]);
            }
        }

        $this->respondWithOk();
    }

    public function enableDiscount(DiscountCollection $discountCollection)
    {
        $discountCollection->enable();

        return $this->respondWithOk();
    }

    public function disableDiscount(DiscountCollection $discountCollection)
    {
        $discountCollection->disable();

        return $this->respondWithOk();
    }
}
