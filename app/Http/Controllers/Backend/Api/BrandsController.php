<?php

namespace Viva\Http\Controllers\Backend\Api;

use Illuminate\Http\Request;
use Viva\Brand;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Resources\BrandResource;
use Viva\Http\Resources\ProductResource;

class BrandsController extends ApiController
{
    public function get(Request $request)
    {
        $brands = Brand::query();

        if ($request->filled('q')) {
            $brands = $brands->where('name', 'like', '%'.$request->q.'%');
        }

        $brands = $brands->orderBy('name')->get();

        return BrandResource::collection($brands);
    }

    public function getProducts(Brand $brand)
    {
        $products = $brand->products()->orderBy('products.title')->get();

        return ProductResource::collection($products);
    }
}
