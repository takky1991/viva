<?php

namespace Viva\Http\Controllers\Backend\Api;

use Illuminate\Http\Request;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\CreateUpdateProductVariantRequest;
use Viva\Http\Resources\ProductResource;
use Viva\Http\Resources\ProductVariantCollection;
use Viva\Http\Resources\ProductVariantResource;
use Viva\Product;
use Viva\ProductVariant;

class ProductsController extends ApiController
{
    public function get(Request $request)
    {
        $products = Product::query();

        if ($request->filled('q')) {
            $products = $products->where('title', 'like', '%'.$request->q.'%');
        }

        $products = $products->orderBy('title')->get();

        return ProductResource::collection($products);
    }

    public function getVariants(Product $product)
    {
        return new ProductVariantCollection($product->variants);
    }

    public function createVariant(CreateUpdateProductVariantRequest $request, Product $product)
    {
        $variant = $product->variants()->create([
            'name' => $request->name,
        ]);

        return new ProductVariantResource($variant);
    }

    public function updateVariant(CreateUpdateProductVariantRequest $request, Product $product, ProductVariant $productVariant)
    {
        $productVariant->update($request->all());

        return new ProductVariantResource($productVariant);
    }

    public function deleteVariant(Product $product, ProductVariant $productVariant)
    {
        $productVariant->delete();

        $this->respondWithDeleted();
    }
}
