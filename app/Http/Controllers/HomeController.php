<?php

namespace Viva\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Viva\Gateways\ProductsGateway;
use Viva\HeroArticle;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setTitle('Kozmetika, dodaci prehrani, medicinska oprema');

        $heroArticles = Cache::remember('homeHeroArticles', 86400, function () { //cache for a day
            return HeroArticle::where('published', true)
                ->with(['photo', 'brand'])
                ->orderBy('published_at', 'DESC')
                ->get();
        });

        $onSaleProducts = Cache::remember('homeProductsOnSale', 86400, function () { //cache for a day
            return (new ProductsGateway)
                ->getItems(['inStock' => true, 'onSale' => true])
                ->whereNotNull('discount_price')
                ->with(['photos', 'brand.slugs', 'slugs'])
                ->limit(15)
                ->get();
        });

        $popularProducts = Cache::remember('homeProductsPopular', 86400, function () use ($onSaleProducts) { //cache for a day
            $productIds = $onSaleProducts->pluck('id')->toArray();

            return (new ProductsGateway)
                ->getItems([
                    'inStock' => true,
                    'bestSeller' => true,
                ])
                ->whereNotIn('id', $productIds)
                ->with(['photos', 'brand.slugs', 'slugs'])
                ->limit(15)
                ->get();
        });

        return view('home', [
            'heroArticles' => $heroArticles,
            'onSaleProducts' => $onSaleProducts,
            'popularProducts' => $popularProducts,
        ]);
    }
}
