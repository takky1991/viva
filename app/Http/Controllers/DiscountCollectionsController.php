<?php

namespace Viva\Http\Controllers;

use Illuminate\View\View;
use Viva\DiscountCollection;
use Viva\Gateways\ProductsGateway;

class DiscountCOllectionsController extends Controller
{
    public $openGraph;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
    }

    public function index(): View
    {
        $this->setTitle('Popusti');
        $this->setDescription('Otkrijte šta je na popustu i iskoristite dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $this->openGraph
            ->set('og:title', 'Popusti')
            ->set('og:description', 'Otkrijte šta je na popustu i iskoristite dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Popusti',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $products = (new ProductsGateway())
            ->getItems(['onSale' => true])
            ->with([
                'photos',
                'slugs',
                'brand.slugs',
            ])
            ->paginate((new ProductsGateway())->defaultPaginationSize);

        return view('discount-collections/index', [
            'products' => $products,
        ]);
    }

    public function show(DiscountCollection $discountCollection): View
    {
        $this->setTitle($discountCollection->name);
        $this->setDescription('Otkrijte popust '.$discountCollection->name.' i iskoristite dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $this->openGraph
            ->set('og:title', $discountCollection->name)
            ->set('og:description', 'Otkrijte popust '.$discountCollection->name.' i iskoristite dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Popusti',
                'link' => route('discount_collections.index'),
            ],
            [
                'label' => $discountCollection->name,
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        $products = (new ProductsGateway())
            ->getItems(['onSale' => true])
            ->whereIn('id', json_decode($discountCollection->product_ids))
            ->with([
                'photos',
                'slugs',
                'brand.slugs',
            ])
            ->paginate((new ProductsGateway())->defaultPaginationSize);

        return view('discount-collections/index', [
            'products' => $products,
            'currentDiscount' => $discountCollection,
        ]);
    }
}
