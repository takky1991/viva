<?php

namespace Viva\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Viva\Http\Controllers\Controller;
use Viva\Providers\RouteServiceProvider;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public $openGraph;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');

        $this->openGraph = $this->openGraph();
    }

    /**
     * Show the application's login form.
     */
    public function showLoginForm(): View
    {
        $this->setTitle('Prijava');

        $this->openGraph
            ->set('og:title', 'Prijava');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Prijava',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('auth.login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if (session()->previousUrl() == route('checkout.one')) {
            return redirect()->route('checkout.one');
        }
    }
}
