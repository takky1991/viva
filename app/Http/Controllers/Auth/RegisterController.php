<?php

namespace Viva\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Viva\Http\Controllers\Controller;
use Viva\Providers\RouteServiceProvider;
use Viva\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::VERIFY_EMAIL;

    public $openGraph;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest');

        $this->openGraph = $this->openGraph();
    }

    /**
     * Show the application registration form.
     */
    public function showRegistrationForm(): View
    {
        $this->setTitle('Registracija');

        $this->openGraph
            ->set('og:title', 'Registracija');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Registracija',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'terms_accepted' => ['accepted'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     */
    protected function create(array $data): User
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'terms_accepted' => true,
        ]);
    }

    protected function registered(Request $request, $user)
    {
        //
    }
}
