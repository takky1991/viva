<?php

namespace Viva\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Viva\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public $openGraph;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
    }

    /**
     * Display the form to request a password reset link.
     */
    public function showLinkRequestForm(): View
    {
        $this->setTitle('Promjena šifre');

        $this->openGraph
            ->set('og:title', 'Promjena šifre');

        $breadcrumbs = [
            [
                'label' => 'Početna',
                'link' => route('home'),
            ],
            [
                'label' => 'Promjena šifre',
            ],
        ];

        $this->setBreadcrumbs($breadcrumbs);

        return view('auth.passwords.email');
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, string $response)
    {
        return redirect()
                ->route('password.request')
                ->withSuccess('Poslali smo Vam e-mail za kreiranje nove šifre!');
    }
}
