<?php

namespace Viva\Http\Controllers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Viva\Country;
use Viva\Events\OrderSuccessful;
use Viva\GuestCart;
use Viva\Http\Requests\CheckoutStepOneRequest;
use Viva\Http\Requests\CheckoutStepThreeRequest;
use Viva\Http\Requests\CheckoutStepTwoRequest;
use Viva\NewsletterContact;
use Viva\Order;
use Viva\ShippingMethod;
use Viva\User;

class CheckoutController extends Controller
{
    public function getCheckout(): RedirectResponse
    {
        // because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        if (empty($cart->shipping_method_id)) {
            return redirect()->route('checkout.one');
        }

        $details = [
            'first_name' => $cart->first_name,
            'last_name' => $cart->last_name,
            'phone' => $cart->phone,
        ];

        if (auth()->guest()) {
            $details['email'] = $cart->email;
        }

        if (! $cart->shippingMethod->pickup) {
            $details['street_address'] = $cart->street_address;
            $details['city'] = $cart->city;
        }

        foreach ($details as $detail) {
            if (empty($detail)) {
                return redirect()->route('checkout.two');
            }
        }

        return redirect()->route('checkout.three');
    }

    public function getStepOne(): View
    {
        $this->setTitle('Način dostave');

        // because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $cart->load('products.slugs', 'products.photos', 'products.brand');
        $shippingMethods = ShippingMethod::orderBy('order_id')->get()->toArray();
        $pickupMethods = ShippingMethod::where('pickup', true)->orderBy('order_id')->get();
        $deliveryMethods = ShippingMethod::where('pickup', false)->orderBy('order_id')->get();

        return view('checkout.one', [
            'cart' => $cart,
            'pickupMethods' => $pickupMethods,
            'deliveryMethods' => $deliveryMethods,
            'shippingMethods' => $shippingMethods,
        ]);
    }

    public function postStepOne(CheckoutStepOneRequest $request): RedirectResponse
    {
        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $cart->shipping_method_id = $request->shipping_method_id;
        $cart->save();

        return redirect()->route('checkout.two');
    }

    public function getStepTwo(): View
    {
        $this->setTitle('Potrebne informacije');

        $country = Country::first();

        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $cart->load('products.slugs', 'products.photos', 'products.brand');

        return view('checkout.two', [
            'user' => auth()->user(),
            'cart' => $cart,
            'country' => $country,
        ]);
    }

    public function postStepTwo(CheckoutStepTwoRequest $request): RedirectResponse
    {
        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        // Save order details to cart
        $cartOrderInfo = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'street_address' => $request->street_address,
            'postalcode' => $request->postalcode ?? null,
            'city' => $request->city,
            'country_id' => $request->street_address ? 1 : null, // Hardcoded for bosnia
            'additional_info' => $request->filled('additional_info') ? $request->additional_info : null,
        ];

        if (auth()->guest()) {
            $cartOrderInfo['email'] = $request->email;
        }

        $cart->update($cartOrderInfo);

        // register user
        if ($request->register == 'true') {
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone' => $request->phone,
                'street_address' => $request->street_address,
                'postalcode' => $request->postalcode,
                'city' => $request->city,
                'country_id' => 1, // for Bosnia
                'terms_accepted' => true,
            ]);

            event(new Registered($user));
            auth()->login($user);
        }

        return redirect()->route('checkout.three');
    }

    public function getStepThree(): View
    {
        $this->setTitle('Način plaćanja');

        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $cart->load('products.slugs', 'products.photos', 'products.brand');

        return view('checkout.three', [
            'cart' => $cart,
        ]);
    }

    public function postStepThree(CheckoutStepThreeRequest $request): RedirectResponse
    {
        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
            Order::where('guest_id', session('guest_id'))->where('status', 'processing_payment')->forceDelete();
        } else {
            $cart = auth()->user()->cart;
            auth()->user()->orders()->where('status', 'processing_payment')->forceDelete();
        }

        // Create order record
        $order = Order::create([
            'user_id' => auth()->check() ? auth()->id() : null,
            'guest_id' => auth()->guest() && session('guest_id') ? session('guest_id') : null,
            'email' => auth()->check() ? auth()->user()->email : $cart->email,
            'shipping_method_id' => $cart->shipping_method_id,
            'shipping_key' => $cart->shippingMethod->key,
            'shipping_name' => $cart->shippingMethod->name,
            'shipping_price' => $cart->free_shipping ? 0 : $cart->shippingMethod->price,
            'original_shipping_price' => $cart->shippingMethod->price,
            'shipping_pickup' => $cart->shippingMethod->pickup,
            'first_name' => $cart->first_name,
            'last_name' => $cart->last_name,
            'phone' => $cart->phone,
            'street_address' => $cart->street_address,
            'postalcode' => $cart->postalcode ?? null,
            'city' => $cart->city,
            'country_id' => $cart->street_address ? 1 : null, // Hardcoded for bosnia
            'additional_info' => $cart->additional_info,
            'payment_method' => $request->payment_method,
            'total_price' => $cart->totalPrice(),
            'total_price_with_shipping' => $cart->totalPriceWithShipping(),
        ]);

        // create order products from cart
        foreach ($cart->products as $product) {
            $order->orderProducts()->create([
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand?->name,
                'brand_description' => $product->brand?->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'free_shipping' => $product->free_shipping,
                'quantity' => $product->pivot->quantity,
                'variants' => $product->pivot->variants,
                'categories_hierarchy' => $product->categories_hierarchy,
                'added_to_cart_at' => $product->pivot->created_at,
            ]);

            $product->buyHit($product->pivot->quantity);
        }

        // Delete cart
        $cart->delete();

        if (auth()->guest()) {
            // Add user to newsletter contacts
            $newsletterContact = NewsletterContact::where('email', $order->email)->first();

            if (! $newsletterContact) {
                NewsletterContact::create([
                    'email' => $order->email,
                    'first_name' => $order->first_name,
                    'last_name' => $order->last_name,
                    'token' => random_token(),
                ]);
            }
        }

        event(new OrderSuccessful($order));

        return redirect()->route('checkout.success', [
            'order_number' => $order->order_number,
        ]);
    }

    public function getSuccess(Order $order): View
    {
        $this->setTitle('Uspješno');

        $firstTimeShowing = ! $order->confirmation_shown;

        $order->load('orderProducts.product.slugs', 'orderProducts.product.photos', 'orderProducts.product.brand');

        return view('checkout.success', [
            'order' => $order,
            'firstTimeShowing' => $firstTimeShowing,
        ]);
    }

    public function onlinePaymentCancel(): RedirectResponse
    {
        return redirect()->route('checkout.three');
    }

    public function onlinePaymentSuccess(Request $request): RedirectResponse
    {
        if (! array_key_exists('QUERY_STRING', $_SERVER)) {
            return redirect()->route('home');
        }

        $queryString = $_SERVER['QUERY_STRING'];
        parse_str($queryString, $queryParams);

        if (! array_key_exists('digest', $queryParams) || ! array_key_exists('order_number', $queryParams)) {
            return redirect()->route('home');
        }

        $requestDigest = $queryParams['digest'];
        unset($queryParams['digest']);

        $routeWithoutDigest = $request->url().'?'.http_build_query($queryParams);
        $digest = hash('sha512', config('app.monri_key').$routeWithoutDigest);

        $order = Order::where('order_number', $request->order_number)->first();

        if ($digest != $requestDigest || ! $order || $order->status != 'processing_payment') {
            return redirect()->route('home');
        }
        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $order->update([
            'status' => 'created',
            'card_payment_status' => 'authorized',
            'card_number_masked' => $queryParams['masked_pan'],
            'card_type' => $queryParams['cc_type'],
        ]);

        foreach ($cart->products as $product) {
            $product->buyHit($product->pivot->quantity);
        }

        // Delete cart
        $cart->delete();

        if (auth()->guest()) {
            // Add user to newsletter contacts
            $newsletterContact = NewsletterContact::where('email', $order->email)->first();

            if (! $newsletterContact) {
                NewsletterContact::create([
                    'email' => $order->email,
                    'first_name' => $order->first_name,
                    'last_name' => $order->last_name,
                    'token' => random_token(),
                ]);
            }
        }

        event(new OrderSuccessful($order));

        return redirect()->route('checkout.success', [
            'order_number' => $order->order_number,
        ]);
    }
}
