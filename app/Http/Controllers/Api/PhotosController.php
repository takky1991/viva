<?php

namespace Viva\Http\Controllers\Api;

use Illuminate\Support\Facades\Storage;
use Viva\Article;
use Viva\Category;
use Viva\HeroArticle;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\PhotoUploadRequest;
use Viva\Http\Resources\PhotoCollection;
use Viva\Photo;
use Viva\PhotoProduct;
use Viva\Product;

class PhotosController extends ApiController
{
    public function store(PhotoUploadRequest $request)
    {
        $photos = collect();

        if ($request->file('photos')) {
            foreach ($request->file('photos') as $file) {
                $token = bin2hex(openssl_random_pseudo_bytes(32));
                $preferredStyle = null;
                $processSynchronous = false;

                if ($request->filled('preferred_style')) {
                    $preferredStyle = $request->preferred_style;
                }

                if ($request->filled('process_synchronous')) {
                    $processSynchronous = $request->process_synchronous;
                }

                if (! Storage::disk('local')->exists('public/'.env('AWS_PHOTO_FOLDER', 'photos'))) {
                    // path does not exist
                    Storage::disk('local')->makeDirectory('public/'.env('AWS_PHOTO_FOLDER', 'photos'));
                }

                Storage::disk('public')->makeDirectory(env('AWS_PHOTO_FOLDER', 'photos').'/'.$token, 0775);
                $path = $file->storeAs(env('AWS_PHOTO_FOLDER', 'photos'), $token.'/original.'.strtolower($file->getClientOriginalExtension()), 'public');

                $photo = new Photo();
                $photo->user_id = auth()->id();
                $data = [
                    'file_name' => $file->getClientOriginalName(),
                    'file_size' => $file->getSize(),
                    'mime_type' => $file->getMimeType(),
                    'extension' => strtolower($file->getClientOriginalExtension()),
                    'uri' => $path,
                    'token' => $token,
                    'target' => $request->target,
                    'target_id' => $request->target_id,
                    'preferred_style' => ($request->filled('preferred_style') ? $request->preferred_style : null),
                ];

                $target = $request->target;
                $targetId = $request->target_id;

                foreach (Photo::getStyles($target) as $style => $dimension) {
                    $data['processed_'.$style] = false;
                }

                $photo->fill($data);
                $photo->save();

                $photo->optimizeAndStore($preferredStyle, $processSynchronous);

                if (! empty($target) && ! empty($targetId)) {
                    switch ($target) {
                        case 'product':
                            $product = Product::findOrFail($targetId);
                            $orderId = $product->hasPhotos() ? $product->photos->last()->photoProduct->order_id + 1 : 0;
                            $product->photos()->attach($photo->id, ['order_id' => $orderId]);
                            $product->save(); // also updates photo on algolia
                            break;

                        case 'hero_article':
                            if ($request->filled('preferred_device') && $request->preferred_device == 'mobile') {
                                HeroArticle::where('id', $targetId)->update(['mobile_photo_id' => $photo->id]);
                            } else {
                                HeroArticle::where('id', $targetId)->update(['photo_id' => $photo->id]);
                            }
                            break;

                        case 'category':
                            Category::where('id', $targetId)->update(['photo_id' => $photo->id]);
                            break;

                        case 'article':
                            Article::where('id', $targetId)->update(['photo_id' => $photo->id]);
                            break;
                    }
                }

                $photo->refresh();

                $photos[] = $photo;
            }

            return new PhotoCollection($photos);
        }

        return $this->respondWithError();
    }

    public function destroy(Photo $photo)
    {
        $photoId = $photo->id;

        //If photo was from an hero article, remove it
        HeroArticle::where('photo_id', $photo->id)->update(['photo_id' => null]);

        //If photo was from an article, remove it
        Article::where('photo_id', $photo->id)->update(['photo_id' => null]);

        //If photo was from an category, remove it
        Category::where('photo_id', $photo->id)->update(['photo_id' => null]);

        // If photo was from a product we need to reorder the remaining photos
        $photoProduct = PhotoProduct::where('photo_id', $photoId)->first();

        if ($photo->delete()) {
            if ($photoProduct) {
                foreach ($photoProduct->product->photos as $index => $productPhoto) {
                    $photoProduct->product->photos()->updateExistingPivot($productPhoto->id, ['order_id' => $index]);
                }

                $photoProduct->product->save(); // Updates the photo on algolia
            }

            return $this->respondWithDeleted();
        }

        return $this->respondWithErrors();
    }
}
