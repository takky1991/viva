<?php

namespace Viva\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Viva\GuestCart;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\AddGuestCartItemRequest;
use Viva\Http\Requests\Api\GetGuestCartRequest;
use Viva\Http\Requests\Api\RemoveGuestCartItemRequest;
use Viva\Http\Resources\GuestCartResource;
use Viva\Product;
use Viva\ProductVariant;

class GuestCartsController extends ApiController
{
    public function get(GetGuestCartRequest $request)
    {
        $guestCart = GuestCart::where('guest_id', $request->gid)->with('products.photos', 'products.slugs', 'products.brand')->first();

        if (empty($guestCart)) {
            return $this->respondWithDeleted('Guest cart not found');
        }

        return new GuestCartResource($guestCart);
    }

    public function addItem(AddGuestCartItemRequest $request)
    {
        $guestCart = GuestCart::where('guest_id', $request->gid)->with('products')->first();
        $variant = $request->variant_id ? ProductVariant::find($request->variant_id) : null;

        if (empty($guestCart)) {
            $guestCart = GuestCart::create([
                'guest_id' => $request->gid,
            ]);

            $guestCart->products()->attach($request->id, [
                'quantity' => $request->quantity,
                'variants' => $variant?->name,
            ]);
        } else {
            $product = $guestCart->products()->find($request->id);

            if (empty($product)) {
                $guestCart->products()->attach($request->id, [
                    'quantity' => $variant ? 1 : $request->quantity,
                    'variants' => $variant?->name,
                ]);
            } else {
                $guestCart->products()->updateExistingPivot($product->id, [
                    'quantity' => $variant ? ($product->pivot->quantity + 1) : ($product->pivot->quantity + $request->quantity),
                    'variants' => $product->pivot->variants.($variant ? (','.$variant->name) : ''),
                ]);
            }
        }

        DB::table('products')->where('id', $request->id)->increment('add_to_cart_count');

        $guestCart->refresh();
        $guestCart->load('products.photos', 'products.slugs', 'products.brand');

        return new GuestCartResource($guestCart);
    }

    public function removeItem(RemoveGuestCartItemRequest $request, Product $product)
    {
        $guestCart = GuestCart::where('guest_id', $request->gid)->first();
        $product = $guestCart->products()->find($product->id);

        if (empty($product)) {
            return $this->respondWithErrors();
        } else {
            $guestCart->products()->detach($product->id);
            $guestCart->refresh();
            $guestCart->load('products');

            if ($guestCart->products->isEmpty()) {
                $guestCart->delete();
            }

            return $this->respondWithDeleted();
        }
    }
}
