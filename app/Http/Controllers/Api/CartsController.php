<?php

namespace Viva\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\AddCartItemRequest;
use Viva\Http\Resources\CartResource;
use Viva\Product;
use Viva\ProductVariant;

class CartsController extends ApiController
{
    public function get()
    {
        $cart = auth()->user()->cart()->with('products.photos', 'products.slugs', 'products.brand')->first();

        if (empty($cart)) {
            return $this->respondWithDeleted('Cart not found');
        }

        return new CartResource($cart);
    }

    public function addItem(AddCartItemRequest $request)
    {
        $cart = auth()->user()->cart;
        $variant = $request->variant_id ? ProductVariant::find($request->variant_id) : null;

        if (empty($cart)) {
            $cart = auth()->user()->cart()->create();
            $cart->products()->attach($request->id, [
                'quantity' => $request->quantity,
                'variants' => $variant?->name,
            ]);
        } else {
            $product = $cart->products()->find($request->id);

            if (empty($product)) {
                $cart->products()->attach($request->id, [
                    'quantity' => $variant ? 1 : $request->quantity,
                    'variants' => $variant?->name,
                ]);
            } else {
                $cart->products()->updateExistingPivot($product->id, [
                    'quantity' => $variant ? ($product->pivot->quantity + 1) : ($product->pivot->quantity + $request->quantity),
                    'variants' => $product->pivot->variants.($variant ? (','.$variant->name) : ''),
                ]);
            }
        }

        DB::table('products')->where('id', $request->id)->increment('add_to_cart_count');

        $cart->refresh();
        $cart->load('products.photos', 'products.slugs', 'products.brand');

        return new CartResource($cart);
    }

    public function removeItem(Product $product)
    {
        $cart = auth()->user()->cart;

        if (empty($cart)) {
            return $this->respondWithErrors('Korpa ne postoji');
        }

        $product = $cart->products()->find($product->id);

        if (empty($product)) {
            return $this->respondWithErrors('Proizvod ne postoji u korpi.');
        } else {
            $cart->products()->detach($product->id);
            $cart->refresh();
            $cart->load('products');

            if ($cart->products->isEmpty()) {
                $cart->delete();
            }

            return $this->respondWithDeleted();
        }
    }
}
