<?php

namespace Viva\Http\Controllers\Api;

use Illuminate\Http\Request;
use Viva\GuestCart;
use Viva\Http\Controllers\ApiController;
use Viva\Order;

class OrdersController extends ApiController
{
    public function createOnlinePaymentOrder(Request $request)
    {
        // Because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth('api')->check()) {
            $cart = auth('api')->user()->cart;
            auth('api')->user()->orders()->where('status', 'processing_payment')->forceDelete();
        } elseif ($request->filled('guest_id')) {
            $cart = GuestCart::where('guest_id', $request->guest_id)->first();
            Order::where('guest_id', $request->guest_id)->where('status', 'processing_payment')->forceDelete();
        }

        // Create order record
        $order = Order::create([
            'user_id' => auth('api')->check() ? auth('api')->id() : null,
            'guest_id' => auth('api')->guest() && $request->filled('guest_id') ? $request->guest_id : null,
            'email' => auth('api')->check() ? auth('api')->user()->email : $cart->email,
            'shipping_method_id' => $cart->shipping_method_id,
            'shipping_key' => $cart->shippingMethod->key,
            'shipping_name' => $cart->shippingMethod->name,
            'shipping_price' => $cart->free_shipping ? 0 : $cart->shippingMethod->price,
            'original_shipping_price' => $cart->shippingMethod->price,
            'shipping_pickup' => $cart->shippingMethod->pickup,
            'first_name' => $cart->first_name,
            'last_name' => $cart->last_name,
            'phone' => $cart->phone,
            'street_address' => $cart->street_address,
            'postalcode' => $cart->postalcode ?? null,
            'city' => $cart->city,
            'country_id' => $cart->street_address ? 1 : null, // Hardcoded for bosnia
            'additional_info' => $cart->additional_info,
            'payment_method' => 'online_payment',
            'total_price' => $cart->totalPrice(),
            'total_price_with_shipping' => $cart->totalPriceWithShipping(),
            'status' => 'processing_payment',
        ]);

        // create order products from cart
        foreach ($cart->products as $product) {
            $order->orderProducts()->create([
                'product_id' => $product->id,
                'brand_id' => $product->brand_id,
                'brand_name' => $product->brand?->name,
                'brand_description' => $product->brand?->description,
                'title' => $product->title,
                'package_description' => $product->package_description,
                'description' => $product->description,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'on_sale' => $product->on_sale,
                'published' => $product->published,
                'in_stock' => $product->in_stock,
                'free_shipping' => $product->free_shipping,
                'quantity' => $product->pivot->quantity,
                'variants' => $product->pivot->variants,
                'categories_hierarchy' => $product->categories_hierarchy,
                'added_to_cart_at' => $product->pivot->created_at,
            ]);
        }

        return [
            'full_name' => $order->first_name.' '.$order->last_name,
            'phone' => $order->phone,
            'email' => $order->email,
            'street_address' => $order->street_address,
            'city' => $order->city,
            'postalcode' => $order->postalcode,
            'order_info' => $cart->shippingMethod->pickup ? ('Preuzimanje u '.$cart->shippingMethod->name) : ('Slanje na adresu ('.$cart->shippingMethod->name.')'),
            'order_number' => $order->order_number,
            'amount' => str_replace('.', '', number_format($cart->totalPriceWithShipping(), 2, '.', ',')),
            'token' => config('app.monri_token'),
            'digest' => hash('sha512', config('app.monri_key').$order->order_number.str_replace('.', '', number_format($cart->totalPriceWithShipping(), 2, '.', ',')).'BAM'),
        ];
    }

    public function seen(Order $order)
    {
        $seen = $order->confirmation_shown;
        $order->confirmation_shown = true;
        $order->save();

        return $seen;
    }
}
