<?php

namespace Viva\Http\Controllers\Api;

use Illuminate\Http\Request;
use Viva\Http\Controllers\ApiController;
use Viva\NewsletterContact;

class WebhooksController extends ApiController
{
    public function mailgunPermanentFail(Request $request)
    {
        $email = $request->get('event-data')['recipient'];
        NewsletterContact::where('email', $email)->delete();

        return $this->respondWithOk();
    }
}
