<?php

namespace Viva\Http\Controllers\Api;

use Viva\Events\ReviewSubmitted;
use Viva\Http\Controllers\ApiController;
use Viva\Http\Requests\Api\CreateBackToStockAlertRequest;
use Viva\Http\Requests\Api\CreateReviewRequest;
use Viva\Http\Resources\ReviewCollection;
use Viva\Product;

class ProductsController extends ApiController
{
    public function createReview(Product $product, CreateReviewRequest $request)
    {
        $review = $product->reviews()->create($request->all());

        if (auth('api')->check()) {
            $review->user_id = auth('api')->id();
            $review->save();
        }

        event(new ReviewSubmitted($review));

        return $this->respondWithOk();
    }

    public function getReviews(Product $product)
    {
        $reviews = $product->publishedReviews()->paginate(5);

        return new ReviewCollection($reviews);
    }

    public function createBackToStockAlert(CreateBackToStockAlertRequest $request, Product $product)
    {
        $product->backToStockAlerts()->firstOrCreate([
            'email' => $request->email,
            'notification_sent' => false,
        ]);

        return $this->respondWithOk();
    }
}
