<?php

namespace Viva\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Viva\Brand;
use Viva\Category;
use Viva\DiscountCollection;
use Viva\Library\OpenGraph\OpenGraph;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public $randomReviewerNames = [
        'Alma Porčić',
        'Edita Halkić',
        'Indira Hodžić',
        'Jasna Bratić',
        'Azra Alagić',
        'Irma Burzić',
        'Amela Hadžić',
        'Emila Pirić',
        'Selma Alibabić',
        'Amra Lipović',
        'Ena Mustedanagić',
        'Nejra Džinić',
        'Berina Ljubijankić',
        'Emina Maglajac',
        'Nermina Odobašić',
        'Fatima Huremagić',
        'Maja Bašić',
        'Sabina Bešlagić',
        'Edita Mulalić',
        'Nikolina Mušinović',
    ];

    public function __construct()
    {
        view()->share('randomReviewerNames', $this->randomReviewerNames);

        $this->setMainMenuCategories();
        $this->setFeaturedBrands();
        $this->setMainMenuDiscounts();
        $this->setIsMobileApp();
    }

    public function openGraph()
    {
        return new OpenGraph();
    }

    public function setTitle($title)
    {
        view()->share('title', $title);
    }

    public function setDescription($description)
    {
        view()->share('description', $description);
    }

    public function setCanonical($value)
    {
        view()->share('canonical', $value);
    }

    public function setBreadcrumbs($breadcrumbs)
    {
        view()->share('breadcrumbs', $breadcrumbs);
    }

    public function setIsMobileApp()
    {
        $isMobileApp = in_array(app()->make('DeviceHandler')->getDevice()['source'], ['android', 'ios']);
        view()->share('isMobileApp', $isMobileApp);
    }

    public function setMainMenuCategories()
    {
        $mainMenuCategories = Cache::remember('mainMenuCategories', 2592000, function () { //cache for a month
            return Category::whereNoParent()
                ->where('menu', true)
                ->with(['slugs', 'menuChildren.slugs', 'menuChildren.menuChildren.slugs'])
                ->get();
        });

        view()->share('mainMenuCategories', $mainMenuCategories);
    }

    public function setFeaturedBrands()
    {
        $featuredBrands = Cache::remember('featuredBrands', 2592000, function () { //cache for a month
            return Brand::where('featured', true)->orderBy('name', 'asc')->with(['slugs'])->get();
        });

        view()->share('featuredBrands', $featuredBrands);
    }

    public function setMainMenuDiscounts()
    {
        $mainMenuDiscounts = Cache::remember('mainMenuDiscounts', 86400, function () { //cache for a day
            return DiscountCollection::where('active', true)->whereNotNull('product_ids')->orderBy('discount_percentage', 'DESC')->get();
        });

        view()->share('mainMenuDiscounts', $mainMenuDiscounts);
    }
}
