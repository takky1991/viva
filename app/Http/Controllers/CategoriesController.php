<?php

namespace Viva\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Viva\Category;
use Viva\Gateways\ProductsGateway;

class CategoriesController extends Controller
{
    public $openGraph;

    public function __construct()
    {
        parent::__construct();

        $this->openGraph = $this->openGraph();
        $this->productsGateway = new ProductsGateway();
    }

    public function show(Request $request, Category $category): View
    {
        $this->setTitle($category->title);
        $this->setDescription('Otkrij sve proizvode u kategoriji '.$category->title.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $this->openGraph
            ->set('og:title', $category->title)
            ->set('og:description', 'Otkrij sve proizvode u kategoriji '.$category->title.' i naruči omiljeni uz dostavu od '.config('app.lowest_shipping_cost').'KM. Samo na '.config('app.name').'.');

        $parentCategory = $category->parent;

        $breadcrumbs = [
            [
                'label' => $category->title,
            ],
        ];

        while ($parentCategory) {
            $breadcrumbs[] = [
                'label' => $parentCategory->title,
                'link' => route('categories.show', ['category' => $parentCategory->getSlug()]),
            ];

            $parentCategory = $parentCategory->parent;
        }

        $breadcrumbs[] = [
            'label' => 'Početna',
            'link' => route('home'),
        ];

        $breadcrumbs = array_reverse($breadcrumbs);

        $this->setBreadcrumbs($breadcrumbs);

        $products = $this->productsGateway
            ->getItems($request->all(), true)
            ->whereHas('categories', function (Builder $query) use ($category) {
                $query->where('categories.id', $category->id);
            });

        // Eager load relationships
        $products->with([
            'photos',
            'slugs',
            'brand.slugs',
            'categories' => function ($query) use ($category) {
                $query->where('categories.id', $category->id);
            },
        ]);

        // Paginate
        $products = $products->paginate($this->productsGateway->defaultPaginationSize);

        return view('categories.show', [
            'category' => $category,
            'products' => $products,
        ]);
    }
}
