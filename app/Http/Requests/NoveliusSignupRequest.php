<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoveliusSignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'terms_accepted' => 'accepted'
        ];
    }
}
