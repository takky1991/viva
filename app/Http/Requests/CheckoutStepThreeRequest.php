<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutStepThreeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'payment_method' => 'required|in:cash_on_delivery',
            'terms_of_purchase' => 'required|accepted',
        ];
    }
}
