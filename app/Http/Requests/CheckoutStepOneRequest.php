<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutStepOneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'shipping_method_id' => 'required|exists:shipping_methods,id',
        ];
    }
}
