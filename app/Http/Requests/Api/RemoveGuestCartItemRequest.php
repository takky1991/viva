<?php

namespace Viva\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RemoveGuestCartItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'gid' => 'required|uuid|exists:guest_carts,guest_id',
        ];
    }

    protected function prepareForValidation()
    {
        $input = $this->all();

        if (! empty($this->headers->get('X-Anonymous-Customer-Unique-Id'))) {
            $input['gid'] = $this->headers->get('X-Anonymous-Customer-Unique-Id');
        }

        // replace old input with new input
        $this->replace($input);
    }
}
