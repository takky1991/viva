<?php

namespace Viva\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Viva\Product;

class AddCartItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $rules = [
            'id' => [
                'required',
                Rule::exists('products')->where(function ($query) {
                    $query->where('published', true)
                        ->where('in_stock', true)
                        ->whereNull('deleted_at');
                }),
            ],
            'quantity' => 'required|integer|min:1|max:50',
        ];

        if ($this->id) {
            $product = Product::find($this->id);

            if ($product && $product->variants->isNotEmpty()) {
                $rules['variant_id'] = [
                    'required',
                    Rule::exists('product_variants', 'id')->where(function ($query) {
                        $query->where('product_id', $this->id)
                            ->where('in_stock', true);
                    }),
                ];
            }
        }

        return $rules;
    }
}
