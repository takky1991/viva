<?php

namespace Viva\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Viva\Rules\MaxPhotosPerProduct;

class PhotoUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $rules = [
            'photos' => 'required|min:1',
            'photos.*' => 'image|mimes:jpeg,jpg,png,gif|max:10000',
            'target' => 'required',
            'target_id' => 'required|numeric|integer',
        ];

        if ($this->input('target') == 'product') {
            $rules['photos'] = [
                'required',
                'min:1',
                new MaxPhotosPerProduct($this->input('target_id'), 10),
            ];
        }

        return $rules;
    }
}
