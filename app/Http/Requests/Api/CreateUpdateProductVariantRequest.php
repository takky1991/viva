<?php

namespace Viva\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateUpdateProductVariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        if ($this->productVariant) {
            return [
                'in_stock' => 'boolean',
            ];
        } else {
            return [
                'name' => [
                    'required',
                    Rule::unique('product_variants')->where(function ($query) {
                        return $query->where('product_id', $this->product->id);
                    }),
                ],
            ];
        }
    }
}
