<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'products' => 'required|array',
            'products.*.id' => [
                'required',
                Rule::exists('products')->where(function ($query) {
                    $query->where('published', true)
                        ->where('in_stock', true)
                        ->whereNull('deleted_at');
                }),
            ],
            'products.*.quantity' => 'nullable|integer|min:1|max:50',
        ];
    }
}
