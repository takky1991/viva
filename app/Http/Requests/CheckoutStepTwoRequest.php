<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Viva\GuestCart;

class CheckoutStepTwoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(Request $request): array
    {
        // because of the cart.products middleware we are sure that a cart with products exists
        $cart = null;

        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'register' => 'in:true,false,0,1',
            'password' => 'required_if:register,true|string|min:8|confirmed',
        ];

        if (auth()->guest()) {
            $rules['email'] = ($request->register != 'true' ? 'required|email|max:255' : 'required|email|max:255|unique:users');
            $rules['terms_accepted'] = ($request->register != 'true' ? '' : 'accepted');
        }

        if (! $cart->shippingMethod->pickup) {
            $rules['street_address'] = 'required|string|max:255';
            $rules['postalcode'] = 'nullable|numeric|integer';
            $rules['city'] = 'required|string|max:255';
        }

        return $rules;
    }
}
