<?php

namespace Viva\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Viva\Country;

class UpdateSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'gender' => 'in:0,1,2',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            //'email' => 'required|string|email|max:255|unique:users,email,' .  auth()->id(),
            'birth_date' => 'nullable|date',
            'street_address' => 'nullable|required_with:postalcode,city|string|max:255',
            'postalcode' => 'nullable|required_with:street_address,city|numeric|integer',
            'city' => 'nullable|required_with:street_address,postalcode|string|max:255',
            'newsletter' => 'boolean',
            //'country_id' => 'required_with:street_address,postalcode,city|in:0,' . implode(',', Country::all()->pluck('id')->toArray())
        ];
    }

    /**
     * Modify the input values
     */
    protected function prepareForValidation(): void
    {
        $input = $this->all();

        if (
            array_key_exists('birth_day', $input) &&
            array_key_exists('birth_month', $input) &&
            array_key_exists('birth_year', $input) &&
            $input['birth_day'] >= 1 && $input['birth_day'] <= 31 &&
            $input['birth_month'] >= 1 && $input['birth_month'] <= 12 &&
            $input['birth_year'] >= now()->year - 100 && $input['birth_year'] <= now()->year
        ) {
            $input['birth_date'] = Carbon::create($input['birth_year'], $input['birth_month'], $input['birth_day']);
        } else {
            $input['birth_date'] = null;
        }

        // replace old input with new input
        $this->replace($input);
    }
}
