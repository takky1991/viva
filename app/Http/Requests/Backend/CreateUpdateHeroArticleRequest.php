<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateHeroArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'required|min:3|max:40',
            'subtitle' => 'required|min:3|max:250',
            'button_text' => 'required|min:3|max:20',
            'button_url' => 'required|min:3',
            'brand_id' => 'nullable|exists:brands,id',
        ];
    }
}
