<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Viva\Category;

class CreateUpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $category = $this->category;

        if (empty($category)) {
            $categories = Category::all()->pluck('id')->toArray();
        } else {
            $categories = Category::where('id', '!=', $category->id)->get()->pluck('id')->toArray();
        }

        return [
            'title' => 'required|min:3|max:30|unique:categories,title,'.($category ? $category->id : ''),
            'description' => 'nullable|min:3',
            'parent_id' => 'nullable|in:'.implode(',', $categories),
        ];
    }
}
