<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Propaganistas\LaravelPhone\PhoneNumber;

class CreateSmsNumberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'number' => 'required|unique:sms_numbers,number|phone:BA,mobile',
        ];
    }

    /**
     * Modify the input values
     */
    protected function prepareForValidation(): void
    {
        $input = $this->all();

        if (array_key_exists('number', $input) && ! empty($input['number'])) {
            $input['number'] = (string) PhoneNumber::make($input['number'], 'BA');
        }

        // replace old input with new input
        $this->replace($input);
    }
}
