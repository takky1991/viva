<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;

class CreateUpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $roles = Role::all()->pluck('name')->toArray();

        return [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'email' => 'required|email|unique:users,email'.(empty($this->user) ? '' : ','.$this->user->id),
            'role_name' => 'required|in:'.implode(',', $roles),
        ];
    }
}
