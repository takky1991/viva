<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $brand = $this->brand;

        return [
            'name' => 'required|min:2|max:30|unique:brands,name,'.($brand ? $brand->id : ''),
            'description' => 'nullable|min:3',
        ];
    }
}
