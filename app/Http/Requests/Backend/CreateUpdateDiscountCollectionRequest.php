<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateDiscountCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $discountCollection = $this->discountCollection;

        return [
            'name' => 'required|min:3|max:30|unique:discount_collections,name,'.($discountCollection ? $discountCollection->id : ''),
            'discount_percentage' => 'required|in:5,10,15,20,25,30,35,40,45,50',
        ];
    }
}
