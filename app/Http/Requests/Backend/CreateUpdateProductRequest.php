<?php

namespace Viva\Http\Requests\Backend;

use Viva\Brand;
use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'required|min:1|max:80',
            'package_description' => 'required',
            'price' => 'required|numeric|min:0.05',
            'discount_price' => 'nullable|numeric|lt:price|min:0.05|required_if:on_sale,1',
            'on_sale' => 'required|boolean',
            'brand_id' => 'required|exists:brands,id',
            'published' => 'required|boolean',
            'categories' => 'required|array',
            'categories.*' => 'exists:categories,id',
            'featured_text' => 'max:25'
        ];
    }

    /**
     * Modify the input values
     */
    protected function prepareForValidation(): void
    {
        $input = $this->all();
        $brand = Brand::find($input['brand_id']);

        if ($brand) {
            if (substr_count($input['title'], $brand->name)) {
                $input['title'] = ucfirst(trim(str_replace($brand->name, '', $input['title'])));
            }

            $lowercase = strtolower($brand->name);
            if (substr_count($input['title'], $lowercase)) {
                $input['title'] = ucfirst(trim(str_replace($lowercase, '', $input['title'])));
            }
        }

        // replace old input with new input
        $this->replace($input);
    }
}
