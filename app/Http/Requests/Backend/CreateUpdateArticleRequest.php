<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $article = $this->article;

        return [
            'title' => 'required|min:5|max:100|unique:articles,title,'.($article ? $article->id : ''),
            'subtitle' => 'required|min:5',
            'published' => 'boolean',
        ];
    }
}
