<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Viva\Supplier;

class CreateUpdatePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $suppliers = Supplier::all()->pluck('id')->toArray();

        $rules = [
            'supplier_id' => 'required|in:'.implode(',', $suppliers),
            'delivered_at' => 'required|date_format:d-m-Y',
            'pay_until' => 'required|date_format:d-m-Y',
            'amount' => 'required|numeric',
            'paid' => 'sometimes|required|boolean',
        ];

        if ($this->payment != null) {
            if ($this->request->get('supplier_id') != null) {
                $rules['invoice_id'] = [
                    'required',
                    Rule::unique('payments')->where(function ($query) {
                        return $query->where('supplier_id', $this->request->get('supplier_id'));
                    })
                    ->ignore($this->payment->id),
                ];
            } else {
                $rules['invoice_id'] = 'required';
            }
        } else {
            if ($this->request->get('supplier_id') != null) {
                $rules['invoice_id'] = [
                    'required',
                    Rule::unique('payments')->where(function ($query) {
                        return $query->where('supplier_id', $this->request->get('supplier_id'));
                    }),
                ];
            } else {
                $rules['invoice_id'] = 'required';
            }
        }

        return $rules;
    }
}
