<?php

namespace Viva\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Viva\Order;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $order = request()->route()->order;
        $statuses = $order->shipping_pickup ? Order::$pickupStatuses : Order::$shippingStatuses;
        $cardPaymentStatuses = Order::$cardPaymentStatuses;

        return [
            'status' => 'required|in:'.implode(',', $statuses),
            'card_payment_status' => 'in:'.implode(',', $cardPaymentStatuses),
        ];
    }
}
