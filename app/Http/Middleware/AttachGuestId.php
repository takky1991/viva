<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AttachGuestId
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->guest() && ! session()->has('guest_id')) {
            session(['guest_id' => newGuid()]);
        } elseif (auth()->check()) {
            session()->forget('guest_id');
        }

        return $next($request);
    }
}
