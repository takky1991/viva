<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ParseIncomingRequest
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        app()->make('DeviceHandler')->setDevice($request);

        return $next($request);
    }
}
