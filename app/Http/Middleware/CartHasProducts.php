<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Viva\GuestCart;

class CartHasProducts
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check() && auth()->user()->cart && auth()->user()->cart->products->isNotEmpty()) {
            return $next($request);
        }

        if (auth()->guest() && session()->has('guest_id')) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();

            if ($cart && $cart->products->isNotEmpty()) {
                return $next($request);
            }
        }

        return redirect()->route('cart.show');
    }
}
