<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Viva\GuestCart;

class CartHasOrderDetails
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->guest()) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->first();
        } else {
            $cart = auth()->user()->cart;
        }

        $details = [
            'first_name' => $cart->first_name,
            'last_name' => $cart->last_name,
            'phone' => $cart->phone,
        ];

        if (auth()->guest()) {
            $details['email'] = $cart->email;
        }

        if (! $cart->shippingMethod->pickup) {
            $details['street_address'] = $cart->street_address;
            $details['city'] = $cart->city;
        }

        foreach ($details as $detail) {
            if (empty($detail)) {
                return redirect()->route('checkout.two');
            }
        }

        return $next($request);
    }
}
