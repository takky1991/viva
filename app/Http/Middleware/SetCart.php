<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Viva\GuestCart;

class SetCart
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check() && ! empty(auth()->user()->cart)) {
            $cart = auth()->user()->cart;
            $cart->load('products.photos', 'products.slugs', 'products.brand');
        } elseif (auth()->guest() && session()->has('guest_id')) {
            $cart = GuestCart::where('guest_id', session('guest_id'))->with('products.photos', 'products.slugs', 'products.brand')->first();
        }

        if (! empty($cart)) {
            view()->share('cartJson', $cart->toSearchableJson());

            if (
                request()->route()->getName() == 'cart.show' ||
                request()->route()->getName() == 'checkout.one' ||
                request()->route()->getName() == 'checkout.two' ||
                request()->route()->getName() == 'checkout.three'
            ) {
                if ($cart->product_price_changed) {
                    view()->share('cartProductPriceChanged', true);
                    $cart->product_price_changed = false;
                    $cart->save();
                }

                if ($cart->product_removed) {
                    view()->share('cartProductRemoved', true);
                    $cart->product_removed = false;
                    $cart->save();
                }
            }
        }

        return $next($request);
    }
}
