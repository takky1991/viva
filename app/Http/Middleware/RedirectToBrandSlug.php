<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectToBrandSlug
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->brand && $brandSlug = $request->brand->getSlug()) {
            if ($brandSlug != $request->route()->originalParameter('brand')) {
                $args = ['brand' => $brandSlug];

                if ($request->category) {
                    $args['category'] = $request->category->getSlug();
                }

                return redirect(route('brands.show', $args), 301);
            }
        }

        return $next($request);
    }
}
