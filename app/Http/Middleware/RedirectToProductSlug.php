<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectToProductSlug
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->product->getSlug() != $request->route()->originalParameter('product')) {
            return redirect($request->product->url(), 301);
        }

        return $next($request);
    }
}
