<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectToDiscountCollectionSlug
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->discountCollection->getSlug() != $request->route()->originalParameter('discountCollection')) {
            return redirect(route('discount_collections.show', ['discountCollection' => $request->discountCollection->getSlug()]), 301);
        }

        return $next($request);
    }
}
