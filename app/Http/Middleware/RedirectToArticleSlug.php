<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectToArticleSlug
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->article->getSlug() != $request->route()->originalParameter('article')) {
            return redirect(route('articles.show', ['article' => $request->article->getSlug()]), 301);
        }

        return $next($request);
    }
}
