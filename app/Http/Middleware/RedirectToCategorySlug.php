<?php

namespace Viva\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Viva\Category;

class RedirectToCategorySlug
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->category && $request->category instanceof Category && $categorySlug = $request->category->getSlug()) {
            if ($categorySlug != $request->route()->originalParameter('category')) {
                $routeName = $request->route()->getName();

                switch ($routeName) {
                    case 'categories.show':
                        return redirect(route('categories.show', ['category' => $categorySlug]), 301);
                    break;

                    case 'brands.show':
                        return redirect(route('brands.show', ['brand' => $request->brand->getSlug(), 'category' => $categorySlug]), 301);
                    break;
                }
            }
        }

        return $next($request);
    }
}
