<?php

namespace Viva\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Viva\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Viva\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Viva\Http\Middleware\TrustProxies::class,
        \Viva\Http\Middleware\ParseIncomingRequest::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Viva\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Viva\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
            \Viva\Http\Middleware\AttachGuestId::class,
            \Viva\Http\Middleware\SecurityHeaders::class,
            \Spatie\Csp\AddCspHeaders::class,
            \Viva\Http\Middleware\SetCart::class,
        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $middlewareAliases = [
        'admin' => \Viva\Http\Middleware\MustBeAdmin::class,
        'auth' => \Viva\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Viva\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'categories.slug' => \Viva\Http\Middleware\RedirectToCategorySlug::class,
        'brands.slug' => \Viva\Http\Middleware\RedirectToBrandSlug::class,
        'product.slug' => \Viva\Http\Middleware\RedirectToProductSlug::class,
        'articles.slug' => \Viva\Http\Middleware\RedirectToArticleSlug::class,
        'discount_collection.slug' => \Viva\Http\Middleware\RedirectToDiscountCollectionSlug::class,
        'cart.products' => \Viva\Http\Middleware\CartHasProducts::class,
        'cart.shipping_method' => \Viva\Http\Middleware\CartHasShippingMethod::class,
        'cart.order_details' => \Viva\Http\Middleware\CartHasOrderDetails::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Viva\Http\Middleware\Authenticate::class,
        \Illuminate\Routing\Middleware\ThrottleRequests::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
        \Viva\Http\Middleware\AttachGuestId::class,
        \Viva\Http\Middleware\SecurityHeaders::class,
        \Viva\Http\Middleware\SetCart::class,
    ];
}
