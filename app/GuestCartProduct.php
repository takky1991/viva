<?php

namespace Viva;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GuestCartProduct extends Pivot
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }

    public function guestCart()
    {
        return $this->belongsTo(\Viva\GuestCart::class);
    }
}
