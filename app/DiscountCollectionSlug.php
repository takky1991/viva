<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class DiscountCollectionSlug extends Model
{
    protected $fillable = [
        'slug',
    ];

    public function discountCollection()
    {
        return $this->belongsTo(\Viva\DiscountCollection::class);
    }
}
