<?php

function flash($type, $message, $title = null, $btnText = null, $btnLink = null)
{
    $notifier = app('flash');
    if (! is_null($message) || ! is_null($type)) {
        return $notifier->message($type, $message, $title, $btnText, $btnLink);
    }

    return $notifier;
}

// Generate Guid
function newGuid()
{
    $s = strtoupper(md5(uniqid(rand(), true)));
    $guidText =
        substr($s, 0, 8).'-'.
        substr($s, 8, 4).'-'.
        substr($s, 12, 4).'-'.
        substr($s, 16, 4).'-'.
        substr($s, 20);

    return $guidText;
}

function formatPrice(float $price)
{
    return number_format($price, 2, '.', ',').'KM';
}

function reverseNumber($number)
{

    /* Convert them into an array. */
    $arr = str_split($number);

    /* Reverse the array. */
    $rev_arr = array_reverse($arr);

    /* Implode them. */
    $rev = implode('', $rev_arr);

    return (int) $rev;
}

function randomCode($size)
{
    $alpha_key = '';
    $keys = range('A', 'Z');

    for ($i = 0; $i < 2; $i++) {
        $alpha_key .= $keys[array_rand($keys)];
    }

    $length = $size - 2;

    $key = '';
    $keys = range(0, 9);

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $alpha_key.$key;
}

function calculateDiscout(float $value, int $discoutPercentage)
{
    $percentage = 100 - $discoutPercentage;
    $newPrice = $value * ($percentage / 100);

    return round($newPrice * 2, 1) / 2;
}

function random_token($count = 64)
{
    return bin2hex(openssl_random_pseudo_bytes($count / 2));
}
