<?php

namespace Viva\Library\OpenGraph;

use Exception;

class OpenGraph
{
    /**
     * Supported peroperties
     *
     * @var array
     */
    protected $availableProperties = [
        'og:title',
        'og:description',
        'og:url',
        'og:type',
        'og:site_name',
        'og:locale',
        'og:image',
        'og:image:width',
        'og:image:height',
        'og:image:alt',
    ];

    protected $defaultProperties = [];

    protected $properties = [];

    /**
     * Set up the default properties
     */
    public function __construct()
    {
        $this->defaultProperties = [
            'og:title' => config('app.name'),
            'og:description' => 'Naruči svoje omiljene proizvode uz dostavu od '.config('app.lowest_shipping_cost').'KM u sve gradove BIH.',
            'og:url' => url()->current(),
            'og:type' => 'website',
            'og:site_name' => config('app.name'),
            'og:locale' => 'bs_BA',
            'og:image' => asset('images/viva-opengraph.png'),
            'og:image:width' => 500,
            'og:image:height' => 500,
            'og:image:alt' => config('app.name'),
        ];
    }

    /**
     * Render the opengraph tags html
     *
     *
     * @throws Exception
     */
    public function render(): string
    {
        if (! is_array($this->defaultProperties) || count($this->defaultProperties) < 1) {
            throw new Exception('No opengraph properties set');
        }

        $tags = '';
        foreach ($this->availableProperties as $key) {
            $value = null;

            if (! empty($this->properties[$key])) {
                $value = $this->properties[$key];
            } elseif (! empty($this->defaultProperties[$key])) {
                $value = $this->defaultProperties[$key];
            }

            if (! empty($value)) {
                $tags .= $this->buildMetaTag($key, $value);
            }
        }

        return $tags;
    }

    /**
     * Build the meta tag string from the key and the value
     *
     * @param  string|array  $value
     */
    public function buildMetaTag(string $key, $value): string
    {
        $tags = '';

        switch ($key) {
            case 'og:image':
                $tags .= '<meta property="'.$key.'" content="'.htmlspecialchars_decode($value).'">'."\n";
                break;

            default:
                $tags .= '<meta property="'.$key.'" content="'.e($value).'">'."\n";
                break;
        }

        return $tags;
    }

    /**
     * Set up the values of the different opengraph attributes
     */
    public function set(string $key, string $value): static
    {
        $this->properties[$key] = $value;

        return $this;
    }

    /**
     * Return the properties that have been set by us
     */
    public function getProperties($key = null): array
    {
        if (! empty($key)) {
            if (isset($this->properties[$key])) {
                return $this->properties[$key];
            }

            return null;
        }

        return $this->properties;
    }
}
