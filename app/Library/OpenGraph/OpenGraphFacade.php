<?php

namespace Viva\Library\OpenGraph;

class OpenGraphFacade extends \Illuminate\Support\Facades\Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor(): string
    {
        return \Viva\Library\OpenGraph\OpenGraph::class;
    }
}
