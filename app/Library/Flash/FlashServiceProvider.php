<?php

namespace Viva\Library\Flash;

use Illuminate\Support\ServiceProvider;

class FlashServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(
            \Viva\Library\Flash\SessionStore::class,
            \Viva\Library\Flash\LaravelSessionStore::class
        );
        $this->app->singleton('flash', function () {
            return $this->app->make(\Viva\Library\Flash\FlashNotifier::class);
        });
    }
}
