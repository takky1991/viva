<?php

namespace Viva\Library\Flash;

class FlashNotifier
{
    /**
     * The session store.
     *
     * @var SessionStore
     */
    protected $session;

    /**
     * The messages collection.
     *
     * @var \Illuminate\Support\Collection
     */
    public $messages;

    /**
     * Create a new FlashNotifier instance.
     */
    public function __construct(SessionStore $session)
    {
        $this->session = $session;
        $this->messages = collect();
    }

    /**
     * Flash a general message.
     *
     * @param  string|null  $level
     */
    public function message($type = null, ?string $message = null, $title = null, $btnText = null, $btnLink = null): static
    {
        $message = compact('type', 'message', 'title', 'btnText', 'btnLink');
        $this->messages->push($message);

        return $this->flash();
    }

    /**
     * Clear all registered messages.
     */
    public function clear(): static
    {
        $this->messages = collect();

        return $this;
    }

    /**
     * Flash all messages to the session.
     */
    protected function flash()
    {
        $this->session->flash('flash_notification', $this->messages);

        return $this;
    }
}
