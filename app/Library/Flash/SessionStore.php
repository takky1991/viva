<?php

namespace Viva\Library\Flash;

interface SessionStore
{
    /**
     * Flash a message to the session.
     *
     * @param  string  $name
     * @param  array  $data
     */
    public function flash($name, $data);
}
