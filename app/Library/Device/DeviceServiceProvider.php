<?php

namespace Viva\Library\Device;

use Illuminate\Support\ServiceProvider;

class DeviceServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->singleton('DeviceHandler', function () {
            return $this->app->make(\Viva\Library\Device\DeviceHandler::class);
        });
    }
}
