<?php

namespace Viva\Library\Device;

use Illuminate\Http\Request;

class DeviceHandler
{
    protected $request;

    protected $device;

    public function setDevice(Request $request)
    {
        $this->request = $request;

        if (str_contains($this->request->header('User-Agent'), 'w2n/Android')) {
            $this->device = [
                'source' => 'android',
                'user_agent' => $this->request->header('User-Agent'),
            ];
        } elseif (str_contains($this->request->header('User-Agent'), 'w2n/iOS')) {
            $this->device = [
                'source' => 'ios',
                'user_agent' => $this->request->header('User-Agent'),
            ];
        } else {
            $this->device = [
                'source' => 'web',
                'user_agent' => $this->request->header('User-Agent'),
            ];
        }
    }

    public function getDevice()
    {
        if (empty($this->request)) {
            $this->device = [
                'source' => 'web',
                'user_agent' => null,
            ];
        }

        return $this->device;
    }
}
