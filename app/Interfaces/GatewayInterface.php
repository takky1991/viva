<?php

namespace Viva\Interfaces;

interface GatewayInterface
{
    public function getItems(array $input = []);
}
