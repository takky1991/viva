<?php

namespace Viva\Interfaces;

interface CartInterface
{
    public function products();

    public function totalPrice(): float;
}
