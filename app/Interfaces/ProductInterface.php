<?php

namespace Viva\Interfaces;

interface ProductInterface
{
    public function realPrice(): float;

    public function onSale(): bool;
}
