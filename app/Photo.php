<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Viva\Jobs\ResizePhoto;

class Photo extends Model
{
    protected $fillable = [
        'title',
        'file_name',
        'file_size',
        'mime_type',
        'extension',
        'uri',
        'token',
        'target',
        'target_id',
        'processed',
        'processed_product_show',
        'processed_product_profile',
        'processed_thumb',
        'processed_hero_article',
        'processed_hero_article_mobile',
        'processed_category',
        'processed_article',
        'processed_article_mobile',
        'preferred_style',
    ];

    public static $targets = [
        'product',
        'hero_article',
        'category',
        'article',
    ];

    public static $styles = [
        'product_show' => '500',
        'product_profile' => '250',
        'thumb' => '65x65',
        'hero_article' => '800x500',
        'category' => '600x300',
    ];

    public static $productStyles = [
        'product_show' => '500',
        'product_profile' => '250',
        'thumb' => '65x65',
    ];

    public static $heroArticleStyles = [
        'hero_article' => '1110x800',
        'hero_article_mobile' => '555x400',
        'thumb' => '65x65',
    ];

    public static $categoryStyles = [
        'category' => '600x300',
        'thumb' => '65x65',
    ];

    public static $articleStyles = [
        'article' => '800x500',
        'article_mobile' => '400x250',
        'thumb' => '65x65',
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo(\Viva\User::class);
    }

    public function photoProduct()
    {
        return $this->hasOne(\Viva\PhotoProduct::class);
    }

    public function heroArticle()
    {
        return $this->belongsTo(\Viva\HeroArticle::class);
    }

    public function article()
    {
        return $this->belongsTo(\Viva\Article::class);
    }

    public function category()
    {
        return $this->belongsTo(\Viva\Category::class);
    }

    /**
     * Methods
     */
    public static function getStyles($target = null)
    {
        switch ($target) {
            case 'product':
                return self::$productStyles;
                break;

            case 'hero_article':
                return self::$heroArticleStyles;
                break;

            case 'category':
                return self::$categoryStyles;
                break;

            case 'article':
                return self::$articleStyles;
                break;

            default:
                return self::$styles;
        }
    }

    public function isStyleProcessed($style)
    {
        if ($style == 'original') {
            return $this->processed;
        }

        return $this->{'processed_'.$style};
    }

    public function getPath($style = 'original')
    {
        return str_replace('original', $style, $this->uri);
    }

    public function markStyleAsProcessed($style)
    {
        logger()->info('Processing Photo #'.$this->id.'|'.$this->uri.' style '.$style);

        $this->update([
            'processed_'.$style => true,
        ]);
    }

    public function url($style = 'original')
    {
        if ($this->isStyleProcessed($style)) {
            $url = Storage::disk('s3')->url($this->uri);
        } else {
            $url = Storage::disk('public')->url($this->uri);
        }

        //if (app()->environment() == 'production') {
        //$url = str_replace('viva-dev.s3.eu-central-1.amazonaws.com', 'cdn.apotekaviva24.ba', $url);
        //}
        return str_replace('original', $style, $url);
    }

    public function optimizeAndStore($preferredStyle = null, $processSynchronous = false)
    {
        $uri = $this->uri;

        //Preferred styles can be seperated by a pipe to pass in more preferred styles
        $preferredStyles = explode('|', $preferredStyle);

        //Check if local file exists
        if (Storage::disk('public')->exists($uri)) {
            foreach (self::getStyles($this->target) as $style => $dimensions) {
                if (in_array($style, $preferredStyles)) {
                    ResizePhoto::dispatchSync($this, $style, $processSynchronous);
                } else {
                    ResizePhoto::dispatch($this, $style);
                }
            }
        } else {
            throw new \Exception('File not exists: '.$uri);
        }
    }
}
