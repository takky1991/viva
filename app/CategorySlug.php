<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class CategorySlug extends Model
{
    protected $fillable = [
        'slug',
    ];

    public function category()
    {
        return $this->belongsTo(\Viva\Category::class);
    }
}
