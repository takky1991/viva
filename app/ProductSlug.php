<?php

namespace Viva;

use Illuminate\Database\Eloquent\Model;

class ProductSlug extends Model
{
    protected $fillable = [
        'slug',
    ];

    public function product()
    {
        return $this->belongsTo(\Viva\Product::class);
    }
}
