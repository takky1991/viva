<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement("
           INSERT INTO `countries` (`id`, `name`, `currency`, `iso_3166_2`, `iso_3166_3`, `phone_country_prefix`)
            VALUE
                (1, 'Bosna i Hercegovina', 'KM', 'BA', 'BIH', '+387')
        ");
    }
}
