<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Viva\Category;
use Viva\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Category::chunk(100, function ($categories) {
            foreach ($categories as $category) {
                $products = Product::factory()->count(10)->create();
                $category->products()->attach($products->pluck('id')->toArray());
            }
        });
    }
}
