<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Viva\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $mainCategories = Category::factory()->count(5)->create();

        foreach ($mainCategories as $mainCategory) {
            $categories = Category::factory()->count(5)->create(['parent_id' => $mainCategory->id]);

            foreach ($categories as $category) {
                Category::factory()->count(5)->create(['parent_id' => $category->id]);
            }
        }
    }
}
