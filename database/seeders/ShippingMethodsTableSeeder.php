<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement("INSERT INTO `shipping_methods` (`key`, `name`, `price`, `pickup`, `order_id`)
            VALUE 
                ('viva_centrala', 'Centralna Apoteka VIVA, Mala Lisa, Cazin', '0.00', true, 1),
                ('viva_belladonna', 'Apoteka VIVA, TC Robot, Ćoralići, Cazin', '0.00', true, 2),
                ('viva_josani', 'Apoteka VIVA, Jošani, Cazin', '0.00', true, 3),
                ('viva_uno', 'Apoteka VIVA, Križ, V.Kladuša', '0.00', true, 4),
                ('viva_gata', 'Apoteka VIVA, Gata, Bihać', '0.00', true, 5),
                ('a2b', 'A2B Brza pošta (1-2 dana)', '2.00', false, 6),
                ('x_express', 'X-Express Brza pošta (1-2 dana)', '3.00', false, 7)
        ");
    }
}
