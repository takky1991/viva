<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
        DB::table('users')->truncate();
        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::table('suppliers')->truncate();
        DB::table('payments')->truncate();
        DB::table('categories')->truncate();
        DB::table('category_slugs')->truncate();
        DB::table('category_product')->truncate();
        DB::table('products')->truncate();
        DB::table('product_slugs')->truncate();
        DB::table('countries')->truncate();
        DB::table('shipping_methods')->truncate();
        DB::table('brands')->truncate();
        DB::table('brand_slugs')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints

        $this->call(RolesAndPermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(ShippingMethodsTableSeeder::class);

        if (app()->environment() == 'local') {
            $this->call(SuppliersTableSeeder::class);
            $this->call(PaymentsTableSeeder::class);
            $this->call(CategoriesTableSeeder::class);
            $this->call(BrandsTableSeeder::class);
            $this->call(ProductsTableSeeder::class);
        }
    }
}
