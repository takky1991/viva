<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('buy_count_30_days')->default(0)->after('buy_count');
            $table->boolean('best_seller_in_brand')->default(false)->after('buy_count_30_days');
            $table->boolean('best_seller_in_category')->default(false)->after('best_seller_in_brand');
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->boolean('best_seller_in_category')->default(false)->after('product_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('buy_count_30_days');
            $table->dropColumn('best_seller_in_brand');
            $table->dropColumn('best_seller_in_category');
        });

        Schema::table('category_product', function (Blueprint $table) {
            $table->dropColumn('best_seller_in_category');
        });
    }
};
