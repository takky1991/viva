<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('newsletter_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('token')->unique();
            $table->timestamps();

            $table->index('user_id');
        });

        DB::insert('INSERT INTO newsletter_contacts (email, user_id, token, created_at, updated_at) SELECT email, id, UUID_SHORT(), NOW(), NOW() FROM users');
        DB::insert('INSERT INTO newsletter_contacts (email, first_name, last_name, token, created_at, updated_at) SELECT DISTINCT email, MIN(first_name), MIN(last_name), UUID_SHORT(), NOW(), NOW() FROM orders WHERE email NOT IN (SELECT email FROM users) GROUP BY email');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('newsletter');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('newsletter_contacts');

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('newsletter')->default(true)->after('terms_accepted');
        });
    }
};
