<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('email_log', function ($table) {
            if (! Schema::hasColumn('email_log', 'id')) {
                $table->increments('id')->first();
                $table->string('from')->after('date')->nullable();
                $table->string('cc')->after('to')->nullable();
                $table->text('headers')->after('body')->nullable();
                $table->text('attachments')->after('headers')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('email_log', function ($table) {
            $table->dropColumn(['id', 'from', 'cc', 'headers', 'attachments']);
        });
    }
};
