<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Viva\Order;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('shipped_notification_sent')->default(false)->after('status');
            $table->boolean('pickup_notification_sent')->default(false)->after('shipped_notification_sent');
        });

        Order::where('status', 'fulfilled')
            ->orWhere('status', 'canceled')
            ->chunk(100, function ($orders) {
                foreach ($orders as $order) {
                    if ($order->shipping_pickup) {
                        $order->pickup_notification_sent = true;
                    } else {
                        $order->shipped_notification_sent = true;
                    }
                    $order->save();
                }
            });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipped_notification_sent');
            $table->dropColumn('pickup_notification_sent');
        });
    }
};
