<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number')->unique();
            $table->unsignedInteger('user_id')->nullable();
            $table->uuid('guest_id')->nullable();
            $table->string('email')->nullable();

            $table->unsignedInteger('shipping_method_id');
            $table->string('shipping_key');
            $table->string('shipping_name');
            $table->decimal('shipping_price', 8, 2);
            $table->boolean('shipping_pickup');

            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('street_address')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('status')->default('created');
            $table->timestamps();
            $table->softDeletes();

            $table->index('user_id');
            $table->index('shipping_method_id');
            $table->index('country_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
