<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('supplier_id');
            $table->string('invoice_id');
            $table->date('delivered_at');
            $table->date('pay_until');
            $table->decimal('amount', 8, 2);
            $table->boolean('paid')->default(false);
            $table->boolean('locked')->default(false);
            $table->timestamps();

            $table->index('user_id');
            $table->index('supplier_id');
            $table->unique(['supplier_id', 'invoice_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};
