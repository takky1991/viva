<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cart_product', function (Blueprint $table) {
            $table->string('variants')->nullable()->after('quantity');
        });

        Schema::table('guest_cart_product', function (Blueprint $table) {
            $table->string('variants')->nullable()->after('quantity');
        });

        Schema::table('order_products', function (Blueprint $table) {
            $table->string('variants')->nullable()->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cart_product', function (Blueprint $table) {
            $table->dropColumn('variants');
        });

        Schema::table('guest_cart_product', function (Blueprint $table) {
            $table->dropColumn('variants');
        });

        Schema::table('order_products', function (Blueprint $table) {
            $table->dropColumn('variants');
        });
    }
};
