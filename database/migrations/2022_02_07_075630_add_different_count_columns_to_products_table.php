<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('open_count')->default(0)->after('featured');
            $table->unsignedBigInteger('add_to_cart_count')->default(0)->after('open_count');
            $table->unsignedBigInteger('buy_count')->default(0)->after('add_to_cart_count');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('open_count');
            $table->dropColumn('add_to_cart_count');
            $table->dropColumn('buy_count');
        });
    }
};
