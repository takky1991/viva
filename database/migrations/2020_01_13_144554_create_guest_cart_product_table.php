<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guest_cart_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('guest_cart_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('quantity')->default(1);
            $table->timestamps();

            // Indexes
            $table->unique(['guest_cart_id', 'product_id']);
            $table->foreign('guest_cart_id')->references('id')->on('guest_carts')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guest_cart_product');
    }
};
