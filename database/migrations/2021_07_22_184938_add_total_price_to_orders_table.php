<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Viva\Order;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('total_price', 6, 2)->after('shipping_pickup');
            $table->decimal('total_price_with_shipping', 6, 2)->after('total_price');
        });

        Order::chunk(100, function ($orders) {
            foreach ($orders as $order) {
                $order->total_price = $order->totalPrice();
                $order->total_price_with_shipping = $order->totalPriceWithShipping();
                $order->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total_price');
            $table->dropColumn('total_price_with_shipping');
        });
    }
};
