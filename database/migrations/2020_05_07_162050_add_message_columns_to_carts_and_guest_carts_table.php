<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->boolean('product_removed')->default(false)->after('shipping_method_id');
            $table->boolean('product_price_changed')->default(false)->after('product_removed');
            $table->boolean('free_shipping')->default(false)->after('product_price_changed');
        });

        Schema::table('guest_carts', function (Blueprint $table) {
            $table->boolean('product_removed')->default(false)->after('shipping_method_id');
            $table->boolean('product_price_changed')->default(false)->after('product_removed');
            $table->boolean('free_shipping')->default(false)->after('product_price_changed');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('product_removed');
            $table->dropColumn('product_price_changed');
            $table->dropColumn('free_shipping');
        });

        Schema::table('guest_carts', function (Blueprint $table) {
            $table->dropColumn('product_removed');
            $table->dropColumn('product_price_changed');
            $table->dropColumn('free_shipping');
        });
    }
};
