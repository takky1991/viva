<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('title')->nullable();
            $table->string('file_name')->nullable();
            $table->unsignedInteger('file_size')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('extension')->nullable();
            $table->string('uri')->nullable();
            $table->string('token')->nullable();
            $table->boolean('processed')->default(false);
            $table->boolean('processed_large')->default(false);
            $table->boolean('processed_medium')->default(false);
            $table->boolean('processed_small')->default(false);
            $table->boolean('processed_profile')->default(false);
            $table->boolean('processed_thumb')->default(false);
            $table->string('preferred_style')->nullable();

            $table->timestamps();

            // Indexes
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('photos');
    }
};
