<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->string('target')->after('token');
            $table->unsignedInteger('target_id')->nullable()->after('target');
            $table->boolean('processed_product_show')->nullable()->after('processed');
            $table->boolean('processed_product_profile')->nullable()->after('processed_product_show');
            $table->dropColumn('processed_large');
            $table->dropColumn('processed_medium');
            $table->dropColumn('processed_small');
            $table->dropColumn('processed_profile');
            $table->boolean('processed_thumb')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->dropColumn('target');
            $table->dropColumn('target_id');
            $table->dropColumn('processed_product_show');
            $table->dropColumn('processed_product_profile');
            $table->boolean('processed_large')->default(false);
            $table->boolean('processed_medium')->default(false);
            $table->boolean('processed_small')->default(false);
            $table->boolean('processed_profile')->default(false);
        });
    }
};
