<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('compare_price');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price', 8, 2)->after('description');
            $table->decimal('discount_price', 8, 2)->nullable()->after('price');
            $table->boolean('on_sale')->default(false)->after('discount_price');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('discount_price');
            $table->dropColumn('on_sale');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('price')->after('description');
            $table->string('compare_price')->nullable()->after('price');
        });
    }
};
