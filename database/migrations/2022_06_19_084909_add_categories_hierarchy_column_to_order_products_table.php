<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_products', function (Blueprint $table) {
            $table->string('categories_hierarchy')->nullable()->after('description');
        });

        DB::table('order_products')
        ->update([
            'categories_hierarchy' => DB::raw('(select categories_hierarchy from products where products.id = order_products.product_id)'),
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_products', function (Blueprint $table) {
            $table->dropColumn('categories_hierarchy');
        });
    }
};
