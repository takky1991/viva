<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('collection_slugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('collection_id');
            $table->string('slug')->unique();
            $table->timestamps();

            $table->index('collection_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('collection_slugs');
    }
};
