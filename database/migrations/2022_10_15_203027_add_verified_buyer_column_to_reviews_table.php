<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->nullable()->after('user_id');
            $table->boolean('verified_buyer')->default(false)->after('rating');

            $table->index('order_id');
        });

        DB::table('reviews')->update(['verified_buyer' => true]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->dropColumn('verified_buyer');
            $table->dropColumn('order_id');
        });
    }
};
