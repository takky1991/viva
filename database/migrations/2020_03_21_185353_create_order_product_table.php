<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');

            $table->unsignedInteger('brand_id')->nullable();
            $table->string('brand_name')->nullable();
            $table->text('brand_description')->nullable();
            $table->string('title');
            $table->string('package_description')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 8, 2);
            $table->decimal('discount_price', 8, 2)->nullable();
            $table->boolean('on_sale');
            $table->boolean('published');
            $table->boolean('in_stock');
            $table->boolean('free_shipping');
            $table->unsignedInteger('quantity')->default(1);

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['order_id', 'product_id']);
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_products');
    }
};
