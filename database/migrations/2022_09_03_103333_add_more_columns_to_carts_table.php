<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->string('first_name')->nullable()->after('free_shipping');
            $table->string('last_name')->nullable()->after('first_name');
            $table->string('phone')->nullable()->after('last_name');
            $table->string('street_address')->nullable()->after('phone');
            $table->string('postalcode')->nullable()->after('street_address');
            $table->string('city')->nullable()->after('postalcode');
            $table->integer('country_id')->nullable()->after('city');
            $table->string('additional_info')->nullable()->after('country_id');

            $table->index('country_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('phone');
            $table->dropColumn('street_address');
            $table->dropColumn('postalcode');
            $table->dropColumn('city');
            $table->dropColumn('country_id');
            $table->dropColumn('additional_info');
        });
    }
};
