<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address_first_name');
            $table->dropColumn('address_last_name');
            $table->dropColumn('address_company');
            $table->dropColumn('address_phone');
            $table->dropColumn('address_line');
            $table->dropColumn('address_city');
            $table->dropColumn('address_postal_code');
            $table->dropColumn('address_country_id');
            $table->dropColumn('address_region');

            $table->smallInteger('gender')->default(0)->after('email_verified_at');
            $table->date('birth_date')->nullable()->after('gender');
            $table->string('street_address')->nullable()->after('birth_date');
            $table->string('postalcode')->nullable()->after('street_address');
            $table->string('city')->nullable()->after('postalcode');
            $table->unsignedInteger('country_id')->nullable()->after('city');

            $table->index('country_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('birth_date');
            $table->dropColumn('street_address');
            $table->dropColumn('postalcode');
            $table->dropColumn('city');
            $table->dropColumn('country_id');

            $table->string('address_first_name')->nullable();
            $table->string('address_last_name')->nullable();
            $table->string('address_company')->nullable();
            $table->string('address_phone')->nullable();
            $table->string('address_line')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_postal_code')->nullable();
            $table->string('address_country_id')->nullable();
            $table->string('address_region')->nullable();
        });
    }
};
