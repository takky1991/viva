<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('hero_articles', function (Blueprint $table) {
            $table->string('background_color')->nullable()->after('button_url');
            $table->string('text_color')->nullable()->after('background_color');
            $table->dateTime('published_at')->nullable()->after('published');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('hero_articles', function (Blueprint $table) {
            $table->dropColumn('background_color');
            $table->dropColumn('text_color');
            $table->dropColumn('published_at');
        });
    }
};
