<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('collections');
        Schema::dropIfExists('collection_slugs');

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('children_ids')->nullable();
            $table->timestamps();

            $table->index('parent_id');
        });

        Schema::create('category_slugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->string('slug')->unique();
            $table->timestamps();

            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('category_slugs');

        Schema::create('collections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->text('children_ids')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->unsignedInteger('photo_id')->nullable();
            $table->timestamps();

            $table->index('parent_id');
            $table->index('photo_id');
        });

        Schema::create('collection_slugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('collection_id');
            $table->string('slug')->unique();
            $table->timestamps();

            $table->index('collection_id');
        });
    }
};
