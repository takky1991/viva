<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('source')->default('web')->after('review_cta_sent');
        });

        Schema::table('guest_carts', function (Blueprint $table) {
            $table->string('source')->default('web')->after('additional_info');
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->string('source')->default('web')->after('additional_info');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('source');
        });

        Schema::table('guest_carts', function (Blueprint $table) {
            $table->dropColumn('source');
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('source');
        });
    }
};
