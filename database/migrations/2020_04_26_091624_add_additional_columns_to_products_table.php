<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('brand_id')->nullable()->after('collection_id');
            $table->boolean('published')->default(false)->after('on_sale');
            $table->boolean('in_stock')->default(false)->after('published');
            $table->boolean('free_shipping')->default(false)->after('in_stock');
            $table->string('package_description')->nullable()->after('title');

            $table->dropColumn('collection_id');

            $table->index('brand_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('collection_id')->after('id');
            $table->dropColumn('published');
            $table->dropColumn('in_stock');
            $table->dropColumn('brand_id');
            $table->dropColumn('package_description');
            $table->dropColumn('free_shipping');
        });
    }
};
