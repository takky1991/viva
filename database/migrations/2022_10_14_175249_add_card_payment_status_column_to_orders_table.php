<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('card_payment_status')->nullable()->after('card_type');
        });

        DB::table('orders')
            ->where('payment_method', 'online_payment')
            ->where('card_payment_processed', false)
            ->update(['card_payment_status' => 'authorized']);

        DB::table('orders')
            ->where('payment_method', 'online_payment')
            ->where('card_payment_processed', true)
            ->update(['card_payment_status' => 'captured']);

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('card_payment_processed');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('card_payment_status');
            $table->boolean('card_payment_processed')->default(false)->after('card_type');
        });
    }
};
