<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_products', function (Blueprint $table) {
            $table->decimal('real_price', 8, 2)->after('on_sale');
        });

        DB::table('order_products')->where('on_sale', false)->update(['real_price' => DB::raw('`price`')]);
        DB::table('order_products')->where('on_sale', true)->update(['real_price' => DB::raw('`discount_price`')]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_products', function (Blueprint $table) {
            $table->dropColumn('real_price');
        });
    }
};
