<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Brand;

class HeroArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->words($nb = 5, $asText = true),
            'subtitle' => $this->faker->words($nb = 10, $asText = true),
            'brand_id' => Brand::factory()->create(),
            'button_text' => 'Pogledaj proizvode',
            'button_url' => 'apotekaviva24.ba/brendovi/avene',
            'published' => true,
        ];
    }
}
