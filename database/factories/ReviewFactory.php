<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Product;

class ReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $product = Product::factory()->create();

        return [
            'product_id' => $product->id,
            'name' => $this->faker->firstName(),
            'content' => $this->faker->realText($maxNbChars = 200, $indexSize = 2),
            'rating' => $this->faker->numberBetween($min = 1, $max = 5),
            'published' => true,
        ];
    }
}
