<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\ShippingMethod;
use Viva\User;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $user = User::factory()->create();
        $shippingMethod = ShippingMethod::factory()->create();

        return [
            'user_id' => $user->id,
            'shipping_method_id' => $shippingMethod->id,
            'shipping_key' => $shippingMethod->key,
            'shipping_name' => $shippingMethod->name,
            'shipping_price' => $shippingMethod->price,
            'original_shipping_price' => $shippingMethod->price,
            'shipping_pickup' => $shippingMethod->pickup,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'phone' => $user->phone,
            'street_address' => $user->street_address,
            'postalcode' => $user->postalcode,
            'city' => $user->city,
            'country_id' => 1, //for bosnia
            'payment_method' => 'cash_on_delivery',
            'status' => 'created',
            'total_price' => $this->faker->randomFloat($nbMaxDecimals = null, $min = 1, $max = 100),
            'total_price_with_shipping' => $this->faker->randomFloat($nbMaxDecimals = null, $min = 1, $max = 100),
        ];
    }
}
