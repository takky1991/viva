<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Brand;
use Viva\Product;

class OrderProductFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $brand = Brand::factory()->create();
        $product = Product::factory()->create(['brand_id' => $brand->id]);

        return [
            'product_id' => $product->id,
            'brand_id' => $product->brand_id,
            'brand_name' => $product->brand?->name,
            'brand_description' => $product->brand?->description,
            'title' => $product->title,
            'package_description' => $product->package_description,
            'description' => $product->description,
            'price' => $product->price,
            'discount_price' => $product->discount_price,
            'on_sale' => $product->on_sale,
            'published' => $product->published,
            'in_stock' => $product->in_stock,
            'free_shipping' => $product->free_shipping,
            'quantity' => 3,
        ];
    }
}
