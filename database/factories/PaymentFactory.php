<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Supplier;
use Viva\User;

class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $users = User::all();
        $suppliers = Supplier::all();

        return [
            'user_id' => $users->random()->id,
            'supplier_id' => $suppliers->random()->id,
            'invoice_id' => uniqid(),
            'delivered_at' => $this->faker->date($format = 'Y-m-d'),
            'pay_until' => $this->faker->date($format = 'Y-m-d'),
            'amount' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100000),
            'paid' => $this->faker->boolean(),
        ];
    }
}
