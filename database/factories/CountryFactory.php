<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CountryFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => 'Bosna i Hercegovina',
            'currency' => 'KM',
            'iso_3166_2' => 'BA',
            'iso_3166_3' => 'BIH',
            'phone_country_prefix' => '+387',
        ];
    }
}
