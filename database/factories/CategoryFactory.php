<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'title' => ucfirst($this->faker->words($nb = 2, $asText = true)),
            'description' => $this->faker->paragraph(),
            'parent_id' => null,
            'menu' => true,
        ];
    }
}
