<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Product;

class ProductVariantFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $product = Product::factory()->create();

        return [
            'product_id' => $product->id,
            'name' => $this->faker->numberBetween($min = 24, $max = 34),
            'in_stock' => true,
        ];
    }
}
