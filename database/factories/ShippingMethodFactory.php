<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ShippingMethodFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'key' => $this->faker->word(),
            'name' => 'VIVA Centralna apoteka, Mala Lisa, Cazin',
            'price' => '5.00',
            'pickup' => true,
            'order_id' => 1,
        ];
    }
}
