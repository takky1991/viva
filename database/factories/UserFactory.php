<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'remember_token' => str_random(10),
            'phone' => $this->faker->phoneNumber(),
            'street_address' => $this->faker->streetAddress(),
            'postalcode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
        ];
    }

    public function admin()
    {
        return $this->state(function () {
            return [
                'first_name' => 'Azra',
                'last_name' => 'Uljevic',
                'email' => 'apotekaviva.cazin@bih.net.ba',
                'password' => bcrypt('031088'),
                'phone' => null,
                'street_address' => null,
                'postalcode' => null,
                'city' => null,
            ];
        });
    }
}
