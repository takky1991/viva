<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Viva\Brand;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $brand = Brand::factory()->create();
        $price = $this->faker->randomFloat($nbMaxDecimals = null, $min = 1, $max = 100);

        return [
            'title' => ucfirst($this->faker->words($nb = 2, $asText = true)),
            'brand_id' => $brand->id,
            'package_description' => $this->faker->words($nb = 2, $asText = true),
            'description' => $this->faker->paragraphs($nb = 2, $asText = true),
            'price' => $price,
            'discount_price' => $this->faker->randomFloat($nbMaxDecimals = null, $min = 1, $max = $price - 1),
            'on_sale' => $this->faker->numberBetween($min = 0, $max = 1),
            'in_stock' => true,
            'published' => true,
            'free_shipping' => false,
        ];
    }

    public function onSale()
    {
        return $this->state(['on_sale' => true]);
    }

    public function notOnSale()
    {
        return $this->state(['on_sale' => false]);
    }

    public function unpublished()
    {
        return $this->state(['published' => false]);
    }
}
