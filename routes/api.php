<?php

use Illuminate\Support\Facades\Route;
use Viva\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('guest-cart')->group(function () {
    Route::get('/', [Api\GuestCartsController::class, 'get']);
    Route::post('item', [Api\GuestCartsController::class, 'addItem']);
    Route::delete('item/{product}', [Api\GuestCartsController::class, 'removeItem']);
});

Route::prefix('products')->group(function () {
    Route::get('{product}/reviews', [Api\ProductsController::class, 'getReviews']);
    Route::post('{product}/reviews', [Api\ProductsController::class, 'createReview']);
    Route::post('{product}/alert', [Api\ProductsController::class, 'createBackToStockAlert']);
});

Route::prefix('orders')->group(function () {
    Route::post('/online-payment', [Api\OrdersController::class, 'createOnlinePaymentOrder']);
    Route::get('{order_number}/seen', [Api\OrdersController::class, 'seen']);
});

Route::prefix('webhooks')->group(function () {
    Route::post('mailgun/permanent-fail', [Api\WebhooksController::class, 'mailgunPermanentFail']);
});

Route::middleware('auth:api')->group(function () {
    Route::prefix('cart')->group(function () {
        Route::get('/', [Api\CartsController::class, 'get']);
        Route::post('item', [Api\CartsController::class, 'addItem']);
        Route::delete('item/{product}', [Api\CartsController::class, 'removeItem']);
    });

    Route::prefix('photos')->group(function () {
        Route::post('/', [Api\PhotosController::class, 'store']);
        Route::delete('{photo}', [Api\PhotosController::class, 'destroy']);
    });
});
