<?php

use Illuminate\Support\Facades\Route;
use Viva\Http\Controllers\ArticlesController;
use Viva\Http\Controllers\Auth;
use Viva\Http\Controllers\BrandsController;
use Viva\Http\Controllers\CartController;
use Viva\Http\Controllers\CategoriesController;
use Viva\Http\Controllers\CheckoutController;
use Viva\Http\Controllers\DiscountCollectionsController;
use Viva\Http\Controllers\HomeController;
use Viva\Http\Controllers\PagesController;
use Viva\Http\Controllers\NoveliusController;
use Viva\Http\Controllers\ProductsController;
use Viva\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function () {
    // Authentication Routes...
    Route::get('prijava', [Auth\LoginController::class, 'showLoginForm'])->name('login');
    Route::post('prijava', [Auth\LoginController::class, 'login']);

    Route::get('registracija', [Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('registracija', [Auth\RegisterController::class, 'register']);

    // Password Reset Routes...
    Route::get('sifra/promjena', [Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('sifra/email', [Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('sifra/promjena/{token}', [Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('sifra/promjena', [Auth\ResetPasswordController::class, 'reset']);
});

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('lokacije', [PagesController::class, 'getLocations'])->name('locations');
Route::get('nacini-placanja', [PagesController::class, 'getPaying'])->name('paying');
Route::get('dostava', [PagesController::class, 'getDelivery'])->name('delivery');
Route::get('povrat-robe', [PagesController::class, 'getReturns'])->name('returns');
Route::get('politika-kolacica', [PagesController::class, 'getCookiePolicy'])->name('cookie_policy');
Route::get('pravila-zastite-privatnosti', [PagesController::class, 'getPrivacyPolicy'])->name('privacy_policy');
Route::get('uvjeti-kupovine', [PagesController::class, 'getTermsOfPurchase'])->name('terms_of_purchase');
Route::get('pravne-napomene', [PagesController::class, 'getLegalNotice'])->name('legal_notice');

// Black friday
Route::get('novelius-signup', [NoveliusController::class, 'getNoveliusBlackFriday'])->name('novelius_black_friday');
Route::post('novelius-signup', [NoveliusController::class, 'postNoveliusBlackFriday'])->name('novelius_black_friday_post');
Route::get('novelius-signup-success', [NoveliusController::class, 'successNoveliusBlackFriday'])->name('novelius_black_friday_success');

Route::prefix('kupovina')->group(function () {
    Route::middleware('cart.products')->group(function () {
        Route::get('/', [CheckoutController::class, 'getCheckout'])->name('checkout');
        Route::get('dostava', [CheckoutController::class, 'getStepOne'])->name('checkout.one');
        Route::post('dostava', [CheckoutController::class, 'postStepOne']);
        Route::get('informacije', [CheckoutController::class, 'getStepTwo'])->middleware('cart.shipping_method')->name('checkout.two');
        Route::post('informacije', [CheckoutController::class, 'postStepTwo'])->middleware('cart.shipping_method');
        Route::get('placanje', [CheckoutController::class, 'getStepThree'])->middleware(['cart.shipping_method', 'cart.order_details'])->name('checkout.three');
        Route::post('placanje', [CheckoutController::class, 'postStepThree'])->middleware(['cart.shipping_method', 'cart.order_details']);
    });

    Route::get('uspjesno/{order_number}', [CheckoutController::class, 'getSuccess'])->name('checkout.success');
});

Route::prefix('korpa')->group(function () {
    Route::get('/', [CartController::class, 'index'])->name('cart.show');
    Route::post('/', [CartController::class, 'update'])->name('cart.update');
    Route::delete('proizvodi/{product}', [CartController::class, 'deleteProduct'])->name('cart.deleteProduct');
});

Route::prefix('kategorije')->group(function () {
    Route::get('{category}', [CategoriesController::class, 'show'])->middleware('categories.slug')->name('categories.show');
});

Route::prefix('brendovi')->group(function () {
    Route::get('/', [BrandsController::class, 'index'])->name('brands.index');
    Route::get('{brand}/{category?}', [BrandsController::class, 'show'])->middleware(['brands.slug', 'categories.slug'])->name('brands.show');
});

Route::prefix('proizvodi')->group(function () {
    Route::get('/', [ProductsController::class, 'index'])->name('products.index');
    Route::get('export', [ProductsController::class, 'exportFacebookFeed'])->name('products.export');
    Route::get('export-google', [ProductsController::class, 'exportGoogleFeed'])->name('products.export-google');
    Route::get('{product}', [ProductsController::class, 'show'])->middleware('product.slug')->name('products.show');
    Route::get('{product}/recenzija', [ProductsController::class, 'createReview'])->middleware('product.slug')->name('products.review');
    Route::post('{product}/recenzija', [ProductsController::class, 'saveReview'])->middleware('product.slug')->name('products.review.save');
});

Route::prefix('popusti')->group(function () {
    Route::get('/', [DiscountCollectionsController::class, 'index'])->name('discount_collections.index');
    Route::get('{discountCollection}', [DiscountCollectionsController::class, 'show'])->middleware('discount_collection.slug')->name('discount_collections.show');
});

Route::prefix('savjeti-novosti')->group(function () {
    Route::get('/', [ArticlesController::class, 'index'])->name('articles.index');
    Route::get('{article}', [ArticlesController::class, 'show'])->middleware('articles.slug')->name('articles.show');
});

Route::get('/newsletter/odjava/{token}', [UserController::class, 'newsletterUnsubscribe'])->name('newsletter_unsubscribe');

Route::prefix('checkout')->group(function () {
    Route::get('/online_payment/cancel', [CheckoutController::class, 'onlinePaymentCancel'])->name('checkout.cancel_online_payment');
    Route::get('/online_payment/success', [CheckoutController::class, 'onlinePaymentSuccess'])->name('checkout.online_payment_success');
});

Route::middleware('auth')->group(function () {
    Route::post('odjava', [Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('email/potvrda', [Auth\VerificationController::class, 'show'])->name('verification.notice');
    Route::get('email/potvrda/{id}/{hash}', [Auth\VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/slanje', [Auth\VerificationController::class, 'resend'])->name('verification.resend');

    Route::prefix('korisnik')->group(function () {
        Route::get('postavke', [UserController::class, 'getSettings'])->name('user.settings');
        Route::post('postavke', [UserController::class, 'postSettings']);
        Route::get('sifra', [UserController::class, 'getPassword'])->name('user.password');
        Route::post('sifra', [UserController::class, 'postPassword']);
        Route::get('narudzbe', [UserController::class, 'getOrders'])->name('user.orders');
    });
});
