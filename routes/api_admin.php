<?php

use Illuminate\Support\Facades\Route;
use Viva\Http\Controllers\Backend\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::prefix('payments')->group(function () {
        Route::post('{payment}/pay', [Api\PaymentsController::class, 'pay']);
    });

    Route::prefix('brands')->group(function () {
        Route::get('/', [Api\BrandsController::class, 'get']);
        Route::get('{brand}/products', [Api\BrandsController::class, 'getProducts']);
    });

    Route::prefix('products')->group(function () {
        Route::get('/', [Api\ProductsController::class, 'get']);

        Route::prefix('{product}/variants')->group(function () {
            Route::get('/', [Api\ProductsController::class, 'getVariants']);
            Route::post('/', [Api\ProductsController::class, 'createVariant']);
            Route::post('{productVariant}', [Api\ProductsController::class, 'updateVariant']);
            Route::delete('{productVariant}', [Api\ProductsController::class, 'deleteVariant']);
        });
    });

    Route::prefix('categories')->group(function () {
        Route::get('/', [Api\CategoriesController::class, 'get']);
    });

    Route::prefix('discount-collections')->group(function () {
        Route::post('{discountCollection}/products/update', [Api\DiscountCollectionsController::class, 'updateProducts']);
        Route::get('{discountCollection}/enable', [Api\DiscountCollectionsController::class, 'enableDiscount']);
        Route::get('{discountCollection}/disable', [Api\DiscountCollectionsController::class, 'disableDiscount']);
    });
});
