<?php

use Illuminate\Support\Facades\Route;
use Viva\Http\Controllers\Backend;

Route::get('/', [Backend\PagesController::class, 'home'])->name('backend.home');

Route::prefix('users')->group(function () {
    Route::get('/', [Backend\UsersController::class, 'index'])->name('backend.users.index');
    Route::get('exportRemarketingEmailList', [Backend\UsersController::class, 'exportRemarketingEmailList']);
    Route::get('create', [Backend\UsersController::class, 'create'])->name('backend.users.create');
    Route::post('store', [Backend\UsersController::class, 'store'])->name('backend.users.store');
    Route::get('{user}/edit', [Backend\UsersController::class, 'edit'])->name('backend.users.edit');
    Route::put('{user}', [Backend\UsersController::class, 'update'])->name('backend.users.update');
    Route::delete('{user}', [Backend\UsersController::class, 'destroy'])->name('backend.users.destroy');
});

Route::prefix('suppliers')->group(function () {
    Route::get('/', [Backend\SuppliersController::class, 'index'])->name('backend.suppliers.index');
    Route::get('create', [Backend\SuppliersController::class, 'create'])->name('backend.suppliers.create');
    Route::post('store', [Backend\SuppliersController::class, 'store'])->name('backend.suppliers.store');
    Route::get('{supplier}/edit', [Backend\SuppliersController::class, 'edit'])->name('backend.suppliers.edit');
    Route::put('{supplier}', [Backend\SuppliersController::class, 'update'])->name('backend.suppliers.update');
    Route::delete('{supplier}', [Backend\SuppliersController::class, 'destroy'])->name('backend.suppliers.destroy');
});

Route::prefix('payments')->group(function () {
    Route::get('/', [Backend\PaymentsController::class, 'index'])->name('backend.payments.index');
    Route::get('pdf', [Backend\PaymentsController::class, 'getPdf'])->name('backend.payments.pdf');
    Route::get('suppliers', [Backend\PaymentsController::class, 'suppliers'])->name('backend.payments.suppliers');
    Route::get('create', [Backend\PaymentsController::class, 'create'])->name('backend.payments.create');
    Route::post('pay', [Backend\PaymentsController::class, 'pay'])->name('backend.payments.pay');
    Route::post('store', [Backend\PaymentsController::class, 'store'])->name('backend.payments.store');
    Route::get('{payment}/edit', [Backend\PaymentsController::class, 'edit'])->name('backend.payments.edit');
    Route::put('{payment}', [Backend\PaymentsController::class, 'update'])->name('backend.payments.update');
    Route::delete('{payment}', [Backend\PaymentsController::class, 'destroy'])->name('backend.payments.destroy');

    Route::post('/', [Backend\PaymentsController::class, 'search'])->name('backend.payments.search');
});

Route::prefix('sms-number')->group(function () {
    Route::get('/', [Backend\SmsNumberController::class, 'index'])->name('backend.sms_numbers.index');
    Route::get('create', [Backend\SmsNumberController::class, 'create'])->name('backend.sms_numbers.create');
    Route::post('store', [Backend\SmsNumberController::class, 'store'])->name('backend.sms_numbers.store');
    Route::delete('{smsNumber}', [Backend\SmsNumberController::class, 'destroy'])->name('backend.sms_numbers.destroy');
});

Route::prefix('shop')->group(function () {
    Route::get('/', [Backend\PagesController::class, 'shopHome'])->name('backend.shop.home');

    Route::prefix('hero-articles')->group(function () {
        Route::get('/', [Backend\Shop\HeroArticlesController::class, 'index'])->name('backend.shop.hero_articles.index');
        Route::get('create', [Backend\Shop\HeroArticlesController::class, 'create'])->name('backend.shop.hero_articles.create');
        Route::get('{heroArticle}/edit', [Backend\Shop\HeroArticlesController::class, 'edit'])->name('backend.shop.hero_articles.edit');
        Route::post('store', [Backend\Shop\HeroArticlesController::class, 'store'])->name('backend.shop.hero_articles.store');
        Route::put('{heroArticle}', [Backend\Shop\HeroArticlesController::class, 'update'])->name('backend.shop.hero_articles.update');
        Route::post('{heroArticle}/togglePublish', [Backend\Shop\HeroArticlesController::class, 'togglePublish'])->name('backend.shop.hero_articles.togglePublish');
        Route::delete('{heroArticle}', [Backend\Shop\HeroArticlesController::class, 'destroy'])->name('backend.shop.hero_articles.destroy');
    });

    Route::prefix('products')->group(function () {
        Route::get('/', [Backend\Shop\ProductsController::class, 'index'])->name('backend.shop.products.index');
        Route::get('create', [Backend\Shop\ProductsController::class, 'create'])->name('backend.shop.products.create');
        Route::get('{product}/show', [Backend\Shop\ProductsController::class, 'show'])->name('backend.shop.products.show');
        Route::get('{product}/preview', [Backend\Shop\ProductsController::class, 'preview'])->name('backend.shop.products.preview');
        Route::get('{product}/edit', [Backend\Shop\ProductsController::class, 'edit'])->name('backend.shop.products.edit');
        Route::post('/', [Backend\Shop\ProductsController::class, 'search'])->name('backend.shop.products.search');
        Route::post('store', [Backend\Shop\ProductsController::class, 'store'])->name('backend.shop.products.store');
        Route::post('{product}/restore', [Backend\Shop\ProductsController::class, 'restore'])->name('backend.shop.products.restore');
        Route::put('{product}', [Backend\Shop\ProductsController::class, 'update'])->name('backend.shop.products.update');
        Route::delete('{product}', [Backend\Shop\ProductsController::class, 'destroy'])->name('backend.shop.products.destroy');
        Route::get('/pdf', [Backend\Shop\ProductsController::class, 'getPdf'])->name('backend.shop.products.pdf');
        Route::get('/csv', [Backend\Shop\ProductsController::class, 'getCsv'])->name('backend.shop.products.csv');
    });

    Route::prefix('reviews')->group(function () {
        Route::get('/', [Backend\Shop\ReviewsController::class, 'index'])->name('backend.shop.reviews.index');
        Route::post('/', [Backend\Shop\ReviewsController::class, 'search'])->name('backend.shop.reviews.search');
        Route::get('{review}/show', [Backend\Shop\ReviewsController::class, 'show'])->name('backend.shop.reviews.show');
        Route::post('{review}/publish', [Backend\Shop\ReviewsController::class, 'togglePublished'])->name('backend.shop.reviews.publish');
        Route::delete('{review}', [Backend\Shop\ReviewsController::class, 'destroy'])->name('backend.shop.reviews.destroy');
    });

    Route::prefix('categories')->group(function () {
        Route::get('/', [Backend\Shop\CategoriesController::class, 'index'])->name('backend.shop.categories.index');
        Route::get('/other', [Backend\Shop\CategoriesController::class, 'indexOther'])->name('backend.shop.categories.index.other');
        Route::get('create', [Backend\Shop\CategoriesController::class, 'create'])->name('backend.shop.categories.create');
        Route::post('store', [Backend\Shop\CategoriesController::class, 'store'])->name('backend.shop.categories.store');
        Route::get('{category}/edit', [Backend\Shop\CategoriesController::class, 'edit'])->name('backend.shop.categories.edit');
        Route::put('{category}', [Backend\Shop\CategoriesController::class, 'update'])->name('backend.shop.categories.update');
        Route::delete('{category}', [Backend\Shop\CategoriesController::class, 'destroy'])->name('backend.shop.categories.destroy');
    });

    Route::prefix('discount-collections')->group(function () {
        Route::get('/', [Backend\Shop\DiscountCollectionsController::class, 'index'])->name('backend.shop.discount_collections.index');
        Route::get('create', [Backend\Shop\DiscountCollectionsController::class, 'create'])->name('backend.shop.discount_collections.create');
        Route::post('store', [Backend\Shop\DiscountCollectionsController::class, 'store'])->name('backend.shop.discount_collections.store');
        Route::get('{discountCollection}/edit', [Backend\Shop\DiscountCollectionsController::class, 'edit'])->name('backend.shop.discount_collections.edit');
        Route::put('{discountCollection}', [Backend\Shop\DiscountCollectionsController::class, 'update'])->name('backend.shop.discount_collections.update');
        Route::delete('{discountCollection}', [Backend\Shop\DiscountCollectionsController::class, 'destroy'])->name('backend.shop.discount_collections.destroy');
    });

    Route::prefix('articles')->group(function () {
        Route::get('/', [Backend\Shop\ArticlesController::class, 'index'])->name('backend.shop.articles.index');
        Route::get('create', [Backend\Shop\ArticlesController::class, 'create'])->name('backend.shop.articles.create');
        Route::post('store', [Backend\Shop\ArticlesController::class, 'store'])->name('backend.shop.articles.store');
        Route::get('{article}/preview', [Backend\Shop\ArticlesController::class, 'preview'])->name('backend.shop.articles.preview');
        Route::get('{article}/edit', [Backend\Shop\ArticlesController::class, 'edit'])->name('backend.shop.articles.edit');
        Route::put('{article}', [Backend\Shop\ArticlesController::class, 'update'])->name('backend.shop.articles.update');
        Route::delete('{article}', [Backend\Shop\ArticlesController::class, 'destroy'])->name('backend.shop.articles.destroy');
    });

    Route::prefix('orders')->group(function () {
        Route::get('/', [Backend\Shop\OrdersController::class, 'index'])->name('backend.shop.orders.index');
        Route::get('{order}/edit', [Backend\Shop\OrdersController::class, 'edit'])->name('backend.shop.orders.edit');
        Route::get('{order}/invoice', [Backend\Shop\OrdersController::class, 'invoice'])->name('backend.shop.orders.invoice');
        Route::put('{order}', [Backend\Shop\OrdersController::class, 'update'])->name('backend.shop.orders.update');
        Route::post('/', [Backend\Shop\OrdersController::class, 'search'])->name('backend.shop.orders.search');
    });

    Route::prefix('brands')->group(function () {
        Route::get('/', [Backend\Shop\BrandsController::class, 'index'])->name('backend.shop.brands.index');
        Route::get('create', [Backend\Shop\BrandsController::class, 'create'])->name('backend.shop.brands.create');
        Route::post('store', [Backend\Shop\BrandsController::class, 'store'])->name('backend.shop.brands.store');
        Route::get('{brand}/edit', [Backend\Shop\BrandsController::class, 'edit'])->name('backend.shop.brands.edit');
        Route::put('{brand}', [Backend\Shop\BrandsController::class, 'update'])->name('backend.shop.brands.update');
    });
});
