import Vue from 'vue';
export const EventBus = new Vue();

Object.defineProperties(Vue.prototype, {
	$eventBus: {
	    get: function () {
	      return EventBus
	    }
	}
})