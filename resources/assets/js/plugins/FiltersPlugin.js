const FiltersPlugin = {
	install(Vue, options) {
		Vue.filter('formatPrice', function (value) {
			return (value).toFixed(2) + 'KM';
		});
	}
}

export default FiltersPlugin;