import Vue from 'vue';

// This utility makes the width and height reactive as well as provides useful breakpoints aligned with tailwind's
// breakpoints. You can use breakpoints.w, breakpoints.h and breakpoints.is anywhere and it will be updated when the
// window resizes.

const screens = {
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200
}

const getBreakpoint = width => {
    if (width >= screens.xl) return 'xl'
    if (width >= screens.lg) return 'lg'
    if (width >= screens.md) return 'md'
    if (width >= screens.sm) return 'sm'
    return 'xs'
}

const breakpoints = Vue.observable({
    w: window.innerWidth,
    h: window.innerHeight,
    is: getBreakpoint(window.innerWidth)
})

function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}

window.addEventListener(
    'resize',
    debounce(() => {
        breakpoints.w = window.innerWidth
        breakpoints.h = window.innerHeight
        breakpoints.is = getBreakpoint(window.innerWidth)
    }, 200),
    false
)

export default breakpoints