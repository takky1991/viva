import Vue from 'vue';
import axios from 'axios';
import * as Sentry from "@sentry/vue";
import Datepicker from 'vuejs-datepicker';
import { EventBus } from './event-bus.js';
import { Integrations } from "@sentry/tracing";

window.$ = window.jQuery = require('jquery');
window.Vue = Vue;

require('ucfirst');
require('./../../vendor/bootstrap3/js/bootstrap');

Vue.component('redactor', () => import('./components/Redactor.vue' /* webpackChunkName: "redactor" */));
Vue.component('upload-resource-photos', () => import('./components/UploadResourcePhotos.vue' /* webpackChunkName: "upload-resource-photos" */));
Vue.component('pay-payment', () => import('./components/admin/PayPayment.vue' /* webpackChunkName: "pay-payment" */));
Vue.component('product-variants', () => import('./components/admin/ProductVariants.vue' /* webpackChunkName: "product-variants" */));
Vue.component('html-multiselect', () => import('./components/admin/HtmlMultiselect.vue' /* webpackChunkName: "html-multiselect" */));
Vue.component('html-multiselect-brands', () => import('./components/admin/HtmlMultiselectBrands.vue' /* webpackChunkName: "html-multiselect-brands" */));
Vue.component('html-multiselect-products', () => import('./components/admin/HtmlMultiselectProducts.vue' /* webpackChunkName: "html-multiselect-products" */));
Vue.component('html-multiselect-categories', () => import('./components/admin/HtmlMultiselectCategories.vue' /* webpackChunkName: "html-multiselect-categories" */));
Vue.component('discount-products-selector', () => import('./components/admin/DiscountProductsSelector.vue' /* webpackChunkName: "discount-products-selector" */));

if (process.env.MIX_ENVIRONMENT && process.env.MIX_ENVIRONMENT == 'production') {
	Sentry.init({
		Vue,
		dsn: "https://fc92030934b34e09ad829dabe52735df@o112325.ingest.sentry.io/1199026",
		integrations: [new Integrations.BrowserTracing()],
	  
		// We recommend adjusting this value in production, or using tracesSampler
		// for finer control
		tracesSampleRate: 1.0,
	});
}

window.axios = axios;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}

var isAbsoluteURLRegex = /^(?:\w+:)\/\//;
axios.interceptors.request.use(function(config) {
    // Concatenate base path if not an absolute URL
    if ( !isAbsoluteURLRegex.test(config.url) ) {
        config.url = Laravel.baseUrl + '/api' + config.url;
    }

    return config;
});

const app = new Vue({
    el: '#backend',
    components: {
    	Datepicker
    }
});
