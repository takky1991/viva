import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import store from './store.js';
import VueSplide from 'vue-carousel';
import * as Sentry from '@sentry/vue';
import { EventBus } from './event-bus.js';
import LazyImg from './directives/LazyImg';
import imageZoom from 'vue-image-zoomer';
import InstantSearch from 'vue-instantsearch';
import { BrowserTracing } from "@sentry/tracing";
import FiltersPlugin from './plugins/FiltersPlugin.js';

require('bootstrap');
require('./frontend-support');

window.Vue = Vue;
window.$ = window.jQuery = require('jquery');

Vue.component('header-cart', () => import('./components/shop/HeaderCart.vue' /* webpackChunkName: "header-cart" */));
Vue.component('quick-cart', () => import('./components/shop/QuickCart.vue' /* webpackChunkName: "quick-cart" */));
Vue.component('add-to-cart-button', () => import('./components/shop/AddToCartButton.vue' /* webpackChunkName: "add-to-cart-button" */));
Vue.component('item-quantity', () => import('./components/shop/ItemQuantity.vue' /* webpackChunkName: "item-quantity" */));
Vue.component('phone-input', () => import('./components/PhoneInput.vue' /* webpackChunkName: "phone-input" */));
Vue.component('checkout-form-one', () => import('./components/checkout/CheckoutFormOne.vue' /* webpackChunkName: "checkout-form-one" */));
Vue.component('checkout-form-two', () => import('./components/checkout/CheckoutFormTwo.vue' /* webpackChunkName: "checkout-form-two" */));
Vue.component('checkout-form-three', () => import('./components/checkout/CheckoutFormThree.vue' /* webpackChunkName: "checkout-form-three" */));
Vue.component('checkout-success', () => import('./components/shop/CheckoutSuccess.vue' /* webpackChunkName: "checkout-success" */));
Vue.component('header-alert', () => import('./components/shop/HeaderAlert.vue' /* webpackChunkName: "header-alert" */));
Vue.component('search-modal', () => import('./components/shop/search/SearchModal.vue' /* webpackChunkName: "search-modal" */));
Vue.component('shipping-info', () => import('./components/shop/ShippingInfo.vue' /* webpackChunkName: "shipping-info" */));
Vue.component('products-carousel', () => import('./components/products/ProductsCarousel.vue' /* webpackChunkName: "products-carousel" */));
Vue.component('products-filter', () => import('./components/products/ProductsFilter.vue' /* webpackChunkName: "products-filter" */));
Vue.component('reviews', () => import('./components/products/Reviews.vue' /* webpackChunkName: "reviews" */));
Vue.component('show-more', () => import('./components/ShowMore.vue' /* webpackChunkName: "show-more" */));
Vue.component('back-to-stock-alert-form', () => import('./components/products/BackToStockAlertForm.vue' /* webpackChunkName: "back-to-stock-alert-form" */));
Vue.component('horizontal-scroll', () => import('./components/HorizontalScroll.vue' /* webpackChunkName: "horizontal-scroll" */));

Vue.use(Vuex);
Vue.use(VueSplide);
Vue.use(FiltersPlugin);
Vue.use(InstantSearch);
Vue.directive('lazy-img', LazyImg);

if (process.env.MIX_ENVIRONMENT && process.env.MIX_ENVIRONMENT == 'production') {
	Sentry.init({
		Vue,
		dsn: "https://fc92030934b34e09ad829dabe52735df@o112325.ingest.sentry.io/1199026",
		integrations: [
			new BrowserTracing()
		],
	  
		// We recommend adjusting this value in production, or using tracesSampler
		// for finer control
		tracesSampleRate: 1.0,
	});
}

window.axios = axios;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}

var isAbsoluteURLRegex = /^(?:\w+:)\/\//;
axios.interceptors.request.use(function(config) {
    // Concatenate base path if not an absolute URL
    if ( !isAbsoluteURLRegex.test(config.url) ) {
        config.url = Laravel.baseUrl + '/api' + config.url;
    }

    return config;
});

const app = new Vue({
	el: '#app',
	components:{
    	imageZoom
    },
	store: new Vuex.Store(store)
});
