let store = {
    state: {
        cart: {
            products: [],
            free_shipping: false
        }
    },

    getters: {
        cartCount: state => {
            let total = 0;

            for (let item of state.cart.products) {
                total += item.quantity;
            }

            return total;
        },

        cartTotalPrice: state => {
            let total = 0;

            for (let item of state.cart.products) {
                total += item.total_price;
            }

            return total;
        }
    },

    mutations: {
        addToCart(state, payload) {
            let found = state.cart.products.find(i => i.id === payload.product.id);

            if (found) {
                found.quantity +=  payload.quantity;
                found.total_price = found.quantity * found.real_price;

                if(payload.variant) {
                    found.variants_string = found.variants_string + ',' + payload.variant.name;
                }
            } else {
                Vue.set(payload.product, 'quantity', payload.quantity);
                Vue.set(payload.product, 'total_price', payload.totalPrice);

                if(payload.variant) {
                    Vue.set(payload.product, 'variants_string', payload.variant.name);
                }

                state.cart.products.unshift(payload.product);
            }
        },

        removeProduct(state, product) {
            let index = state.cart.products.indexOf(product);

            if (index > -1) {
                state.cart.products.splice(index, 1);
            }
        },

        setCart(state, cart) {
            state.cart.products = cart.products;
            state.cart.free_shipping = cart.free_shipping;
        }
    },

    actions: {
        addToCart({commit}, payload) {
            commit('addToCart', payload);

            if(Laravel.authenticated) {
                axios.post('/cart/item', {
                    id: payload.product.id,
                    quantity: payload.quantity,
                    variant_id: payload.variant?.id
                })
                .then((response) => {
                    commit('setCart', response.data);
                });
            } else {
                axios.post('/guest-cart/item', {
                    id: payload.product.id,
                    quantity: payload.quantity,
                    variant_id: payload.variant?.id
                }, {headers: {'X-Anonymous-Customer-Unique-Id': Laravel.gid}})
                .then((response) => {
                    commit('setCart', response.data);
                });
            }
        },

        removeProduct({commit}, product) {
            commit('removeProduct', product);

            if(Laravel.authenticated) {
                axios.delete('/cart/item/' + product.id)
                    .then((response) => {
                        axios.get('/cart')
                            .then((response) => {
                                if(response.status == 204) {
                                    commit('setCart', {
                                        products: [],
                                        free_shipping: false
                                    });
                                } else {
                                    commit('setCart', response.data);
                                }
                            })
                            .catch((error) => {
                                //
                            });
                    });
            } else {
                axios.delete('/guest-cart/item/' + product.id, {headers: {'X-Anonymous-Customer-Unique-Id': Laravel.gid}})
                    .then((response) => {
                        // This will cause a 422 because when the last item in the cart is removed
                        // because the cart is already deleted from the db
                        axios.get('/guest-cart', {headers: {'X-Anonymous-Customer-Unique-Id': Laravel.gid}})
                            .then((response) => {
                                commit('setCart', response.data);
                            })
                            .catch((error) => {
                                if(error.response.status == 422) {
                                    commit('setCart', {
                                        products: [],
                                        free_shipping: false
                                    });
                                }
                            });
                    });
            }
        },

        setCart({commit}, cart) {
            commit('setCart', cart);
        }
    }
};

export default store;