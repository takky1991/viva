document.addEventListener('DOMContentLoaded', (event) => {
    /*==================================================================
    [ Menu mobile ]*/
    $('.btn-show-menu-mobile').on('click', function() {
        $(this).toggleClass('is-active');
        $('.jquery-accordion-menu').slideToggle(100);

        document.body.classList.toggle('no-scroll');
    });

    const navExpand = [].slice.call(document.querySelectorAll('.nav-expand'))
    const backLink = `<span class="nav-link nav-back-link">
            <i class="fa fa-angle-left" aria-hidden="true" style="margin-right: 15px;"></i>
            Nazad
        </span>`

    navExpand.forEach(item => {
        item.querySelector('.nav-expand-content .nav-back').insertAdjacentHTML('afterbegin', backLink)
        item.querySelector('.nav-link').addEventListener('click', () => {
            item.classList.add('active');
            item.parentNode.classList.add('no-scroll');
            window.setTimeout(() => {
                item.parentNode.scrollTop = 0;
            }, 200);
        })
        item.querySelector('.nav-back-link').addEventListener('click', () => {
            item.parentNode.classList.remove('no-scroll');
            item.classList.remove('active');
        })
    })

    /*[ Back to top ]
    ===========================================================*/
    var windowH = $(window).height()/2;

    $(window).on('scroll',function(){
        if ($(this).scrollTop() > windowH) {
            $("#myBtn").css('display','flex');
        } else {
            $("#myBtn").css('display','none');
        }
    });

    $('#myBtn').on("click", function(){
        $('html, body').animate({scrollTop: 0}, 300);
    });
});