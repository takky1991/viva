import lozad from 'lozad';

export default {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        const observer = lozad(el);
        observer.observe();
    }
};