@component('mail::message')
Pozdrav.
 
Donosimo Vam dobre vijesti. 
<br>
Proizvod koji ste tražili je ponovno dostupan.
<br>
@component('mail::table')
|                                 |                                |
|:-------------------------------:|:-------------------------------|
| <img src="{{$product->mainPhoto() ? $product->mainPhoto()->url('product_profile') : asset('/images/logo_270_contrast.png')}}" style="margin-right: 5px; min-width: 70px;"> | {{$product->brand?->name}} <br> <b>{{$product->name()}}</b> <br> {{$product->package_description}}| 
@endcomponent

@component('mail::button', ['url' => route('products.show', ['product' => $product->getSlug()])])
Idi na proizvod
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent