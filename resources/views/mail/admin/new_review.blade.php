@component('mail::message')
Pozdrav {{$notifiable->first_name}}.

Dospjela je nova recenzija na {{config('app.name')}} web shopu za proizvod {{$product->fullName()}}.

@component('mail::panel')
<b>Ime:</b> <br>
{{$review->name}} <br>
<br>
<b>Ocjena:</b> <br>
{{$review->rating}} <br>
<br>
<b>Recenzija:</b> <br>
{{$review->content}} <br>
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent