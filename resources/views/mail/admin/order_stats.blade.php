@component('mail::message')
Pozdrav {{$notifiable->first_name}}.

{{$stats['period']}} izvještaj za {{$stats['date']}}: <br>
Broj narudžbi: <b>{{$stats['totalOrders']}}</b> <br> 
Vrijednost bez poštarina: <b>{{formatPrice($stats['totalPrice'])}}</b> <br> 
Vrijednost poštarina: <b>{{formatPrice($stats['totalShippingPrice'])}}</b>

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent