@component('mail::message')
Pozdrav {{$notifiable->first_name}}.

Dospjela je nova narudžba na {{config('app.name')}} web shopu.

@component('mail::panel')
<b>Način preuzimanja:</b> <br>
@if($order->shipping_pickup)
Preuzimanje u {{$order->shipping_name}}
@else
Slanje na adresu <br> {{$order->shipping_name}}
@endif
<br>
<br>
@if($order->shipping_pickup)
<b>Rok za preuzimanje:</b> <br>
{{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}
<br>
<br>
@endif
<b>Način plaćanja:</b> <br>
@if($order->payment_method == 'online_payment')
Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif
@else
Pouzećem @if($order->shipping_pickup){{'(gotovina, kartično)'}}@else{{'(gotovina)'}}@endif
@endif
<br>
<br>
<b>Podaci o kupcu: </b> <br>
{{$order->first_name}} {{$order->last_name}} <br>
@if(!$order->shipping_pickup)
{{$order->street_address}} <br>
{{$order->postalcode}} {{$order->city}} <br>
{{$order->country->name}} <br>
@endif
Tel: {{$order->phone}} <br>
Email: {{$order->email}} <br>
@if($order->additional_info)
<br>
<b>Napomene uz narudžbu:</b> <br>
{{$order->additional_info}}<br>
@endif
<br>
<b>Broj narudžbe:</b> {{$order->order_number}}
@endcomponent

@component('mail::table')
|                                |                                                                    |
|:-------------------------------|-------------------------------------------------------------------:|
@foreach ($orderProducts as $orderProduct)
| <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">{{$orderProduct->fullName()}} @if($orderProduct->variants) <br> Br. {{$orderProduct->variants}} @endif</a> | {{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}} <br> <b>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</b> |
@endforeach
|                                   <b>Proizvodi</b>                                                       | <b>{{formatPrice($order->total_price)}}</b>                       |
|                                   <b>Dostava</b>                                                         | <b>{{formatPrice($order->shipping_price)}}</b>                    |
|                                   <b>Ukupno</b>                                                          | <b>{{formatPrice($order->total_price_with_shipping)}}</b>         |
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent