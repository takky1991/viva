@component('mail::message')
Pozdrav {{$user->first_name}}.
 
Zaprimili smo zahtjev za promjenu šifre za Vaš {{config('app.name')}} korisnički račun.

@component('mail::button', ['url' => $url])
Promjeni šifru
@endcomponent

Link za promjenu šifre će isteći za {{config('auth.passwords.users.expire')}} minuta.
<br>
Ako niste zatražili promjenu šifre, daljnji koraci nisu potrebni.

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent