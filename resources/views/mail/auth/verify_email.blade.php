@component('mail::message')
Pozdrav {{$user->first_name}}.
 
Kliknite na dugme ispod kako bi verifikovali Vašu email adresu.

@component('mail::button', ['url' => $url])
Verifikuj
@endcomponent

Ako niste Vi kreirali {{config('app.name')}} račun, ignorišite ovaj email.

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent