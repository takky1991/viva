@component('mail::message')
Pozdrav {{$order->first_name}}.

@if($order->shipping_pickup) 
Zaprimili smo Vašu narudžbu. 
Narudžba će biti spremna u poslovnici <b>{{$order->shipping_name}}</b>. 
Rok za preuzimanje je <b>{{config('app.pickup_days_limit')}} dana</b> od dana naručivanja tj. do <b>{{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}</b> (do kraja radnog vremena poslovnice).
Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći <a href="{{route('locations')}}">ovdje</a>.
@else
Zaprimili smo Vašu narudžbu. Naručeni proizvodi će biti poslani u najkraćem vremenskom roku na Vašu adresu. Sve narudžbe koje su zaprimljene petkom nakon 14h, subotom ili nedjeljom, šalju se sljedeći radni dan tj. u ponedjeljak.
@endif

U slučaju kašnjenja ili nedostupnosti naručenih proizvoda blagovremeno ćete biti obaviješteni.
Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona <a href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a> ili putem maila na adresu <a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a>

@component('mail::panel')
<b>Način preuzimanja:</b> <br>
@if($order->shipping_pickup)
Preuzimanje u {{$order->shipping_name}}
@else
Slanje na adresu <br> {{$order->shipping_name}}
@endif
<br>
<br>
@if($order->shipping_pickup)
<b>Rok za preuzimanje:</b> <br>
{{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}
<br>
<br>
@endif
<b>Način plaćanja:</b> <br>
@if($order->payment_method == 'online_payment')
Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif
@else
Pouzećem @if($order->shipping_pickup){{'(gotovina, kartično)'}}@else{{'(gotovina)'}}@endif
@endif
<br>
<br>
<b>Podaci o kupcu: </b> <br>
{{$order->first_name}} {{$order->last_name}} <br>
@if(!$order->shipping_pickup)
{{$order->street_address}} <br>
{{$order->postalcode}} {{$order->city}} <br>
{{$order->country->name}} <br>
@endif
Tel: {{$order->phone}} <br>
Email: {{$order->email}} <br>
@if($order->additional_info)
<br>
<b>Napomene uz narudžbu:</b> <br>
{{$order->additional_info}}<br>
@endif
<br>
<b>Broj narudžbe:</b> {{$order->order_number}}
@endcomponent

@component('mail::table')
|                                |                                                                    |
|:-------------------------------|-------------------------------------------------------------------:|
@foreach ($orderProducts as $orderProduct)
| <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">{{$orderProduct->fullName()}} @if($orderProduct->variants) <br> Br. {{$orderProduct->variants}} @endif</a> | {{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}} <br> <b>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</b> |
@endforeach
|                                   <b>Proizvodi</b>                                                       | <b>{{formatPrice($order->total_price)}}</b>                       |
|                                   <b>Dostava</b>                                                         | <b>{{formatPrice($order->shipping_price)}}</b>                    |
|                                   <b>Ukupno</b>                                                          | <b>{{formatPrice($order->total_price_with_shipping)}}</b>         |
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent