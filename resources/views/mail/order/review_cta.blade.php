@component('mail::message')
Pozdrav {{$order->first_name}}.

Da li ste zadovoljni sa naručenim proizvodima? <br>
Podijelite Vaša iskustva i ujedno pomozite drugima u odabiru. <br>
Vaše mišljenje nam je jako bitno!

@component('mail::table')
|                                 |                                |
|:-------------------------------:|:-------------------------------|
@foreach ($orderProducts as $orderProduct)
| <img src="{{$orderProduct->product->mainPhoto() ? $orderProduct->product->mainPhoto()->url('product_profile') : asset('/images/logo_270_contrast.png')}}" style="margin-right: 5px; min-width: 70px;"> | {{$orderProduct->brand?->name}} <br> <b>{{$orderProduct->name()}}</b> <br> {{$orderProduct->package_description}} <br><br> <a href="{{route('products.review', ['product' => $orderProduct->product->getSlug(), 'order_number' => $order->order_number])}}" style="font-size:14px; background:#009900; padding:5px 10px; border-radius: 5px; color:#ffffff; cursor:pointer; white-space: nowrap; text-decoration: none;">Dodaj Recenziju</a>         | 
@endforeach
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent