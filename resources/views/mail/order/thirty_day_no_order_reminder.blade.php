@component('mail::message')
Pozdrav {{$notifiable->first_name}}.
 
Prošlo je dosta vremena od Vaše posljednje narudžbe. Vjerovatno ste pri kraju ili ste već potrošili Vaše proizvode te Vas podsjećamo da nadopunite zalihe.

@component('mail::table')
|                                 |                                |
|:-------------------------------:|:-------------------------------|
@foreach ($products as $product)
| <img src="{{$product->mainPhoto() ? $product->mainPhoto()->url('product_profile') : asset('/images/logo_270_contrast.png')}}" style="margin-right: 5px; min-width: 70px;"> | {{$product->brand?->name}} <br> <b>{{$product->name()}}</b> <br> {{$product->package_description}} <br><br> <a href="{{route('products.show', ['product' => $product->getSlug()])}}" style="font-size:14px; background:#009900; padding:5px 10px; border-radius: 5px; color:#ffffff; cursor:pointer; white-space: nowrap; text-decoration: none;">Idi na proizvod</a>         | 
@endforeach
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent