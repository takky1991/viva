@component('mail::message')
Pozdrav {{$order->first_name}}.
 
Dobre vijesti! 
Vaša narudžba je krenula prema Vama i možete je očekivati u najkraćem vremenskom roku na vratima.

Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona <a href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a> ili putem maila na adresu <a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a>

@component('mail::table')
|                                |                                                                    |
|:-------------------------------|-------------------------------------------------------------------:|
@foreach ($orderProducts as $orderProduct)
| <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">{{$orderProduct->fullName()}} @if($orderProduct->variants) <br> Br. {{$orderProduct->variants}} @endif</a> | {{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}} <br> <b>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</b> |
@endforeach
|                                   <b>Proizvodi</b>                                                       | <b>{{formatPrice($order->total_price)}}</b>                       |
|                                   <b>Dostava</b>                                                         | <b>{{formatPrice($order->shipping_price)}}</b>                    |
|                                   <b>Ukupno</b>                                                          | <b>{{formatPrice($order->total_price_with_shipping)}}</b>         |
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent