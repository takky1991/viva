@component('mail::message')
Pozdrav {{$order->first_name}}.
 
Dobre vijesti!
Vaša narudžba je spremna za preuzimanje u apoteci <b>{{$order->shipping_name}}</b>. 
Rok za preuzimanje je <b>{{config('app.pickup_days_limit')}} dana</b> od dana naručivanja tj. do <b>{{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}</b> (do kraja radnog vremena poslovnice).
Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći <a href="{{route('locations')}}">ovdje</a>.

Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona <a href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a> ili putem maila na adresu <a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a>

@component('mail::table')
|                                |                                                                    |
|:-------------------------------|-------------------------------------------------------------------:|
@foreach ($orderProducts as $orderProduct)
| <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">{{$orderProduct->fullName()}} @if($orderProduct->variants) <br> Br. {{$orderProduct->variants}} @endif</a> | {{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}} <br> <b>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</b> |
@endforeach
|                                   <b>Proizvodi</b>                                                       | <b>{{formatPrice($order->total_price)}}</b>                       |
|                                   <b>Dostava</b>                                                         | <b>{{formatPrice($order->shipping_price)}}</b>                    |
|                                   <b>Ukupno</b>                                                          | <b>{{formatPrice($order->total_price_with_shipping)}}</b>         |
@endcomponent

<br>
<br>
Hvala,<br>
Vaš {{config('app.name')}} tim
@endcomponent