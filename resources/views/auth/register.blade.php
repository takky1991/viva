@extends('layouts.app')

@section('content')
<!-- Content page -->
<section class="pt-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">                
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="first_name">Ime</label>
                        <input id="first-name" type="text" class="input-field @if($errors->has('first_name')) invalid @endif" name="first_name" value="{{ old('first_name') }}" placeholder="Unesite ime" autofocus>
                        @if ($errors->has('first_name'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('first_name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="last_name">Prezime</label>
                        <input id="last-name" type="text" class="input-field @if($errors->has('last_name')) invalid @endif" name="last_name" value="{{ old('last_name') }}" placeholder="Unesite prezime">
                        @if ($errors->has('last_name'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('last_name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ old('email') }}" placeholder="Unesite e-mail">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">Šifra</label>
                        <input id="password" type="password" class="input-field @if($errors->has('password')) invalid @endif" name="password" placeholder="Unesite šifru">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Potvrda šifre</label>
                        <input id="password-confirmation" type="password" class="input-field @if($errors->has('password_confirmation')) invalid @endif" name="password_confirmation" placeholder="Potvrdite šifu">
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="terms_accepted" name="terms_accepted" {{ old('terms_accepted') ? 'checked' : '' }}>
                            <label class="custom-control-label @if($errors->has('terms_accepted')) invalid @endif" for="terms_accepted"><span id="customRadioLabel1">Prihvatam</span> <a href="{{route('privacy_policy')}}" class="link" target="_blank">Uvjete korištenja</a></label>
                        </div>
                        @if ($errors->has('terms_accepted'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('terms_accepted') }}
                            </div>
                        @endif
                    </div>

                    <br>
                    <button id="register-button" type="submit" class="btn btn-primary w-100">Registruj profil <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
