@extends('layouts.app')

@section('content')
<!-- Content page -->
<section class="pt-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="email">E-mail adresa</label>
                        <input id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ old('email') }}" placeholder="Unesite Vašu e-mail adresu" required autofocus>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <br>
                    <button id="send-email-button" type="submit" class="btn btn-primary w-100">Pošalji link <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
