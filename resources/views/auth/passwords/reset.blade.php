@extends('layouts.app')

@section('content')
<!-- Content page -->
<section class="pt-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">                
                <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <h1 class="text-center">
                        Promjena šifre
                    </h1>
                    
                    <br>

                    <div class="form-group">
                        <label for="email">E-mail adresa</label>
                        <input id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ old('email') }}" placeholder="Unesite Vašu e-mail adresu" required autofocus>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">Nova šifra</label>
                        <input id="password" type="password" class="input-field @if($errors->has('password')) invalid @endif" name="password" placeholder="Unesite novu šifru" required>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Potvrda nove šifre</label>
                        <input id="password-confirm" type="password" class="input-field @if($errors->has('password_confirmation')) invalid @endif" name="password_confirmation" placeholder="Ponovno unesite novu šifru" required>
                        @if ($errors->has('password_confirmation'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <br>
                    <button id="change-password-button" type="submit" class="btn btn-primary w-100">Promjeni šifru <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
