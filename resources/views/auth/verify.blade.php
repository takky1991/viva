@extends('layouts.app')

@section('content')

<!-- Content page -->
<section class="pt-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <h1 class="text-center">
                    Uspješno ste se registrovali 🎉🎊
                </h1>

                <br>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        Poslali smo novi email na Vašu adresu.
                    </div>
                @endif

                Kako bi mogli koristiti sve mogućnosti web shopa, potrebno je da verifikujete Vašu email adresu.
                Poslali smo Vam email sa daljim uputama.
                <br>
                <br>

                <form id="resend-form" class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                    @csrf

                    Ako niste primili email, <a href="javascript:void(0)" onclick="$('#resend-form').submit()" id="resend-confirmation-email-button" class="p-0 m-0 align-baseline link">kliknite za ponovno slanje</a>.
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
