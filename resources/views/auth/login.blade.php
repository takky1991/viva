@extends('layouts.app')

@section('content')

<!-- Content page -->
<section class="pt-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">                
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
    
                    <div class="form-group">
                        <label for="email">E-mail adresa</label>
                        <input id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ old('email') }}" placeholder="Unesite Vašu e-mail adresu" required autofocus>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
    
                    <div class="form-group">
                        <label for="password">Šifra</label>
                        <input id="password" type="password" class="input-field @if($errors->has('password')) invalid @endif" name="password" placeholder="Unesite Vašu šifru" required>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
    
                    <br>
    
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Zapamti me</label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <button id="login-button" type="submit" class="btn btn-primary w-100">Prijavi se <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                    </div>

                    <br>
                    <a id="forgot-password" class="link" href="{{ route('password.request') }}">Zaboravili ste šifru?</a>

                    <br>
                    <br>

                    Nemate račun? <a href="{{route('register')}}" class="link">Registrujte se</a>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
