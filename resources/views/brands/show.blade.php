@extends('layouts.app')

@section('content')

<div class="products-index mt-4">
	<div class="container-xl">
		<div class="row">
			<div class="d-none d-lg-block col-lg-3 product-filters">
				<span>{{$products->total()}} Proizvoda</span>
				<hr>
				<ul class="categories">
					<li @if(empty($category))class="font-weight-bold"@endif>
						@if(empty($category))
							<span>{{$brand->name}}</span>
						@else
							<a href="{{route('brands.show', ['brand' => $brand->getSlug()])}}">{{$brand->name}}</a>
						@endif
						<ul class="pl-3">
							@foreach ($mainMenuCategories as $mainMenuCategory)
								@if(in_array($mainMenuCategory->id, $availableCategories))
									@foreach ($mainMenuCategory->menuChildren as $mainMenuCategoryChild)
										@if(in_array($mainMenuCategoryChild->id, $availableCategories))
											@if($category && $category->id == $mainMenuCategoryChild->id)
												<li class="font-weight-bold">
													<span>{{$mainMenuCategoryChild->title}}</span>
													@if($mainMenuCategoryChild->menuChildren->isNotEmpty())
														<ul class="pl-3">
															@foreach ($mainMenuCategoryChild->menuChildren as $item)
																@if(in_array($item->id, $availableCategories))
																	<li>
																		<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $item->getSlug()])}}">{{$item->title}}</a>
																	</li>
																@endif
															@endforeach
														</ul>
													@endif
												</li>
											@elseif($category && $category->parent && $category->parent->id == $mainMenuCategoryChild->id)
												<li class="font-weight-bold">
													<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $mainMenuCategoryChild->getSlug()])}}">{{$mainMenuCategoryChild->title}}</a>
													@if($mainMenuCategoryChild->menuChildren->isNotEmpty())
														<ul class="pl-3">
															@foreach ($mainMenuCategoryChild->menuChildren as $item)
																@if(in_array($item->id, $availableCategories))
																	<li>
																		@if($category->id == $item->id)
																			<span>{{$item->title}}</span>
																		@else
																			<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $item->getSlug()])}}">{{$item->title}}</a>
																		@endif
																	</li>
																@endif
															@endforeach
														</ul>
													@endif
												</li>
											@else
												<li>
													<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $mainMenuCategoryChild->getSlug()])}}">{{$mainMenuCategoryChild->title}}</a>
												</li>
											@endif
										@endif
									@endforeach
								@endif
							@endforeach
						</ul>
					</li>
				</ul>
			</div>
			<div class="col-lg-9">
				<div class="d-lg-none mobile-categories-wrap">
					<div class="mobile-categories">
						@if($category && $category->parent && in_array($category->parent->id, $availableCategories) && $category->parent->parent != null)
							<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $category->parent->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2 back">
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
							</a>
						@elseif($category && $category->parent && in_array($category->parent->id, $availableCategories) && $category->parent->parent == null)
							<a href="{{route('brands.show', ['brand' => $brand->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2 back">
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
							</a>
						@endif
						@if($category)
							<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $category->getSlug()])}}" class="btn btn-primary btn-sm mr-2">{{$category->title}}</a>
							@if($category->menuChildren->isNotEmpty())
								<i class="fa fa-chevron-right mr-2" aria-hidden="true"></i>
							@endif
						@endif
						@foreach ($mainMenuCategories as $mainMenuCategory)
							@if(in_array($mainMenuCategory->id, $availableCategories))
								@foreach ($mainMenuCategory->menuChildren as $mainMenuCategoryChild)
									@if($category && $mainMenuCategoryChild->id == $category->id)
										@if($mainMenuCategoryChild->menuChildren->isNotEmpty())
											@foreach ($mainMenuCategoryChild->menuChildren as $item)
												@if(in_array($item->id, $availableCategories))
													<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $item->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item->title}}</a>
												@endif
											@endforeach
										@endif
									@elseif(empty($category))
										@if(in_array($mainMenuCategoryChild->id, $availableCategories))
											<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $mainMenuCategoryChild->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$mainMenuCategoryChild->title}}</a>
										@endif
									@else
										@foreach ($mainMenuCategoryChild->menuChildren as $item)
											@if(in_array($item->id, $availableCategories) && $item->id != $category->id && $item->parent->id == $category->parent->id)
												<a href="{{route('brands.show', ['brand' => $brand->getSlug(), 'category' => $item->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item->title}}</a>
											@endif
										@endforeach
									@endif
								@endforeach
							@endif
						@endforeach
						<div class="mr-5 d-inline-block"></div>
					</div>
				</div>
				<div class="d-lg-none py-4">{{$products->total()}} Proizvoda</div>
				@if($products->isNotEmpty())
					@include('products/partials/products-grid', [
						'skipLazyLoad' => 4,
						'dataLayerList' => 'Brand Products List'
					])
				@else
					<div class="alert alert-warning" role="alert">
						Proizvodi nisu pronađeni
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection