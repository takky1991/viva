@extends('layouts.app')

@section('content')
<div class="brands-index">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<nav class="alphabet-list text-center mt-5">
                    <ul class="alphabet-list-ul list-unstyled">
                        @foreach ($alphabetGroups as $key => $value)
                            <li class="alphabet-list-li d-inline-block"><a onclick="$('html, body').animate({scrollTop: $('#brand-{{$key}}').offset().top -60 }, 'slow');" href="#brand-{{$key}}">{{$key}}</a></li>
                        @endforeach
                    </ul>
                </nav>

                @foreach ($alphabetGroups as $key => $brands)
                    <section id="brand-{{$key}}" class="brand-alphabet-section">
                        <div class="brand-letter">{{$key}}</div>
                            <ul class="brand-list">
                                @foreach ($brands as $brand)
                                    <li><a href="{{route('brands.show', ['brand' => $brand->getSlug()])}}">{{$brand->name}}</a></li>
                                @endforeach
                            </ul>
                    </section>
                @endforeach
            </div>
		</div>
	</div>
</div>
@endsection