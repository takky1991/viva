@extends('layouts.app')

@section('content')
<div class="homepage">
    @if($heroArticles->count() == 1)
        @php
            $heroArticle = $heroArticles->first();
        @endphp
        <div class="container-xl hero-container mb-5">
            <a href="{{$heroArticle->button_url}}">
                <div class="hero d-flex flex-column-reverse flex-md-row">
                    <div class="hero-content d-flex flex-column justify-content-center" style="@if($heroArticle->background_color) background-color: {{$heroArticle->background_color}}; @endif @if($heroArticle->text_color) color: {{$heroArticle->text_color}} @endif">
                        <div class="d-none d-md-block mb-2 w-100">
                            <img src="{{asset('images/logos/horizontal-logo-white.svg')}}" alt="{{config('app.name')}}" width="150px">
                        </div>
                        <h1 class="hero-title mb-2">{{$heroArticle->title}}</h1>
                        <p class="hero-description mb-3">{{$heroArticle->subtitle}}</p> 
                        <div>
                            <button class="btn" @if($heroArticle->background_color && $heroArticle->text_color) style="color: {{$heroArticle->background_color}}; background-color: {{$heroArticle->text_color}};" @endif>{{$heroArticle->button_text}}</button>
                        </div>
                    </div>
                    <img 
                        src="{{$heroArticle->mobilePhoto?->url('hero_article_mobile')}}"
                        alt="{{$heroArticle->title}}" 
                        class="d-md-none hero-image"
                        width="555"
                        height="400"
                    >
                    <img 
                        src="{{$heroArticle->photo?->url('hero_article')}}"
                        alt="{{$heroArticle->title}}" 
                        class="d-none d-md-block hero-image"
                        width="555"
                        height="400"
                    >
                </div>
            </a>
        </div>
    @elseif($heroArticles->count() > 1)
        <div class="container-xl hero-container mb-5">
            <carousel 
                :per-page="1"
                :loop="true"
                :mouse-drag="false"
                :navigation-enabled="true"
                :pagination-enabled="false"
                :autoplay="true"
                :autoplay-timeout="4000"
                :autoplay-hover-pause="false"
                navigation-prev-label="<i class='fa fa-chevron-left' aria-hidden='true'></i>"
                navigation-next-label="<i class='fa fa-chevron-right' aria-hidden='true'></i>"
            >
                @foreach ($heroArticles as $heroArticle)
                    <slide style="@if($heroArticle->background_color) background-color: {{$heroArticle->background_color}}; @endif" @if($loop->index > 0) v-cloak @endif>
                        <a href="{{$heroArticle->button_url}}">
                            <div class="hero d-flex flex-column-reverse flex-md-row h-100">
                                <div class="hero-content d-flex flex-column justify-content-center h-100" style="@if($heroArticle->background_color) background-color: {{$heroArticle->background_color}}; @endif @if($heroArticle->text_color) color: {{$heroArticle->text_color}} @endif">
                                    <div class="d-none d-md-block mb-2 w-100">
                                        <img src="{{asset('images/logos/horizontal-logo-white.svg')}}" alt="{{config('app.name')}}" width="150px">
                                    </div>
                                    <h1 class="hero-title mb-2">{{$heroArticle->title}}</h1>
                                    <p class="hero-description mb-3">{{$heroArticle->subtitle}}</p> 
                                    <div>
                                        <button class="btn" @if($heroArticle->background_color) style="color: {{$heroArticle->background_color}}; background-color: {{$heroArticle->text_color}};" @endif>{{$heroArticle->button_text}}</button>
                                    </div>
                                </div>

                                @if($loop->index == 0)
                                    <img
                                        src="{{$heroArticle->mobilePhoto?->url('hero_article_mobile')}}"
                                        alt="{{$heroArticle->title}}"
                                        class="d-md-none hero-image"
                                        width="555"
                                        height="400"
                                    >
                                    <img
                                        src="{{asset('images/carousel_loading.png')}}"
                                        data-src="{{$heroArticle->photo?->url('hero_article')}}"
                                        alt="{{$heroArticle->title}}"
                                        class="d-none d-md-block hero-image"
                                        width="555"
                                        height="400"
                                        v-lazy-img
                                    >
                                @else
                                    <img
                                        src="{{asset('images/carousel_loading.png')}}"
                                        data-src="{{$heroArticle->mobilePhoto?->url('hero_article_mobile')}}"
                                        alt="{{$heroArticle->title}}"
                                        class="d-md-none hero-image"
                                        width="555"
                                        height="400"
                                        v-lazy-img
                                    >
                                    <img
                                        src="{{asset('images/carousel_loading.png')}}"
                                        data-src="{{$heroArticle->photo?->url('hero_article')}}"
                                        alt="{{$heroArticle->title}}"
                                        class="d-none d-md-block hero-image"
                                        width="555"
                                        height="400"
                                        v-lazy-img
                                    >
                                @endif
                            </div>
                        </a>
                    </slide>
                @endforeach
            </carousel>
        </div>
    @endif

    <div class="container-xl mb-5">
        <horizontal-scroll inline-template v-cloak>
            <div class="horizontal-slider">
                <div class="horizontal-slider-items shop-by-skin-type" ref="horizontalSliderItems">
                    <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/intro.png')}}" alt="Traži po tipu kože" width="170" height="195" v-lazy-img>
                    <a href="{{route('categories.show', ['category' => 'suha-koza'])}}">
                        <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/dry.png')}}" alt="Suha koža" width="170" height="195" v-lazy-img>
                    </a>
                    <a href="{{route('categories.show', ['category' => 'masna-koza'])}}">
                        <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/oily.png')}}" alt="Masna koža" width="170" height="195" v-lazy-img>
                    </a>
                    <a href="{{route('categories.show', ['category' => 'normalna-do-mjesovita-koza'])}}">
                        <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/normal.png')}}" alt="Normalna koža" width="170" height="195" v-lazy-img>
                    </a>
                    <a href="{{route('categories.show', ['category' => 'osjetljiva-koza-lica'])}}">
                        <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/sensitive.png')}}" alt="Osjetljiva koža" width="170" height="195" v-lazy-img>
                    </a>
                    <a href="{{route('categories.show', ['category' => 'normalna-do-mjesovita-koza'])}}">
                        <img src="{{asset('images/spin_icon.jpg')}}" data-src="{{asset('images/shop_by_skin_type/combination.png')}}" alt="Mješovita koža" width="170" height="195" v-lazy-img>
                    </a>
                </div>
                <div v-if="hasScroll" class="horizontal-slider-slide">
                    <input type="range" min="1" step="1" :max="mobileMaxScroll" :value="mobileScrollValue" @input="updateScroll" class="slider" ref="mobileSlider">
                </div>
            </div>
        </horizontal-scroll>
    </div>

    @if(isset($onSaleProducts) && $onSaleProducts->count() > 4)
        <section class="on-sale products-index products-carousel">
            <div class="container-xl">
                <div class="row products-grid">
                    <div class="product-wrap col-lg-12">
                        <div class="carousel-title">
                            <h2>Proizvodi na popustu</h2>
                            <a href="{{route('discount_collections.index')}}" class="link font-weight-bold brand">Pogledaj sve >></a>
                        </div>
                        <products-carousel inline-template v-cloak>
                            <div v-if="isTabletOrSmaller" class="horizontal-slider">
                                <div class="horizontal-slider-items" ref="horizontalSliderItems">
                                    @foreach ($onSaleProducts as $onSaleProduct)
                                        @include('products/partials/product', [
                                            'product' => $onSaleProduct,
                                            'dataLayerList' => 'Homepage On Sale Products List',
                                            'dataLayerPosition' => $loop->index,
                                            'inCarousel' => true
                                        ])
                                    @endforeach
                                </div>
                                <div v-if="hasScroll" class="horizontal-slider-slide">
                                    <input type="range" min="1" step="1" :max="mobileMaxScroll" :value="mobileScrollValue" @input="updateScroll" class="slider" ref="mobileSlider">
                                </div>
                            </div>
                            <carousel v-else
                                :loop="true"
                                :per-page-custom="[[576, 3], [880, 4], [1200, 5]]" 
                                :scroll-per-page="true" 
                                :pagination-enabled="true"
                                pagination-active-color="#009900"
                                pagination-color="#e5e5e5"
                                :navigation-enabled="true"
                                navigation-prev-label="<i class='fa fa-chevron-left' aria-hidden='true'></i>"
                                navigation-next-label="<i class='fa fa-chevron-right' aria-hidden='true'></i>"
                            >
                                @foreach ($onSaleProducts as $onSaleProduct)
                                    <slide>
                                        @include('products/partials/product', [
                                            'product' => $onSaleProduct,
                                            'dataLayerList' => 'Homepage On Sale Products List',
                                            'dataLayerPosition' => $loop->index,
                                            'inCarousel' => true
                                        ])
                                    </slide>
                                @endforeach
                            </carousel>
                        </products-carousel>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(isset($popularProducts) && $popularProducts->count() > 4)
        <section class="on-sale products-index products-carousel">
            <div class="container-xl">
                <div class="row products-grid">
                    <div class="product-wrap col-lg-12">
                        <div class="carousel-title">
                            <h2>Popularni proizvodi</h2>
                            <a href="{{route('products.index', ['bestSeller' => 'true'])}}" class="link font-weight-bold brand">Pogledaj sve >></a>
                        </div>
                        <products-carousel inline-template v-cloak>
                            <div v-if="isTabletOrSmaller" class="horizontal-slider">
                                <div class="horizontal-slider-items" ref="horizontalSliderItems">
                                    @foreach ($popularProducts as $popularProduct)
                                        @include('products/partials/product', [
                                            'product' => $popularProduct,
                                            'dataLayerList' => 'Homepage Popular Products List',
                                            'dataLayerPosition' => $loop->index,
                                            'inCarousel' => true
                                        ])
                                    @endforeach
                                </div>
                                <div v-if="hasScroll" class="horizontal-slider-slide">
                                    <input type="range" min="1" step="1" :max="mobileMaxScroll" :value="mobileScrollValue" @input="updateScroll" class="slider" ref="mobileSlider">
                                </div>
                            </div>
                            <carousel v-else
                                :loop="true"
                                :per-page-custom="[[576, 3], [880, 4], [1200, 5]]" 
                                :scroll-per-page="true" 
                                :pagination-enabled="true"
                                pagination-active-color="#009900"
                                pagination-color="#e5e5e5"
                                :navigation-enabled="true"
                                navigation-prev-label="<i class='fa fa-chevron-left' aria-hidden='true'></i>"
                                navigation-next-label="<i class='fa fa-chevron-right' aria-hidden='true'></i>"
                            >
                                @foreach ($popularProducts as $popularProduct)
                                    <slide>
                                        @include('products/partials/product', [
                                            'product' => $popularProduct,
                                            'dataLayerList' => 'Homepage Popular Products List',
                                            'dataLayerPosition' => $loop->index,
                                            'inCarousel' => true
                                        ])
                                    </slide>
                                @endforeach
                            </carousel>
                        </products-carousel>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(isset($isMobileApp) && !$isMobileApp)
        <div class="container-xl app-banner-container">
            <div class="row">
                <div class="col-12">
                    <div class="app-banner p-4 p-md-5 d-flex flex-column flex-md-row align-items-center">
                        <img class="app-photo" data-src="{{asset('images/apps/phone.png')}}" v-lazy-img/>
                        <div class="content">
                            <p class="title">Da li znate za našu aplikaciju?</p>
                            <p>Uz aplikaciju ApotekaViva24, cjelokupna funkcionalnost web-shopa dostupna je i na vašem pametnom telefonu ili tabletu.</p>
                            <div class="mt-3">
                                <a href="https://apps.apple.com/us/app/apotekaviva24/id1667045751?itsct=apps_box_link&itscg=30200" target="_blank">
                                    <img class="mb-1" style="height: 40px;" data-src="{{asset('images/apps/apple-badge.svg')}}" v-lazy-img/>
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.apotekaviva24.ba" target="_blank">
                                    <img class="mb-1" style="height: 40px;" data-src="{{asset('images/apps/google-play-badge.png')}}" v-lazy-img/>
                                </a>
                                <a href="https://url.cloud.huawei.com/k0JzHSqfT2" target="_blank">
                                    <img class="mb-1" style="height: 40px;" data-src="{{asset('images/apps/huawei-badge.png')}}" v-lazy-img/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@endsection