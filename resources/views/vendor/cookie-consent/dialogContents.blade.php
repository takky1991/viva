@if(app()->environment() != 'testing' && isset($isMobileApp) && !$isMobileApp)
<div class="js-cookie-consent cookie-consent">
    <div class="container-xl">
        <div class="d-flex flex-wrap justify-content-between">
            <p class="w-100"><b>{{config('app.name')}} kolačići</b></p>
            <span class="cookie-consent__message mb-2 mb-md-0">
                Koristimo kolačiće kako bi Vam osigurali bolje iskustvo i funkcionalnost stranice.
                <br>
                Više informacija o kolačićima možete potražiti <a href="{{route('cookie_policy')}}">ovdje</a>, a za nastavak pregleda i korištenje stranice, kliknite na <b>Prihvatam</b>.
            </span>
        
            <button class="js-cookie-consent-agree cookie-consent__agree btn btn-outline-primary btn-sm">
                Prihvatam
            </button>
        </div>
    </div>
</div>
@endif