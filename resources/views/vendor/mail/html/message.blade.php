@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<div class="app-banner">
<div class="title">Da li znate za našu aplikaciju?</div>
<p>Uz aplikaciju ApotekaViva24, cjelokupna funkcionalnost web-shopa dostupna je i na vašem pametnom telefonu ili tabletu.</p>
<div style="margin-top: 20px">
<a href="https://apps.apple.com/us/app/apotekaviva24/id1667045751?itsct=apps_box_link&itscg=30200" target="_blank">
<img style="height: 40px; margin-bottom: 5px;" src="{{asset('images/apps/apple-badge.png')}}"/>
</a>
<a href="https://play.google.com/store/apps/details?id=com.apotekaviva24.ba&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank">
<img style="height: 40px; margin-bottom: 5px;" src="{{asset('images/apps/google-play-badge.png')}}"/>
</a>
<a href="https://url.cloud.huawei.com/k0JzHSqfT2" target="_blank">
<img style="height: 40px; margin-bottom: 5px;" src="{{asset('images/apps/huawei-badge.png')}}"/>
</a>
</div>
</div>
<div class="content-cell">
Copyright &copy; 2020 - {{now()->year}} | {{config('app.name')}} | Sva prava zadržava
</div>
@endcomponent
@endslot
@endcomponent
