@extends('layouts.app')

@section('content')
<!-- Articles -->
<div class="articles-index mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				@if($articles->isNotEmpty())
					@include('articles/partials/articles-grid', ['skipLazyLoad' => 2])
				@else
					<div class="alert alert-warning" role="alert">
						Savjeti i novosti nisu pronađeni
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection