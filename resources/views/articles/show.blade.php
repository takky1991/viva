@extends('layouts.app')

@section('content')

<section class="articles-show mt-5 mb-5">
    <article itemscope itemtype="http://schema.org/Article">
        <div class="container-xl">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1 class="text-center mb-5" itemprop="headline">{{$article->title}}</h1>
                </div>
            </div>
        </div>

        @if($article->photo)
            <img 
                srcset="
                    {{$article->photo->url('article_mobile')}} 1500w,
                    {{$article->photo->url('article')}} 1800w
                "
                alt="{{$article->title}}" 
                class="mb-5 d-lg-none"
                width="800"
                height="500"
            >
            <div class="container-xl">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1" itemprop="image" content="{{$article->photo->url('article')}}">
                        <img 
                            srcset="
                                {{$article->photo->url('article_mobile')}} 1500w,
                                {{$article->photo->url('article')}} 1800w
                            "
                            alt="{{$article->title}}" 
                            class="mb-5 d-none d-md-none d-lg-block"
                            width="800"
                            height="500"
                        >
                    </div>
                </div>
            </div>
        @endif

        <div class="container-xl">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="article-body" itemprop="articleBody">
                        <p class="font-weight-bold lead mb-5">{{$article->subtitle}}</p>
    
                        <div class="articles-content redactor-styles">
                            {!!$article->content!!}
                        </div>

                        <div itemprop="author" itemscope itemtype="http://schema.org/Organization">
                            <meta itemprop="name" content="{{config('app.name')}}"/>
                            <meta itemprop="url" content="{{route('home')}}"/>
                            <span>Autor: {{config('app.name')}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-xl">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <hr>
                    @if(isset($brands) && $brands->isNotEmpty())
                        <div class="related-items mt-5">
                            <h5 class="font-weight-bold">Vezani brendovi</h5>
                            @foreach ($brands as $brand)
                                <div class="related-item mt-2">
                                    <a href="{{route('brands.show', ['brand' => $brand->getSlug()])}}">{{$brand->name}}</a>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(isset($categories) && $categories->isNotEmpty())
                        <div class="related-items mt-5">
                            <h5 class="font-weight-bold">Vezane kategorije</h5>
                            @foreach ($categories as $category)
                                <div class="related-item mt-2">
                                    <a href="{{route('categories.show', ['category' => $category->getSlug()])}}">{{$category->title}}</a>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(isset($products) && $products->isNotEmpty())
                        <div class="related-items products-index mt-5">
                            <h5 class="font-weight-bold mb-5">Vezani proizvodi</h5>
                            @include('products/partials/products-grid', ['dataLayerList' => 'Article Show Products List'])
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </article>
</section>
@endsection