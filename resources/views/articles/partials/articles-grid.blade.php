<div class="articles-grid">
    <div class="row" itemscope itemtype="http://schema.org/ItemList">
        @foreach($articles as $article)
            <div class="article-wrap col-12 col-md-6 mb-5">
                @if(isset($skipLazyLoad) && $loop->iteration <= $skipLazyLoad)
                    @include('articles/partials/article', ['lazyLoadPhoto' => false ])
                @else
                    @include('articles/partials/article')
                @endif
            </div>
        @endforeach
    </div>

    @if($articles instanceof \Illuminate\Pagination\LengthAwarePaginator )
        <div class="d-flex justify-content-center w-100">
            {{$articles->onEachSide(1)->links()}}
        </div>
    @endif
</div>