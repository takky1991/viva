<article id="article-{{$loop->iteration}}" class="article" itemprop="itemListElement" itemscope itemtype="http://schema.org/Article">
    <meta itemprop="position" content="{{$loop->iteration}}"/>
    <a href="{{route('articles.show', ['article' => $article->getSlug()])}}" itemprop="url">
        @if($article->photo)
            <img src="{{$article->photo->url('article_mobile')}}" alt="{{$article->title}}" itemprop="image" class="w-100">
        @else 
            <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" itemprop="image" class="w-100">
        @endif
        <div class="p-4" style="min-height: 245px;">
            <h2 class="mb-2" itemprop="headline">{{$article->title}}</h2>
            <p itemprop="description" class="mb-2">{{$article->subtitle}}</p>
            <strong>Više</strong>
        </div>
    </a>
</article>