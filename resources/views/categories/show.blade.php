@extends('layouts.app')

@section('content')

<div class="products-index mt-4">
	<div class="container-xl">
		<div class="row">
			<div class="d-none d-lg-block col-lg-3 product-filters">
				<span>{{$products->total()}} Proizvoda</span>
				<hr>
				{{-- <products-filter inline-template>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="on_sale" v-model="filters.onSale">
						<label class="custom-control-label" for="on_sale">Na popustu</label>
					</div>
				</products-filter> --}}

				<ul class="categories">
					@foreach ($mainMenuCategories as $mainMenuCategory)
						@if($mainMenuCategory->id == $category->id)
							<li class="font-weight-bold">
								<span>{{$category->title}}</span>
								<ul class="pl-3">
									@foreach ($mainMenuCategory->menuChildren as $childCategory)
										<li>
											<a href="{{route('categories.show', ['category' => $childCategory->getSlug()])}}">{{$childCategory->title}}</a>
										</li>
									@endforeach
								</ul>
							</li>
						@elseif($category->parent && $mainMenuCategory->id == $category->parent->id)
							<li>
								<a href="{{route('categories.show', ['category' => $mainMenuCategory->getSlug()])}}">{{$mainMenuCategory->title}}</a>
								<ul class="pl-3">
									@foreach ($mainMenuCategory->menuChildren as $mainMenuCategoryChild)
										@if($mainMenuCategoryChild->id == $category->id)
											<li class="font-weight-bold">
												<span>{{$mainMenuCategoryChild->title}}</span>
												@if($mainMenuCategoryChild->menuChildren->isNotEmpty())
													<ul class="pl-3">
														@foreach ($mainMenuCategoryChild->menuChildren as $childCategory)
															<li>
																<a href="{{route('categories.show', ['category' => $childCategory->getSlug()])}}">{{$childCategory->title}}</a>
															</li>
														@endforeach
													</ul>
												@endif
											</li>
										@else
											<li>
												<a href="{{route('categories.show', ['category' => $mainMenuCategoryChild->getSlug()])}}">{{$mainMenuCategoryChild->title}}</a>
											</li>
										@endif
									@endforeach
								</ul>
							</li>
						@elseif($category->parent && $category->parent->parent && $mainMenuCategory->id == $category->parent->parent->id)
							<li>
								<a href="{{route('categories.show', ['category' => $mainMenuCategory->getSlug()])}}">{{$mainMenuCategory->title}}</a>
								<ul class="pl-3">
									@foreach ($mainMenuCategory->menuChildren as $mainMenuCategoryChild)
										@if($mainMenuCategoryChild->id == $category->parent->id)
											<li class="font-weight-bold">
												<a href="{{route('categories.show', ['category' => $mainMenuCategoryChild->getSlug()])}}">{{$mainMenuCategoryChild->title}}</a>
												<ul class="pl-3">
													@foreach ($mainMenuCategoryChild->menuChildren as $childCategory)
														@if($childCategory->id == $category->id)
															<li>
																<span>{{$childCategory->title}}</span>
															</li>
														@else
															<li>
																<a href="{{route('categories.show', ['category' => $childCategory->getSlug()])}}">{{$childCategory->title}}</a>
															</li>
														@endif
													@endforeach
												</ul>
											</li>
										@else
											<li>
												<a href="{{route('categories.show', ['category' => $mainMenuCategoryChild->getSlug()])}}">{{$mainMenuCategoryChild->title}}</a>
											</li>
										@endif
									@endforeach
								</ul>
							</li>
						@endif
					@endforeach
				</ul>
			</div>
			<div class="col-lg-9">
				<div class="d-lg-none mobile-categories-wrap">
					<div class="mobile-categories">
						@if($category && $category->parent)
							<a href="{{route('categories.show', ['category' => $category->parent->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2 back">
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
							</a>
						@endif
						@if($category)
							<a href="{{route('categories.show', ['category' => $category->getSlug()])}}" class="btn btn-primary btn-sm mr-2">{{$category->title}}</a>
							@if($category->menuChildren->isNotEmpty())
								<i class="fa fa-chevron-right mr-2" aria-hidden="true"></i>
							@endif
						@endif
						@foreach ($mainMenuCategories as $mainMenuCategory)
							@if($mainMenuCategory->id == $category->id)
								@foreach ($mainMenuCategory->menuChildren as $item)
									<a href="{{route('categories.show', ['category' => $item->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item->title}}</a>
								@endforeach
							@else
								@foreach ($mainMenuCategory->menuChildren as $item)
									@if($item->id == $category->id)
										@if($item->menuChildren->isNotEmpty())
											@foreach ($item->menuChildren as $item1)
												<a href="{{route('categories.show', ['category' => $item1->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item1->title}}</a>
											@endforeach
										@else
											@foreach ($mainMenuCategory->menuChildren as $item1)
												@if($item1->id != $category->id)
													<a href="{{route('categories.show', ['category' => $item1->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item1->title}}</a>
												@endif
											@endforeach
										@endif
									@else
										@foreach ($item->menuChildren as $item1)
											@if($item->id == $category->parent?->id)
												@if($item1->id != $category->id)
													<a href="{{route('categories.show', ['category' => $item1->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item1->title}}</a>
												@endif
											@endif
										@endforeach
									@endif
								@endforeach
							@endif
						@endforeach
						<div class="mr-5 d-inline-block"></div>
					</div>
				</div>
				<div class="d-lg-none py-4">{{$products->total()}} Proizvoda</div>
				@if($products->isNotEmpty())
					@include('products/partials/products-grid', [
						'skipLazyLoad' => 4,
						'dataLayerList' => 'Category Products List'
					])
				@else
					<div class="alert alert-warning" role="alert">
						Proizvodi nisu pronađeni
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection