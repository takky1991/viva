<div class="top-bar">
    <header-alert 
        admin-link="{{route('backend.home')}}"
        locations-link="{{route('locations')}}"
        register-link="{{route('register')}}"
        login-link="{{route('login')}}"
        settings-link="{{route('user.settings')}}"
        logout-link="{{route('logout')}}"
        :show-admin-link="{{auth()->check() && auth()->user()->hasAnyRole('admin|employee|writer') ? 'true' : 'false'}}"
    ></header-alert>
</div>

<div class="header-wrap mb-lg-3">
    <header class="header container-xl">
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-inline-block d-lg-none col p-0 btn-show-menu-mobile hamburger hamburger--squeeze">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </div>
            <div class="col p-0 header-logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/logos/horizontal-logo.svg')}}" alt="{{config('app.name')}}">
                </a>
            </div>
            <div class="d-none d-lg-block search-field w-100 mx-5" @click="$eventBus.$emit('openSearchModal')">
                <input class="px-5 w-100" type="text" placeholder="Tražite proizvod, brend ili kategoriju..." value="{{request()->sq ?? ''}}">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
            <div class="col p-0 d-flex align-items-center justify-content-end">
                <div class="d-lg-none icon-header-item pr-3" @click="$eventBus.$emit('openSearchModal')" id="header-search-icon">
                    <i class="fa fa-search icon-header-search" aria-hidden="true"></i>
                </div>
                <header-cart @if(!empty($cartJson)) :cart="{{$cartJson}}" @endif></header-cart>
            </div>
        </div>
        @include('layouts/mobile-menu')
    </header>

    <div class="container-xl">
        <nav class="nav d-none d-lg-flex">
            <div class="dropdown show mr-3">
                <a class="nav-link dropdown-toggle" href="{{route('products.index')}}" id="dropdownMenuLink">
                    Proizvodi
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <ul>
                        @isset($mainMenuCategories)
                            @foreach($mainMenuCategories as $mainCategory)
                                <li>
                                    <a class="dropdown-item" href="{{route('categories.show', ['category' => $mainCategory->getSlug()])}}">{{$mainCategory->title}}</a>
                                    @if($mainCategory->menuChildren->isNotEmpty())
                                        <div class="submenu flex-wrap justify-content-start">
                                            @php $categoriesWithoutChildren = [] @endphp
                                            @foreach($mainCategory->menuChildren as $category)
                                                @if($category->menuChildren->isNotEmpty())
                                                    <ul class="mb-5 @if(!$loop->last) mr-3 @endif">
                                                        <li class="mb-2 font-weight-bold">
                                                            <a href="{{route('categories.show', ['category' => $category->getSlug()])}}">{{$category->title}}</a>
                                                        </li>
                                                        @foreach ($category->menuChildren as $categoryChild)
                                                            <li>
                                                                <a href="{{route('categories.show', ['category' => $categoryChild->getSlug()])}}">{{$categoryChild->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    @php
                                                        $categoriesWithoutChildren[] = $category
                                                    @endphp
                                                @endif
                                            @endforeach

                                            @if(sizeof($categoriesWithoutChildren) > 0)
                                                <ul class="mb-5">
                                                    @foreach($categoriesWithoutChildren as $categoryWithoutChildren)
                                                        <li>
                                                            <a href="{{route('categories.show', ['category' => $categoryWithoutChildren->getSlug()])}}">{{$categoryWithoutChildren->title}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                    @endif
                                </li>
                            @endforeach                            
                        @endisset
                    </ul>
                </div>
            </div>
            <a href="{{route('brands.index')}}" class="nav-link">Brendovi</a>
            <a href="{{route('discount_collections.index')}}" class="nav-link" style="color: #009900">Popusti <i class="fa fa-percent" aria-hidden="true"></i></a>
            <a href="{{route('articles.index')}}" class="nav-link">Savjeti i Novosti</a>
        </nav>
    </div>
</div>