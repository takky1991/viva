@if(app()->environment() == 'production')
    <link rel="preconnect" href="https://cdn.apotekaviva24.ba" crossorigin>
    <link rel="dns-prefetch" href="https://cdn.apotekaviva24.ba">
@else
    <link rel="preconnect" href="{{'https://' . config('filesystems.disks.s3.bucket') . '.s3.eu-central-1.amazonaws.com'}}" crossorigin>
    <link rel="dns-prefetch" href="{{'https://' . config('filesystems.disks.s3.bucket') . '.s3.eu-central-1.amazonaws.com'}}">
@endif