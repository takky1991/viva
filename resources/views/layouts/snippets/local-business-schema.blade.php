<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Pharmacy",
        "image": [
            "https://apotekaviva24.ba/images/logos/horizontal-logo.svg"
        ],
        "@id": "https://apotekaviva24.ba/",
        "name": "Centralna Apoteka Viva",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Mala Lisa bb",
            "addressLocality": "Cazin",
            "addressRegion": "USK",
            "postalCode": "77220",
            "addressCountry": "BIH"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 44.9581561,
            "longitude": 15.9060268
        },
        "url": "https://apotekaviva24.ba/",
        "priceRange": "$",
        "telephone": "+38737536051",
        "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday"
                ],
                "opens": "07:00",
                "closes": "22:00"
            },
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": "Sunday",
                "opens": "08:00",
                "closes": "21:00"
            }
        ],
        "department": [
            {
                "@type": "Pharmacy",
                "image": [
                    "https://apotekaviva24.ba/images/logos/horizontal-logo.svg"
                ],
                "name": "Apoteka Viva",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "Zuhdije Žalića bb Križ",
                    "addressLocality": "Velika Kladuša",
                    "addressRegion": "USK",
                    "postalCode": "77230",
                    "addressCountry": "BIH"
                },
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": 45.174344,
                    "longitude": 15.8097371
                },
                "url": "https://apotekaviva24.ba/",
                "priceRange": "$",
                "telephone": "+38737770161",
                "openingHoursSpecification": [
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": [
                            "Monday",
                            "Tuesday",
                            "Wednesday",
                            "Thursday",
                            "Friday",
                            "Saturday"
                        ],
                        "opens": "07:00",
                        "closes": "21:00"
                    },
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": "Sunday",
                        "opens": "08:00",
                        "closes": "20:00"
                    }
                ]
            },
            {
                "@type": "Pharmacy",
                "image": [
                    "https://apotekaviva24.ba/images/logos/horizontal-logo.svg"
                ],
                "name": "Apoteka Viva",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "TC Robot, Ćoralići bb",
                    "addressLocality": "Cazin",
                    "addressRegion": "USK",
                    "postalCode": "77220",
                    "addressCountry": "BIH"
                },
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": 44.970632,
                    "longitude": 15.897689
                },
                "url": "https://apotekaviva24.ba/",
                "priceRange": "$",
                "telephone": "+38737536110",
                "openingHoursSpecification": [
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": [
                            "Monday",
                            "Tuesday",
                            "Wednesday",
                            "Thursday",
                            "Friday",
                            "Saturday"
                        ],
                        "opens": "08:00",
                        "closes": "21:00"
                    },
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": "Sunday",
                        "opens": "08:00",
                        "closes": "14:00"
                    }
                ]
            },
            {
                "@type": "Pharmacy",
                "image": [
                    "https://apotekaviva24.ba/images/logos/horizontal-logo.svg"
                ],
                "name": "Apoteka Viva",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "Gata bb",
                    "addressLocality": "Bihać",
                    "addressRegion": "USK",
                    "postalCode": "77000",
                    "addressCountry": "BIH"
                },
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": 44.930144,  
                    "longitude": 15.809262
                },
                "url": "https://apotekaviva24.ba/",
                "priceRange": "$",
                "telephone": "+38737370037",
                "openingHoursSpecification": [
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": [
                            "Monday",
                            "Tuesday",
                            "Wednesday",
                            "Thursday",
                            "Friday",
                            "Saturday"
                        ],
                        "opens": "08:00",
                        "closes": "16:00"
                    }
                ]
            },
            {
                "@type": "Pharmacy",
                "image": [
                    "https://apotekaviva24.ba/images/logos/horizontal-logo.svg"
                ],
                "name": "Apoteka Viva",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "TC Ikanović, Jošani bb",
                    "addressLocality": "Cazin",
                    "addressRegion": "USK",
                    "postalCode": "77000",
                    "addressCountry": "BIH"
                },
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": 44.953570,  
                    "longitude": 15.941810
                },
                "url": "https://apotekaviva24.ba/",
                "priceRange": "$",
                "telephone": "+38737518110",
                "openingHoursSpecification": [
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": [
                            "Monday",
                            "Tuesday",
                            "Wednesday",
                            "Thursday",
                            "Friday",
                            "Saturday"
                        ],
                        "opens": "07:00",
                        "closes": "22:00"
                    },
                    {
                        "@type": "OpeningHoursSpecification",
                        "dayOfWeek": "Sunday",
                        "opens": "08:00",
                        "closes": "16:00"
                    }
                ]
            }
        ]
    }
</script>