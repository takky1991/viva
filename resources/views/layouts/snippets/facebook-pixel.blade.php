<!-- Facebook Pixel Code -->
<script>
    setTimeout(function() {
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '815136905699278'); 
        fbq('track', 'PageView');
    }, @if(request()->route()?->getName() == 'checkout.success') 1 @else 3000 @endif);
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=815136905699278&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->