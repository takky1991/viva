@php
    $preloads = [
        'font' => [
            [
                'link' => asset('fonts/Poppins-Regular.ttf'),
                'type' => 'font/ttf'
            ],
            [
                'link' => asset('fonts/Poppins-SemiBold.ttf'),
                'type' => 'font/ttf'
            ],
            [
                'link' => asset('fonts/Poppins-Bold.ttf'),
                'type' => 'font/ttf'
            ]
        ]
    ];
@endphp

@foreach($preloads as $asset => $links)
    @foreach($links as $link)
        <link rel="preload" href="{{$link['link']}}" as="{{$asset}}" type="{{$link['type']}}" crossorigin="anonymous">
    @endforeach
@endforeach
