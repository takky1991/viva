<!DOCTYPE html>
<html lang="bs-BA">
    <head>
        @include('layouts/snippets/preconnect')
        @include('layouts/snippets/preload')

        @if(request()->route()?->getName() == 'home')
            @include('layouts/snippets/app-schema')
            @include('layouts/snippets/logo-schema')
            @include('layouts/snippets/sitelinks-search')
            @include('layouts/snippets/local-business-schema')
        @endif

        <script>
            window.dataLayer = window.dataLayer || [];

            @auth()
                dataLayer.push({
                    user_id: {{auth()->id()}}
                });
            @endauth
        </script>

        @stack('gtm_data_layer')

        @if(app()->environment() == 'production')
            @include('layouts/snippets/gtag-head')
            <meta name="facebook-domain-verification" content="zdl4kz2iti5u8jwmybucjqb0ebgq11" />
            <meta name="google-site-verification" content="kVAtGB19A_Z2hnjCxLNuGrzUYjWQl6K4jvOw83dO7i0" />
        @endif

        <title>@if(isset($title) && $title){{$title}} - {{config('app.name')}}@else{{config('app.name')}}@endif @if(app()->environment() != 'production') - {{ app()->environment() }}@endif</title>

        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="cleartype" content="on" />
        <meta name="HandheldFriendly" content="true">

        <!-- Mobile meta tags -->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="application-name" content="{{config('app.name')}}">
        <meta name="apple-mobile-web-app-title" content="{{config('app.name')}}">
        <meta name="msapplication-starturl" content="/">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, shrink-to-fit=no">

        <!-- Favicons -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('images/favicon/site.webmanifest')}}">

        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#009900">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#009900">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#009900">

        @if(!empty($description))
            <meta name="description" content="{{ $description }}" />
        @else
            <meta name="description" content="Naruči svoje omiljene proizvode uz dostavu od {{config('app.lowest_shipping_cost')}}KM u sve gradove BIH." />
        @endif

        <meta name="language" content="bs"/>
        <link rel="canonical" href="{{!empty($canonical) ? $canonical : url()->current()}}"/>

        {!! OpenGraph::render() !!}

        @if(app()->environment() != 'production')
            <meta name="robots" content="noindex,nofollow"/>
        @endif        

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}"/>
        <!-- Styles -->
        <link href="{{ mix('css/app/app.css') }}" rel="stylesheet">

        <script type="text/javascript">
            window.Laravel = @php echo json_encode([
                'authenticated' => auth()->check(),
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/'),
                'gid' => session()->has('guest_id') ? session('guest_id') : null,
                'free_shipping_threshold' => (float) config('app.free_shipping_threshold'),
                'free_shipping_threshold_alert' => (float) config('app.free_shipping_threshold_alert'),
                'lowest_shipping_cost' => (int) config('app.lowest_shipping_cost'),
                'phone' => config('app.phone'),
                'phone_short' => config('app.phone_short'),
                'algolia_id' => config('scout.algolia.id'),
                'algolia_key' => config('scout.algolia.key')
            ]); @endphp
        </script>
    </head>
    <body>
        @if(app()->environment() == 'production')
            @include('layouts/snippets/gtag-body')
        @endif

        <div id="app">
            @if(!in_array(request()->route()?->getName(), [
                'checkout',
                'checkout.one',
                'checkout.two',
                'checkout.three',
                'checkout.success'
            ]))
                @include('layouts/header')

                <search-modal 
                    products-url="{{route('products.index')}}" 
                    brands-url="{{route('brands.index')}}"
                    @isset($featuredBrands)
                        :featured-brands="{{json_encode(\Viva\Http\Resources\BrandResource::collection($featuredBrands)->jsonSerialize())}}"
                    @endisset
                ></search-modal>
                <quick-cart 
                    cart-url="{{route('cart.show')}}" 
                    checkout-url="{{route('checkout')}}"
                ></quick-cart>

                {{-- Banner --}}
                {{-- <div class="banner">
                    <a href="{{route('discount_collections.index')}}" style="color: #fff">
                        <i class="fa fa-percent" aria-hidden="true"></i> <b>BLACK FRIDAY</b> <i class="fa fa-percent" aria-hidden="true"></i> 
                        <br class="d-lg-none">
                        &nbsp;&nbsp;
                        Pogledaj sve proizvode <span style="text-decoration: underline; color: #fff">ovdje</span>!
                    </a>
                </div> --}}

                @isset($breadcrumbs)
                    <div class="breadcrumb-header position-relative">
                        <div class="container-xl position-relative @if(!in_array(request()->route()?->getName(), [
                                'products.show',
                                'articles.show',
                                'backend.shop.articles.preview'
                            ])) big @else py-3 @endif">
                            @include('layouts/partials/breadcrumbs')

                            @if(!in_array(request()->route()?->getName(), [
                                'products.show',
                                'articles.show',
                                'backend.shop.articles.preview'
                            ]))
                                <h1 class="font-weight-bold">{{$breadcrumbs[sizeof($breadcrumbs) - 1]['label']}}</h1>
                            @endif
                        </div>
                        @if(!in_array(request()->route()?->getName(), [
                            'products.show',
                            'articles.show',
                            'backend.shop.articles.preview'
                        ]))
                            <div class="decoration">{{$breadcrumbs[sizeof($breadcrumbs) - 1]['label']}}</div>
                        @endif
                    </div>
                @endisset
            @endisset

            @yield('content')

            @if(!in_array(request()->route()?->getName(), [
                'checkout',
                'checkout.one',
                'checkout.two',
                'checkout.three',
                'checkout.success'
            ]))
                @include('layouts/footer')
            @endif

            <!-- Back to top -->
            <div class="btn-back-to-top" id="myBtn">
                <span class="symbol-btn-back-to-top">
                    <i class="fa fa-chevron-up"></i>
                </span>
            </div>
        </div>

        <!-- Scripts -->
        <script type="text/javascript" src="{{ mix('js/app/manifest.js') }}" defer></script>
        <script type="text/javascript" src="{{ mix('js/app/vendor.js') }}" defer></script>
        <script type="text/javascript" src="{{ mix('js/app/app.js') }}" defer></script>
        <script src="https://use.fontawesome.com/32549faaa1.js" defer></script>

        @if(app()->environment() == 'production')
            @include('layouts/snippets/facebook-pixel')
        @endif

        @include('cookie-consent::index')

        @if(!empty($_SERVER['SERVER_ADDR']))
            <!-- Server: {{ $_SERVER['SERVER_ADDR'] }} -->
        @endif
    </body>
</html>
