<!-- Footer -->
<footer class="text-center text-lg-left mt-5">
	<div class="need-help container-xl mb-5 d-flex flex-column flex-md-row align-items-center justify-content-center">
		<span class="mr-md-2">Trebate pomoć?</span>
		<span class="phone">
			<a href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a>
		</span>
		<span class="d-none d-md-inline-block mx-2">|</span>
		<span class="mail">
			<a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a>
		</span>
	</div>
	<div class="footer-content pb-5 pt-3 pt-lg-5">
		<div class="container-xl">
			<div class="row">
				<div class="col-lg-3 collapse-block pb-3 pt-lg-3">
					<p class="footer-title d-none d-lg-block"><b>WEBSHOP</b></p>
					<button class="d-block d-lg-none w-100 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseWebshop" aria-expanded="false" aria-controls="footerCollapseWebshop">
						<p class="footer-title">
							<b>WEBSHOP</b> 
							<i class="fa fa-chevron-down right" aria-hidden="true"></i>
                            <i class="fa fa-chevron-up right" aria-hidden="true"></i>
						</p>
					</button>
					<div class="collapse dont-collapse-lg" id="footerCollapseWebshop">
						<ul class="pt-3">
							<li>
								<a href="{{route('home')}}">Početna</a>
							</li>
							@isset($mainMenuCategories)
								@foreach($mainMenuCategories as $mainCategory)
								<li>
									<a href="{{route('categories.show', ['category' => $mainCategory->getSlug()])}}">{{$mainCategory->title}}</a>
								</li>
								@endforeach
							@endisset
							<li>
								<a href="{{route('brands.index')}}">Brendovi</a>
							</li>
							<li>
								<a href="{{route('articles.index')}}">Savjeti i Novosti</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="col-lg-3 collapse-block py-3">
					<p class="footer-title d-none d-lg-block"><b>INFORMACIJE</b></p>
					<button class="d-block d-lg-none w-100 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseInformation" aria-expanded="false" aria-controls="footerCollapseInformation">
						<p class="footer-title">
							<b>INFORMACIJE</b> 
							<i class="fa fa-chevron-down right" aria-hidden="true"></i>
                            <i class="fa fa-chevron-up right" aria-hidden="true"></i>
						</p>
					</button>
					<div class="collapse dont-collapse-lg" id="footerCollapseInformation">
						<ul class="pt-3">
							<li>
								<a href="{{route('locations')}}">Naše Apoteke</a>
							</li>
							<li>
								<a href="{{route('delivery')}}">Dostava</a>
							</li>
							<li>
								<a href="{{route('paying')}}">Način plaćanja</a>
							</li>
							<li>
								<a href="{{route('returns')}}">Povrat robe</a>
							</li>
						</ul>
					</div>
				</div>
	
				<div class="col-lg-3 collapse-block py-3">
					<p class="footer-title d-none d-lg-block"><b>{{config('app.name')}}</b></p>
					<button class="d-block d-lg-none w-100 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseViva" aria-expanded="false" aria-controls="footerCollapseViva">
						<p class="footer-title">
							<b>{{config('app.name')}}</b> 
							<i class="fa fa-chevron-down right" aria-hidden="true"></i>
                            <i class="fa fa-chevron-up right" aria-hidden="true"></i>
						</p>
					</button>
					<div class="collapse dont-collapse-lg" id="footerCollapseViva">
						<ul class="pt-3">
							<li>
								<a href="{{route('cookie_policy')}}">Politika kolačića</a>
							</li>
							<li>
								<a href="{{route('privacy_policy')}}">Pravila zaštite privatnosti</a>
							</li>
							<li>
								<a href="{{route('terms_of_purchase')}}">Uvjeti kupovine</a>
							</li>
							<li>
								<a href="{{route('legal_notice')}}">Pravne napomene</a>
							</li>
						</ul>
					</div>
				</div>
	
				<div class="col-lg-3 py-3">
					<p class="footer-title"><b>PRATI NAS</b></p>
					<ul class="pt-1 social-profiles">
						<li class="d-inline-block mr-3">
							<a href="https://www.facebook.com/apotekaviva24" target="_blank" rel="noreferrer noopener">
								<i class="fa fa-facebook align-top mr-1"></i> Facebook
							</a>
						</li>
						<li class="d-inline-block">
							<a href="https://www.instagram.com/apotekaviva24" target="_blank" rel="noreferrer noopener">
								<i class="fa fa-instagram align-top mr-1"></i> Instagram
							</a>
						</li>
					</ul>

					@if(isset($isMobileApp) && !$isMobileApp)
						<p class="footer-title pt-4"><b>PREUZMI APLIKACIJU</b></p>
						<ul class="pt-1">
							<li class="d-inline-block">
								<a href="https://apps.apple.com/us/app/apotekaviva24/id1667045751?itsct=apps_box_link&itscg=30200" target="_blank">
									<img style="height: 40px;" data-src="{{asset('images/apps/apple-badge.svg')}}" v-lazy-img/>
								</a>
							</li>
							<li class="d-inline-block">
								<a href="https://play.google.com/store/apps/details?id=com.apotekaviva24.ba" target="_blank">
									<img style="height: 40px;" data-src="{{asset('images/apps/google-play-badge.png')}}" v-lazy-img/>
								</a>
							</li>
							<li class="d-inline-block">
								<a href="https://url.cloud.huawei.com/k0JzHSqfT2" target="_blank">
									<img style="height: 40px;" data-src="{{asset('images/apps/huawei-badge.png')}}" v-lazy-img/>
								</a>
							</li>
						</ul>
					@endif

					<p class="pt-4 footer-title"><b>PARTNERI</b></p>
					<ul class="pt-1 shipping-partners">
						<li class="d-inline-block mr-3">
							<img data-src="{{asset('images/logos/x_express.png')}}" height="25" title="X Express Brza Pošta" v-lazy-img>
						</li>
						<li class="d-inline-block">
							<img data-src="{{asset('images/logos/a2b.png')}}" height="30" title="A2B Brza Pošta" v-lazy-img>
						</li>
					</ul>
				</div>
			</div>

			<div class="text-center">
				<a href="https://www.mastercard.com" target="_blank">
					<img class="m-2" data-src="{{asset('images/cards/mastercard.svg')}}" title="Mastercard" alt="Mastercard logo" style="height: 25px;" v-lazy-img>
				</a>
				<a href="https://brand.mastercard.com/brandcenter/more-about-our-brands.html" target="_blank">
					<img class="m-2" data-src="{{asset('images/cards/maestro.svg')}}" title="Maestro" alt="Maestro logo" style="height: 25px;" v-lazy-img>
				</a>
				<a href="https://www.visa.co.uk/about-visa/visa-in-europe.html" target="_blank">
					<img class="m-2" data-src="{{asset('images/cards/visa.svg')}}" title="Visa" alt="Visa logo" style="height: 25px;" v-lazy-img>
				</a>
				<a href="https://www.mastercard.ba/bs-ba/korisnici/podrska/sigurnost-i-zastita/identity-check.html" target="_blank">
					<img class="m-2" data-src="{{asset('images/cards/mastercard_id_check.svg')}}" title="Mastercard ID Check" alt="Mastercard ID Check logo" style="height: 25px;" v-lazy-img>
				</a>
				<a href="https://www.visa.co.uk/pay-with-visa/featured-technologies/verified-by-visa.html" target="_blank" onclick="javascript:window.open('https://www.visaeurope.com/making-payments/verified-by-visa/','Verified by Visa','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=540, height=540'); return false;">
					<img class="m-2" data-src="{{asset('images/cards/visa_secure.svg')}}" title="Visa Secure" alt="Visa Secure logo" style="height: 25px;" v-lazy-img>
				</a>
				<a href="https://monri.com" target="_blank">
					<img class="m-2" data-src="{{asset('images/cards/monri_pay_web.png')}}" title="Monri" alt="Monri logo" style="height: 43px" v-lazy-img>
				</a>
			</div>

			<hr>
	
			<div class="footer-notes">
				<p class="text-center">Sve cijene su sa uračunatim PDV-om na koje može biti dodan trošak dostave.</p>
				<p class="text-center">Sadržaj stranice je informativnog karaktera i nije zamjena za liječnički pregled ili savjet farmaceuta. <br> Za obavijesti o mjerama opreza, rizicima i nuspojavama obratite se svom liječniku ili farmaceutu.</p>
				<br>
				<p class="text-center"><b>Copyright &copy; 2020 - {{now()->year}} | {{config('app.name')}} | Sva prava zadržava</b></p>
			</div>
		</div>
	</div>
</footer>