<ol class="bread-crumb d-flex align-items-center m-0" itemscope itemtype="https://schema.org/BreadcrumbList">
    @foreach ($breadcrumbs as $breadcrumb)
        @if($loop->last)
            <li class="d-none d-lg-block" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a itemprop="item" href="{{url()->current()}}">
                    <span itemprop="name">{{$breadcrumb['label']}}</span>
                </a>
                <meta itemprop="position" content="{{$loop->iteration}}" />
            </li>
        @else
            <li 
                @if($loop->index != $loop->count - 2) class="d-none d-lg-block" @endif
                itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
            >
                <a itemprop="item" href="{{$breadcrumb['link']}}">
                    <span itemprop="name">
                        <i class="d-inline-block d-lg-none fa fa-angle-left mr-2" aria-hidden="true"></i> 
                        @if($breadcrumb['label'] == 'Početna')
                            <i class="fa fa-home d-none d-lg-block" aria-hidden="true"></i>
                            <span class="d-lg-none">{{$breadcrumb['label']}}</span>
                        @else
                            {{$breadcrumb['label']}}
                        @endif
                    </span>
                </a>
                <meta itemprop="position" content="{{$loop->iteration}}" />
            </li>
            <i class="d-none d-lg-block fa fa-angle-right mx-2" aria-hidden="true"></i>
        @endif
    @endforeach
</ol>