<div class="mobile-menu">
    <div class="jquery-accordion-menu">
        <nav class="nav-drill">
            <ul class="nav-items nav-level-1">
                @isset($mainMenuCategories)
                    @foreach($mainMenuCategories as $mainCategory)
                        <li class="nav-item @if($mainCategory->menuChildren->isNotEmpty()) nav-expand @endif">
                            @if($mainCategory->menuChildren->isNotEmpty())
                                <span class="nav-link first-level nav-expand-link">
                                    {{$mainCategory->title}}
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </span>
                            @else
                                <a class="nav-link first-level" href="{{route('categories.show', ['category' => $mainCategory->getSlug()])}}">
                                    {{$mainCategory->title}}
                                </a>
                            @endif
                            @if($mainCategory->menuChildren->isNotEmpty())
                                <ul class="nav-items nav-expand-content nav-level-2">
                                    <li class="nav-item nav-back d-flex justify-content-between">
                                        <a class="nav-link see-all" href="{{route('categories.show', ['category' => $mainCategory->getSlug()])}}">Vidi sve</a>
                                    </li>
                                    @foreach($mainCategory->menuChildren as $category)
                                        <li class="nav-item @if($category->menuChildren->isNotEmpty()) nav-expand @endif">
                                            @if($category->menuChildren->isNotEmpty())
                                                <span class="nav-link nav-expand-link">
                                                    {{$category->title}}
                                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                                </span>
                                            @else
                                                <a class="nav-link" href="{{route('categories.show', ['category' => $category->getSlug()])}}">
                                                    {{$category->title}}
                                                </a>
                                            @endif
                                            @if($category->menuChildren->isNotEmpty())
                                                <ul class="nav-items nav-expand-content nav-level-3">
                                                    <li class="nav-item nav-back d-flex justify-content-between">
                                                        <a class="nav-link see-all" href="{{route('categories.show', ['category' => $category->getSlug()])}}">Vidi sve</a>
                                                    </li>
                                                    @foreach($category->menuChildren as $categoryChild)
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="{{route('categories.show', ['category' => $categoryChild->getSlug()])}}">
                                                                {{$categoryChild->title}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                @endisset
                <hr>
                <li class="nav-item">
                    <a href="{{route('brands.index')}}" class="nav-link first-level">Brendovi</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('discount_collections.index')}}" class="nav-link first-level font-weight-bold" style="color: #009900">Popusti <i class="fa fa-percent" aria-hidden="true"></i></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('articles.index')}}" class="nav-link first-level">Savjeti i Novosti</a>
                </li>

                @auth
                    <hr>
                    <li class="nav-item">
                        <a href="{{ route('user.settings') }}" id="user-avatar" class="nav-link first-level"><i class="fa fa-user-o mr-1" aria-hidden="true" style="font-size: 20px"></i> Moj račun</a>
                        @hasanyrole('admin|employee|writer')
                            <a href="{{route('backend.home')}}" class="nav-link first-level">Admin</a>
                        @endhasanyrole
                    </li>
                @endauth
                <hr>
                <li class="nav-item">
                    <a href="{{ route('locations') }}" class="nav-link first-level"><i class="fa fa-map-marker mr-2" aria-hidden="true"></i> Naše Apoteke</a>
                </li>
                <li class="nav-item">
                    <a href="tel:{{config('app.phone')}}" class="nav-link first-level"><i class="fa fa-volume-control-phone mr-2" aria-hidden="true"></i> {{config('app.phone_short')}}</a>
                </li>
                <li class="nav-item">
                    <a href="mailto:{{config('app.email')}}" class="nav-link first-level"><i class="fa fa-envelope-o mr-2" aria-hidden="true"></i> {{config('app.email')}}</a>
                </li>
                <li class="nav-item">
                    <div class="nav-link first-level">
                        <a href="https://www.facebook.com/apotekaviva24" target="_blank" rel="noreferrer noopener" class="link pr-3"><i class="fa fa-facebook" style="font-size: 24px;"></i></a>
                        <a href="https://www.instagram.com/apotekaviva24" target="_blank" rel="noreferrer noopener" class="link"><i class="fa fa-instagram" style="font-size: 24px;"></i></a>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="pt-4 px-3">
                        @guest
                            <a href="{{ route('login') }}" class="btn btn-primary w-100">Prijava</a>
                            <a href="{{ route('register') }}" class="btn btn-outline-primary w-100 mt-3">Registracija</a>
                        @else
                            <a href="{{ route('logout') }}" class="btn btn-outline-secondary w-100 mt-4"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                Odjava
                            </a>
                        @endguest
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</div>