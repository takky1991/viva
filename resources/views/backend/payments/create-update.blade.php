@extends('backend.layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.payments.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Sva Plaćanja</a>
				@if(empty($payment))
					<h3>Dodaj Plaćanje</h3>
				@else
					<h3>Uredi Plaćanje</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="payment-form" 
					method="POST" 
					action="{{ empty($payment) ? route('backend.payments.store') : route('backend.payments.update', ['payment' => $payment])}}">
					{{ csrf_field() }}

					@if(!empty($payment))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
								  	<div class="form-group {{ $errors->has('supplier_id') ? ' has-error' : '' }}">
								  		<label for="supplier_id">Dobavljač</label>
								  		<select class="form-control" id="supplier_id" name="supplier_id">
								  			<option value="" selected>Odaberi dobavljača</option>
								  			@foreach($suppliers as $supplier)
										  		<option value="{{$supplier->id}}" {{empty($payment) ? (old('supplier_id') == $supplier->id ? 'selected' : '') : (($payment->supplier->id == $supplier->id) ? 'selected' : '')}}>
										  			{{$supplier->name}}
										  		</option>
										  	@endforeach
										</select>
										@if($errors->has('supplier_id'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('supplier_id') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('invoice_id') ? ' has-error' : '' }}">
								  		<label for="invoice_id">Br. fakture</label>
								  		<input type="text" 
										  		class="form-control" 
										  		id="invoice_id" 
										  		name="invoice_id" 
										  		value="{{empty($payment) ? old('invoice_id') : $payment->invoice_id }}">
										@if($errors->has('invoice_id'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('invoice_id') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('delivered_at') ? ' has-error' : '' }}">
								  		<label for="delivered_at">Datum prijema</label>
										<datepicker input-class="form-control" 
											id="delivered_at" 
											name="delivered_at" 
											language="bs"
											format="dd-MM-yyyy"
											value="{{empty($payment) ? 
												(old('delivered_at') ? \Carbon\Carbon::parse(old('delivered_at'))->format('m-d-Y') : '') : $payment->delivered_at->format('m-d-Y') }}">
										</datepicker>
										@if($errors->has('delivered_at'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('delivered_at') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('pay_until') ? ' has-error' : '' }}">
								  		<label for="pay_until">Valuta plaćanja</label>
										<datepicker input-class="form-control" 
											id="pay_until" 
											name="pay_until" 
											language="bs"
											format="dd-MM-yyyy"
											value="{{empty($payment) ? 
												(old('pay_until') ? \Carbon\Carbon::parse(old('delivered_at'))->format('m-d-Y') : '') : $payment->pay_until->format('m-d-Y') }}">
										</datepicker>
										@if($errors->has('pay_until'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('pay_until') }}</strong>
		                                    </span>
		                                @endif
									</div>
									
									<div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
								  		<label for="amount">Iznos</label>
										<div class="input-group">
										  	<input type="number" 
										  		class="form-control" 
										  		id="amount" 
										  		name="amount" 
										  		placeholder="99.99" 
										  		aria-describedby="amount-addon"
										  		value="{{empty($payment) ? old('amount') : $payment->amount }}">
										  	<span class="input-group-addon" id="amount-addon">KM</span>
										</div>
										@if($errors->has('amount'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('amount') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('discount') ? ' has-error' : '' }}">
										<label for="discount">Rabat</label>
										<input type="text" 
											class="form-control" 
											id="discount" 
											name="discount" 
											placeholder="npr 20%" 
											value="{{empty($payment) ? old('discount') : $payment->discount }}"
										>
										@if($errors->has('discount'))
											<span class="help-block">
												<strong>{{ $errors->first('discount') }}</strong>
											</span>
										@endif
									</div>
						  		</div>
							</div>
						</div>
						<div class="col-md-4">
						  	@if(auth()->user()->hasRole('admin'))
								<div class="panel panel-default">
							  		<div class="panel-body">
							  			@if(!empty($payment))
							  				<div class="form-group">
							  					<label for="paid">Kreirao/la</label>
							  					<p>{{$payment->user?->first_name}} {{$payment->user?->last_name}}</p>
							  				</div>
							  			@endif

										<div class="form-group {{ $errors->has('paid') ? ' has-error' : '' }}" style="display: inline-block;margin-right: 50px;">
											<label for="paid">Plaćeno</label>
											<div>
									  			<label class="switch">
													<input type="hidden" name="paid" value="0">
												  	<input type="checkbox" 
												  		id="paid" 
												  		name="paid"
												  		value="1" 
												  		{{empty($payment) ? (old('paid') ? 'checked' : '') : ($payment->paid ? 'checked' : '')}}>
												  	<span class="slider"></span>
												</label>
											</div>
											@if($errors->has('paid'))
			                                    <span class="help-block">
			                                        <strong>{{ $errors->first('paid') }}</strong>
			                                    </span>
			                                @endif
										</div>

										@if(!empty($payment) && $payment->discount)
											<div class="form-group {{ $errors->has('discount_paid') ? ' has-error' : '' }}" style="display: inline-block;margin-right: 50px;">
												<label for="discount_paid">Rabat uplaćen</label>
												<div>
													<label class="switch">
														<input type="hidden" name="discount_paid" value="0">
														<input type="checkbox" 
															id="discount_paid" 
															name="discount_paid"
															value="1" 
															{{empty($payment) ? (old('discount_paid') ? 'checked' : '') : ($payment->discount_paid ? 'checked' : '')}}>
														<span class="slider"></span>
													</label>
												</div>
												@if($errors->has('discount_paid'))
													<span class="help-block">
														<strong>{{ $errors->first('discount_paid') }}</strong>
													</span>
												@endif
											</div>
										@endif
							  		</div>
							  	</div>
							@endif
						</div>
					</div>
				</form>
				<hr>
			  	@if(!empty($payment))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati plaćanje?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-payment-form').submit();
	                }">
	                    Obriši
	                </button>

	                <form 
						id="delete-payment-form" 
	                	action="{{ route('backend.payments.destroy', ['payment' => $payment]) }}" 
	                	method="POST" 
	                	style="display: none;"
					>
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('payment-form').submit();">
			  		{{empty($payment) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection