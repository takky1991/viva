@extends('backend.layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<form class="form-inline" action="{{route('backend.payments.search')}}" method="POST">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-xs-12">
						<a class="btn btn-success btn-lg" href="{{route('backend.payments.create')}}">Nova Faktura</a>
					</div>
				</div>

				<br>

				<div class="index-filter">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<input type="text" name="invoiceId" class="form-control" placeholder="Br. fakture" value="{{request()->get('invoiceId') ?? ''}}">
							</div>
							<div class="form-group">
								<datepicker input-class="form-control" 
									id="delivered_at_from_filter" 
									name="deliveredAtFrom"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Datum prijema od"
									value="{{!empty(request()->get('deliveredAtFrom')) ? \Carbon\Carbon::parse(request()->get('deliveredAtFrom'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>
		
							<div class="form-group">
								<datepicker input-class="form-control" 
									id="delivered_at_to_filter" 
									name="deliveredAtTo"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Datum prijema do"
									value="{{!empty(request()->get('deliveredAtTo')) ? \Carbon\Carbon::parse(request()->get('deliveredAtTo'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>
		
							<div class="form-group">
								<datepicker input-class="form-control" 
									id="pay_until_filter" 
									name="payUntil"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Valuta plaćanja"
									value="{{!empty(request()->get('payUntil')) ? \Carbon\Carbon::parse(request()->get('payUntil'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>
		
							<div class="form-group">
								<select class="form-control" id="supplier" name="supplier">
									<option value="">Svi dobavljači</option>
									@foreach($suppliers as $item)
										<option value="{{$item->id}}" {{Request::get('supplier') == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
									@endforeach
								</select>
							</div>
	
							<div class="form-group">
								<select class="form-control" id="paid" name="paid">
									<option value="">Plaćeno i Neplaćeno</option>
									<option value="true" {{Request::get('paid') == 'true' ? 'selected' : ''}}>Samo Plaćeno</option>
									<option value="false" {{Request::get('paid') == 'false' ? 'selected' : ''}}>Samo Neplaćeno</option>
								</select>
							</div>
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary btn-lg">Prikaži</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-body">
			  		<ul class="list-group">
			  			@forelse($suppliers as $supplier)
						  	<li class="list-group-item">
						    	{{$supplier->name}}
						    	<div class="pull-right">
									@if($supplier->unpaidPayments->isNotEmpty())
							    		<span class="label label-danger">{{$supplier->unpaidPayments->count()}}</span> 
									@endif
							    	@if($supplier->duePayments->isNotEmpty())
										<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: rgb(217, 83, 79)"></i>
									@endif
							    	<a class="btn btn-default btn-xs" 
							    		style="margin-left: 40px;" 
							    		href="{{route('backend.payments.index', ['supplier' => $supplier->id])}}">
							    		Fakture
							    	</a>
						    	</div>
						  	</li>
					  	@empty
					  		Nema rezultata
					  	@endforelse
					</ul>
				</div>
			</div>
		</div>
	</div>
@endsection