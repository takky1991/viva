<html>
    <head>
        <title>Fakture</title>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
        
            td, th {
                border: 1px solid #ddd;
                padding: 8px;
            }
        
            tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        
            th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #04AA6D;
                color: white;
            }
        </style>
    </head>
    <body>
        <p style="margin-bottom: 25px">
            <b>Ukupno faktura: {{$total}}</b> <br>
            <b>Ukupna vrijednost: {{$totalAmount}}KM</b>
        </p>
        <table>
            <thead>
                <tr>
                    <th>Dobavljac</th>
                    <th>Br. fakture</th>
                    <th>Datum prijema</th>
                    <th>Valuta placanja</th>
                    <th>Iznos</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($payments as $payment)
                    <tr>
                        <td>{{$payment->supplier->name}}</td>
                        <td>{{$payment->invoice_id}}</td>
                        <td>{{$payment->delivered_at->format('d M Y')}}</td>
                        <td>{{$payment->pay_until->format('d M Y')}}</td>
                        <td>{{$payment->amount}}KM</td>
                    </tr>
                @endforeach 
            </tbody>
        </table>
    </body>
</html>