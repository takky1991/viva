@extends('backend.layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<form class="form-inline" action="{{route('backend.payments.search')}}" method="POST">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-xs-12">
						<a class="btn btn-success btn-lg" href="{{route('backend.payments.create')}}">Nova Faktura</a>
					</div>
				</div>

				<br>
				
				<div class="index-filter">
					<div class="row">
						<div class="col-xs-12">
							{{-- <div class="form-group">
								<input class="styled-checkbox" id="unpaid" name="unpaid" type="checkbox" value="1" {{Request::get('unpaid') ? 'checked' : ''}}>
								<label for="unpaid">Neplaćeno</label>
							</div>
							<div class="form-group">
								<input class="styled-checkbox" id="paid" name="paid" type="checkbox" value="1" {{Request::get('paid') ? 'checked' : ''}}>
								<label for="paid">Plaćeno</label>
							</div> --}}
							<div class="form-group">
								<input type="text" name="invoiceId" class="form-control" placeholder="Br. fakture" value="{{request()->get('invoiceId') ?? ''}}">
							</div>
							<div class="form-group">
								<datepicker input-class="form-control" 
									id="delivered_at_from_filter" 
									name="deliveredAtFrom"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Datum prijema od"
									value="{{!empty(request()->get('deliveredAtFrom')) ? \Carbon\Carbon::parse(request()->get('deliveredAtFrom'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>

							<div class="form-group">
								<datepicker input-class="form-control" 
									id="delivered_at_to_filter" 
									name="deliveredAtTo"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Datum prijema do"
									value="{{!empty(request()->get('deliveredAtTo')) ? \Carbon\Carbon::parse(request()->get('deliveredAtTo'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>

							<div class="form-group">
								<datepicker input-class="form-control" 
									id="pay_until_filter" 
									name="payUntil"
									language="bs"
									format="dd-MM-yyyy"
									placeholder="Valuta plaćanja"
									value="{{!empty(request()->get('payUntil')) ? \Carbon\Carbon::parse(request()->get('payUntil'))->format('m-d-Y') : ''}}">
								</datepicker>
							</div>
							
							<div class="form-group">
								<select class="form-control" id="supplier" name="supplier">
									<option value="">Svi dobavljači</option>
									@foreach($suppliers as $item)
										<option value="{{$item->id}}" {{Request::get('supplier') == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<select class="form-control" id="paid" name="paid">
									<option value="">Plaćeno i Neplaćeno</option>
									<option value="true" {{Request::get('paid') == 'true' ? 'selected' : ''}}>Samo Plaćeno</option>
									<option value="false" {{Request::get('paid') == 'false' ? 'selected' : ''}}>Samo Neplaćeno</option>
								</select>
							</div>

							<div class="form-group">
								<select class="form-control" id="discount" name="discount">
									<option value="">Rabat</option>
									<option value="true" {{Request::get('discount') == 'true' ? 'selected' : ''}}>Da</option>
									<option value="false" {{Request::get('discount') == 'false' ? 'selected' : ''}}>Ne</option>
								</select>
							</div>
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary btn-lg">Prikaži</button>
							@if(auth()->user()->hasRole('admin'))
								<a href="{{route('backend.payments.pdf', request()->all())}}" class="btn btn-success btn-lg" target="_blank">PDF</a>
							@endif
							<a href="{{route('backend.payments.index')}}">Resetuj filter</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table" style="margin-bottom: 0">
			                <thead>
			                      <tr>
			                        <th>Ukupno faktura</th>
			                        <th>Plaćeninh faktura</th>
			                        <th>Neplaćeninh faktura</th>
			                        <th>Ukupno plaćeno</th>
			                        <th>Ukupno dugovanje</th>
			                      </tr>
			                </thead>
			                <tbody>
			                    <tr>
			                        <td style="font-size: 20px"><strong>{{$paymentsCount}}</strong></td>
			                        <td style="color: #4CAF50; font-size: 20px"><strong>{{$paymentsPaid}}</strong></td>
			                        <td style="color: #d9534f; font-size: 20px"><strong>{{$paymentsUnpaid}}</strong></td>
			                        <td style="color: #4CAF50; font-size: 20px"><strong>{{number_format($paymentsTotalPaid,2,",",".")}}KM</strong></td>
			                        <td style="color: #d9534f; font-size: 20px"><strong>{{number_format($paymentsTotalUnpaid,2,",",".")}}KM</strong></td>
			                    </tr>
			                </tbody>
			            </table>
        			</div>
        		</div>
        	</div>
        </div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Dobavljač</th>
						        	<th>Br. fakture</th>
						        	<th>Datum prijema</th>
						        	<th>Valuta plaćanja</th>
						        	<th>Iznos</th>
						        	<th>
										Plaćeno 
										@if($payments->isNotEmpty() && request()->filled('supplier')) 
											(<a 
												href="javascript:void(0)" 
												style="color:#337ab7"
												onclick="if (confirm('Da li zaista želite platiti sve?')) {
													event.preventDefault();
													document.getElementById('pay-all-form').submit();
												}"
											>
												Plati
											</a>)
											<form
												id="pay-all-form" 
												action="{{ route('backend.payments.pay', request()->all()) }}" 
												method="POST" 
												style="display: none;"
											>
												{{ csrf_field() }}
											</form>
										@endif
									</th>
						        	<th>Kreirao/la</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@forelse($payments as $payment)
						    	<tr>
									<td>
										{{$payment->supplier->name}}
									</td>
									<td>
										{{$payment->invoice_id}}
									</td>
									<td>
										{{$payment->delivered_at->format('d M Y')}}
									</td>
									<td>
										{{$payment->pay_until->format('d M Y')}}
										@if($payment->pay_until <= now() && !$payment->paid)
											<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: rgb(217, 83, 79)"></i>
										@endif
									</td>
									<td>
										{{$payment->amount}}KM @if($payment->discount) <span style="@if($payment->discount_paid) color: #4CAF50 @else color: #d9534f @endif">({{$payment->discount}})</span> @endif
									</td>
									<td>
										<pay-payment 
											:payment-id="{{$payment->id}}"
											:paid="{{$payment->paid == 1 ? 'true' : 'false'}}"
											:show-pay-link="{{auth()->user()->hasRole('admin') ? 'true' : 'false'}}"
										></pay-payment>
									</td>
									<td>
										{{$payment->user?->first_name}} {{$payment->user?->last_name}}
										@if(auth()->user()->hasRole('admin'))
											<span style="float: right;">
												@if($payment->locked)
													<span class="glyphicon glyphicon-lock"></span>
												@endif
												<a class="btn btn-primary btn-xs" href="{{route('backend.payments.edit', ['payment' => $payment])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
											</span>
										@else
											@if($payment->locked)
												<span class="glyphicon glyphicon-lock" style="float: right;"></span>
											@else
												<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.payments.edit', ['payment' => $payment])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
											@endif
										@endif
									</td>
								</tr>
								@empty
								<tr>
									<td>Nema rezultata</td>
								</tr>
								@endforelse
						    </tbody>
				  		</table>
				  		{{ $payments->appends(Request::all())->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection