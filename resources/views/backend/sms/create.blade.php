@extends('backend.layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.sms_numbers.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Brojevi</a>
				<h3>Dodaj Broj</h3>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="user-form" 
					method="POST" 
					action="{{ route('backend.sms_numbers.store') }}">
					{{ csrf_field() }}

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						  			<div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
									    <label for="number">Broj</label>
							    		<div class="input-group">
									    	<span class="input-group-addon" id="phone-number">+387</span>
									    	<input class="form-control" 
									    		type="number" 
									    		id="number" 
									    		name="number" 
									    		placeholder="61621622" 
									    		aria-describedby="phone-number"
									    		value="{{empty($user) ? old('number') : $user->number }}">
									  	</div>
								    	@if($errors->has('number'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('number') }}</strong>
		                                    </span>
		                                @endif
						  			</div>
						  		</div>
							</div>
						</div>
					</div>
				</form>
				<hr>
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('user-form').submit();">
			  		Kreiraj
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection