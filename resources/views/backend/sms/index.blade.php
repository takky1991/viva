@extends('backend.layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.sms_numbers.create')}}">Novi broj</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-body">
					<label>Ukupno brojeva: {{$smsNumbers->count()}}</label>
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Broj</th>
						      	</tr>
						    </thead>
						    <tbody>
								@forelse($smsNumbers as $smsNumber)
						    	<tr>
									<td>
										{{$smsNumber->number}}
										<button
											style="float: right;" 
											class="btn btn-danger btn-xs"
						                    onclick="if (confirm('Da li zaista želite obrisati broj?')) {
						                    			event.preventDefault();
						                             	document.getElementById('delete-number-form-{{$smsNumber->id}}').submit();
											}">
						                    Obriši
						                </button>

						                <form id="delete-number-form-{{$smsNumber->id}}" 
						                	action="{{ route('backend.sms_numbers.destroy', ['smsNumber' => $smsNumber]) }}" 
						                	method="POST" 
						                	style="display: none;">
						                	<input type="hidden" name="_method" value="DELETE">
						                    {{ csrf_field() }}
						                </form>
									</td>
								</tr>
								@empty
								<tr>
									<td>Nema rezultata</td>
								</tr>
								@endforelse
						    </tbody>
				  		</table>
				  		{{ $smsNumbers->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection