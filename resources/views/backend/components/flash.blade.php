@foreach(session('flash_notification', collect())->toArray() as $flash_message)
	<div class="alert alert-{{ $flash_message['type'] }}">
        @if(isset($flash_message['title']))
        	<strong>{{ $flash_message['title'] }}</strong> 
        @endif 
       {{ $flash_message['message'] }}
        @if(isset($flash_message['btnText']))
        	<a class="btn btn-primary" href="{{ $flash_message['btnLink'] }}">{{ $flash_message['btnText'] }}</a>
        @endif
    </div>
@endforeach

{{ session()->forget('flash_notification') }}