@extends('backend.layouts.main')

@section('main-content')
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="{{route('backend.home')}}">
                <img src="{{asset('images/logos/horizontal-logo.svg')}}">
            </a>
        </div>

        <ul class="list-unstyled components">
            <li>
                @include('backend/layouts/partials/user-menu')
            </li>
            <li class="{{request()->route()->getName() == 'backend.shop.home' ? 'active' : ''}}">
                <a href="{{route('backend.shop.home')}}">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Početna
                </a>
            </li>

            @can('index', Viva\Order::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.orders.index' ||
                    request()->route()->getName() == 'backend.shop.orders.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.orders.index')}}">
                        <i class="fa fa-truck" aria-hidden="true"></i> Narudžbe
                    </a>
                </li>
            @endcan

            @can('index', Viva\HeroArticle::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.hero-articles.index' ||
                    request()->route()->getName() == 'backend.shop.hero-articles.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.hero_articles.index')}}">
                        <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Hero članak
                    </a>
                </li>
            @endcan

            @can('index', Viva\Article::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.articles.index' ||
                    request()->route()->getName() == 'backend.shop.articles.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.articles.index')}}">
                        <i class="fa fa-file-o" aria-hidden="true"></i> Blog
                    </a>
                </li>
            @endcan

            @can('index', Viva\Product::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.products.index' ||
                    request()->route()->getName() == 'backend.shop.products.create' ||
                    request()->route()->getName() == 'backend.shop.products.show' ||
                    request()->route()->getName() == 'backend.shop.products.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.products.index')}}">
                        <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Proizvodi
                    </a>
                </li>
            @endcan

            @can('index', Viva\Review::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.reviews.index' ||
                    request()->route()->getName() == 'backend.shop.reviews.show') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.reviews.index')}}">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Recenzije
                    </a>
                </li>
            @endcan

            @can('index', Viva\DiscountCollection::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.discount_collections.index' ||
                    request()->route()->getName() == 'backend.shop.discount_collections.create' ||
                    request()->route()->getName() == 'backend.shop.discount_collections.show' ||
                    request()->route()->getName() == 'backend.shop.discount_collections.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.discount_collections.index')}}">
                        <i class="fa fa-percent" aria-hidden="true"></i> Popusti
                    </a>
                </li>
            @endcan

            @can('index', Viva\Category::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.categories.index' ||
                    request()->route()->getName() == 'backend.shop.categories.create' ||
                    request()->route()->getName() == 'backend.shop.categories.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.categories.index')}}">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Kategorije
                    </a>
                </li>
            @endcan

            @can('index', Viva\Brand::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.shop.brands.index' ||
                    request()->route()->getName() == 'backend.shop.brands.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.shop.brands.index')}}">
                        <span class="glyphicon glyphicon-certificate" aria-hidden="true"></span> Brendovi
                    </a>
                </li>
            @endcan
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" id="sidebarCollapse" class="navbar-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="{{route('home')}}" target="_blank">Shop</a></li>
                </ul>
            </div>
        </nav>

        @include('backend/components/flash')

        @if(sizeof($errors))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <label>{{$error}}</label> <br>
                @endforeach
            </div>
        @endisset
        
        @yield('content')
    </div>
@endsection