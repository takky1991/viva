<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Dobrodošli, {{auth()->user()->first_name}}</a>
<ul class="collapse list-unstyled" id="homeSubmenu">
    <li class="{{request()->route()->getName() == 'backend.home' ? 'active' : ''}}"><a href="{{ route('backend.home') }}">Administracija</a></li>
    <li class="{{request()->route()->getName() == 'backend.shop.home' ? 'active' : ''}}"><a href="{{ route('backend.shop.home') }}">Shop</a></li>
    <li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>