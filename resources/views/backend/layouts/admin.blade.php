@extends('backend.layouts.main')

@section('main-content')
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="{{route('backend.home')}}">
                <img src="{{asset('images/logos/horizontal-logo.svg')}}">
            </a>
        </div>

        <ul class="list-unstyled components">
            <li>
                @include('backend/layouts/partials/user-menu')
            </li>
            <li class="{{request()->route()->getName() == 'backend.home' ? 'active' : ''}}">
                <a href="{{route('backend.home')}}">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span> Početna
                </a>
            </li>

            @can('index', Viva\User::class)
                <li class="{{
                    (request()->route()->getName() == 'backend.users.index' ||
                    request()->route()->getName() == 'backend.users.create' ||
                    request()->route()->getName() == 'backend.users.edit') ? 'active' : ''}}">
                    <a href="{{route('backend.users.index')}}">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Korisnici
                    </a>
                </li>
            @endcan

            @can('index', Viva\Payment::class)
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="{{(
                        request()->route()->getName() == 'backend.payments.index' ||
                        request()->route()->getName() == 'backend.payments.create' ||
                        request()->route()->getName() == 'backend.payments.edit' ||
                        request()->route()->getName() == 'backend.suppliers.index' ||
                        request()->route()->getName() == 'backend.suppliers.create' ||
                        request()->route()->getName() == 'backend.suppliers.edit'
                    ) ? 'true' : 'false'}}">
                        <span class="glyphicon glyphicon-duplicate"></span> Plaćanja
                    </a>
                    <ul class="collapse list-unstyled {{(
                        request()->route()->getName() == 'backend.payments.index' ||
                        request()->route()->getName() == 'backend.payments.create' ||
                        request()->route()->getName() == 'backend.payments.edit' ||
                        request()->route()->getName() == 'backend.suppliers.index' ||
                        request()->route()->getName() == 'backend.suppliers.create' ||
                        request()->route()->getName() == 'backend.suppliers.edit'
                    ) ? 'in' : ''}}" id="pageSubmenu">
                        <li class="{{
                            (request()->route()->getName() == 'backend.payments.index' ||
                            request()->route()->getName() == 'backend.payments.create' ||
                            request()->route()->getName() == 'backend.payments.suppliers' ||
                            request()->route()->getName() == 'backend.payments.edit') ? 'active' : ''}}">
                            <a href="{{route('backend.payments.suppliers')}}">Fakture</a>
                        </li>

                        @can('index', Viva\Supplier::class)
                            <li class="{{
                                (request()->route()->getName() == 'backend.suppliers.index' ||
                                request()->route()->getName() == 'backend.suppliers.create' ||
                                request()->route()->getName() == 'backend.suppliers.edit') ? 'active' : ''}}">
                                <a href="{{route('backend.suppliers.index')}}">Dobavljači</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('index', Viva\SmsNumber::class)
                <li class="{{(
                    request()->route()->getName() == 'backend.sms_numbers.index' || 
                    request()->route()->getName() == 'backend.sms_numbers.create') ? 'active' : ''}}">
                    <a href="{{route('backend.sms_numbers.index')}}">
                        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span> SMS
                    </a>
                </li>
            @endcan
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" id="sidebarCollapse" class="navbar-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="{{route('home')}}" target="_blank">Shop</a></li>
                </ul>
            </div>
        </nav>

        @include('backend/components/flash')
        
        @yield('content')
    </div>
@endsection