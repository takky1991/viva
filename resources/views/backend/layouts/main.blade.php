<!DOCTYPE html>
<html lang="bs-BA">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/admin/backend.css') }}" rel="stylesheet">
    @stack('styles')

    <script type="text/javascript">
        window.Laravel = @php 
            echo json_encode([
                'authenticated' => auth()->check(),
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/')
            ]);
        @endphp
    </script>
</head>
<body>
    <div id="backend">
        <div class="wrapper">
            @yield('main-content')
        </div>
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ mix('js/admin/manifest.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/admin/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/admin/backend.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
             $('#sidebarCollapse').on('click', function () {
                 $('#sidebar').toggleClass('active');
                 $(this).toggleClass('active');
             });
         });
     </script>
</body>
</html>
