@extends('backend.layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.suppliers.create')}}">Novi Dobavljač</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Naziv</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@forelse($suppliers as $supplier)
						    	<tr>
									<td>
										<a href="{{route('backend.suppliers.edit', ['supplier' => $supplier])}}">
											{{$supplier->name}}
										</a>
										<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.suppliers.edit', ['supplier' => $supplier])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
									</td>
								</tr>
								@empty
								<tr>
									<td>Nema rezultata</td>
								</tr>
								@endforelse
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection