@extends('backend.layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.suppliers.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Dobavljači</a>
				@if(empty($supplier))
					<h3>Dodaj Dobavljača</h3>
				@else
					<h3>Uredi Dobavljača {{empty($supplier) ? '' : '- ' . $supplier->name}}</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="supplier-form" 
					method="POST" 
					action="{{ empty($supplier) ? route('backend.suppliers.store') : route('backend.suppliers.update', ['supplier' => $supplier])}}">
					{{ csrf_field() }}

					@if(!empty($supplier))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
								    	<label for="name">Naziv</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="name" 
								    		name="name" 
								    		placeholder="Ime dobavljača" 
								    		value="{{empty($supplier) ? old('name') : $supplier->name }}">
								    	@if($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						</div>
					</div>
				</form>
				<hr>
			  	@if(!empty($supplier))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati dobavljača?')) {	                    	
	                    		event.preventDefault();
	                            document.getElementById('delete-supplier-form').submit();
	                }">
	                    Obriši
	                </button>

	                <form id="delete-supplier-form" 
	                	action="{{ route('backend.suppliers.destroy', ['supplier' => $supplier]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('supplier-form').submit();">
			  		{{empty($supplier) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection