@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.articles.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi blog članci</a>
				@if(empty($article))
					<h3>Dodaj blog članak</h3>
				@else
					<h3>Uredi blog članak {{empty($article) ? '' : '- ' . $article->title}}</h3>
					<a href="{{route('backend.shop.articles.preview', ['article' => $article])}}" target="_blank" class="btn btn-primary">Pregled</a>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="article-form" 
					method="POST" 
					action="{{ empty($article) ? route('backend.shop.articles.store') : route('backend.shop.articles.update', ['article' => $article])}}">
					{{ csrf_field() }}

					@if(!empty($article))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
								    	<label for="title">Naslov</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="title" 
								    		name="title" 
								    		value="{{empty($article) ? old('title') : $article->title }}">
								    	@if($errors->has('title'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('title') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

									<div class="form-group {{ $errors->has('subtitle') ? ' has-error' : '' }}">
								    	<label for="subtitle">Podnaslov</label>
										<textarea class="form-control" name="subtitle" id="subtitle" cols="30" rows="10">{{empty($article) ? old('subtitle') : $article->subtitle}}</textarea>
								    	@if($errors->has('subtitle'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('subtitle') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						</div>

						@if(!empty($article))
							<div class="col-xs-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
											<label for="content">Sadržaj</label>
											<redactor 
												id="description" 
												name="content" 
												min-height="300px"
												value="{{ !empty($article) ? $article->content : old('content') }}" 
											></redactor>
											@if($errors->has('content'))
												<span class="help-block">
													<strong>{{ $errors->first('content') }}</strong>
												</span>
											@endif
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<h4>Slika (1600 x 1000)</h4>
										<upload-resource-photos 
											target="article" 
											:target-id="{{$article->id}}"
											@if($article->photo) 
												:initial-photos="{{$article->getPhotosJson()}}" 
											@endif
										></upload-resource-photos>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="form-group {{ $errors->has('product_ids') ? ' has-error' : '' }}">
											<label for="product_ids">Vezani proizvodi</label>
											<html-multiselect-products 
												@if(isset($relatedProducts) && $relatedProducts->isNotEmpty())
													:initial-products="{{json_encode($relatedProducts)}}"
												@endif
											></html-multiselect-products>
											@if($errors->has('product_ids'))
												<span class="help-block">
													<strong>{{ $errors->first('product_ids') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('brand_ids') ? ' has-error' : '' }}">
											<label for="brand_ids">Vezani brendovi</label>
											<html-multiselect-brands
												@if(isset($relatedBrands) && $relatedBrands->isNotEmpty())
													:initial-brands="{{json_encode($relatedBrands)}}"
												@endif
											></html-multiselect-brands>
											@if($errors->has('brand_ids'))
												<span class="help-block">
													<strong>{{ $errors->first('brand_ids') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('category_ids') ? ' has-error' : '' }}">
											<label for="category_ids">Vezane kategorije</label>
											<html-multiselect-categories
												@if(isset($relatedCategories) && $relatedCategories->isNotEmpty())
													:initial-categories="{{json_encode($relatedCategories)}}"
												@endif
											></html-multiselect-categories>
											@if($errors->has('category_ids'))
												<span class="help-block">
													<strong>{{ $errors->first('category_ids') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('published') ? ' has-error' : '' }}" style="display: inline-block;margin-right: 50px;">
											<label for="published">Objavljeno</label>
											<div>
												<label class="switch">
													<input type="hidden" name="published" value="0">
													<input type="checkbox" 
														id="published" 
														name="published"
														value="1" 
														{{empty($article) ? (old('published') ? 'checked' : '') : ($article->published ? 'checked' : '')}}>
													<span class="slider"></span>
												</label>
											</div>
											@if($errors->has('published'))
												<span class="help-block">
													<strong>{{ $errors->first('published') }}</strong>
												</span>
											@endif
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
				</form>
				<hr>
                @if(!empty($article))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati blog članak?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-article-form').submit();
						}">
	                    Obriši
	                </button>

	                <form id="delete-article-form" 
	                	action="{{ route('backend.shop.articles.destroy', ['article' => $article]) }}" 
	                	method="POST" 
	                	style="display:none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('article-form').submit();">
			  		{{empty($article) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection