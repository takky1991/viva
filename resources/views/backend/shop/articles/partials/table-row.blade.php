<tr>
	<td>
		<a href="{{route('backend.shop.articles.edit', ['article' => $article])}}">
			{{$article->title}}
		</a>
	</td>
    <td>
        @if($article->published)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
        <a class="btn btn-primary btn-xs" style="float:right;" href="{{route('backend.shop.articles.edit', ['article' => $article])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
    </td>
</tr>