@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.shop.articles.create')}}">Novi blog članak</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Naslov</th>
						        	<th>Objavljeno</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/articles/partials/table-row', $articles, 'article', 'backend/shop/articles/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection