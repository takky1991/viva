@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.shop.discount_collections.create')}}">Novi Popust</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>									
						        	<th>Naziv</th>
						        	<th>Popust</th>
						        	<th>Početak</th>
						        	<th>Kraj</th>
						        	<th>Aktivno</th>
						      	</tr>
						    </thead>
						    <tbody>
								@each('backend/shop/discount-collections/partials/table-row', $discountCollections, 'discountCollection', 'backend/shop/discount-collections/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection