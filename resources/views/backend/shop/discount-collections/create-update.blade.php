@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.discount_collections.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Popusti</a>
				@if(empty($discountCollection))
					<h3>Dodaj Popust</h3>
				@else
					<h3>Uredi Popust {{empty($discountCollection) ? '' : '- ' . $discountCollection->name}}</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				@if(!empty($discountCollection))
					<button class="btn btn-danger btn-lg"
						onclick="if (confirm('Da li zaista želite obrisati popust? Ukoliko je popust aktivan u trenutku brisanja, svim proizvodima ce biti uklonjen popust.')) {
									event.preventDefault();
									document.getElementById('delete-discount-collection-form').submit();
						}">
						Obriši
					</button>
				@endif
				<button class="btn btn-success btn-lg" 
					type="submit"
					onclick="event.preventDefault();
							document.getElementById('discount-collection-form').submit();">
					{{empty($discountCollection) ? 'Kreiraj' : 'Sačuvaj'}}
				</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="discount-collection-form" 
					method="POST" 
					action="{{ empty($discountCollection) ? route('backend.shop.discount_collections.store') : route('backend.shop.discount_collections.update', ['discountCollection' => $discountCollection])}}">
					{{ csrf_field() }}

					@if(!empty($discountCollection))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
								    	<label for="name">Naziv</label>
										<span class="help-block">
											<i class="fa fa-info-circle" aria-hidden="true"></i> Ne koristiti sva velika slova i uvijek dodati na kraj vrijednost popusta u procentima!
										</span>
								    	<input class="form-control" 
								    		type="text" 
								    		id="name" 
								    		name="name" 
								    		placeholder="Npr. Avene Sun -20%" 
								    		value="{{empty($discountCollection) ? old('name') : $discountCollection->name }}">
								    	@if($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
                                    <div class="form-check {{ $errors->has('discount_percentage') ? ' has-error' : '' }}">
                                        <label for="name">Popust:</label>
                                        <input type="radio" class="form-check-input" id="discount_5" name="discount_percentage" value="5" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '5')checked @endif>
                                        <label class="form-check-label" for="discount_5">5%</label>

                                        <input type="radio" class="form-check-input" id="discount_10" name="discount_percentage" value="10" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '10')checked @endif>
                                        <label class="form-check-label" for="discount_10">10%</label>

                                        <input type="radio" class="form-check-input" id="discount_15" name="discount_percentage" value="15" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '15')checked @endif>
                                        <label class="form-check-label" for="discount_15">15%</label>

                                        <input type="radio" class="form-check-input" id="discount_20" name="discount_percentage" value="20" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '20')checked @endif>
                                        <label class="form-check-label" for="discount_20">20%</label>

                                        <input type="radio" class="form-check-input" id="discount_25" name="discount_percentage" value="25" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '25')checked @endif>
                                        <label class="form-check-label" for="discount_25">25%</label>

										<input type="radio" class="form-check-input" id="discount_30" name="discount_percentage" value="30" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '30')checked @endif>
                                        <label class="form-check-label" for="discount_30">30%</label>

										<input type="radio" class="form-check-input" id="discount_35" name="discount_percentage" value="35" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '35')checked @endif>
                                        <label class="form-check-label" for="discount_35">35%</label>

										<input type="radio" class="form-check-input" id="discount_40" name="discount_percentage" value="40" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '40')checked @endif>
                                        <label class="form-check-label" for="discount_40">40%</label>

										<input type="radio" class="form-check-input" id="discount_45" name="discount_percentage" value="45" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '45')checked @endif>
                                        <label class="form-check-label" for="discount_45">45%</label>

										<input type="radio" class="form-check-input" id="discount_50" name="discount_percentage" value="50" @if(!empty($discountCollection) && $discountCollection->discount_percentage == '50')checked @endif>
                                        <label class="form-check-label" for="discount_50">50%</label>

                                        @if($errors->has('discount_percentage'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('discount_percentage') }}</strong>
		                                    </span>
		                                @endif
                                    </div>
						  		</div>
							</div>
						</div>

                        @if(!empty($discountCollection))
                            <div class="col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <discount-products-selector 
											:initial-discount-collection="{{$discountCollection->toSearchableJson()}}"
											@if(!empty($selectedProducts))
												:initial-selected-products="{{json_encode($selectedProducts)}}"
											@endif
										></discount-products-selector>
                                    </div>
                                </div>
                            </div>
                        @endif
					</div>
				</form>
				<hr>
			  	@if(!empty($discountCollection))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati popust? Ukoliko je popust aktivan u trenutku brisanja, svim proizvodima ce biti uklonjen popust.')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-discount-collection-form').submit();
						}">
	                    Obriši
	                </button>

	                <form id="delete-discount-collection-form" 
	                	action="{{ route('backend.shop.discount_collections.destroy', ['discountCollection' => $discountCollection]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('discount-collection-form').submit();">
			  		{{empty($discountCollection) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection