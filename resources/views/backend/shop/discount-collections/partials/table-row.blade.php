<tr>
	<td>
		<a href="{{route('backend.shop.discount_collections.edit', ['discountCollection' => $discountCollection])}}">
			{{$discountCollection->name}}
		</a>
	</td>
	<td>
		{{$discountCollection->discount_percentage}}%
	</td>
	<td>{{$discountCollection->start_at ? $discountCollection->start_at->format('d.M.Y H:i') : '-'}}</td>
	<td>{{$discountCollection->end_at ? $discountCollection->end_at->format('d.M.Y H:i') : '-'}}</td>
	<td>
		@if($discountCollection->active)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
		<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.discount_collections.edit', ['discountCollection' => $discountCollection])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
	</td>
</tr>