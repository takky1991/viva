@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<form class="form-inline" action="{{route('backend.shop.orders.search')}}" method="POST">
				@csrf()

				<div class="index-filter">
					<div class="form-group">
						<input 
							class="form-control" 
							type="input" 
							name="sq" 
							placeholder="Ime, prezime, br. narudžbe"
							@if(request()->filled('sq')) value="{{request()->sq}}" @endif
						>
					</div>
					<div class="form-group">
						<select class="form-control" name="status">
							<option value="">Status narudžbe</option>
							@foreach (\Viva\Order::$statusWording as $key => $label)
								<option value="{{$key}}" {{request()->get('status') == $key ? 'selected' : ''}}>{{$label}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" name="cardPaymentStatus">
							<option value="">Status kartičnog plaćanja</option>
							<option value="all" {{request()->get('cardPaymentStatus') == 'all' ? 'selected' : ''}}>Sve</option>
							@foreach (\Viva\Order::$cardPaymentStatuseWording as $key => $label)
								<option value="{{$key}}" {{request()->get('cardPaymentStatus') == $key ? 'selected' : ''}}>{{$label}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
				    	<select class="form-control" id="shipping_method_id" name="shippingMethodId">
						  	<option value="">Dostava</option>
							@foreach ($shippingMethods as $shippingMethod)
						  		<option value="{{$shippingMethod->id}}" {{Request::get('shippingMethodId') == $shippingMethod->id ? 'selected' : ''}}>{{$shippingMethod->name}}</option>
							@endforeach
						</select>
				  	</div>
					<div class="form-group">
						<datepicker input-class="form-control" 
							id="orders_from_filter" 
							name="ordersFromDate"
							language="bs"
							format="dd-MM-yyyy"
							placeholder="Datum od"
							value="{{!empty(request()->get('ordersFromDate')) ? \Carbon\Carbon::parse(request()->get('ordersFromDate'))->format('m-d-Y') : ''}}">
						</datepicker>
					</div>
					<div class="form-group">
						<datepicker input-class="form-control" 
							id="orders_to_filter" 
							name="ordersToDate"
							language="bs"
							format="dd-MM-yyyy"
							placeholder="Datum do"
							value="{{!empty(request()->get('ordersToDate')) ? \Carbon\Carbon::parse(request()->get('ordersToDate'))->format('m-d-Y') : ''}}">
						</datepicker>
					</div>
					<button class="btn btn-primary btn-lg">Traži</button>
				</div>
			</form>
		</div>
	</div>
	<a href="{{route('backend.shop.orders.index')}}">Resetuj filter</a>
	<br>
	<br>
	<p>
		<b>Narudžbe: {{$orders->total()}}</b> <br>
		<b>Vrijednost: {{formatPrice($totalPrice)}}</b> <br>
		<b>Prosječna vrijednost: {{$orders->total() ? formatPrice($totalPrice / $orders->total()) : '0.00KM'}}</b> <br>
		<b>Vrijednost poštarina: {{formatPrice($totalShippingPrice)}}</b>
	</p>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Br. narudžbe</th>
						        	<th>Ime i prezime</th>
						        	<th>Način preuzimanja</th>
						        	<th>Način plaćanja</th>
						        	<th>Status</th>
                                    <th>Vrijeme</th>
                                    <th>Vrijednost (poštarina)</th>
                                    <th></th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/orders/partials/table-row', $orders, 'order', 'backend/shop/orders/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
			{{ $orders->appends(request()->all())->links() }}
		</div>
	</div>
@endsection