
<!DOCTYPE html>
<html lang="bs-BA">
    <head>
        <meta charset="utf-8">
        <title>Račun {{$order->order_number}}</title>
        <style>
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            a {
                color: #5D6975;
                text-decoration: underline;
            }

            body {
                position: relative;
                margin: 0 auto; 
                color: #001028;
                background: #FFFFFF; 
                font-size: 12px;
                font-family: dejavu sans;
            }

            header {
                margin-bottom: 30px;
            }

            #header {
                margin-bottom: 30px;
            }

            #header img {
                width: 200px;
            }

            h1 {
                color: #5D6975;
                font-size: 2.4em;
                line-height: 1.4em;
                font-weight: normal;
                margin: 0 0 20px 0;
            }

            #customer {
                float: left;
            }

            #customer span {
                color: #5D6975;
                text-align: right;
                width: 52px;
                margin-right: 10px;
                display: inline-block;
            }

            #company {
                float: right;
                text-align: right;
            }

            #shipping {
                float: right;
                text-align: right;
            }

            #customer div,
            #company div {
                white-space: nowrap;        
            }

            table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
            }

            table th,
            table td {
                text-align: right;
            }

            table th {
                padding: 5px 0px;
                color: #5D6975;
                border-bottom: 1px solid #C1CED9;
                white-space: nowrap;        
                font-weight: normal;
            }

            table .product,
            table .desc {
                text-align: left;
                max-width: 250px;
            }

            table td {
                padding: 10px 0;
            }

            table td.product,
            table td.desc {
                vertical-align: top;
            }

            table td.grand {
                border-top: 1px solid #C1CED9;
                font-weight: bold;
            }

            #notices .notice {
                color: #5D6975;
            }

            footer {
                color: #5D6975;
                width: 100%;
                height: 30px;
                position: absolute;
                bottom: 0;
                border-top: 1px solid #C1CED9;
                padding: 8px 0;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <header class="clearfix">
            <div id="header">
                <div style="display: inline-block;">
                    <img src="{{asset('/images/logos/horizontal-logo-black-400.png')}}" alt="{{config('app.name')}}">
                    <h1>Račun {{$order->order_number}}</h1>
                </div>
                <div id="company" class="clearfix">
                    <div style="font-weight: bold">{{config('app.domain')}}</div>
                    <div>PZU Apoteka Viva</div>
                    <div>Mala Lisa bb<br /> Cazin 77220, BIH</div>
                    <div>037 536 051</div>
                    <div><a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a></div>
                </div>
            </div>
            <div id="shipping">
                <div style="font-weight: bold">Način preuzimanja:</div>
                @if($order->shipping_pickup) 
                    <div>Preuzimanje u {{$order->shipping_name}}</div>
                @else 
                    <div>Slanje na adresu <br> {{$order->shipping_name}}</div>
                @endif
                <br>
                <div style="font-weight: bold">Način plaćanja:</div>
                @if($order->payment_method == 'online_payment')
                    Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif<br>
                @else
                    Pouzećem @if($order->shipping_pickup){{'(gotovina, kartično)'}}@else{{'(gotovina)'}}@endif<br>
                @endif
                <br>
                <div style="font-weight: bold">Broj narudžbe:</div>
                <div>{{$order->order_number}}</div>
                <br>
                <div>{{$order->created_at->format('d.m.Y H:i')}}</div>
            </div>
            <div id="customer">
                <div style="font-weight: bold">Podaci o kupcu:</div>
                <div>{{$order->first_name}} {{$order->last_name}}</div>
                <div>{{$order->street_address}}</div>
                <div>{{$order->city}} {{$order->postal_code}}, BIH</div>
                <div>{{$order->phone}}</div>
                <div><a href="mailto:{{$order->email}}">{{$order->email}}</a></div>
            </div>
        </header>
        <main>
            <table>
                <thead>
                    <tr>
                        <th class="product">PROIZVOD</th>
                        <th>CIJENA</th>
                        <th>KOLIČINA</th>
                        <th class="total">UKUPNO</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->orderProducts as $orderProduct)
                        <tr>
                            <td class="product">
                                {{$orderProduct->fullName()}}
                                @if($orderProduct->variants)
                                    <br>
                                    Br. {{$orderProduct->variants}}
                                @endif
                            </td>
                            <td class="unit">{{formatPrice($orderProduct->realPrice())}}</td>
                            <td class="qty">{{$orderProduct->quantity}}</td>
                            <td class="total">{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3">PROIZVODI</td>
                        <td class="total">{{formatPrice($order->total_price)}}</td>
                    </tr>
                    <tr>
                        <td colspan="3">DOSTAVA</td>
                        <td class="total">{{formatPrice($order->shipping_price)}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="grand total">UKUPNO</td>
                        <td class="grand total">{{formatPrice($order->total_price_with_shipping)}}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <div>Hvala što kupujete na {{config('app.domain')}} :)</div>
        </main>
        <footer>
            Račun je kreiran na računaru i važi bez potpisa i pečata.
        </footer>
    </body>
</html>