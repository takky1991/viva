<tr>
	<td>
		{{$order->order_number}}
	</td>
    <td>
        <a href="{{route('backend.shop.orders.index', ['email' => $order->email])}}" style="color: #337ab7">
            {{$order->first_name}} {{$order->last_name}} <br>
            {{$order->email}} ({{\Viva\Order::where('email', $order->email)->count()}})
        </a>
	</td>
	<td>
		<strong>{{$order->shipping_pickup ? 'Preuzimanje' : 'Dostava'}}</strong><br> 
        {{$order->shipping_name}}
	</td>
    <td>
        @if($order->payment_method == 'online_payment')
            Kartično ({{$order->card_type}}) <i class="fa fa-credit-card" aria-hidden="true"></i> <br>
            @switch($order->card_payment_status)
                @case('authorized')
                    <span class="label label-warning">{{\Viva\Order::$cardPaymentStatuseWording[$order->card_payment_status]}}</span>
                @break
                @case('voided')
                    <span class="label label-default">{{\Viva\Order::$cardPaymentStatuseWording[$order->card_payment_status]}}</span>
                @break
                @case('captured')
                    <span class="label label-primary">{{\Viva\Order::$cardPaymentStatuseWording[$order->card_payment_status]}}</span>
                @break
                @case('refund')
                    <span class="label label-danger">{{\Viva\Order::$cardPaymentStatuseWording[$order->card_payment_status]}}</span>
                @break
                @case('processed')
                    <span class="label label-success">{{\Viva\Order::$cardPaymentStatuseWording[$order->card_payment_status]}}</span>
                @break
            @endswitch
        @else
            Pouzećem
        @endif
	</td>
	<td>
        @switch($order->status)
            @case('created')
                <span class="label label-warning">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('ready_for_pickup')
                <span class="label label-info">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('ready_for_sending')
                <span class="label label-info">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('shipped')
                <span class="label label-primary">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('fulfilled')
                <span class="label label-success">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('canceled')
                <span class="label label-default">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('returned')
                <span class="label label-danger">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
            @case('processing_payment')
                <span class="label label-info">{{\Viva\Order::$statusWording[$order->status]}}</span>
                @break
        @endswitch

        @if($order->comment)
            <i class="fa fa-comment-o" aria-hidden="true"></i>
        @endif
    </td>
    <td>
		{{$order->created_at->format('d.m.Y H:i')}}
	</td>
    <td>
        {{formatPrice($order->total_price)}} ({{formatPrice($order->shipping_price)}})
    </td>
	<td>
		<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.orders.edit', ['order' => $order])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
    </td>
</tr>