@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.orders.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Sve Narudžbe</a>
				<h3>Narudžba {{$order->order_number}} <a class="btn btn-primary" href="{{route('backend.shop.orders.invoice', ['order' => $order])}}" target="_blank">Račun</a></h3>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="order-form" 
					method="POST" 
					action="{{route('backend.shop.orders.update', ['order' => $order])}}">
					{{ csrf_field() }}

					<input type="hidden" name="_method" value="PUT">

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>Način preuzimanja:</b> <br> 
                                            @if($order->shipping_pickup) 
                                                Preuzimanje u {{$order->shipping_name}} 
                                            @else 
                                                Slanje na adresu <br> {{$order->shipping_name}}
                                            @endif <br>
                                            <br>
                                            <b>Način plaćanja:</b> <br> 
                                            @if($order->payment_method == 'online_payment')
                                                Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif   
                                            @else
                                                Pouzećem
                                            @endif<br>
                                            <br>
                                            <b>Vrijeme narudžbe:</b> <br> 
                                            {{$order->created_at->format('d.m.Y. H:i')}}
                                            <br>
                                            <br>
                                            @if($order->shipping_pickup) 
                                                <b>Rok za preuzimanje:</b> <br> 
                                                {{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}
                                                <br>
                                                <br>
                                            @endif
                                            <b>Broj narudžbe:</b> <br> 
                                            {{$order->order_number}}
                                        </div>
                                        <div class="col-md-6">
                                            <b>Podaci o kupcu:</b> <br>
                                            {{$order->first_name}} {{$order->last_name}} <br>
                                            @if(!$order->shipping_pickup)
                                                {{$order->street_address}} <br>
                                                {{$order->postalcode}} {{$order->city}} <br>
                                                {{$order->country->name}} <br>
                                                <br>
                                            @endif

                                            Tel: {{$order->phone}} <br>
                                            Email: {{$order->email}}

                                            @if($order->additional_info)
                                                <br>
                                                <br>
                                                <b>Napomene uz narudžbu:</b> <br>
                                                {{$order->additional_info}}
                                            @endif
                                        </div>
                                    </div>
						  		</div>
							</div>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group {{ $errors->has('comment') ? ' has-error' : '' }}">
                                        <label for="comment">Komentar na narudžbu</label>
                                        <textarea class="form-control" name="comment" id="comment" placeholder="Npr. Narudžba nije poslana jer jedan od proizvoda nije na stanju.">{{$order->comment}}</textarea>
                                        @if($errors->has('comment'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('comment') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
						  		<div class="panel-body">
                                    <h4>Status narudžbe</h4>
                                    <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                        <select class="form-control" id="status" name="status">
                                            @foreach($order->shipping_pickup ? \Viva\Order::$pickupStatuses : \Viva\Order::$shippingStatuses as $item)
                                                <option value="{{$item}}" {{($order->status == $item ? 'selected' : '')}}>
                                                    {{Viva\Order::$statusWording[$item]}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('status'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                        @endif
                                        * Aktiviranjem stanja `Spremno za preuzimanje` ili `Narudžba poslana` kupac ce biti obavješten putem emaila.
                                    </div>

                                    @if($order->payment_method == 'online_payment')
                                        <br>
                                        <h4>Status kartičnog plaćanja</h4>
                                        <div class="form-group {{ $errors->has('card_payment_status') ? ' has-error' : '' }}">
                                            <select class="form-control" id="card_payment_status" name="card_payment_status">
                                                @foreach(\Viva\Order::$cardPaymentStatuses as $item)
                                                    <option value="{{$item}}" {{($order->card_payment_status == $item ? 'selected' : '')}}>
                                                        {{Viva\Order::$cardPaymentStatuseWording[$item]}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('card_payment_status'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('card_payment_status') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    @endif
						  		</div>
						  	</div>
						</div>
                    </div>
                    
                    <div class="row">
						<div class="col-xs-12">
							<div class="panel panel-default">
						  		<div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr>
                                                    <th>Proizvod</th>
                                                    <th></th>
                                                    <th>Količina / Cijena</th>
                                                    <th>Cijena ukupno</th>
                                                </tr>
                                          </thead>
                                            <tbody>
                                                @foreach ($order->orderProducts as $orderProduct)
                                                    <tr>
                                                        <td>
                                                            @if($orderProduct->product && $orderProduct->product->hasPhotos())
                                                                <img src="{{$orderProduct->product->mainPhoto()->url('thumb')}}" alt="{{$orderProduct->fullName()}}" width="50px">
                                                            @else
                                                                <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" width="50px">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">
                                                                {{$orderProduct->fullName()}}
                                                                @if($orderProduct->variants)
                                                                    <br>
                                                                    Br. {{$orderProduct->variants}}
                                                                @endif
                                                            </a>
                                                        </td>
                                                        <td>{{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}}</td>
                                                        <td>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</td>
                                                    </tr>
                                                @endforeach
                                              </tbody>
                                        </table>

                                        Proizvodi: {{formatPrice($order->total_price)}} <br>
                                        Dostava: {{formatPrice($order->shipping_price)}} <br>
                                        <h4>Ukupno: {{formatPrice($order->total_price_with_shipping)}}</h4>
                                    </div>
						  		</div>
							</div>
						</div>
					</div>
				</form>
				<hr>
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('order-form').submit();">
			  		Sačuvaj
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection