@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<form class="form-inline" action="{{route('backend.shop.reviews.search')}}" method="POST">
				{{ csrf_field() }}
				<div class="index-filter">
					<div class="form-group">
						<select class="form-control" id="published" name="published">
							<option value="">Objavljeno i neobjavljeno</option>
							<option value="true" {{Request::get('published') == 'true' ? 'selected' : ''}}>Objavljeno</option>
							<option value="false" {{Request::get('published') == 'false' ? 'selected' : ''}}>Nije objavljeni</option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary btn-lg">Prikaži</button>
				</div>
			</form>
		</div>
	</div>
	<br>
	<p><b>Recenzije: {{$reviews->total()}}</b></p>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Autor</th>
						        	<th>Ocjena</th>
						        	<th>Proizvod</th>
						        	<th>Vrijeme</th>
						        	<th>Objavljeno</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/reviews/partials/table-row', $reviews, 'review', 'backend/shop/reviews/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
			{{ $reviews->appends(Request::all())->links() }}
		</div>
	</div>
@endsection