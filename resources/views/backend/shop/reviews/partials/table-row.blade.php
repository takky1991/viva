<tr>
    <td>
		{{$review->name}} @if($review->verified_buyer) <i class="fa fa-check-circle" aria-hidden="true" style="color: #009900"></i> @endif
	</td>
    <td>
		@for($i = 0; $i < $review->rating; $i++)
            <i class="fa fa-star" aria-hidden="true" style="color: #ffc107"></i>
        @endfor
        @for($i = 0; $i < 5 - $review->rating; $i++)
            <i class="fa fa-star-o" aria-hidden="true" style="color: #ffc107"></i>
        @endfor
		({{$review->product->rating_count}})
	</td>
	<td>
		<a href="{{$review->product->url()}}">{{$review->product->fullName()}}</a>
	</td>
    <td>
        {{$review->created_at->format('d.m.Y H:i')}}
    </td>
	<td>
		@if($review->published)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
	    <a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.reviews.show', ['review' => $review])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
	</td>
</tr>