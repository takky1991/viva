@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.reviews.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Sve Recenzije</a>
				<h3>Recenzija za proizvod {{$review->product->fullName()}}</h3>
				@if($review->published)
					<button class="btn btn-warning"
						onclick="if (confirm('Da li zaista želite sakriti recenziju?')) {
							event.preventDefault();
							document.getElementById('publish-review-form').submit();
						}">
						Sakrij
					</button>
				@else
					<button class="btn btn-success"
						onclick="if (confirm('Da li zaista želite objaviti recenziju?')) {
							event.preventDefault();
							document.getElementById('publish-review-form').submit();
						}">
						Objavi
					</button>
				@endif

                <form id="publish-review-form" 
                    action="{{ route('backend.shop.reviews.publish', ['review' => $review]) }}" 
                    method="POST" 
                    style="display: none;">
                    {{ csrf_field() }}
                </form>

				<button class="btn btn-danger"
                    onclick="if (confirm('Da li zaista želite obrisati recenziju?')) {
                        event.preventDefault();
                        document.getElementById('delete-review-form').submit();
                    }">
                    Obriši
                </button>

                <form id="delete-review-form" 
                    action="{{ route('backend.shop.reviews.destroy', ['review' => $review]) }}" 
                    method="POST" 
                    style="display: none;">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field() }}
                </form>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-default">
			  		<div class="panel-body">
			    		<div class="form-group">
					    	<label>Autor</label>
					    	<p>{{$review->name}}</p>
					  	</div>
                        <div class="form-group">
					    	<label>Ocjena</label>
                            <p>
                                @for($i = 0; $i < $review->rating; $i++)
                                    <i class="fa fa-star" aria-hidden="true" style="color: #ffc107"></i>
                                @endfor
                                @for($i = 0; $i < 5 - $review->rating; $i++)
                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #ffc107"></i>
                                @endfor
                            </p>
					  	</div>
					  	<div class="form-group">
					    	<label>Recenzija</label>
					    	<p>{{ $review->content }}</p>
					  	</div>
						<div class="form-group">
					    	<label>Objavljeno</label>
					    	<p>
								@if($review->published)
									<span class="label label-success">DA</span>
								@else
									<span class="label label-danger">NE</span>
								@endif
							</p>
					  	</div>
                        <div class="form-group">
					    	<label>Vrijeme recenzije</label>
					    	<p>{{ $review->created_at->format('d.m.Y H:i') }}</p>
					  	</div>
			  		</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection