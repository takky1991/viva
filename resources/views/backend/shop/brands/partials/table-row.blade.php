<tr>
	<td>
		<a href="{{route('backend.shop.brands.edit', ['brand' => $brand])}}">
			{{$brand->name}}
		</a>
		<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.brands.edit', ['brand' => $brand])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
	</td>
</tr>