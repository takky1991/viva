@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.shop.brands.create')}}">Novi Brend</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Naziv</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/brands/partials/table-row', $brands, 'brand', 'backend/shop/brands/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection