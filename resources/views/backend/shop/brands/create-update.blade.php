@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.brands.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Brendovi</a>
				@if(empty($brand))
					<h3>Dodaj Brend</h3>
				@else
					<h3>Uredi Brend {{empty($brand) ? '' : '- ' . $brand->name}}</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="brand-form" 
					method="POST" 
					action="{{ empty($brand) ? route('backend.shop.brands.store') : route('backend.shop.brands.update', ['brand' => $brand])}}">
					{{ csrf_field() }}

					@if(!empty($brand))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
								    	<label for="name">Naziv</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="name" 
								    		name="name" 
								    		placeholder="Npr Avene" 
								    		value="{{empty($brand) ? old('name') : $brand->name }}">
								    	@if($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
								    	<label for="description">Opis</label>
								    	<textarea class="form-control" 
								    		id="description" 
								    		name="description" 
								    		rows="10">{{empty($brand) ? old('description') : $brand->description}}</textarea>
								    	@if($errors->has('description'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('description') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
						  		<div class="panel-body">
									<h4>Slike</h4>
						  			<label>Images to come</label>
						  		</div>
						  	</div>
						</div>
					</div>
				</form>
				<hr>
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('brand-form').submit();">
			  		{{empty($brand) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection