@extends('backend.layouts.shop')

@section('content')
@push('styles')
    <style type="text/css">
        .lst-kix_p1005g4a4fak-2>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-2
        }

        ol.lst-kix_hdqyu5ur24as-8.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-8 0
        }

        ol.lst-kix_fnhch209rih-0.start {
            counter-reset: lst-ctn-kix_fnhch209rih-0 0
        }

        .lst-kix_gw9li0agsysy-0>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-0, decimal) ". "
        }

        .lst-kix_hpxmc6bbv6ou-8>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-8
        }

        .lst-kix_gw9li0agsysy-1>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-1, lower-latin) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-4.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-4 0
        }

        .lst-kix_gw9li0agsysy-7>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-7, lower-latin) ". "
        }

        .lst-kix_gw9li0agsysy-8>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-8, lower-roman) ". "
        }

        .lst-kix_gw9li0agsysy-3>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-3, decimal) ". "
        }

        .lst-kix_gw9li0agsysy-2>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-2, lower-roman) ". "
        }

        ol.lst-kix_1v082d6dqfqv-4.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-4 0
        }

        ol.lst-kix_koefyqyztmt4-4.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-4 0
        }

        .lst-kix_koefyqyztmt4-5>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-5
        }

        ol.lst-kix_gw9li0agsysy-0.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-0 0
        }

        ol.lst-kix_ioswdliet6ho-4.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-4 0
        }

        .lst-kix_gw9li0agsysy-4>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-4, lower-latin) ". "
        }

        .lst-kix_gw9li0agsysy-5>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-5, lower-roman) ". "
        }

        .lst-kix_gw9li0agsysy-6>li:before {
            content: ""counter(lst-ctn-kix_gw9li0agsysy-6, decimal) ". "
        }

        ol.lst-kix_hdqyu5ur24as-3.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-3 0
        }

        .lst-kix_hpxmc6bbv6ou-4>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-4
        }

        .lst-kix_1v082d6dqfqv-6>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-6
        }

        ol.lst-kix_hpxmc6bbv6ou-0.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-0 0
        }

        ol.lst-kix_hpxmc6bbv6ou-7.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-7 0
        }

        ol.lst-kix_eomrt7z9go42-1.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-1 0
        }

        .lst-kix_k5rolkuor3l-3>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-3
        }

        ol.lst-kix_i8b15sd0stzu-3.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-3 0
        }

        ol.lst-kix_k5rolkuor3l-5.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-5 0
        }

        .lst-kix_ioswdliet6ho-1>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-1, lower-latin) ". "
        }

        .lst-kix_ioswdliet6ho-0>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-0, decimal) ". "
        }

        .lst-kix_gw9li0agsysy-0>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-0
        }

        .lst-kix_p1005g4a4fak-6>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-6
        }

        .lst-kix_ioswdliet6ho-2>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-2, lower-roman) ". "
        }

        .lst-kix_ioswdliet6ho-3>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-3, decimal) ". "
        }

        .lst-kix_fnhch209rih-5>li {
            counter-increment: lst-ctn-kix_fnhch209rih-5
        }

        ol.lst-kix_eomrt7z9go42-8.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-8 0
        }

        .lst-kix_ioswdliet6ho-8>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-8, lower-roman) ". "
        }

        .lst-kix_ioswdliet6ho-6>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-6, decimal) ". "
        }

        .lst-kix_ioswdliet6ho-7>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-7, lower-latin) ". "
        }

        ol.lst-kix_gw9li0agsysy-5.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-5 0
        }

        .lst-kix_ioswdliet6ho-4>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-4, lower-latin) ". "
        }

        .lst-kix_ioswdliet6ho-5>li:before {
            content: ""counter(lst-ctn-kix_ioswdliet6ho-5, lower-roman) ". "
        }

        .lst-kix_qtf3nhqvwlsi-5>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-5
        }

        ol.lst-kix_hdqyu5ur24as-3 {
            list-style-type: none
        }

        .lst-kix_koefyqyztmt4-0>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-0, decimal) ". "
        }

        ol.lst-kix_hdqyu5ur24as-2 {
            list-style-type: none
        }

        .lst-kix_qtf3nhqvwlsi-4>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-4, lower-latin) ". "
        }

        .lst-kix_qtf3nhqvwlsi-6>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-6, decimal) ". "
        }

        ol.lst-kix_hdqyu5ur24as-5 {
            list-style-type: none
        }

        ol.lst-kix_hdqyu5ur24as-4 {
            list-style-type: none
        }

        ol.lst-kix_hdqyu5ur24as-7 {
            list-style-type: none
        }

        .lst-kix_koefyqyztmt4-2>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-2, lower-roman) ". "
        }

        ol.lst-kix_hdqyu5ur24as-6 {
            list-style-type: none
        }

        .lst-kix_gw9li0agsysy-7>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-7
        }

        .lst-kix_1v082d6dqfqv-3>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-3
        }

        ol.lst-kix_hdqyu5ur24as-8 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-5.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-5 0
        }

        .lst-kix_qtf3nhqvwlsi-0>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-0, decimal) ". "
        }

        .lst-kix_qtf3nhqvwlsi-2>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-2, lower-roman) ". "
        }

        .lst-kix_qtf3nhqvwlsi-8>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-8, lower-roman) ". "
        }

        ol.lst-kix_i8b15sd0stzu-6 {
            list-style-type: none
        }

        .lst-kix_gw9li0agsysy-8>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-8
        }

        .lst-kix_koefyqyztmt4-8>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-8, lower-roman) ". "
        }

        ol.lst-kix_i8b15sd0stzu-5 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-4 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-3 {
            list-style-type: none
        }

        .lst-kix_koefyqyztmt4-6>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-6, decimal) ". "
        }

        .lst-kix_hpxmc6bbv6ou-1>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-1
        }

        ol.lst-kix_i8b15sd0stzu-8 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-7 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-8.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-8 0
        }

        ol.lst-kix_qtf3nhqvwlsi-4.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-4 0
        }

        ol.lst-kix_p1005g4a4fak-7.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-7 0
        }

        ol.lst-kix_1v082d6dqfqv-6.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-6 0
        }

        .lst-kix_koefyqyztmt4-4>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-4, lower-latin) ". "
        }

        ol.lst-kix_eomrt7z9go42-6.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-6 0
        }

        .lst-kix_k5rolkuor3l-0>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-0
        }

        ol.lst-kix_hdqyu5ur24as-1 {
            list-style-type: none
        }

        ol.lst-kix_hdqyu5ur24as-0 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-3.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-3 0
        }

        ol.lst-kix_k5rolkuor3l-3.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-3 0
        }

        ol.lst-kix_i8b15sd0stzu-2 {
            list-style-type: none
        }

        .lst-kix_qtf3nhqvwlsi-1>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-1
        }

        ol.lst-kix_i8b15sd0stzu-1 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-0 {
            list-style-type: none
        }

        .lst-kix_selof5716uj7-6>li:before {
            content: "\0025cf  "
        }

        .lst-kix_koefyqyztmt4-2>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-2
        }

        .lst-kix_selof5716uj7-8>li:before {
            content: "\0025a0  "
        }

        .lst-kix_hdqyu5ur24as-2>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-2
        }

        ol.lst-kix_k5rolkuor3l-0.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-0 0
        }

        .lst-kix_eomrt7z9go42-3>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-3
        }

        .lst-kix_fnhch209rih-1>li {
            counter-increment: lst-ctn-kix_fnhch209rih-1
        }

        ol.lst-kix_ioswdliet6ho-1.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-1 0
        }

        .lst-kix_ioswdliet6ho-6>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-6
        }

        ol.lst-kix_qtf3nhqvwlsi-2.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-2 0
        }

        .lst-kix_eomrt7z9go42-2>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-2
        }

        ol.lst-kix_ft5pzj1q5rad-2.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-2 0
        }

        ol.lst-kix_gw9li0agsysy-2.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-2 0
        }

        .lst-kix_p1005g4a4fak-0>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-0, decimal) ". "
        }

        .lst-kix_ft5pzj1q5rad-5>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-5
        }

        .lst-kix_selof5716uj7-4>li:before {
            content: "\0025cb  "
        }

        .lst-kix_k5rolkuor3l-7>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-7
        }

        .lst-kix_ioswdliet6ho-7>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-7
        }

        .lst-kix_selof5716uj7-2>li:before {
            content: "\0025a0  "
        }

        .lst-kix_p1005g4a4fak-8>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-8, lower-roman) ". "
        }

        .lst-kix_hpxmc6bbv6ou-0>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-0
        }

        .lst-kix_selof5716uj7-0>li:before {
            content: "\0025cf  "
        }

        ol.lst-kix_ioswdliet6ho-2.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-2 0
        }

        .lst-kix_p1005g4a4fak-2>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-2, lower-roman) ". "
        }

        .lst-kix_p1005g4a4fak-4>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-4, lower-latin) ". "
        }

        ol.lst-kix_koefyqyztmt4-6.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-6 0
        }

        .lst-kix_koefyqyztmt4-1>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-1
        }

        .lst-kix_p1005g4a4fak-6>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-6, decimal) ". "
        }

        ol.lst-kix_hdqyu5ur24as-6.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-6 0
        }

        .lst-kix_i8b15sd0stzu-5>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-5
        }

        ol.lst-kix_qtf3nhqvwlsi-0 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-0 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-1 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-2 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-3 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-0.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-0 0
        }

        ol.lst-kix_qtf3nhqvwlsi-4 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-4 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-3 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-5 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-2 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-6 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-1 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-7 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-8 {
            list-style-type: none
        }

        ol.lst-kix_koefyqyztmt4-8 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-7 {
            list-style-type: none
        }

        .lst-kix_hpxmc6bbv6ou-7>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-7
        }

        ol.lst-kix_qtf3nhqvwlsi-6 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-5 {
            list-style-type: none
        }

        .lst-kix_gw9li0agsysy-1>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-1
        }

        .lst-kix_ft5pzj1q5rad-8>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-8
        }

        ol.lst-kix_ft5pzj1q5rad-1.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-1 0
        }

        ol.lst-kix_fnhch209rih-3.start {
            counter-reset: lst-ctn-kix_fnhch209rih-3 0
        }

        .lst-kix_koefyqyztmt4-6>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-6
        }

        ol.lst-kix_hpxmc6bbv6ou-4.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-4 0
        }

        ol.lst-kix_gw9li0agsysy-3.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-3 0
        }

        .lst-kix_hdqyu5ur24as-8>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-8
        }

        .lst-kix_i8b15sd0stzu-4>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-4
        }

        ol.lst-kix_k5rolkuor3l-7.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-7 0
        }

        ol.lst-kix_1v082d6dqfqv-7.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-7 0
        }

        ol.lst-kix_koefyqyztmt4-1.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-1 0
        }

        .lst-kix_p1005g4a4fak-1>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-1
        }

        .lst-kix_k5rolkuor3l-2>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-2
        }

        .lst-kix_hdqyu5ur24as-1>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-1
        }

        .lst-kix_1v082d6dqfqv-8>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-8, lower-roman) ". "
        }

        .lst-kix_i8b15sd0stzu-8>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-8, lower-roman) ". "
        }

        ol.lst-kix_fnhch209rih-2.start {
            counter-reset: lst-ctn-kix_fnhch209rih-2 0
        }

        .lst-kix_1v082d6dqfqv-7>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-7, lower-latin) ". "
        }

        ol.lst-kix_k5rolkuor3l-6.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-6 0
        }

        .lst-kix_1v082d6dqfqv-4>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-4, lower-latin) ". "
        }

        .lst-kix_i8b15sd0stzu-4>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-4, lower-latin) ". "
        }

        .lst-kix_1v082d6dqfqv-3>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-3, decimal) ". "
        }

        .lst-kix_i8b15sd0stzu-5>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-5, lower-roman) ". "
        }

        .lst-kix_k5rolkuor3l-7>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-7, lower-latin) ". "
        }

        .lst-kix_1v082d6dqfqv-0>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-0, decimal) ". "
        }

        .lst-kix_i8b15sd0stzu-0>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-0, decimal) ". "
        }

        .lst-kix_k5rolkuor3l-8>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-8, lower-roman) ". "
        }

        .lst-kix_k5rolkuor3l-3>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-3, decimal) ". "
        }

        .lst-kix_k5rolkuor3l-4>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-4, lower-latin) ". "
        }

        ol.lst-kix_koefyqyztmt4-2.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-2 0
        }

        .lst-kix_i8b15sd0stzu-1>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-1, lower-latin) ". "
        }

        ol.lst-kix_fnhch209rih-8.start {
            counter-reset: lst-ctn-kix_fnhch209rih-8 0
        }

        .lst-kix_k5rolkuor3l-0>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-0, decimal) ". "
        }

        .lst-kix_ft5pzj1q5rad-6>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-6
        }

        .lst-kix_k5rolkuor3l-6>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-6
        }

        .lst-kix_fnhch209rih-8>li {
            counter-increment: lst-ctn-kix_fnhch209rih-8
        }

        ol.lst-kix_ft5pzj1q5rad-0.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-0 0
        }

        .lst-kix_qtf3nhqvwlsi-8>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-8
        }

        .lst-kix_ioswdliet6ho-5>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-5
        }

        .lst-kix_koefyqyztmt4-1>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-1, lower-latin) ". "
        }

        .lst-kix_qtf3nhqvwlsi-3>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-3, decimal) ". "
        }

        .lst-kix_qtf3nhqvwlsi-7>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-7, lower-latin) ". "
        }

        ol.lst-kix_hpxmc6bbv6ou-5.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-5 0
        }

        .lst-kix_ioswdliet6ho-3>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-3
        }

        .lst-kix_eomrt7z9go42-4>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-4
        }

        .lst-kix_ft5pzj1q5rad-1>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-1
        }

        ol.lst-kix_1v082d6dqfqv-0.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-0 0
        }

        ol.lst-kix_koefyqyztmt4-0.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-0 0
        }

        ol.lst-kix_fnhch209rih-4.start {
            counter-reset: lst-ctn-kix_fnhch209rih-4 0
        }

        .lst-kix_hpxmc6bbv6ou-2>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-2
        }

        ol.lst-kix_gw9li0agsysy-7.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-7 0
        }

        ol.lst-kix_1v082d6dqfqv-3.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-3 0
        }

        .lst-kix_koefyqyztmt4-5>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-5, lower-roman) ". "
        }

        ol.lst-kix_hpxmc6bbv6ou-2.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-2 0
        }

        ol.lst-kix_gw9li0agsysy-0 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-1 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-2 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-3 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-4 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-5 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-6 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-7 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-8 {
            list-style-type: none
        }

        .lst-kix_selof5716uj7-7>li:before {
            content: "\0025cb  "
        }

        ol.lst-kix_eomrt7z9go42-0.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-0 0
        }

        .lst-kix_eomrt7z9go42-6>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-6
        }

        ol.lst-kix_fnhch209rih-7.start {
            counter-reset: lst-ctn-kix_fnhch209rih-7 0
        }

        .lst-kix_qtf3nhqvwlsi-3>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-3
        }

        .lst-kix_hpxmc6bbv6ou-0>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-0, decimal) ". "
        }

        .lst-kix_i8b15sd0stzu-2>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-2
        }

        .lst-kix_gw9li0agsysy-4>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-4
        }

        .lst-kix_1v082d6dqfqv-1>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-1
        }

        .lst-kix_p1005g4a4fak-1>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-1, lower-latin) ". "
        }

        .lst-kix_hpxmc6bbv6ou-8>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-8, lower-roman) ". "
        }

        ol.lst-kix_1v082d6dqfqv-1.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-1 0
        }

        .lst-kix_selof5716uj7-3>li:before {
            content: "\0025cf  "
        }

        .lst-kix_ft5pzj1q5rad-8>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-8, lower-roman) ". "
        }

        ol.lst-kix_fnhch209rih-6.start {
            counter-reset: lst-ctn-kix_fnhch209rih-6 0
        }

        .lst-kix_hpxmc6bbv6ou-4>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-4, lower-latin) ". "
        }

        .lst-kix_eomrt7z9go42-0>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-0, decimal) ". "
        }

        .lst-kix_koefyqyztmt4-4>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-4
        }

        .lst-kix_fnhch209rih-0>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-0, decimal) ". "
        }

        .lst-kix_fnhch209rih-4>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-4, lower-latin) ". "
        }

        .lst-kix_k5rolkuor3l-4>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-4
        }

        .lst-kix_coph4ng7xdqh-2>li:before {
            content: "\0025a0  "
        }

        .lst-kix_eomrt7z9go42-8>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-8, lower-roman) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-3 {
            list-style-type: none
        }

        ol.lst-kix_ft5pzj1q5rad-2 {
            list-style-type: none
        }

        .lst-kix_ft5pzj1q5rad-4>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-4, lower-latin) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-5 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-5.start {
            counter-reset: lst-ctn-kix_fnhch209rih-5 0
        }

        ol.lst-kix_ft5pzj1q5rad-4 {
            list-style-type: none
        }

        .lst-kix_hdqyu5ur24as-6>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-6
        }

        .lst-kix_fnhch209rih-8>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-8, lower-roman) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-1 {
            list-style-type: none
        }

        ol.lst-kix_k5rolkuor3l-8.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-8 0
        }

        ol.lst-kix_ft5pzj1q5rad-0 {
            list-style-type: none
        }

        .lst-kix_coph4ng7xdqh-6>li:before {
            content: "\0025cf  "
        }

        .lst-kix_eomrt7z9go42-4>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-4, lower-latin) ". "
        }

        .lst-kix_ft5pzj1q5rad-0>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-0, decimal) ". "
        }

        ol.lst-kix_hpxmc6bbv6ou-3.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-3 0
        }

        .lst-kix_p1005g4a4fak-5>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-5, lower-roman) ". "
        }

        ol.lst-kix_gw9li0agsysy-8.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-8 0
        }

        ol.lst-kix_ft5pzj1q5rad-7 {
            list-style-type: none
        }

        ol.lst-kix_ft5pzj1q5rad-6 {
            list-style-type: none
        }

        ol.lst-kix_ft5pzj1q5rad-8 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-2.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-2 0
        }

        .lst-kix_ft5pzj1q5rad-7>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-7
        }

        .lst-kix_gw9li0agsysy-2>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-2
        }

        .lst-kix_qtf3nhqvwlsi-7>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-7
        }

        ol.lst-kix_hdqyu5ur24as-2.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-2 0
        }

        ol.lst-kix_k5rolkuor3l-4.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-4 0
        }

        .lst-kix_1v082d6dqfqv-8>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-8
        }

        ol.lst-kix_p1005g4a4fak-3.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-3 0
        }

        ol.lst-kix_k5rolkuor3l-8 {
            list-style-type: none
        }

        .lst-kix_fnhch209rih-3>li {
            counter-increment: lst-ctn-kix_fnhch209rih-3
        }

        .lst-kix_p1005g4a4fak-4>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-4
        }

        ol.lst-kix_eomrt7z9go42-7.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-7 0
        }

        ol.lst-kix_hpxmc6bbv6ou-1.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-1 0
        }

        ol.lst-kix_qtf3nhqvwlsi-8.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-8 0
        }

        .lst-kix_k5rolkuor3l-5>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-5
        }

        .lst-kix_hdqyu5ur24as-5>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-5, lower-roman) ". "
        }

        ol.lst-kix_1v082d6dqfqv-5.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-5 0
        }

        .lst-kix_hdqyu5ur24as-2>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-2, lower-roman) ". "
        }

        .lst-kix_hdqyu5ur24as-6>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-6, decimal) ". "
        }

        .lst-kix_hdqyu5ur24as-3>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-3, decimal) ". "
        }

        .lst-kix_hdqyu5ur24as-4>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-4, lower-latin) ". "
        }

        ol.lst-kix_k5rolkuor3l-1 {
            list-style-type: none
        }

        ol.lst-kix_k5rolkuor3l-0 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-6.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-6 0
        }

        ol.lst-kix_k5rolkuor3l-3 {
            list-style-type: none
        }

        ol.lst-kix_k5rolkuor3l-2 {
            list-style-type: none
        }

        ol.lst-kix_k5rolkuor3l-5 {
            list-style-type: none
        }

        ol.lst-kix_k5rolkuor3l-4 {
            list-style-type: none
        }

        .lst-kix_i8b15sd0stzu-7>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-7
        }

        ol.lst-kix_k5rolkuor3l-7 {
            list-style-type: none
        }

        .lst-kix_i8b15sd0stzu-3>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-3
        }

        ol.lst-kix_k5rolkuor3l-6 {
            list-style-type: none
        }

        .lst-kix_eomrt7z9go42-1>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-1
        }

        .lst-kix_hdqyu5ur24as-7>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-7, lower-latin) ". "
        }

        .lst-kix_koefyqyztmt4-3>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-3
        }

        .lst-kix_hdqyu5ur24as-7>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-7
        }

        .lst-kix_hdqyu5ur24as-8>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-8, lower-roman) ". "
        }

        ol.lst-kix_i8b15sd0stzu-4.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-4 0
        }

        .lst-kix_ft5pzj1q5rad-0>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-0
        }

        .lst-kix_ft5pzj1q5rad-3>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-3
        }

        ol.lst-kix_ioswdliet6ho-5.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-5 0
        }

        .lst-kix_p1005g4a4fak-0>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-0
        }

        .lst-kix_gw9li0agsysy-6>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-6
        }

        .lst-kix_ioswdliet6ho-8>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-8
        }

        ol.lst-kix_eomrt7z9go42-2.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-2 0
        }

        ol.lst-kix_hpxmc6bbv6ou-6.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-6 0
        }

        .lst-kix_hdqyu5ur24as-4>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-4
        }

        .lst-kix_qtf3nhqvwlsi-0>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-0
        }

        .lst-kix_koefyqyztmt4-0>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-0
        }

        ol.lst-kix_koefyqyztmt4-3.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-3 0
        }

        ol.lst-kix_fnhch209rih-8 {
            list-style-type: none
        }

        .lst-kix_p1005g4a4fak-8>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-8
        }

        ol.lst-kix_fnhch209rih-0 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-5 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-1 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-4 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-2 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-7 {
            list-style-type: none
        }

        .lst-kix_i8b15sd0stzu-0>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-0
        }

        ol.lst-kix_fnhch209rih-3 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-3.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-3 0
        }

        ol.lst-kix_1v082d6dqfqv-6 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-4 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-1.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-1 0
        }

        ol.lst-kix_1v082d6dqfqv-1 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-5 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-0 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-6 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-3 {
            list-style-type: none
        }

        ol.lst-kix_fnhch209rih-7 {
            list-style-type: none
        }

        ol.lst-kix_1v082d6dqfqv-2 {
            list-style-type: none
        }

        ol.lst-kix_gw9li0agsysy-4.start {
            counter-reset: lst-ctn-kix_gw9li0agsysy-4 0
        }

        ol.lst-kix_1v082d6dqfqv-8 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-1.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-1 0
        }

        ol.lst-kix_fnhch209rih-1.start {
            counter-reset: lst-ctn-kix_fnhch209rih-1 0
        }

        ol.lst-kix_ioswdliet6ho-0.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-0 0
        }

        ul.lst-kix_selof5716uj7-7 {
            list-style-type: none
        }

        ul.lst-kix_selof5716uj7-8 {
            list-style-type: none
        }

        .lst-kix_fnhch209rih-0>li {
            counter-increment: lst-ctn-kix_fnhch209rih-0
        }

        ol.lst-kix_hdqyu5ur24as-4.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-4 0
        }

        .lst-kix_ioswdliet6ho-1>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-1
        }

        ol.lst-kix_koefyqyztmt4-8.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-8 0
        }

        ol.lst-kix_hpxmc6bbv6ou-8.start {
            counter-reset: lst-ctn-kix_hpxmc6bbv6ou-8 0
        }

        ul.lst-kix_selof5716uj7-5 {
            list-style-type: none
        }

        ul.lst-kix_selof5716uj7-6 {
            list-style-type: none
        }

        ul.lst-kix_selof5716uj7-3 {
            list-style-type: none
        }

        ul.lst-kix_selof5716uj7-4 {
            list-style-type: none
        }

        ul.lst-kix_selof5716uj7-1 {
            list-style-type: none
        }

        ol.lst-kix_ft5pzj1q5rad-5.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-5 0
        }

        ul.lst-kix_selof5716uj7-2 {
            list-style-type: none
        }

        .lst-kix_p1005g4a4fak-7>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-7
        }

        .lst-kix_hdqyu5ur24as-0>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-0, decimal) ". "
        }

        ul.lst-kix_selof5716uj7-0 {
            list-style-type: none
        }

        .lst-kix_hpxmc6bbv6ou-1>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-1, lower-latin) ". "
        }

        ol.lst-kix_eomrt7z9go42-4.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-4 0
        }

        .lst-kix_k5rolkuor3l-8>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-8
        }

        .lst-kix_hpxmc6bbv6ou-3>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-3, decimal) ". "
        }

        ol.lst-kix_koefyqyztmt4-5.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-5 0
        }

        .lst-kix_ft5pzj1q5rad-4>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-4
        }

        .lst-kix_eomrt7z9go42-8>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-8
        }

        ol.lst-kix_hdqyu5ur24as-7.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-7 0
        }

        ol.lst-kix_1v082d6dqfqv-8.start {
            counter-reset: lst-ctn-kix_1v082d6dqfqv-8 0
        }

        .lst-kix_hpxmc6bbv6ou-7>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-7, lower-latin) ". "
        }

        ol.lst-kix_i8b15sd0stzu-6.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-6 0
        }

        .lst-kix_ft5pzj1q5rad-7>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-7, lower-latin) ". "
        }

        .lst-kix_hpxmc6bbv6ou-5>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-5, lower-roman) ". "
        }

        .lst-kix_ioswdliet6ho-0>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-0
        }

        .lst-kix_fnhch209rih-7>li {
            counter-increment: lst-ctn-kix_fnhch209rih-7
        }

        .lst-kix_eomrt7z9go42-1>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-1, lower-latin) ". "
        }

        .lst-kix_fnhch209rih-1>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-1, lower-latin) ". "
        }

        .lst-kix_fnhch209rih-3>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-3, decimal) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-3.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-3 0
        }

        ol.lst-kix_eomrt7z9go42-5.start {
            counter-reset: lst-ctn-kix_eomrt7z9go42-5 0
        }

        .lst-kix_hdqyu5ur24as-3>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-3
        }

        .lst-kix_koefyqyztmt4-7>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-7
        }

        .lst-kix_k5rolkuor3l-1>li {
            counter-increment: lst-ctn-kix_k5rolkuor3l-1
        }

        ol.lst-kix_i8b15sd0stzu-7.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-7 0
        }

        ol.lst-kix_k5rolkuor3l-2.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-2 0
        }

        .lst-kix_1v082d6dqfqv-4>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-4
        }

        .lst-kix_ft5pzj1q5rad-5>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-5, lower-roman) ". "
        }

        .lst-kix_coph4ng7xdqh-1>li:before {
            content: "\0025cb  "
        }

        .lst-kix_coph4ng7xdqh-3>li:before {
            content: "\0025cf  "
        }

        .lst-kix_eomrt7z9go42-7>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-7, lower-latin) ". "
        }

        .lst-kix_fnhch209rih-7>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-7, lower-latin) ". "
        }

        .lst-kix_ft5pzj1q5rad-3>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-3, decimal) ". "
        }

        .lst-kix_ft5pzj1q5rad-1>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-1, lower-latin) ". "
        }

        .lst-kix_coph4ng7xdqh-5>li:before {
            content: "\0025a0  "
        }

        .lst-kix_eomrt7z9go42-3>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-3, decimal) ". "
        }

        .lst-kix_eomrt7z9go42-5>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-5, lower-roman) ". "
        }

        .lst-kix_fnhch209rih-5>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-5, lower-roman) ". "
        }

        ol.lst-kix_qtf3nhqvwlsi-3.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-3 0
        }

        .lst-kix_hpxmc6bbv6ou-6>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-6
        }

        ol.lst-kix_p1005g4a4fak-8.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-8 0
        }

        .lst-kix_fnhch209rih-4>li {
            counter-increment: lst-ctn-kix_fnhch209rih-4
        }

        .lst-kix_qtf3nhqvwlsi-6>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-6
        }

        ol.lst-kix_hdqyu5ur24as-5.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-5 0
        }

        .lst-kix_coph4ng7xdqh-7>li:before {
            content: "\0025cb  "
        }

        .lst-kix_coph4ng7xdqh-8>li:before {
            content: "\0025a0  "
        }

        ol.lst-kix_ft5pzj1q5rad-7.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-7 0
        }

        ol.lst-kix_i8b15sd0stzu-1.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-1 0
        }

        ol.lst-kix_koefyqyztmt4-7.start {
            counter-reset: lst-ctn-kix_koefyqyztmt4-7 0
        }

        ol.lst-kix_k5rolkuor3l-1.start {
            counter-reset: lst-ctn-kix_k5rolkuor3l-1 0
        }

        .lst-kix_eomrt7z9go42-0>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-0
        }

        ol.lst-kix_ioswdliet6ho-7.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-7 0
        }

        .lst-kix_qtf3nhqvwlsi-4>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-4
        }

        .lst-kix_i8b15sd0stzu-6>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-6
        }

        ol.lst-kix_p1005g4a4fak-6.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-6 0
        }

        .lst-kix_koefyqyztmt4-8>li {
            counter-increment: lst-ctn-kix_koefyqyztmt4-8
        }

        .lst-kix_fnhch209rih-6>li {
            counter-increment: lst-ctn-kix_fnhch209rih-6
        }

        ol.lst-kix_p1005g4a4fak-0.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-0 0
        }

        .lst-kix_1v082d6dqfqv-0>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-0
        }

        .lst-kix_i8b15sd0stzu-7>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-7, lower-latin) ". "
        }

        .lst-kix_1v082d6dqfqv-6>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-6, decimal) ". "
        }

        .lst-kix_1v082d6dqfqv-5>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-5, lower-roman) ". "
        }

        .lst-kix_i8b15sd0stzu-6>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-6, decimal) ". "
        }

        .lst-kix_eomrt7z9go42-7>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-7
        }

        ol.lst-kix_qtf3nhqvwlsi-5.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-5 0
        }

        ol.lst-kix_ft5pzj1q5rad-6.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-6 0
        }

        .lst-kix_k5rolkuor3l-6>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-6, decimal) ". "
        }

        .lst-kix_1v082d6dqfqv-1>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-1, lower-latin) ". "
        }

        .lst-kix_1v082d6dqfqv-2>li:before {
            content: ""counter(lst-ctn-kix_1v082d6dqfqv-2, lower-roman) ". "
        }

        .lst-kix_hpxmc6bbv6ou-5>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-5
        }

        .lst-kix_i8b15sd0stzu-3>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-3, decimal) ". "
        }

        .lst-kix_k5rolkuor3l-5>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-5, lower-roman) ". "
        }

        .lst-kix_i8b15sd0stzu-2>li:before {
            content: ""counter(lst-ctn-kix_i8b15sd0stzu-2, lower-roman) ". "
        }

        .lst-kix_1v082d6dqfqv-5>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-5
        }

        ul.lst-kix_coph4ng7xdqh-4 {
            list-style-type: none
        }

        ul.lst-kix_coph4ng7xdqh-5 {
            list-style-type: none
        }

        ul.lst-kix_coph4ng7xdqh-6 {
            list-style-type: none
        }

        ul.lst-kix_coph4ng7xdqh-7 {
            list-style-type: none
        }

        ul.lst-kix_coph4ng7xdqh-0 {
            list-style-type: none
        }

        .lst-kix_1v082d6dqfqv-2>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-2
        }

        ul.lst-kix_coph4ng7xdqh-1 {
            list-style-type: none
        }

        ol.lst-kix_qtf3nhqvwlsi-6.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-6 0
        }

        .lst-kix_ioswdliet6ho-2>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-2
        }

        ul.lst-kix_coph4ng7xdqh-2 {
            list-style-type: none
        }

        .lst-kix_gw9li0agsysy-3>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-3
        }

        ul.lst-kix_coph4ng7xdqh-3 {
            list-style-type: none
        }

        .lst-kix_k5rolkuor3l-2>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-2, lower-roman) ". "
        }

        ul.lst-kix_coph4ng7xdqh-8 {
            list-style-type: none
        }

        .lst-kix_k5rolkuor3l-1>li:before {
            content: ""counter(lst-ctn-kix_k5rolkuor3l-1, lower-latin) ". "
        }

        .lst-kix_p1005g4a4fak-3>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-3
        }

        ol.lst-kix_p1005g4a4fak-5.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-5 0
        }

        ol.lst-kix_eomrt7z9go42-8 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-7 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-6 {
            list-style-type: none
        }

        .lst-kix_fnhch209rih-2>li {
            counter-increment: lst-ctn-kix_fnhch209rih-2
        }

        ol.lst-kix_eomrt7z9go42-5 {
            list-style-type: none
        }

        .lst-kix_qtf3nhqvwlsi-2>li {
            counter-increment: lst-ctn-kix_qtf3nhqvwlsi-2
        }

        ol.lst-kix_eomrt7z9go42-4 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-3 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-2 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-1 {
            list-style-type: none
        }

        ol.lst-kix_eomrt7z9go42-0 {
            list-style-type: none
        }

        .lst-kix_qtf3nhqvwlsi-5>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-5, lower-roman) ". "
        }

        ol.lst-kix_qtf3nhqvwlsi-7.start {
            counter-reset: lst-ctn-kix_qtf3nhqvwlsi-7 0
        }

        ol.lst-kix_ioswdliet6ho-8 {
            list-style-type: none
        }

        .lst-kix_qtf3nhqvwlsi-1>li:before {
            content: ""counter(lst-ctn-kix_qtf3nhqvwlsi-1, lower-latin) ". "
        }

        ol.lst-kix_ioswdliet6ho-6 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-7 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-4 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-5 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-2 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-6.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-6 0
        }

        ol.lst-kix_ioswdliet6ho-3 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-0 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-1 {
            list-style-type: none
        }

        .lst-kix_ioswdliet6ho-4>li {
            counter-increment: lst-ctn-kix_ioswdliet6ho-4
        }

        .lst-kix_koefyqyztmt4-7>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-7, lower-latin) ". "
        }

        .lst-kix_ft5pzj1q5rad-2>li {
            counter-increment: lst-ctn-kix_ft5pzj1q5rad-2
        }

        .lst-kix_eomrt7z9go42-5>li {
            counter-increment: lst-ctn-kix_eomrt7z9go42-5
        }

        ol.lst-kix_p1005g4a4fak-4.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-4 0
        }

        .lst-kix_koefyqyztmt4-3>li:before {
            content: ""counter(lst-ctn-kix_koefyqyztmt4-3, decimal) ". "
        }

        ol.lst-kix_ft5pzj1q5rad-8.start {
            counter-reset: lst-ctn-kix_ft5pzj1q5rad-8 0
        }

        .lst-kix_hdqyu5ur24as-5>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-5
        }

        .lst-kix_i8b15sd0stzu-1>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-1
        }

        ol.lst-kix_hdqyu5ur24as-1.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-1 0
        }

        .lst-kix_hdqyu5ur24as-1>li:before {
            content: ""counter(lst-ctn-kix_hdqyu5ur24as-1, lower-latin) ". "
        }

        ol.lst-kix_i8b15sd0stzu-2.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-2 0
        }

        .lst-kix_i8b15sd0stzu-8>li {
            counter-increment: lst-ctn-kix_i8b15sd0stzu-8
        }

        ol.lst-kix_p1005g4a4fak-2.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-2 0
        }

        .lst-kix_hpxmc6bbv6ou-2>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-2, lower-roman) ". "
        }

        .lst-kix_p1005g4a4fak-5>li {
            counter-increment: lst-ctn-kix_p1005g4a4fak-5
        }

        ol.lst-kix_hpxmc6bbv6ou-0 {
            list-style-type: none
        }

        ol.lst-kix_i8b15sd0stzu-0.start {
            counter-reset: lst-ctn-kix_i8b15sd0stzu-0 0
        }

        .lst-kix_ft5pzj1q5rad-6>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-6, decimal) ". "
        }

        ol.lst-kix_hdqyu5ur24as-0.start {
            counter-reset: lst-ctn-kix_hdqyu5ur24as-0 0
        }

        ol.lst-kix_p1005g4a4fak-4 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-6 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-3 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-5 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-6 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-8 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-5 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-7 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-8 {
            list-style-type: none
        }

        .lst-kix_selof5716uj7-5>li:before {
            content: "\0025a0  "
        }

        ol.lst-kix_hpxmc6bbv6ou-2 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-7 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-1 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-4 {
            list-style-type: none
        }

        ol.lst-kix_hpxmc6bbv6ou-3 {
            list-style-type: none
        }

        .lst-kix_hpxmc6bbv6ou-6>li:before {
            content: ""counter(lst-ctn-kix_hpxmc6bbv6ou-6, decimal) ". "
        }

        ol.lst-kix_p1005g4a4fak-0 {
            list-style-type: none
        }

        ol.lst-kix_p1005g4a4fak-2 {
            list-style-type: none
        }

        .lst-kix_gw9li0agsysy-5>li {
            counter-increment: lst-ctn-kix_gw9li0agsysy-5
        }

        ol.lst-kix_p1005g4a4fak-1 {
            list-style-type: none
        }

        ol.lst-kix_ioswdliet6ho-8.start {
            counter-reset: lst-ctn-kix_ioswdliet6ho-8 0
        }

        .lst-kix_hpxmc6bbv6ou-3>li {
            counter-increment: lst-ctn-kix_hpxmc6bbv6ou-3
        }

        .lst-kix_p1005g4a4fak-7>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-7, lower-latin) ". "
        }

        .lst-kix_selof5716uj7-1>li:before {
            content: "\0025cb  "
        }

        .lst-kix_eomrt7z9go42-2>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-2, lower-roman) ". "
        }

        ol.lst-kix_p1005g4a4fak-1.start {
            counter-reset: lst-ctn-kix_p1005g4a4fak-1 0
        }

        .lst-kix_p1005g4a4fak-3>li:before {
            content: ""counter(lst-ctn-kix_p1005g4a4fak-3, decimal) ". "
        }

        .lst-kix_hdqyu5ur24as-0>li {
            counter-increment: lst-ctn-kix_hdqyu5ur24as-0
        }

        .lst-kix_coph4ng7xdqh-0>li:before {
            content: "\0025cf  "
        }

        li.li-bullet-0:before {
            margin-left: -18pt;
            white-space: nowrap;
            display: inline-block;
            min-width: 18pt
        }

        .lst-kix_fnhch209rih-2>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-2, lower-roman) ". "
        }

        .lst-kix_eomrt7z9go42-6>li:before {
            content: ""counter(lst-ctn-kix_eomrt7z9go42-6, decimal) ". "
        }

        .lst-kix_1v082d6dqfqv-7>li {
            counter-increment: lst-ctn-kix_1v082d6dqfqv-7
        }

        .lst-kix_coph4ng7xdqh-4>li:before {
            content: "\0025cb  "
        }

        .lst-kix_ft5pzj1q5rad-2>li:before {
            content: ""counter(lst-ctn-kix_ft5pzj1q5rad-2, lower-roman) ". "
        }

        .lst-kix_fnhch209rih-6>li:before {
            content: ""counter(lst-ctn-kix_fnhch209rih-6, decimal) ". "
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c2 {
            margin-left: 36pt;
            padding-top: 0pt;
            padding-left: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c4 {
            margin-left: 72pt;
            padding-top: 0pt;
            padding-left: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c1 {
            margin-left: 36pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left;
            height: 11pt
        }

        .c0 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c5 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c22 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Arial";
            font-style: normal
        }

        .c16 {
            background-color: #337ab7;
            color: #ffffff;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c20 {
            padding-top: 20pt;
            padding-bottom: 6pt;
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c26 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 20pt;
            font-family: "Arial";
            font-style: normal
        }

        .c25 {
            color: #000000;
            font-weight: 400;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c6 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c18 {
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c19 {
            text-decoration-skip-ink: none;
            -webkit-text-decoration-skip: none;
            text-decoration: underline
        }

        .c9 {
            background-color: #777777;
            color: #ffffff
        }

        .c23 {
            background-color: #337ab7;
            color: #ffffff
        }

        .c11 {
            background-color: #009900;
            color: #ffffff
        }

        .c21 {
            background-color: #d80b15;
            color: #ffffff
        }

        .c24 {
            background-color: #f0ad4e;
            color: #ffffff
        }

        .c12 {
            max-width: 1000px;
            padding: 10px;
        }

        @media screen and (min-width: 992px) {
            .c12 {
                padding: 45pt 72pt 72pt 72pt
            }
        }

        .c8 {
            padding: 0;
            margin: 0
        }

        .c10 {
            background-color: #5bc0de;
            color: #ffffff
        }

        .c13 {
            height: 11pt
        }

        .c7 {
            font-weight: 700
        }

        .c15 {
            margin-left: 72pt
        }

        .c14 {
            background-color: #5bc0de
        }

        .c17 {
            font-style: italic
        }

        .c3 {
            background-color: #ffffff
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        .doc-content p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        .doc-content h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .doc-content h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
@endpush

<div class="c3 c12 doc-content">
    <h1 class="c20" id="h.9klr333tlcm5">
        <span class="c26">Pravilnik za rad na apotekaviva24.ba web shop-u</span>
    </h1>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6">
        <span class="c5">Pristigle narud&#382;be</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_i8b15sd0stzu-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span>Novopristigle narud&#382;be imaju status </span>
            <span class="c7 c24">Narud&#382;ba kreirana</span>
            <span class="c0">&nbsp;i pojavljuju se na vrhu tabele </span>
        </li>
    </ol>
    <p class="c1">
        <span class="c0"></span>
    </p>
    <ol class="c8 lst-kix_i8b15sd0stzu-0" start="2">
        <li class="c2 li-bullet-0">
            <span class="c0">Postoje tri na&#269;ina za preuzimanje narud&#382;be od kojih kupac bira jedan: </span>
        </li>
    </ol>
    <ol class="c8 lst-kix_i8b15sd0stzu-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c0">Slanje na adresu </span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Preuzimanje u poslovnici (apoteci)</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Hitna dostava</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6">
        <span class="c5">Slanje na adresu</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_1v082d6dqfqv-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Ako je narud&#382;ba pristigla prije 14:00h, na dan kada se &scaron;alju narud&#382;be (pon - pet), potrebno je narud&#382;bu pripremiti prije dolaska kurira kako bi isti dan mogla biti poslana</span>
        </li>
    </ol>
    <p class="c1">
        <span class="c0"></span>
    </p>
    <ol class="c8 lst-kix_1v082d6dqfqv-0" start="2">
        <li class="c2 li-bullet-0">
            <span class="c0">Priprema narud&#382;be:</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_1v082d6dqfqv-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c0">Isprintati elektronski ra&#269;un za narud&#382;bu</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Koriste&#263;i elektronski ra&#269;un izdvojiti sve naru&#269;ene proizvode </span>
        </li>
    </ol>
    <ol class="c8 lst-kix_i8b15sd0stzu-1" start="4">
        <li class="c4 li-bullet-0">
            <span class="c0">Sve izdvojene proizvode otkucati na kasi (obratiti pa&#382;nju na karti&#269;na pla&#263;anja)</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Fiskalni ra&#269;un zaheftati za elektronski</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Izbaciti duplikat fiskalnog ra&#269;una, upisati na njega broj narud&#382;be, ime i prezime poru&#269;ioca i ostaviti u arhivu</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Narud&#382;bi dodati prikladne testere ili gratise i karticu zahvalnosti</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">U slu&#269;aju da se radi o medicinskoj opremi (inhalator, tlakomjer i sl.) potrebno je ispuniti i ovjeriti garantni list</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Koriste&#263;i podatke sa elektronskog ra&#269;una ispuniti nalog za kurirsku slu&#382;bu (bez otkupnine ako je karti&#269;no pla&#263;anje!)</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Sve skupa spakovati koriste&#263;i pucketavu foliju kako ne bi do&scaron;lo do eventualnih o&scaron;te&#263;enja pri transportu te nalog nalijepiti na paket</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">U slu&#269;aju slanja proizvoda u staklenim ambala&#382;ama (sirup, sok i sl.) potrebno je zalijepiti naljepnicu &ldquo;LOMLJIVO&rdquo; na paket</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Nakon &scaron;to su prethodni koraci ura&#273;eni, potrebno je narud&#382;bu ozna&#269;iti kao </span>
            <span class="c7 c10">Spremno za slanje</span>
            <span class="c0 c3">&nbsp;</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c3">Kad kurir preuzme paket potrebno je narud&#382;bu ozna&#269;iti kao </span>
            <span class="c7 c16">Narud&#382;ba poslana</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6">
        <span class="c5">Preuzimanje u poslovnici (apoteci)</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_eomrt7z9go42-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Priprema narud&#382;be za Centralnu apoteku:</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_eomrt7z9go42-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c0">Isprintati elektronski ra&#269;un</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Koriste&#263;i elektronski ra&#269;un izdvojiti sve naru&#269;ene proizvode </span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Izdvojene proizvode rezervisati i ostaviti sa strane zajedno sa elektronskim ra&#269;unom (max 3 dana) i narud&#382;bu ozna&#269;iti kao </span>
            <span class="c10 c7">Spremno za preuzimanje</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Kada kupac dodje po proizvode potrebno je otkucati fiskalni ra&#269;un i novac staviti u kasu, a narud&#382;bu ozna&#269;iti sa </span>
            <span class="c11 c7">Narud&#382;ba izvr&scaron;ena</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <ol class="c8 lst-kix_eomrt7z9go42-0" start="2">
        <li class="c2 li-bullet-0">
            <span class="c0">Priprema narud&#382;be za ostale apoteke</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_eomrt7z9go42-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span>P</span>
            <span class="c0">otrebno je provjeriti dostupnost naru&#269;enih proizvoda u apoteci koja je odabrana za preuzimanje</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Ako su naru&#269;eni proizvodi dostupni u toj apoteci, potrebno je obavjestiti osoblje apoteke o naru&#269;enim proizvodima i dati informacije o kupcu</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Ako naru&#269;eni proizvodi nisu dostupni u toj apoteci, napraviti internu otpremnicu i u najkra&#263;em roku poslati zajedno sa proizvodima u apoteku</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Nakon toga narud&#382;bu ozna&#269;iti sa </span>
            <span class="c10 c7 c18">Spremno za preuzimanje</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c5 c14"></span>
    </p>
    <p class="c6 c13">
        <span class="c5 c14"></span>
    </p>
    <p class="c6 c13">
        <span class="c5 c14"></span>
    </p>
    <p class="c6">
        <span class="c5 c3">Hitna dostava</span>
    </p>
    <p class="c6 c13">
        <span class="c5 c3"></span>
    </p>
    <ol class="c8 lst-kix_koefyqyztmt4-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0 c3">Naru&#269;ioc je du&#382;an prije odabira ovog na&#269;ina dostave kontaktirati apoteku kako bi se utvdila mogu&#263;nost dostavljanja na tra&#382;enu lokaciju u roku 24h</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0 c3">Radnik apoteke provjerava mogu&#263;nosti sa brzom po&scaron;tom i daje povratnu informaciju naru&#269;iocu</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0 c3">Postoju mogu&#263;nost da &#263;e naru&#269;ioc napraviti hitnu narud&#382;bu bez konsultacije sa apotekom. U tom slu&#269;aju apotekar provjerava mogu&#263;nosti dostave sa brzom po&scaron;tom i daje povratnu informaciju naru&#269;iocu.</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c3">Ukoliko je mogu&#263;e izvr&scaron;iti hitnu narud&#382;bu izvr&scaron;iti korake i sekcije </span>
            <span class="c7">Slanje na adresu</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c0 c3"></span>
    </p>
    <p class="c6 c13">
        <span class="c0 c3"></span>
    </p>
    <p class="c6 c13">
        <span class="c0 c3"></span>
    </p>
    <p class="c6">
        <span class="c5 c3">Pokloni i testeri</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_qtf3nhqvwlsi-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Uz svaku narud&#382;bu potrebno je prilo&#382;iti odgovaraju&#263;i poklon ili tester (ukoliko je to mogu&#263;e sa aspekta dostupnosti testera i poklon&#269;i&#263;a)</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0">Prilikom odabira poklona ili testera potrebno je voditi ra&#269;una o smislenosti cjelokupne narud&#382;be (spol naru&#269;ioca, naru&#269;eni proizvodi)</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_qtf3nhqvwlsi-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c17">Lo&scaron; primjer</span>
            <span class="c0">&nbsp;- Gospodin naru&#269;uje suplemente, vitamine i minerale i kao tester dobije kremu za bore oko o&#269;iju.</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Dobar primjer</span>
            <span class="c0">&nbsp;- Gospodin naru&#269;uje suplemente, vitamine i minerale i kao poklon dobije Viva &#269;aj od mente ili nekoliko kesica magnezija.</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Lo&scaron; primjer</span>
            <span class="c0">&nbsp;- Gospo&#273;a naru&#269;je hranu za bebe i kao poklon dobije &#269;aj za dojilje ili tufere za grudi. (O&#269;igledno je da majka ne doji ako naru&#269;uje hranu za bebe.)</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Dobar primjer</span>
            <span class="c0">&nbsp;- Gospo&#273;a naru&#269;je hranu za bebe i kao poklon dobije fla&scaron;icu za hranjenje ili pantenol mast.</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Lo&scaron; primjer</span>
            <span class="c0">&nbsp;- Gospo&#273;a naru&#269;uje kozmeti&#269;ke proizvode i kao gratis dobije Viva &#269;aj od mente.</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Lo&scaron; primjer</span>
            <span class="c0">&nbsp;- Gospo&#273;a naru&#269;uje kozmeti&#269;ke proizvode za suhu ko&#382;u &nbsp;i kao gratis dobije tester kreme za masnu ko&#382;u.</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c17">Dobar primjer</span>
            <span>&nbsp;- Gospo&#273;a naru&#269;uje kozmeti&#269;ke proizvode za suhu ko&#382;u i kao gratis dobije tester drugog brenda za suhu ko&#382;u ili ne&scaron;to neutralno</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6">
        <span class="c5">Povratnice</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_hpxmc6bbv6ou-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Kada kurir donese novac potrebno ga je prebrojati i utvrditi da je iznos ispravan</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0">Koriste&#263;i spisak od kurira, upore&#273;uju&#263;i imena naru&#269;ioca, izdvojiti duplikate ra&#269;una iz arhive </span>
        </li>
        <li class="c2 li-bullet-0">
            <span>Za prona&#273;ene ra&#269;une ozna&#269;iti narud&#382;bu kao </span>
            <span class="c7 c11">Narud&#382;ba izvr&scaron;ena</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0">Duplikat ra&#269;una baciti a novac ostaviti u blagajnu</span>
        </li>
        <li class="c2 li-bullet-0">
            <span class="c0">Izvje&scaron;taj od kurira ostaviti u arhivu</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6">
        <span class="c5">Otkazivanje i povrat</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_ioswdliet6ho-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span>Kupac mo&#382;e da otka&#382;e narud&#382;bu ukoliko ona nije ve&#263; poslana i u tom slu&#269;aju narud&#382;bu ozna&#269;avamo kao </span>
            <span class="c7 c9">Narud&#382;ba otkazana</span>
            <span class="c3 c7">&nbsp;</span>
            <span class="c0 c3">(upisati razlog, ako ga je kupac naveo, u polje &ldquo;Komentar na narud&#382;bu&rdquo;)</span>
        </li>
    </ol>
    <p class="c1">
        <span class="c0 c3"></span>
    </p>
    <ol class="c8 lst-kix_ioswdliet6ho-0" start="2">
        <li class="c2 li-bullet-0">
            <span class="c3">Kupac mo&#382;e da izvr&scaron;i povrat narud&#382;be ukoliko je ona poslana i u tom slu&#269;aju narud&#382;bu ozna&#269;avamo kao </span>
            <span class="c7 c21">Narud&#382;ba vra&#263;ena</span>
            <span class="c3">&nbsp;(upisati razlog, ako ga je kupac naveo, u polje &ldquo;Komentar na narud&#382;bu&rdquo;)</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6">
        <span class="c5">Dnevno a&#382;uriranje narud&#382;bi</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_fnhch209rih-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span>Svakodnevno je potrebno pregledati narud&#382;be sa statusima:</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_fnhch209rih-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c10 c7">Spremno za preuzimanje</span>
            <span class="c3">&nbsp;- Provjeriti da li je kupac preuzeo narud&#382;bu (tako&#273;er za drudge apoteke). Ako kupac nije preuzeo narud&#382;bu a pro&scaron;lo je vi&scaron;e od 3 dana od rezervacije, potrebno je promijeniti status narud&#382;be na </span>
            <span class="c9 c7">Narud&#382;ba otkazana</span>
            <span class="c0 c3">&nbsp;i navesti to kao razlog otkazivanja u polju &ldquo;Komentar na narud&#382;bu&rdquo;</span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c7 c23">Narud&#382;ba poslana</span>
            <span class="c0 c3">&nbsp;- Provjeriti da li postoje narud&#382;be koje su poslana prije 7 i vi&scaron;e dana za koje jo&scaron; nismo dobili povratnicu. Ako postoje provjeriti njihov status sa brzom po&scaron;tom</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6">
        <span class="c5">Rje&scaron;avanje mogu&#263;ih problema</span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <ol class="c8 lst-kix_gw9li0agsysy-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Naru&#269;eni proizvod nije dostupan</span>
        </li>
    </ol>
    <ol class="c8 lst-kix_gw9li0agsysy-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span class="c0">Provjeriti da li je proizvod dostupan u drugim apotekama. Ako jeste, dostaviti ga u centralu </span>
        </li>
        <li class="c4 li-bullet-0">
            <span class="c0">Ako proizvod nije dostupan u drugim apotekama potrebno je provjeriti dostupnost kod dostavlja&#269;a te obavijestiti kupca o eventualnom vremenu &#269;ekanja</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Potrebno je dodati komentar u polje </span>
            <span class="c3">&ldquo;Komentar na narud&#382;bu&rdquo;</span>
            <span class="c0">&nbsp;da je proizvod nedostupan i da se &#269;eka kako bi drugi uposlenici mogli da prate stanje narud&#382;e</span>
        </li>
    </ol>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <p class="c6">
        <span class="c5">A&#382;uriranje dostupnosti proizvoda na web shopu</span>
    </p>
    <p class="c6 c13">
        <span class="c0"></span>
    </p>
    <ol class="c8 lst-kix_k5rolkuor3l-0 start" start="1">
        <li class="c2 li-bullet-0">
            <span class="c0">Prilikom izdvajanja naru&#269;enih proizvoda potrebno je provjeriti preostalo stanje </span>
        </li>
    </ol>
    <ol class="c8 lst-kix_k5rolkuor3l-1 start" start="1">
        <li class="c4 li-bullet-0">
            <span>Naru&#269;eni proizvod nije na stanju - </span>
            <span class="c19">proizvod ozna&#269;iti kao nedostupan</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Naru&#269;eni proizvod je zadnji na stanju, ali ga ima u drugim apotekama - </span>
            <span class="c19">nije potrebno mijenjati dostupnost proizvoda</span>
        </li>
        <li class="c4 li-bullet-0">
            <span>Naru&#269;eni proizvod je zadnji na stanju i nije dostupan u drugim apotekama - </span>
            <span class="c19">proizvod ozna&#269;iti kao nedostupan</span>
        </li>
    </ol>
    <p class="c6 c13 c15">
        <span class="c25 c19"></span>
    </p>
    <ol class="c8 lst-kix_k5rolkuor3l-0" start="2">
        <li class="c2 li-bullet-0">
            <span class="c0">Svakodnevno a&#382;urirati nedostupne proizvode na web shop-u i provjeriti da li su do&scaron;li na stanje. Ako jesu ozna&#269;iti ih da su dostupni.</span>
        </li>
    </ol>
    <p class="c6 c13 c15">
        <span class="c19 c25"></span>
    </p>
    <p class="c6 c13">
        <span class="c5"></span>
    </p>
    <p class="c6 c13">
        <span class="c0 c3"></span>
    </p>
</div>
@endsection