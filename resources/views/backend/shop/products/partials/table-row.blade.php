<tr>
	<td>
		<div class="background-img" 
			style="background-image: url('{{$product->mainPhoto() ? $product->mainPhoto()->url('thumb') : asset('images/no-photo-available.png')}}');border-radius:50%;width: 50px;height: 50px;"
		></div>
	</td>
	<td>
		{{$product->fullName()}}
	</td>
	<td>
		{{$product->sku}}
	</td>
	<td>
		<span style="@if($product->on_sale)text-decoration: line-through;@endif">{{formatPrice($product->price)}}</span>
		@if($product->on_sale)
			<span>{{$product->discount_price}}KM</span>
		@endif
	</td>
	<td>
		@if($product->order_products_count)
			{{$product->order_products_count}} ({{formatPrice($product->totalPrice)}})
		@else
			-
		@endif
	</td>
	<td>
		@if($product->in_stock)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		@if($product->free_shipping)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		@if($product->featured)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		@if($product->published)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		@if($product->trashed())
			<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.products.show', ['product' => $product])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Pregled</a>
		@else 
			<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.products.edit', ['product' => $product])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
		@endif
	</td>
</tr>