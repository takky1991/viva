@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<form class="form-inline" action="{{route('backend.shop.products.search')}}" method="POST">
				{{ csrf_field() }}
				<a class="btn btn-success btn-lg" href="{{route('backend.shop.products.create')}}">Novi Proizvod</a>
				
				<div class="index-filter">
					<div class="form-group">
						<input class="form-control" type="text" name="searchTerm" placeholder="Nazin proizvoda..." value="{{Request::get('searchTerm')}}">
					</div>

				  	<div class="form-group">
				    	<select class="form-control" id="category" name="category">
						  	<option value="">Sve kategorije</option>
						  	@foreach($categories as $item)
						  		<option value="{{$item->id}}" {{Request::get('category') == $item->id ? 'selected' : ''}}>{{$item->title}}</option>
						  	@endforeach
						</select>
					</div>
					  
					<div class="form-group">
				    	<select class="form-control" id="brand" name="brand">
						  	<option value="">Svi brendovi</option>
						  	@foreach($brands as $brand)
						  		<option value="{{$brand->id}}" {{Request::get('brand') == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
						  	@endforeach
						</select>
				  	</div>

				  	<div class="form-group">
				    	<select class="form-control" id="on_sale" name="onSale">
						  	<option value="">Sve cijene</option>
						  	<option value="true" {{Request::get('on_sale') == 'true' ? 'selected' : ''}}>Na sniženju</option>
						  	<option value="false" {{Request::get('on_sale') == 'false' ? 'selected' : ''}}>Bez sniženja</option>
						</select>
				  	</div>

					<div class="form-group">
				    	<select class="form-control" id="in_stock" name="inStock">
						  	<option value="">Sve dostupnosti</option>
						  	<option value="true" {{Request::get('inStock') == 'true' ? 'selected' : ''}}>Na stanju</option>
						  	<option value="false" {{Request::get('inStock') == 'false' ? 'selected' : ''}}>Nije na stanju</option>
						</select>
				  	</div>

					<div class="form-group">
				    	<select class="form-control" id="published" name="published">
						  	<option value="">Objavljeno i neobjavljeno</option>
						  	<option value="true" {{Request::get('published') == 'true' ? 'selected' : ''}}>Objavljeno</option>
						  	<option value="false" {{Request::get('published') == 'false' ? 'selected' : ''}}>Nije objavljeni</option>
						</select>
				  	</div>

				  	<div class="form-group">
				    	<select class="form-control" id="archived" name="archived">
						  	<option value="">Aktivni</option>
						  	<option value="true" {{Request::get('archived') == 'true' ? 'selected' : ''}}>Arhivirani</option>
						</select>
				  	</div>
					  
					<br>

					<div class="form-group">
						<datepicker input-class="form-control" 
							id="products_from_filter" 
							name="productsFromDate"
							language="bs"
							format="dd-MM-yyyy"
							placeholder="Datum od"
							value="{{!empty(request()->get('productsFromDate')) ? \Carbon\Carbon::parse(request()->get('productsFromDate'))->format('m-d-Y') : ''}}">
						</datepicker>
					</div>
					<div class="form-group">
						<datepicker input-class="form-control" 
							id="products_to_filter" 
							name="productsToDate"
							language="bs"
							format="dd-MM-yyyy"
							placeholder="Datum do"
							value="{{!empty(request()->get('productsToDate')) ? \Carbon\Carbon::parse(request()->get('productsToDate'))->format('m-d-Y') : ''}}">
						</datepicker>
					</div>
				  	<button type="submit" class="btn btn-primary btn-lg">Prikaži</button>
					@if(request()->filled('productsFromDate') && request()->filled('productsToDate'))
						<a 
							class="btn btn-success btn-lg" 
							target="_blank" 
							href="{{route('backend.shop.products.pdf', request()->all())}}"
						>Prikaži PDF</a>
						<a 
							class="btn btn-success btn-lg" 
							target="_blank" 
							href="{{route('backend.shop.products.csv', request()->all())}}"
						>Prikaži Excel (CSV)</a>
					@endif
				</div>
			</form>
		</div>
	</div>
	<br>
	<a href="{{route('backend.shop.products.index')}}">Resetuj filter</a>
	<br>
	<br>
	<p><b>Proizvodi: {{$productsCount}}</b></p>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th></th>
						        	<th>Naziv</th>
						        	<th>Šifra</th>
						        	<th>Cijena</th>
						        	<th>Prodano</th>
						        	<th>Na skladištu</th>
						        	<th>Bes. dostava</th>
						        	<th>Izdvojen</th>
						        	<th>Objavljeno</th>
						        	<th></th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/products/partials/table-row', $products, 'product', 'backend/shop/products/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
			{{ $products->appends(Request::all())->links() }}
		</div>
	</div>
@endsection