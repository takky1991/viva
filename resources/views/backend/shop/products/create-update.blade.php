@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.products.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Proizvodi</a>

				@if(empty($product))
					<h3>Dodaj Proizvod</h3>
				@else
					<h3>Uredi Proizvod {{empty($product) ? '' : '- ' . $product->title}}</h3>
				@endif
				<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('product-form').submit();">
			  		{{empty($product) ? 'Kreiraj' : 'Sačuvaj'}}
				</button>
				@if(!empty($product))
					<a href="{{route('backend.shop.products.preview', ['product' => $product])}}" class="btn btn-primary btn-lg" target="_blank">Pregled</a>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="product-form" 
					method="POST" 
					action="{{ empty($product) ? route('backend.shop.products.store') : route('backend.shop.products.update', ['product' => $product])}}">
					{{ csrf_field() }}

					@if(!empty($product))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
									<h4>Osnovne informacije</h4>
									<div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
										<label for="brand_id">Brend</label>
										<select class="form-control" id="brand_id" name="brand_id">
											<option value="" selected>Odaberi brend</option>
											@foreach($brands as $item)
												<option value="{{$item->id}}" {{empty($product) ? (old('brand_id') == $item->id ? 'selected' : '') : ($product->brand_id == $item->id ? 'selected' : '')}}>
													{{$item->name}}
												</option>
											@endforeach
									  	</select>
									  	@if($errors->has('brand_id'))
										  	<span class="help-block">
											  	<strong>{{ $errors->first('brand_id') }}</strong>
										  	</span>
									  	@endif
								  	</div>

						    		<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
								    	<label for="title">Naziv proizvoda</label>
								    	<input type="text" 
								    		class="form-control" 
								    		id="title" 
								    		name="title" 
								    		placeholder="Npr. Tylol Hot" 
								    		value="{{empty($product) ? old('title') : $product->title }}">
											<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Naziv proizvoda ne smije da sadrži ime brenda (npr. "La Roche-Posay" ili "LRP") i opis pakovanja (npr. 100ml ili 1 par)!</span>
								    	@if($errors->has('title'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('title') }}</strong>
		                                    </span>
		                                @endif
									</div>
									  
									<div class="form-group {{ $errors->has('package_description') ? ' has-error' : '' }}">
								    	<label for="package_description">Opis pakovanja</label>
								    	<input type="text" 
								    		class="form-control" 
								    		id="package_description" 
								    		name="package_description" 
								    		placeholder="Npr. 40ml" 
								    		value="{{empty($product) ? old('package_description') : $product->package_description }}">
								    	@if($errors->has('package_description'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('package_description') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
								    	<label for="description">Opis proizvoda</label>
								    	<redactor 
								    		id="description" 
								    		name="description" 
								    		min-height="300px"
								    		value="{{ !empty($product) ? $product->description : old('description') }}" 
								    	></redactor>
								    	@if($errors->has('description'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('description') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

									<div class="form-group {{ $errors->has('featured_text') ? ' has-error' : '' }}">
								    	<label for="featured_text">Promotivni tekst</label>
								    	<input type="text" 
								    		class="form-control" 
								    		id="featured_text" 
								    		name="featured_text" 
								    		placeholder="Npr. -50% na drugi proizvod" 
								    		value="{{empty($product) ? old('featured_text') : $product->featured_text }}">
								    	@if($errors->has('featured_text'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('featured_text') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						  	<div class="panel panel-default">
						  		<div class="panel-body">
						  			<h4>Cijena</h4>
						  			<div class="row">
						  				<div class="col-md-6">
						  					<div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
						  						<label for="price">Cijena proizvoda</label>
								    			<input type="text" 
								    				class="form-control" 
								    				id="price" 
								    				name="price" 
								    				placeholder="0,00KM" 
								    				value="{{empty($product) ? old('price') : $product->price }}">
								    			@if ($errors->has('price'))
		                                    		<span class="help-block">
		                                        		<strong>{{ $errors->first('price') }}</strong>
		                                    		</span>
		                                		@endif
								  			</div>
						  				</div>
						  			</div>
						  			<div class="row">
						  				<div class="col-md-6">
						  					<div class="form-group {{ $errors->has('discount_price') ? ' has-error' : '' }}">
									    		<label for="discount_price">Snižena cijena</label>
									    		<input type="text" 
									    			class="form-control" 
									    			id="discount_price" 
									    			name="discount_price" 
									    			placeholder="0,00KM" 
									    			value="{{empty($product) ? old('discount_price') : $product->discount_price }}">
									    		@if ($errors->has('discount_price'))
			                                    	<span class="help-block">
			                                        	<strong>{{ $errors->first('discount_price') }}</strong>
			                                    	</span>
			                                	@endif
								  			</div>
						  				</div>
						  				<div class="col-md-6">
							  				<div class="form-group {{ $errors->has('on_sale') ? ' has-error' : '' }}" style="display: inline-block;margin-right: 50px;">
												<label for="on_sale">Na sniženju</label>
												<div>
										  			<label class="switch">
														<input type="hidden" name="on_sale" value="0">
													  	<input type="checkbox" 
													  		id="on_sale" 
													  		name="on_sale"
													  		value="1" 
													  		{{empty($product) ? (old('on_sale') ? 'checked' : '') : ($product->on_sale ? 'checked' : '')}}>
													  	<span class="slider"></span>
													</label>
												</div>
												@if($errors->has('on_sale'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('on_sale') }}</strong>
				                                    </span>
				                                @endif
											</div>
						  				</div>
						  			</div>
						  		</div>
						  	</div>
						  	@if(!empty($product))
							  <div class="panel panel-default">
								  <div class="panel-body">
										<h4>Varijante</h4>
									  <product-variants :product-id="{{$product->id}}"></product-variants>
								  </div>
							  </div>
								<div class="panel panel-default">
							  		<div class="panel-body">
										<h4>Slike (1200 x 1200)</h4>
							  			<upload-resource-photos 
											target="product" 
											:target-id="{{$product->id}}"
											@if($product->photos->isNotEmpty()) 
												:initial-photos="{{$product->getPhotosJson()}}" 
											@endif
										></upload-resource-photos>
							  		</div>
							  	</div>
							@endif
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
						  		<div class="panel-body">
									<h4>Dostupnost</h4>
									<div class="form-group {{ $errors->has('in_stock') ? ' has-error' : '' }}">
										<label for="in_stock">Na skladištu</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="in_stock" value="0">
												<input type="checkbox" 
													id="in_stock" 
													name="in_stock"
													value="1" 
													{{empty($product) ? (old('in_stock') ? 'checked' : '') : ($product->in_stock ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> OPREZ Uklanjanjem proizvoda sa skladišta, proizvod nije moguće dodati u korpu a postojeći se uklanjaju iz korpe!</span> <br> <br>
										<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> OPREZ Dodavanjem proizvoda na skladište, šalju se obavijesti kupcima da je proizvod ponovno dostupan (samo onima koji su poslali zahtjev)!</span>
										@if($errors->has('in_stock'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('in_stock') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('published') ? ' has-error' : '' }}">
										<label for="published">Objavi proizvod</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="published" value="0">
												<input type="checkbox" 
													id="published" 
													name="published"
													value="1" 
													{{empty($product) ? (old('published') ? 'checked' : '') : ($product->published ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> OPREZ Uklanjanjem proizvoda sa liste objavljenig, proizvod nije vidljiv i ne može se dodati u korpu, ali se ne uklanja iz postojećih korpi!</span>
										@if($errors->has('published'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('published') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('free_shipping') ? ' has-error' : '' }}">
										<label for="free_shipping">Besplatna dostava</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="free_shipping" value="0">
												<input type="checkbox" 
													id="free_shipping" 
													name="free_shipping"
													value="1" 
													{{empty($product) ? (old('free_shipping') ? 'checked' : '') : ($product->free_shipping ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										@if($errors->has('free_shipping'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('free_shipping') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('featured') ? ' has-error' : '' }}">
										<label for="featured">Izdvojen</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="featured" value="0">
												<input type="checkbox" 
													id="featured" 
													name="featured"
													value="1" 
													{{empty($product) ? (old('featured') ? 'checked' : '') : ($product->featured ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Izdvojeni proizvodi se pojavljuju na početnoj stranici!</span>
										@if($errors->has('featured'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('featured') }}</strong>
		                                    </span>
		                                @endif
									</div>
						  		</div>
							</div>
							  
							<div class="panel panel-default">
								<div class="panel-body">
								  <h4>Organizacija</h4>
									<div class="form-group {{ $errors->has('categories') ? ' has-error' : '' }}">
										<label for="categories">Kategorija</label>
										<html-multiselect 
											placeholder="Odaberi kategorije"
											label="title"
											track-by="id"
											:options="{{json_encode($categories->toArray())}}"
											@if(!empty($product) && $product->categories->isNotEmpty())
												:selected="{{json_encode($product->categories->toArray())}}" 
											@endif
										></html-multiselect>
										<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Dovoljno je dodatni krajnju kategoriju. Roditelj kategorije se dodaju automatski.</span>
										@if($errors->has('categories'))
										  	<span class="help-block">
											  	<strong>{{ $errors->first('categories') }}</strong>
										  	</span>
									  	@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<hr>
			  	@if(!empty($product))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite arhivirati proizvod?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-product-form').submit();
						}">
	                    Arhiviraj
	                </button>

	                <form id="delete-product-form" 
	                	action="{{ route('backend.shop.products.destroy', ['product' => $product]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('product-form').submit();">
			  		{{empty($product) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection