@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.products.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Proizvodi</a>
				<h3>Proizvod {{empty($product) ? '' : '- ' . $product->fullName()}}</h3>
				<button class="btn btn-success btn-lg"
                    onclick="if (confirm('Da li zaista želite aktivirati proizvod?')) {
                    			event.preventDefault();
                             	document.getElementById('restore-product-form').submit();
					}">
                    Aktiviraj proizvod
                </button>

                <form id="restore-product-form" 
                	action="{{ route('backend.shop.products.restore', ['product' => $product]) }}" 
                	method="POST" 
                	style="display: none;">
                    {{ csrf_field() }}
                </form>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-default">
			  		<div class="panel-body">
			    		<div class="form-group">
					    	<label>Naziv</label>
					    	<p>{{$product->title}}</p>
					  	</div>

					  	<div class="form-group">
					    	<label>Opis</label>
					    	<div class="redactor-styles" style="border: 1px solid #999;padding: 15px;border-radius: 5px;">{!! $product->description !!}</div>
					  	</div>

					  	<div class="form-group">
					    	<label>Cijena</label>
					    	<p>{{$product->price}}</p>
					  	</div>

					  	<div class="form-group">
					    	<label>Snizena cijena</label>
					    	<p>{{$product->discount_price ?? '-'}}</p>
					  	</div>

					  	<div class="form-group">
					    	<label>Na snizenju</label>
					    	<p>{{$product->on_sale ? 'Da' : 'Ne'}}</p>
					  	</div>

						<div class="form-group">
					    	<label>Izdvojen</label>
					    	<p>{{$product->featured ? 'Da' : 'Ne'}}</p>
						</div>

					  	<div class="form-group">
					    	<label style="display: block;">Slike</label>
					    	@foreach($product->photos as $photo)
					    		<div class="background-img" style="width:100px; height:100px; margin:10px; display: inline-block; background-image: url('{{$photo->url('thumb')}}');"></div>
					    	@endforeach
					  	</div>
			  		</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection