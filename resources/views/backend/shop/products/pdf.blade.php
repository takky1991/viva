<html>
    <head>
        <title>{{$from}} - {{$to}}</title>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
        
            td, th {
                border: 1px solid #ddd;
                padding: 8px;
            }
        
            tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        
            th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #04AA6D;
                color: white;
            }
        </style>
    </head>
    <body>
        <p style="margin-bottom: 25px">
            <b>Period: {{$from}} - {{$to}}</b>
        </p>
        
        <p style="margin-bottom: 25px">
            Ukupno proizvoda: <b>{{$products->count()}}</b>
        </p>

        <p style="margin-bottom: 25px">
            Ukupno prodanih kutija (vrijednost): <b>{{$products->sum('order_products_count')}} ({{formatPrice($products->sum('totalPrice'))}})</b>
        </p>
        
        <table>
            <thead>
                <tr>
                    <th>Naziv</th>
                    <th>Br. kutija (vrijednost)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{$product->fullName()}}</td>
                        <td>
                            @if($product->order_products_count)
                                {{$product->order_products_count}} ({{formatPrice($product->totalPrice)}})
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                @endforeach 
            </tbody>
        </table>
    </body>
</html>