<tr>
	<td>
		<div class="background-img" style="background-image: url('{{$heroArticle->photo ? $heroArticle->photo->url('thumb') : asset('images/no-photo-available.png')}}');border-radius:50%;width: 50px;height: 50px;"></div>
	</td>
	<td>
		{{$heroArticle->title}}
    </td>
    <td>
		{{$heroArticle->subtitle}}
	</td>
	<td>
		{{$heroArticle->brand?->name}}
	</td>
	<td>
		{{$heroArticle->button_text}}
	</td>
	<td>
		@if($heroArticle->published)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.hero_articles.edit', ['heroArticle' => $heroArticle])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
	</td>
</tr>