@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.shop.hero_articles.create')}}">Novi Hero članak</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
                                    <th></th>
						        	<th>Naslov</th>
						        	<th>Podnaslov</th>
						        	<th>Brend</th>
						        	<th>Tekst za dugme</th>
						        	<th>Objavljeno</th>
                                    <th></th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@each('backend/shop/hero-articles/partials/table-row', $heroArticles, 'heroArticle', 'backend/shop/hero-articles/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
			{{ $heroArticles->links() }}
		</div>
	</div>
@endsection