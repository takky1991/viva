@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.hero_articles.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Hero članci</a>
				@if(empty($heroArticle))
					<h3>Dodaj Hero članak</h3>
				@else
					<h3>Uredi Hero članak {{empty($heroArticle) ? '' : '- ' . $heroArticle->title}}</h3>
					<button class="btn btn-success btn-lg"
	                    onclick="if (confirm('Da li zaista želite objaviti/sakriti hero članak?')) {
	                    			event.preventDefault();
	                             	document.getElementById('publish-hero-article-form').submit();
						}">
	                    {{$heroArticle->published ? 'Sakrij' : 'Objavi'}}
	                </button>

	                <form id="publish-hero-article-form" 
	                	action="{{ route('backend.shop.hero_articles.togglePublish', ['heroArticle' => $heroArticle]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                    {{ csrf_field() }}
	                </form>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="hero-article-form"
					method="POST" 
					action="{{ empty($heroArticle) ? route('backend.shop.hero_articles.store') : route('backend.shop.hero_articles.update', ['heroArticle' => $heroArticle])}}">
					{{ csrf_field() }}

					@if(!empty($heroArticle))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
								    	<label for="title">Naslov</label>
										<span class="help-block">
											<i class="fa fa-info-circle" aria-hidden="true"></i> Ne koristiti sva velika slova!
										</span>
								    	<input class="form-control" 
								    		type="text" 
								    		id="title" 
								    		name="title" 
								    		placeholder="Npr. Cerave čistači -15%" 
								    		value="{{empty($heroArticle) ? old('title') : $heroArticle->title }}">
								    	@if($errors->has('title'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('title') }}</strong>
		                                    </span>
		                                @endif
                                    </div>
                                      
                                    <div class="form-group {{ $errors->has('subtitle') ? ' has-error' : '' }}">
								    	<label for="subtitle">Podnaslov</label>
										<span class="help-block">
											<i class="fa fa-info-circle" aria-hidden="true"></i> Ne koristiti sva velika slova!
										</span>
								    	<textarea class="form-control" 
								    		id="subtitle" 
								    		name="subtitle" 
											placeholder="Npr. Tvoje omiljene CeraVe umivalice na popustu 15% do 30.09."
								    		rows="10">{{empty($heroArticle) ? old('subtitle') : $heroArticle->subtitle}}</textarea>
								    	@if($errors->has('subtitle'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('subtitle') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
								  		<label for="brand_id">Brend</label>
								  		<select class="form-control" id="brand_id" name="brand_id">
										  	<option value="" selected>Bez brenda</option>
								  			@foreach($brands as $item)
										  		<option value="{{$item->id}}" {{empty($heroArticle) ? (old('brand_id') == $item->id ? 'selected' : '') : ($heroArticle->brand_id == $item->id ? 'selected' : '')}}>
										  			{{$item->name}}
										  		</option>
										  	@endforeach
										</select>
										@if($errors->has('brand_id'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('brand_id') }}</strong>
		                                    </span>
		                                @endif
                                    </div>
                                    
                                    <div class="form-group {{ $errors->has('button_text') ? ' has-error' : '' }}">
								    	<label for="button_text">Tekst za dugme</label>
										<span class="help-block">
											<i class="fa fa-info-circle" aria-hidden="true"></i> Ne koristiti sva velika slova!
										</span>
								    	<input class="form-control" 
								    		type="text" 
								    		id="button_text" 
								    		name="button_text" 
								    		placeholder="Npr. Pogledaj proizvode" 
								    		value="{{empty($heroArticle) ? old('button_text') : $heroArticle->button_text }}">
								    	@if($errors->has('button_text'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('button_text') }}</strong>
		                                    </span>
		                                @endif
									</div>
									
									<div class="form-group {{ $errors->has('button_url') ? ' has-error' : '' }}">
								    	<label for="button_url">URL za dugme</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="button_url" 
								    		name="button_url" 
								    		placeholder="Pogledaj proizvode" 
								    		value="{{empty($heroArticle) ? old('button_url') : $heroArticle->button_url }}">
								    	@if($errors->has('button_url'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('button_url') }}</strong>
		                                    </span>
		                                @endif
                                    </div>

									<div class="form-group {{ $errors->has('background_color') ? ' has-error' : '' }}">
								    	<label for="background_color">Boja pozadine</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="background_color" 
								    		name="background_color" 
								    		placeholder="#009900" 
								    		value="{{empty($heroArticle) ? old('background_color') : $heroArticle->background_color }}">
								    	@if($errors->has('background_color'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('background_color') }}</strong>
		                                    </span>
		                                @endif
                                    </div>

									<div class="form-group {{ $errors->has('text_color') ? ' has-error' : '' }}">
								    	<label for="text_color">Boja teksta</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="text_color" 
								    		name="text_color" 
								    		placeholder="#000000" 
								    		value="{{empty($heroArticle) ? old('text_color') : $heroArticle->text_color }}">
								    	@if($errors->has('text_color'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('text_color') }}</strong>
		                                    </span>
		                                @endif
                                    </div>
						  		</div>
							</div>
						</div>
						@if(!empty($heroArticle))
						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-body">
									<h4>Slika (1110 x 800) desktop</h4>
									<upload-resource-photos 
										target="hero_article" 
										:target-id="{{$heroArticle->id}}"
										preferred-style="thumb|hero_article|hero_article_mobile"
										@if($heroArticle->photo) 
											:initial-photos="{{$heroArticle->getPhotosJson()}}" 
										@endif
									></upload-resource-photos>

									<h4>Slika (1110 x 800) mobile</h4>
									<upload-resource-photos 
										target="hero_article" 
										:target-id="{{$heroArticle->id}}"
										preferred-device="mobile"
										preferred-style="thumb|hero_article|hero_article_mobile"
										@if($heroArticle->mobilePhoto) 
											:initial-photos="{{$heroArticle->getMobilePhotosJson()}}" 
										@endif
									></upload-resource-photos>
								</div>
							</div>
						</div>
						@endif
					</div>
				</form>
				<hr>
			  	@if(!empty($heroArticle))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati hero članak?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-hero-article-form').submit();
						}">
	                    Obriši
	                </button>

	                <form id="delete-hero-article-form" 
	                	action="{{ route('backend.shop.hero_articles.destroy', ['heroArticle' => $heroArticle]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('hero-article-form').submit();">
			  		{{empty($heroArticle) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection