@extends('backend.layouts.shop')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.shop.categories.create')}}">Nova Kategorija</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
				<li role="presentation" class="{{request()->route()->getName() == 'backend.shop.categories.index' ? 'active' : ''}}"><a href="{{route('backend.shop.categories.index')}}">Za menu</a></li>
				<li role="presentation" class="{{request()->route()->getName() == 'backend.shop.categories.index.other' ? 'active' : ''}}"><a href="{{route('backend.shop.categories.index.other')}}">Ostalo</a></li>
			</ul>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
									<th></th>
						        	<th>Naziv</th>
						        	<th>Za menu</th>
						        	<th>Istaknuto</th>
						      	</tr>
						    </thead>
						    <tbody>
								@each('backend/shop/categories/partials/table-row', $categories, 'category', 'backend/shop/categories/partials/empty')
						    </tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection