@extends('backend.layouts.shop')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.shop.categories.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Sve Kategorije</a>
				@if(empty($category))
					<h3>Dodaj Kategoriju</h3>
				@else
					<h3>Uredi Kategoriju {{empty($category) ? '' : '- ' . $category->title}}</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="category-form" 
					method="POST" 
					action="{{ empty($category) ? route('backend.shop.categories.store') : route('backend.shop.categories.update', ['category' => $category])}}">
					{{ csrf_field() }}

					@if(!empty($category))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
								    	<label for="title">Naziv</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="title" 
								    		name="title" 
								    		placeholder="Masna koza" 
								    		value="{{empty($category) ? old('title') : $category->title }}">
								    	@if($errors->has('title'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('title') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
								  		<label for="parent_id">Roditelj Kategorija</label>
								  		<select class="form-control" id="parent_id" name="parent_id">
										  	<option value="" selected>Bez roditelja</option>
								  			@foreach($categories as $item)
										  		<option value="{{$item->id}}" {{empty($category) ? (old('parent_id') == $item->id ? 'selected' : '') : ($category->parent_id == $item->id ? 'selected' : '')}}>
										  			{{$item->title}}
										  		</option>
										  	@endforeach
										</select>
										@if($errors->has('parent_id'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('parent_id') }}</strong>
		                                    </span>
		                                @endif

										@if(!empty($category))
											<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Ako pridružite kategoriju nekom roditelju, svi proizvodi ove kategorije će postati i proizvodi roditelj kategorije.</span> <br>
											<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Ako uklonite roditelj kategoriju ili je promijenite, proizvodi ove kategorije će ostati pridruženi staroj rotiljek kategoriji.</span>
										@endif
									</div>

								  	<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
								    	<label for="description">Opis</label>
								    	<textarea class="form-control" 
								    		id="description" 
								    		name="description" 
								    		rows="10">{{empty($category) ? old('description') : $category->description}}</textarea>
								    	@if($errors->has('description'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('description') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
						  		<div class="panel-body">
									<h4>Organizacija</h4>

									<div class="form-group {{ $errors->has('menu') ? ' has-error' : '' }}">
										<label for="menu">Za meni</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="menu" value="0">
												<input type="checkbox" 
													id="menu" 
													name="menu"
													value="1" 
													{{empty($category) ? (old('menu') ? 'checked' : '') : ($category->menu ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										@if($errors->has('menu'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('menu') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group {{ $errors->has('featured') ? ' has-error' : '' }}">
										<label for="featured">Istaknuto</label>
								  		<div>
											<label class="switch">
											  	<input type="hidden" name="featured" value="0">
												<input type="checkbox" 
													id="featured" 
													name="featured"
													value="1" 
													{{empty($category) ? (old('featured') ? 'checked' : '') : ($category->featured ? 'checked' : '')}}>
												<span class="slider"></span>
										  </label>
										</div>
										@if($errors->has('featured'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('featured') }}</strong>
		                                    </span>
		                                @endif
									</div>
						  		</div>
							</div>
							  
							@if(!empty($category))
								<div class="panel panel-default">
									<div class="panel-body">
										<h4>Slika (1200 x 600)</h4>
										<upload-resource-photos 
											target="category" 
											:target-id="{{$category->id}}"
											@if($category->photo) 
												:initial-photos="{{$category->getPhotosJson()}}" 
											@endif
										></upload-resource-photos>
									</div>
								</div>
							@endif
						</div>
					</div>
				</form>
				<hr>
			  	{{-- @if(!empty($category))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati kategoriju?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-category-form').submit();
						}">
	                    Obriši
	                </button>

	                <form id="delete-category-form" 
	                	action="{{ route('backend.shop.categories.destroy', ['category' => $category]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif --}}
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('category-form').submit();">
			  		{{empty($category) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection