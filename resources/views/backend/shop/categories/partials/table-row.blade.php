<tr>
	<td>
		<div class="background-img" style="background-image: url('{{$category->photo ? $category->photo->url('thumb') : asset('images/no-photo-available.png')}}');border-radius:50%;width: 50px;height: 50px;"></div>
	</td>
	<td>
		<a href="{{route('backend.shop.categories.edit', ['category' => $category])}}">
			<?php $index = empty($index) ? 0 : $index; ?>
			@for ($i = 0; $i < $index; $i++)
				{!! '&nbsp; &nbsp; &nbsp;' !!}
			@endfor
			{{$category->title}}
		</a>
	</td>
	<td>
		@if($category->menu)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
	</td>
	<td>
		@if($category->featured)
			<span class="label label-success">DA</span>
		@else
			<span class="label label-danger">NE</span>
		@endif
		<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.shop.categories.edit', ['category' => $category])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
	</td>
</tr>

@if($category->relationLoaded('menuChildren'))
	@if($category->menuChildren->isNotEmpty())
		@foreach($category->menuChildren as $child)
			@include('backend/shop/categories/partials.table-row', ['category' => $child, 'index' => $index + 1])
		@endforeach
	@endif
@elseif($category->relationLoaded('children'))
	@if($category->children->isNotEmpty())
		@foreach($category->children as $child)
			@include('backend/shop/categories/partials.table-row', ['category' => $child, 'index' => $index + 1])
		@endforeach
	@endif
@endif