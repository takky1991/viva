@extends('backend.layouts.admin')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<a class="btn btn-success btn-lg" href="{{route('backend.users.create')}}">Novi Korisnik</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
				  		<table class="table table-hover">
				    		<thead>
						      	<tr>
						        	<th>Ime i Prezime</th>
						        	<th>Uloga</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	@forelse($users as $user)
						    	<tr>
									<td>
										<a href="{{route('backend.users.edit', ['user' => $user])}}">
											{{$user->first_name}} {{$user->last_name}}
										</a>
									</td>
									<td>
										{{$user->getRoleNames()->first()}}
										<a class="btn btn-primary btn-xs" style="float: right;" href="{{route('backend.users.edit', ['user' => $user])}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uredi</a>
									</td>
								</tr>
								@empty
								<tr>
									<td>Nema rezultata</td>
								</tr>
								@endforelse
						    </tbody>
				  		</table>
				  		{{ $users->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection