@extends('backend.layouts.admin')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="row">
			<div class="col-xs-12">
				<a href="{{route('backend.users.index')}}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Svi Korisnici</a>
				@if(empty($user))
					<h3>Dodaj Korisnika</h3>
				@else
					<h3>Uredi Korisnika {{empty($user) ? '' : '- ' . $user->first_name . ' ' . $user->last_name}}</h3>
				@endif
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<form id="user-form" 
					method="POST" 
					action="{{ empty($user) ? route('backend.users.store') : route('backend.users.update', ['user' => $user])}}">
					{{ csrf_field() }}

					@if(!empty($user))
						<input type="hidden" name="_method" value="PUT">
					@endif

					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
						  		<div class="panel-body">
						    		<div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
								    	<label for="first_name">Ime</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="first_name" 
								    		name="first_name" 
								    		placeholder="John" 
								    		value="{{empty($user) ? old('first_name') : $user->first_name }}">
								    	@if($errors->has('first_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('first_name') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
								    	<label for="last_name">Prezime</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="last_name" 
								    		name="last_name" 
								    		placeholder="Doe" 
								    		value="{{empty($user) ? old('last_name') : $user->last_name }}">
								    	@if($errors->has('last_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('last_name') }}</strong>
		                                    </span>
		                                @endif
								  	</div>

								  	<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
								    	<label for="email">Email</label>
								    	<input class="form-control" 
								    		type="text" 
								    		id="email" 
								    		name="email" 
								    		placeholder="john_doe@gmail.com" 
								    		value="{{empty($user) ? old('email') : $user->email }}">
								    	@if($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
								  	</div>
						  		</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
						  		<div class="panel-body">
						  			<div class="form-group {{ $errors->has('role_name') ? ' has-error' : '' }}">
								  		<label for="role_name">Uloga Korisnika</label>
								  		<select class="form-control" id="role_name" name="role_name">
								  			@foreach($roles as $role)
										  		<option value="{{$role->name}}" {{empty($user) ? (old('role_name') == $role->name ? 'selected' : '') : ($user->hasRole($role->name) ? 'selected' : '')}}>
										  			{{$role->name}}
										  		</option>
										  	@endforeach
										</select>
										@if($errors->has('role_name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('role_name') }}</strong>
		                                    </span>
		                                @endif
									</div>
						  		</div>
						  	</div>
						</div>
					</div>
				</form>
				<hr>
			  	@if(!empty($user))
				  	<button class="btn btn-danger btn-lg"
	                    onclick="if (confirm('Da li zaista želite obrisati korisnika?')) {
	                    			event.preventDefault();
	                             	document.getElementById('delete-user-form').submit();
	                    }">
	                    Obriši
	                </button>

	                <form id="delete-user-form" 
	                	action="{{ route('backend.users.destroy', ['user' => $user]) }}" 
	                	method="POST" 
	                	style="display: none;">
	                	<input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field() }}
	                </form>
			  	@endif
			  	<button class="btn btn-success btn-lg" 
			  		type="submit"
			  		onclick="event.preventDefault();
	                        document.getElementById('user-form').submit();">
			  		{{empty($user) ? 'Kreiraj' : 'Sačuvaj'}}
			  	</button>
			</div>
		</div>
	</div>
</div>
@endsection