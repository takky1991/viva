@extends('layouts.app')

@section('content')
<div style="background: #2d2c2c; padding-bottom:100px;">
    <div class="container-xl locations">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <img class="mb-5 w-100 m-auto" src="{{asset('images/novelius.png')}}" alt="Novelius">
                @if(session()->has('success'))
                    <div class="alert alert-info" role="alert">
                        {{session('success')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <form class="form-horizontal" method="POST" action="{{ route('novelius_black_friday_post') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label style="color:white" for="first_name">Ime</label>
                        <input style="
                            color:white; 
                            background:#2d2c2c;
                            box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                            -webkit-box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                            -moz-box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                        " id="first-name" type="text" class="input-field @if($errors->has('first_name')) invalid @endif" name="first_name" value="{{ old('first_name') }}" placeholder="Unesite ime" autofocus>
                        @if ($errors->has('first_name'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('first_name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label style="color:white" for="email">E-mail</label>
                        <input style="
                            color:white; 
                            background:#2d2c2c;
                            box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                            -webkit-box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                            -moz-box-shadow: 0px 0px 20px -5px rgba(255,255,255,0.75);
                        " id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ old('email') }}" placeholder="Unesite e-mail">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="terms_accepted" name="terms_accepted" {{ old('terms_accepted') ? 'checked' : '' }}>
                            <label class="custom-control-label @if($errors->has('terms_accepted')) invalid @endif" for="terms_accepted"><span style="color:white" id="customRadioLabel1">Prihvatam</span> <a href="{{route('privacy_policy')}}" class="link" target="_blank">Uvjete korištenja</a></label>
                        </div>
                        @if ($errors->has('terms_accepted'))
                            <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                {{ $errors->first('terms_accepted') }}
                            </div>
                        @endif
                    </div>

                    <br>
                    <button id="register-button" type="submit" class="btn btn-primary w-100">Prijavi se <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection