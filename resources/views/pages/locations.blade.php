@extends('layouts.app')

@section('content')
<div class="mt-5">
	<div class="container-xl locations">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
                <p class="locations-subtitle text-center mt-3 mb-5">Potražite naše apoteke. Pronađite informacije o radnim vremenima apoteka, njihovim lokacijama i kontakt informacije.</p>
                <b>PZU Apoteka Viva</b>
                <ul>
                    <li>&#x2022; Adresa: Mala Lisa bb, 77220 Cazin, Bosna i Hercegovina</li>
                    <li>&#x2022; Telefon: +387 (0)37 536 051</li>
                    <li>&#x2022; Email: {{config('app.email')}}</li>
                    <li>&#x2022; ID broj: 4263338490009</li>
                    <li>&#x2022; PDV broj: 263338490009</li>
                </ul>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="alert alert-light text-center overflow-hidden p-0 mb-4">
                            <iframe frameborder="0" style="border:0" width="100%"
                                src="https://www.google.com/maps/embed/v1/place?key={{config('maps.api_key')}}&q=place_id:ChIJTTMviO9IYUcRDYkZWEDmylQ">
                            </iframe>
                            <div class="p-3">
                                <h3 class="mb-2">Centralna Apoteka <br>VIVA Cazin</h3>
                                <p class="mb-4">
                                    Mala Lisa bb, 77220 Cazin <br>
                                    Bosna i Hercegovina
                                </p>
                                <p class="mb-4">
                                    <b class="strong-font-color">Telefon:</b> <a href="tel:+38737536051">+387 37 536 051</a>
                                </p>
                                <p class="mb-2">
                                    <b class="strong-font-color">Radno vrijeme:</b> <br> 
                                    Ponedjeljak - Subota od 07h do 22h <br>
                                    Nedjelja od 08h do 21h
                                </p>
                                <p>
                                    <b class="strong-font-color">Zimsko radno vrijeme:</b> <br>
                                    Ponedjeljak - Subota od 07h do 21h <br>
                                    Nedjelja od 08h do 21h
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="alert alert-light text-center overflow-hidden p-0 mb-4">
                            <iframe frameborder="0" style="border:0" width="100%"
                                src="https://www.google.com/maps/embed/v1/place?key={{config('maps.api_key')}}&q=place_id:ChIJV3UUxHtJYUcRSCzTwK2IF7g">
                            </iframe>
                            <div class="p-3">
                                <h3 class="mb-2">Apoteka VIVA <br> Cazin TC Robot</h3>
                                <p class="mb-4">
                                    Ćoralići bb TC Robot, Cazin 77220 <br>
                                    Bosna i Hercegovina
                                </p>
                                <p class="mb-4">
                                    <b class="strong-font-color">Telefon:</b> <a href="tel:+38737536110">+387 37 536 110</a>
                                </p>
                                <p class="mb-2">
                                    <b class="strong-font-color">Radno vrijeme:</b> <br> 
                                    Ponedjeljak - Subota od 08h do 21h <br>
                                    Nedjelja od 08h do 14h
                                </p>
                                <p>
                                    <b class="strong-font-color">Zimsko radno vrijeme:</b> <br>
                                    Ponedjeljak - Subota od 08h do 21h <br>
                                    Nedjelja od 08h do 14h
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="alert alert-light text-center overflow-hidden p-0 mb-4">
                            <iframe frameborder="0" style="border:0" width="100%"
                                src="https://www.google.com/maps/embed/v1/place?key={{config('maps.api_key')}}&q=place_id:ChIJsct_FHBJYUcRS08xjsAjk1U">
                            </iframe>
                            <div class="p-3">
                                <h3 class="mb-2">Apoteka VIVA <br> Cazin TC Ikanović</h3>
                                <p class="mb-4">
                                    Jošani bb, Cazin 77220 <br>
                                    Bosna i Hercegovina
                                </p>
                                <p class="mb-4">
                                    <b class="strong-font-color">Telefon:</b> <a href="tel:+38737518110">+387 37 518 110</a>
                                </p>
                                <p class="mb-2">
                                    <b class="strong-font-color">Radno vrijeme:</b> <br> 
                                    Ponedjeljak - Subota od 07h do 22h <br>
                                    Nedjelja od 08h do 16h
                                </p>
                                <p>
                                    <b class="strong-font-color">Zimsko radno vrijeme:</b> <br>
                                    Ponedjeljak - Subota od 07h do 22h <br>
                                    Nedjelja od 08h do 16h
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="alert alert-light text-center overflow-hidden p-0 mb-4">
                            <iframe frameborder="0" style="border:0" width="100%"
                                src="https://www.google.com/maps/embed/v1/place?key={{config('maps.api_key')}}&q=place_id:ChIJzR1eEWytZkcRTdK6ZtZeFWY">
                            </iframe>
                            <div class="p-3">
                                <h3 class="mb-2">Apoteka VIVA <br> Velika Kladuša</h3>
                                <p class="mb-4">
                                    Zuhdije Žalića bb Križ, Velika Kladuša 77230 <br>
                                    Bosna i Hercegovina
                                </p>
                                <p class="mb-4">
                                    <b class="strong-font-color">Telefon:</b> <a href="tel:+38737770161">+387 37 770 161</a>
                                </p>
                                <p class="mb-2">
                                    <b class="strong-font-color">Radno vrijeme:</b> <br> 
                                    Ponedjeljak - Subota od 07h do 21h <br>
                                    Nedjelja od 08h do 20h
                                </p>
                                <p>
                                    <b class="strong-font-color">Zimsko radno vrijeme:</b> <br>
                                    Ponedjeljak - Subota od 07h do 21h <br>
                                    Nedjelja od 08h do 20h
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="alert alert-light text-center overflow-hidden p-0 mb-4">
                            <iframe frameborder="0" style="border:0" width="100%"
                                src="https://www.google.com/maps/embed/v1/place?key={{config('maps.api_key')}}&q=place_id:ChIJkxk0x6lFYUcRcHGhgW-RiLI">
                            </iframe>
                            <div class="p-3">
                                <h3 class="mb-2">Apoteka VIVA <br> Gata - Bihać</h3>
                                <p class="mb-4">
                                    Gata bb, Bihać 77000 <br>
                                    Bosna i Hercegovina
                                </p>
                                <p class="mb-4">
                                    <b class="strong-font-color">Telefon:</b> <a href="tel:+38737370037">+387 37 370 037</a>
                                </p>
                                <p class="mb-2">
                                    <b class="strong-font-color">Radno vrijeme:</b> <br> 
                                    Ponedjeljak - Subota od 08h do 16h <br>
                                    Nedjelja neradna
                                </p>
                                <p>
                                    <b class="strong-font-color">Zimsko radno vrijeme:</b> <br>
                                    Ponedjeljak - Subota od 08h do 16h <br>
                                    Nedjelja neradna
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection