@extends('layouts.app')

@section('content')
<div style="background: #2d2c2c; padding-bottom:100px;">
    <div class="container-xl locations pt-3">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="alert alert-info" role="alert">
                    Uspješno ste se prijavili za najveći BLACK FRIDAY POPUST na NOVELIUS MEDICAL 🔥
                </div>                
                <img class="mb-5 w-100 m-auto" src="https://cdn.apotekaviva24.ba/email/public/Copy+of+Mejl-SLIKA+(1).png" alt="Novelius">
            </div>
        </div>
    </div>
</div>
@endsection