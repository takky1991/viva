@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KO IMA PRAVO NA POVRAT ILI ZAMJENU?
                        </h2>
                        <p class="mb-5">
                            Pravo na povrat ili zamjenu proizvoda imaju svi kupci web shopa na sve proizvode.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KO SNOSI TROŠKOVE POVRATA ILI ZAMJENE?
                        </h2>
                        <p class="mb-5">
                            Ako smo Vam poslali pogrešan proizvod, proizvod kojem je istekao rok ili oštećen proizvod, rado ćemo snositi troškove povrata ili zamjene.
                            U svim drugim slučajevima, troškove povrata ili zamjene snose kupci.  
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KOJI JE ROK ZA POVRAT ILI ZAMJENU?
                        </h2>
                        <p class="mb-5">
                            Rok za povrat ili zamjenu je 8 dana od dana primanja proizvoda.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAKO MOGU ZATRAŽITI POVRAT ILI ZAMJENU?
                        </h2>
                        <p class="mb-5">
                            Povrat ili zamjenu možete zatražiti putem emaila ili telefonski.
                        <p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            UKOLIKO VRATIM PROIZVOD, DA LI ĆU DOBITI PUNI IZNOS KOJI SAM PLATIO/LA?
                        </h2>
                        <p class="mb-5">
                            Ukoliko pregledom proizvoda utvrdimo da je vraćena roba neoštećena i u originalnoj ambalaži uz odgovarajući fiskalni račun, vratit ćemo Vam puni iznos novca. 
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA AKO JE VRAĆENA ROBA OŠTEĆENA?
                        </h2>
                        <p class="mb-5">
                            Ako roba nije u odgovarajućem stanju (korištena i oštećena ili je originalna ambalaža oštećena) ili nemate fiskalni račun, nismo u mogućnosti izvršiti povrat novca. 
                            Također snosite troškove poštarine kako bi Vam roba ponovno bila poslana na kućnu adresu. 
                        </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection