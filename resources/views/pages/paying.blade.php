@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
						<h2 class="mb-2 font-weight-bold" style="font-size: 20px">
							KAKO MOGU PLATITI NARUDŽBU?
						</h2>
						<p class="mb-3">Vašu narudžbu možete platiti na jedan od sljedećih načina:</p>
						<p class="mb-3">
							<b>&#x2022; Plaćanje bankovnom karticom</b> - omogućuje kupcu da iznos narudžbe plati <b>Mastercard®</b>, <b>Maestro®</b> ili <b>Visa</b> karticom bilo koje banke.
							Plaćanje se vrši preko Monri webpay servisa. Web shop {{config('app.domain')}} ne pohranjuje brojeve kreditnih kartica i brojevi nisu dostupni neovlaštenim osobama.
							<div class="text-center my-3">
								<a href="https://www.mastercard.com" target="_blank">
									<img class="m-2" data-src="{{asset('images/cards/mastercard.svg')}}" title="Mastercard" alt="Mastercard logo" style="height: 25px;" v-lazy-img>
								</a>
								<a href="https://brand.mastercard.com/brandcenter/more-about-our-brands.html" target="_blank">
									<img class="m-2" data-src="{{asset('images/cards/maestro.svg')}}" title="Maestro" alt="Maestro logo" style="height: 25px;" v-lazy-img>
								</a>
								<a href="https://www.visa.co.uk/about-visa/visa-in-europe.html" target="_blank">
									<img class="m-2" data-src="{{asset('images/cards/visa.svg')}}" title="Visa" alt="Visa logo" style="height: 25px;" v-lazy-img>
								</a>
								<a href="https://www.mastercard.ba/bs-ba/korisnici/podrska/sigurnost-i-zastita/identity-check.html" target="_blank">
									<img class="m-2" data-src="{{asset('images/cards/mastercard_id_check.svg')}}" title="Mastercard ID Check" alt="Mastercard ID Check logo" style="height: 25px;" v-lazy-img>
								</a>
								<a href="https://www.visa.co.uk/pay-with-visa/featured-technologies/verified-by-visa.html" target="_blank" onclick="javascript:window.open('https://www.visaeurope.com/making-payments/verified-by-visa/','Verified by Visa','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=540, height=540'); return false;">
									<img class="m-2" data-src="{{asset('images/cards/visa_secure.svg')}}" title="Visa Secure" alt="Visa Secure logo" style="height: 25px;" v-lazy-img>
								</a>
								<a href="https://monri.com" target="_blank">
									<img class="m-2" data-src="{{asset('images/cards/monri_pay_web.png')}}" title="Monri" alt="Monri logo" style="height: 43px" v-lazy-img>
								</a>
							</div>
						</p>
						<p class="mb-3">
							<b>&#x2022; Plaćanje pouzećem</b> - omogućuje kupcu da iznos narudžbe plati dostavljaču prilikom same dostave na navedenu adresu. 
							Plaćanje pouzećem se vrši isključivo u gotovini odnosno iznos nije moguće platiti kreditnom karticom. 
							U slučaju da primatelj nije u mogućnosti zaprimiti pošiljku u vrijeme dostave, zbog odsutnosti, dostavljač ostavlja obavijest.
						</p>
						<p class="mb-3">
							<b>&#x2022; Plaćanje u poslovnici</b> - ako ste odlučili preuzeti robu u nekoj od Viva apoteka moguće je platiti u poslovnici gotovinom ili kartično.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection