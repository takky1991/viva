@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">						
                        <p class="mb-5">
                            Kako bi internetska stranica radila ispravno te kako bi se mogla unaprjeđivati u svrhu poboljšanja vašeg iskustva 
                            pregledavanja, potrebno je minimalnu količinu informacija (cookies) spremiti na vaše računalo. 
                            Više od 90% internetskih stranica koristi praksu kolačića. 
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA JE KOLAČIĆ?
                        </h2>
                        <p class="mb-5">
                            Kolačić je informacija spremljena na Vaše računalo od strane web stranice koju posjetite. 
                            Kolačići obično spremaju Vaše postavke, postavke za web stranicu, kao što su preferirani jezik ili adresa. 
                            Kasnije, kada opet otvorite istu web stranicu internet preglednik šalje natrag kolačiće koji pripadaju toj stranici. 
                            Ovo omogućava stranici da prikaže informacije prilagođene Vašim potrebama. 
                            Kolačići mogu spremati širok pojas informacija uključujući osobne informacije (kao što je Vaše ime ili e-mail adresa). 
                            Ipak, ova informacija može biti spremljena jedino ako Vi to omogućite - web stranice ne mogu dobiti pristup informacijama koji im Vi niste dali 
                            i ne mogu pristupiti drugim datotekama na Vašem računalu ili mobilnom uređaju. 
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAKO ONEMOGUĆITI KOLAČIĆE?
                        </h2>
                        <p class="mb-5">
                            Isključivanjem kolačića ne dopuštate pohranjivanje istih na vlastitom računalu. 
                            Postavke kolačića mogu se konfigurirati i mijenjati u izabranom internetskom pregledniku. 
                            Kako biste vidjeli postavke, odaberite preglednik koji koristite (Chrome, Firefox, Internet Explorer 9, Internet Explorer 7 i 8 te Operu ili Safari (stranice na engleskom jeziku)). 
                            Ukoliko onemogućite kolačiće, utoliko nećete biti u mogućnosti koristiti određene funkcionalnosti internetskih stranica.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA SU PRIVREMENI KOLAČIĆI?
                        </h2>
                        <p class="mb-5">
                            Privremeni kolačići ili kolačići sesije uklanjaju se s računala po zatvaranju internet preglednika. Uz pomoć njih web-mjesta pohranjuju privremene podatke, poput proizvoda u korpi za kupovinu.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA SU STALNI KOLAČIĆI?
                        </h2>
                        <p class="mb-5">
                            Stalni, ili spremljeni, kolačići ostaju na Vašem osobnom računalu i nakon zatvaranja internetskog preglednika. 
                            Uz pomoć ovih kolačića internetske stranice pohranjuju podatke kako bi Vam se olakšalo korištenje. 
                            Npr. pomoću njih web-mjesta pohranjuju podatke, kao što su ime za prijavu i lozinka, tako da se ne morate prijavljivati prilikom svakog posjeta određenom mjestu.
                            Stalni kolačići ostat će zabilježeni na računalu danima, mjesecima ili godinama.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA SU KOLAČIĆI OD PRVE STRANE?
                        </h2>
                        <p class="mb-5">
                            Kolačići od prve strane dolaze s web-mjesta koje gledate, a mogu biti stalni ili privremeni. Pomoću njih web-mjesta mogu pohraniti podatke koje će ponovo koristiti prilikom sljedeće posjete web-mjestu.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA SU KOLAČIĆI OD TREĆE STRANE?
                        </h2>
                        <p class="mb-5">
                            Kolačići treće strane na Vaše računalo dolaze s drugih web mjesta koje se nalaze na internetskoj stranici koju pregledavate. 
                            Pomoću tih kolačića web-mjesta mogu pratiti korištenje Interneta u marketinške svrhe.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KORISTI LI OVA WEB STRANICA KOLAČIĆE?
                        </h2>
                        <p class="mb-5">
                            Da, ova internetska stranica koristi kolačiće kako bi Vam osigurala jednostavnije i bolje korisničko iskustvo.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAKVE KOLAČIĆE KORISTI OVA WEB STRANICA I ZAŠTO?
                        </h2>
                        <p class="mb-5">
                            Privremene kolačiće (Session cookies) – riječ je o kolačićima koji će se automatski izbrisati prilikom zatvaranja internetskog preglednika u kojem radite
                            Stalne kolačiće (Persistent cookies) – riječ je o kolačićima koji će ostati ''zabilježeni'' u vašem internetskom pregledniku dok ne isteknu ili ih sami ručno ne izbrišete. 
                            Prikupljene informacije su anonimne, a ne uključuju vaše privatne podatke
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            DA LI NA WEB STRANICI IMA KOLAČIĆA TREĆE STRANE?
                        </h2>
                        <p class="mb-5">
                            Ima nekoliko vanjskih servisa koji korisniku spremaju limitirane kolačiće. 
                            Ovi kolačići nisu postavljeni od strane ove internetske stranice, ali neki služe za normalno funkcioniranje određenih mogućnosti koje korisnicima olakšavaju pristup sadržaju. 
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            MJERENJE POSJEĆENOSTI
                        </h2>
                        <p class="mb-5">
                            Ova web stranica koristi servise za mjerenje posjećenosti, to je: Google Analytics. 
                            Niti temeljem ovako prikupljenih podataka nije moguća identifikacija korisnika, a prikupljeni podaci se koriste isključivo u statističke svrhe. 
                            Ako želite onemogućiti da vam navedeni servis sprema kolačiće, možete to zabraniti na <a href="https://tools.google.com/dlpage/gaoptout" class="link" target="_blank">ovom linku</a>.
                        </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection