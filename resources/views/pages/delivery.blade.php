@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KOJA JE CIJENA DOSTAVE?
                        </h2>
                        <p class="mb-5">
                            Cijene dostave su: <br>
                            @foreach ($shippingMethods as $shippingMethod)
                                &bull; {{$shippingMethod->name}} {{formatPrice($shippingMethod->price)}}<br>
                            @endforeach
                            Da bi ostvarili besplatnu dostavu potrebno je da vrijednost narudžbe bude {{config('app.free_shipping_threshold')}}KM ili više.
                        </p>

                        <h2 id="hitna-dostava" class="mb-2 font-weight-bold" style="font-size: 20px">
                            ŠTA JE HITNA DOSTAVA?
                        </h2>
                        <p class="mb-5">
                            Hitna dostava je dostava u 24h.
                            Za Hitnu dostavu potrebno je izvršiti prethodnu provjeru mogućnosti dostavljanja u 24h na traženu lokaciju. 
                            Provjera se može izvršiti putem emaila <a class="link" href="mailto:{{config('app.email')}}">{{config('app.email')}}</a> ili putem telefona <a class="link" href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a>.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAD OSTVARUJEM BESPLATNU DOSTAVU?
                        </h2>
                        <p class="mb-5">
                            Da bi ostvarili besplatnu dostavu potrebno je da ukupna vrijednost naručenih proizvoda dostigne ili pređe iznos od {{config('app.free_shipping_threshold')}}KM.
                            Besplatnu dostavu je također moguće ostvariti u sklopu akcija i promocija ukoliko je tako naznačeno. 
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            GDJE VRŠITE ISPORUKE NARUDŽBI?
                        </h2>
                        <p class="mb-5">
                            Postoje dva načina za isporuku narudžbe: <br>
                            &bull; dostava na kućnu adresu gdje narudžbe šaljemo u sve gradove i mjesta na teritoriji Bosne i Hercegovine <br>
                            &bull; preuzimanje u jednoj od naših lokacija (bez dodatnih troškova), koje možete pronaći <a href="{{route('locations')}}" class="link">ovdje</a>
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAD ĆE MOJA NARUDŽBA BITI POSLANA?
                        </h2>
                        <p class="mb-5">
                            Ukoliko ste napravili narudžbu prije 14:00h, narudžba se šalje isti dan (osim vikendom i praznicima). U suprotnom se šalje sljedeći radni dan.
                            Ako neki od proizvoda nije dostupan u trenutku naručivanja, moguća su čekanja i do 8 dana.
                            Ako naručeni proizvod ne može biti dostupan u tom roku biti ćete obavješteni.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KAD ĆE MOJA NARUDŽBA BITI ISPORUČENA?
                        </h2>
                        <p class="mb-5">
                            Poslane narudžbe stižu u roku od 24 do 48 sati od trenutka slanja.
                            Duža vremena dostave mogu nastati za vrijeme praznika i neradnih dana.
                            {{config('app.name')}} web shop ne preuzima odgovornost u slučaju kašnjenja isporuke zbog netačne ili nepotpune adrese, više sile ili problema u saobraćaju.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            DO KAD MOGU PREUZETI NARUDŽBU U POSLOVNICI?
                        </h2>
                        <p class="mb-5">
                            Rok za preuzimanje narudžbe u poslovnici je {{config('app.pickup_days_limit')}} dana.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            DOSTAVILI STE MI POGREŠAN PROIZVOD, ŠTA SAD?
                        </h2>
                        <p class="mb-5">
                            U slučaju da je isporučeni proizvod različit od onog koji ste naručili i platili, naručeni proizvod će vam biti dostavljen besplatno u roku 8 dana od zaprimanja obavijesti o pogrešnoj isporuci. 
                            Ako to nije moguće, imate pravo na povrat novca u iznosu plaćene cijene proizvoda i cijene dostave. 
                            Također, dužni ste bez odlaganja vratiti pogrešno dostavljeni proizvod, putem organizirane kurirske službe.
                            Troškove dostave u ovom slučaju ne snosi kupac.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            DOSTAVLJENI PROIZVOD JE OŠTEĆEN, ŠTA SAD?
                        </h2>
                        <p class="mb-5">
                            Osoblje {{config('app.name')}} web shopa, prije slanja, pregleda ispravnost, oštećenja i rokove upotrebe naručenih proizvoda.
                            Kupci su dužni pošiljku preuzeti i pregledati pred dostavljačem, a sve kako bi se izbjegle naknadne reklamacije zbog mogućnosti oštećenja pošiljke prilikom dostave.
                            U slučaju da je proizvod ipak oštećen u toku transporta, možete izvršiti reklamaciju proizvoda.
                            Troškove dostave u ovom slučaju ne snosi kupac.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            DOSTAVLJENI PROIZVOD MI NE ODGOVARA, ŠTA SAD?
                        </h2>
                        <p class="mb-5">
                            U slučaju da Vam dostavljeni proizvod iz bilo kojeg razloga ne odgovara, uvijek možete zamijeniti ili vratiti proizvod prema našim pravilima zamjene ili povrata proizvoda.
                            Troškove dostave u ovom slučaju snosi kupac.
                        </p>
                        
                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            ODBIJAM PREUZETI NARUDŽBU, ŠTA SAD?
                        </h2>
                        <p class="mb-3">
                            Ukoliko kupac ne preuzme narudžbu ili odbije preuzeti narudžbu bez valjanog razloga, PZU Apoteke Viva zadržava pravo zahtijevati nadoknadu troškova manipulacije, transporta i drugih mogućih troškova.
                        </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection