@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
                        <p class="mb-3">
                            Opšti uvjeti zaštite osobnih podataka odnose se na osobne podatke koji se nalaze na web stranici i koji su korištenjem web stranice {{config('app.domain')}} obrađeni,
                            odnosno prikupljeni i pohranjeni.
                        </p>
                        
                        <p class="mb-3">
                            PZU Apoteka Viva prikuplja i obrađuje osobne podatke zakonito, pošteno i transparentno. 
                            Isto tako poduzima tehničke i organizacijske mjere kako bi se zaštitili osobni podaci od neovlaštenog pristupa i zlouporabe.
                            Podaci se prikupljaju za posebne i zakonte svrhe i isključivo u nužnom opsegu te se obrađuju na način koji je u skladu s tim svrhama.
                            PZU Apoteka Viva će težiti prikupljanju tačnih podatke koje će po potrebi ili zahtjevu ispitanika ispravljati, brisati ili mijenjati.
                            Prikupljeni podaci čuvat će se onoliko dugo koliko je potrebno s obzirom na svrhu zbog koje su prikupljeni odnosno na rok u skladu sa važećim zakonom.
                        </p>

                        <p class="mb-5">
                            Mole se korisnici da pročitaju Pravila zaštite privatnosti. Davanjem osobnih podataka putem web stranice {{config('app.domain')}} 
                            i pristankom na Uvjete kupovine putem web stranice, korisnik potvrđuje da je pročitao, razumio i da se slažete s pravilima zaštite privatnosti, 
                            te pristaje na prikupljanje, obradu i korištenje svojih osobnih podataka u skladu sa Pravilima zaštite privatnosti.
                            Ukoliko se korisnik ne slaže s opštim uvjetima poslovanja, dužan je napustiti i ne koristiti web stranicu.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            OSOBNI PODACI KOJI SE OBRAĐUJU
                        </h2>
                        <p class="mb-5">
                            Na internetskoj stranici {{config('app.domain')}} PZU Apoteka Viva prikuplja slijedeće osobne podatke ispitanika: 
                            ime i prezime, adresa (ulica, grad, poštanski broj), broj telefona, e-mail, spol, datum rođenja.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            KORIŠTENJE OSOBNIH PODATAKA
                        </h2>
                        <p class="mb-2">
                            Kada se korisnik uključi u određene aktivnosti na web stranici {{config('app.domain')}} kao što su otvaranje korisničkog računa,
                            korištenje internetske prodavnice, ispunjavanje anketa, komentara, objavljivanje sadržaja, sudjelovanje u takmičenjima ili nagradnim igrama, 
                            slanje povratnih informacija, traženje informacija o uslugama, javljanje na oglas za posao, 
                            PZU Apoteka Viva može od korisnika zatražiti davanje određenih dodatnih osobnih podataka. 
                            U tom slučaju, prije davanja dodatnih osobnih podataka korisnik je obvezan proučiti Pravila zaštite privatnosti i pristati na njihovu primjenu.
                        </p>
                        
                        <p class="mb-2">
                            PZU Apoteka Viva osobne podatke prikuplja radi sklapanja i ispunjenja ugovora o kupoprodaji, 
                            evidencije ostvarenih narudžbi kupaca, jedinstvene identifikacije korisnika, analize poslovanja, marketinga, provođenja sistema nagrađivanja, 
                            dostave proizvoda, izrade dokumentacije vezane za kupoprodaju, tehničke podrške te autorizacije plaćanja putem kreditnih i debitnih kartica.
                        </p>

                        <p class="mb-5">
                            Davanjem osobnih podataka i prihvaćanjem Pravila zaštite privatnosti korisnik pristaje da ga PZU Apoteka Viva, obavještava o svojim promotivnim aktivnostima, 
                            proizvodima i uslugama.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            SIGURNOST PLAĆANJA KREDITNIM KARTICAMA
                        </h2>
                        <p class="mb-5">
                            Tajnost Vaših podataka je zaštićena i osigurana upotrebom TLS enkripcije. Stranice za naplatu putem
                            interneta osigurane su korištenjem Secure Socket Layer (SSL) protokola sa 128-bitnom enkripcijom
                            podataka. SSL enkripcija je postupak šifriranja podataka radi sprječavanja neovlaštenog pristupa
                            prilikom njihovog prenosa.
                            Time je omogućen siguran prenos informacija te onemogućen nedozvoljen pristup podacima prilikom
                            komunikacije između korisnikovog računara i WebPay servisa, te obratno.
                            WebPay servis i finansijske ustanove razmjenjuju podatke upotrebom virtualne privatne mreže (VPN),
                            koja je zaštićena od neautorizovanog pristupa.
                            Monri Payment Gateway je sertifikovan prema PCI DSS Level 1 sigurnosnom standardu propisanom
                            Visa i Mastercard pravilima.
                            PZU Apoteka Viva ne pohranjuje brojeve kreditnih kartica i brojevi nisu dostupni neovlaštenim osobama.
                        </p>

                        <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                            OGRANIČAVANJE ODGOVORNOSTI
                        </h2>
                        <p class="mb-2">
                            Iako poduzimamo dostupne tehničke, organizacijske i kadrovske mjere zaštite osobnih podataka od slučajne ili namjerne zlouporabe, uništenja, gubitka, 
                            neovlaštene promjene ili dostupa ne možemo jamčiti da neki od osobnih podataka koje prikupljamo neće nikada biti slučajno otkriveni, 
                            suprotno odredbama ovih Pravila zaštite privatnosti.
                        </p>
                        
                        <p class="mb-2">
                            Do najveće mjere dopuštene zakonom isključujemo odgovornost za štetu nanesenu korisnicima ili trećim osobama slučajnim otkrivanjem osobnih podataka.
                        </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection