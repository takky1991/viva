@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
                        <p class="mb-3">
                            Korisnici su dužni prije početka korištenja web stranice {{config('app.domain')}}, privatne zdravstvene ustanove Apoteka Viva sa sjedištem u Cazinu, Bosna i Hercegovina, Mala Lisa bb, upoznati se s općim uvjetima poslovanja web stranice.
                            Pristupanjem web stranici ili korištenjem bilo kojeg dijela njezinog sadržaja korisnik prihvaća opće uvjete poslovanja web stranice kao i sva ostala pravila i uvjete korištenja web stranice i usluga koje se putem nje pružaju. 
                            Korisnici su saglasni da neće koristiti web stranicu na način da štete autorima ili trećim osobama, te prihvaćaju sve rizike korištenja web stranice i usluga. 
                            Ukoliko se korisnik ne slaže s navedenim, dužan je prestati koristiti web stranicu i usluge koje se putem nje pružaju.
                        </p>
                        <p class="mb-3">
                            Sadržaj web stranice zaštićen je autorskim pravima. Mijenjanje, posuđivanje, prodavanje ili distribuiranje sadržaja moguće je samo uz prethodnu pisanu dozvolu privatne zdravstvene ustanove Apoteka Viva.
                        </p>
                        <p class="mb-3">
                            Korisnici koriste web stranicu na vlastitu odgovornost. Privatna zdravstvena ustanova Apoteka Viva ni na koji način nije odgovorna za štetu koju korisnik može pretrpjeti korištenjem web stranice {{config('app.domain')}}. 
                            Autori i druge fizičke ili pravne osobe uključene u stvaranje, proizvodnju i distribuciju {{config('app.domain')}} web stranice nisu odgovorni ni za kakvu štetu nastalu kao posljedica korištenja ili nemogućnosti korištenja iste.
                        </p>
                        <p class="mb-3">
                            Ova web stranica sadrži informacije treće strane i veze do drugih internet stranica nad kojima Apoteka Viva nema kontrolu.
                            Apoteka Viva nije odgovorna niti za točnost niti za bilo koji drugi aspekt takvih informacija i ne preuzima nikakvu odgovornost za takve informacije. 
                            Apoteka Viva zadržava pravo izmjene sadržaja ove web stranice na bilo koji način, u bilo kojem trenutku i iz bilo kojeg razloga, i neće biti odgovorna ni za kakve moguće posljedice proizašle iz takvih promjena. 
                        </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection