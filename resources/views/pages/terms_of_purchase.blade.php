@extends('layouts.app')

@section('content')

<div class="mt-5">
	<div class="container-xl">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="row">
					<div class="col-12">
						<div>
                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                PRODAVAČ
                            </h2>
                            Podavač robe istaknute na ovoj stranici je:
                            <br>
                            <br>
                            <b>PZU Apoteka Viva</b>
                            <ul>
                                <li>&#x2022; Adresa: Mala Lisa bb, 77220 Cazin, Bosna i Hercegovina</li>
                                <li>&#x2022; Telefon: +387 (0)37 536 051</li>
                                <li>&#x2022; Email: {{config('app.email')}}</li>
                                <li>&#x2022; ID broj: 4263338490009</li>
                                <li>&#x2022; PDV broj: 263338490009</li>
                            </ul>
                            <p class="mb-5"></p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                KUPAC
                            </h2>
							<p class="mb-5">
                                Kupac robe je posjetitelj internet trgovine koji odabere barem jedan proizvod, popuni elektronski formular narudžbe i pošalje ga prodavaču. 
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                PROIZVODI
                            </h2>
							<p class="mb-5">
                                Internet prodavnica {{config('app.domain')}} garantuje za kvalitet i porijeklo proizvoda. 
                                Proizvodi su originalne robne marke i nabavljaju direktno od proizvođača ili od ovlaštenih distributera.
                                Internet prodavnica {{config('app.domain')}} nastoji da sve proizvode na sajtu opiše što je tačnije moguće. 
                                Sve cijene proizvoda su izražene u KM i uključuju PDV.
                                Proizvodi su predstavljeni po brendovima i kategorijama. 
                                Svaki proizvod posjeduje opis i fotografiju. 
                                Fotografija proizvoda Vam može poslužiti kao osnovna informacija o izgledu proizvoda. 
                                Napominjemo da je fotografija proizvoda data informativno i da ne možemo da garantujemo da bezuslovno vjerno prikazuje izgled proizvoda.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                REGISTRACIJA
                            </h2>
							<p class="mb-5">
                                Kupac može ili ne mora biti registrovan na {{config('app.domain')}} kako bi izvršio kupovinu. 
                                Prilikom registracije od kupca se zahtijeva da unese ime, prezime, e-mail adresu i lozinku.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                NARUČIVANJE
                            </h2>
							<p class="mb-5">
                                Naručivanje robe moguće je 24 sata na dan, 7 dana u sedmici. 
                                Roba se naručuje isključivo putem web stranice korištenjem elektronske forme. 
                                Kupac može ili ne mora biti registrovan na web stranici kako bi izvršio narudžbu. 
                                Sve cijene su izražene u KM i uključuju PDV. 
                                Roba se naručuje elektronskim oblikom, pristiskom na određeni proizvod te spremanjem istog u Korpu.
                                Od kupca se zahtjeva da unese kontakt podatke i adresu kako bi se zaključio proces narudžbe. 
                                Roba se smatra naručenom u trenutku kada kupac odabere te potvrdi načine plaćanja.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                PLAĆANJE
                            </h2>
							<p class="mb-5">
                                Naručene proizvode sa troškovima dostave ili bez njih, ovisno o iznosu narudžbe, kupac će prodavaču platiti na jedan od dostupnih načina plaćanja u tom trenutku.  
                                Sve informacije o plaćanja možete pronaći <a href="{{route('paying')}}" class="link" target="_blank">ovdje</a>.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                POTVRDA NARUDŽBE
                            </h2>
							<p class="mb-5">
                                Po zaprimanju narudžbe kupac će primiti obavijest email-om da je narudžba zaprimljena te da se provjerava raspoloživost naručenih proizvoda. 
                                O raspoloživosti i potvrdi narudžbe kupac će biti obavješten pismenim putem na email ili telefonskim putem na broj koji je naveden u narudžbi.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                DOSTAVA ROBE
                            </h2>
							<p class="mb-5">
                                Dostava robe obavlja se unutar 8 radnih dana od trenutka potvrde raspoloživosti naručenih proizvoda. 
                                Kupac ima pravo prilikom preuzimanja pošiljke odbiti preuzeti pošiljku na kojoj su vidljiva vanjska oštećenja.
                                Da bi ostvarili besplatnu dostavu, na željenu adresu, potrebno je da narudžba dostigne ili pređe iznos od {{config('app.free_shipping_threshold')}}KM. 
                                Za dostavu narudžbe koja ne prelazi navedeni iznos od {{config('app.free_shipping_threshold')}}KM, trošak se naplaćuje u iznosu navedenom u procesu naručivanja i zavisi od odabranog načina dostave.

                                Naručenu robu također možete preuzeti u jedno od naših <a href="{{route('locations')}}" class="link" target="_blank">lokacija</a>. 
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                POVRAT ROBE
                            </h2>
							<p class="mb-5">
                                Pravo na povrat imaju svi kupci web shopa na sve proizvode. Povrat robe moguć je unutar 8 dana od primanja pošiljke. 
                                Povrat ste dužni zatražiti putem e-maila u kojem se navodi broj narudžbe ili telefonski. 
                                Ako odobrimo zahtjev, robu nam vraćate u roku od dva radna dana od odobrenja zahtjeva. 
                                Ako osoblje web shopa procijeni da je vraćena roba neoštećena, u originalnoj ambalaži i ima priložen odgovarajući fiskalni račun, izvršit ćemo povrat novca na vaš račun. 
                                Ako roba nije u odgovarajućem stanju (otvorena, oštećena ili ne posjeduje odgovarajući fiskalni račun), morat ćete platiti poštarinu kako bi Vam roba ponovno bila poslana na kućnu adresu. 
                                Sve troškove povrata robe snosi kupac.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                ZAMJENA ROBE
                            </h2>
							<p class="mb-5">
                                Pravo na zamjenu robe imaju svi kupci web shopa na sve proizvode. Zamjena robe moguća je unutar 8 dana od primanja pošiljke. 
                                Zamjenu ste dužni zatražiti putem e-maila u kojem se navodi broj narudžbe ili telefonski. 
                                U e-mailu trebate objasniti želite li proizvod mijenjati za isti proizvod ili za drugi proizvod iz naše ponude. 
                                Proizvod vraćate na našu adresu unutar 2 radna dana od odobrenog zahtjeva. 
                                Kada utvrdimo da je vraćeni proizvod neoštećen, u originalnoj ambalaži i ima priložen odgovarajući fiskalni račun, napravit ćemo zamjenu. 
                                Ako proizvod mijenjate za proizvod veće vrijednosti, najprije ćete trebati uplatiti razliku na naš račun, 
                                a ako je manje vrijednosti mi ćemo novac vratiti na vaš račun. 
                                Sve troškove zamjene robe snosi kupac.
                            </p>

                            <h2 class="mb-2 font-weight-bold" style="font-size: 20px">
                                REKLAMACIJA ROBE
                            </h2>
							<p>
                                U slučaju oštećenja proizvoda prilikom isporuke, pogrešno poslanog proizvoda ili greškom poslanog proizvoda koji nije u roku trajanja, 
                                kupac je dužan reklamirati proizvod u roku od 2 radna dana. Kupac će dobiti novi ispravan proizvod bez dodatnih troškova.
                            </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection