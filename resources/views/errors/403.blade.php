@extends('layouts.app')

@section('content')
    <br>
    <br>
    <br>
    <br>
    <div class="container text-center">
        <p style="font-size: 50px;"><b>OOPS!</b> &#128546;</p>
        <br>
        <h4>Niste ovlašteni za pristup ovoj stranici</h4>
        <br>
        <a href="{{route('home')}}" class="link font-weight-bold">Vratite se na početnu stranicu ></a>
    </div>
    <br>
    <br>
    <br>
    <br>
@endsection