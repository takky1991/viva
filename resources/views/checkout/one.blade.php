@extends('layouts.app')

@section('content')
<!-- Shoping Cart -->
<div class="checkout-page container-xl">
    <checkout-form-one 
        initial-shipping-method="{{old('shipping_method_id', $cart->shipping_method_id)}}"
        :shipping-methods="{{json_encode($shippingMethods)}}" 
        :has-free-shipping="{{$cart->free_shipping ? 'true' : 'false'}}"
        inline-template
    >
        <div class="row">
            <div class="checkout-content col-lg-7">
                @include('checkout/partials/header')
                @include('checkout/partials/cart-collapse')
                @include('checkout/partials/progress-bar')

                {{-- <div class="alert alert-info" role="alert">
                    Od 16.05.2023. došlo je do poskupljenja usluga dostavljača A2B.
                </div> --}}

                @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif

                @if ($errors->has('shipping_method_id'))
                    <div class="alert alert-danger" role="alert">
                        Odaberite način dostave.
                    </div>
                @endif

                <br>
                
                <form action="{{route('checkout.one')}}" method="POST">
                    @csrf()    
                    
                    <h4 class="title">
                        Odaberite način dostave
                    </h4>

                    <br>

                    <div class="form-group">
                        <h5>Dostava na adresu</h5>
                        <br>
                        @foreach ($deliveryMethods as $method)
                            @if($method->key != 'x_express_fast')
                                <div class="in-panel">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="{{$method->key}}" name="shipping_method_id" v-model="shippingMethod" value="{{$method->id}}" {{ old('shipping_method_id', $cart->shipping_method_id) == $method->id ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="{{$method->key}}" id="{{$method->key}}_label">
                                            <img class="mb-2 mb-md-0 mr-2" src="{{asset('images/logos/' . $method->key . '.png')}}" alt="{{$method->name}}" style="width: 50px">
                                            <br class="d-block d-md-none">
                                            <span>{{$method->name}}</span> @if($method->key == 'x_express_fast') <span class="fast">Hitno</span> @endif
                                            <br class="d-block d-md-none">
                                            <span class="float-md-right"><b>+ {{formatPrice($cart->free_shipping ? 0 : $method->price)}}</b></span>
                                            @if($method->key == 'x_express_fast')
                                                <br>
                                                <span style="font-size: 12px;"><i class="fa fa-info-circle" aria-hidden="true"></i> Prije odabira saznaj više <a class="link" href="{{route('delivery')}}#hitna-dostava">ovdje</a>.</span>
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        <br>

                        <h5>Preuzimanje u apoteci</h5>
                        <br>
                        @foreach ($pickupMethods as $method)
                            <div class="in-panel">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="{{$method->key}}" name="shipping_method_id" v-model="shippingMethod" value="{{$method->id}}" {{ old('shipping_method_id', $cart->shipping_method_id) == $method->id ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="{{$method->key}}" id="{{$method->key}}_label">
                                        <span>{{$method->name}}</span>
                                        <br class="d-block d-md-none">
                                        <span class="float-md-right"><b>+ {{formatPrice($cart->free_shipping ? 0 : $method->price)}}</b></span>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <br>

                    <div class="d-md-flex justify-content-between align-items-center">
                        <a href="{{route('cart.show')}}" class="d-none d-md-inline-block link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad na korpu</a>
                        <button 
                            id="checkout-one-submit-button" 
                            type="submit" 
                            class="checkout-submit-button btn btn-primary"
                            style="min-width: 200px"
                        >
                            Dalje <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i>
                        </button>
                        <div class="d-md-none mt-5 text-center">
                            <a href="{{route('cart.show')}}" class="link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad na korpu</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="d-none d-lg-block col-lg-5">
                @include('checkout/partials/cart-info-one')
            </div>
        </div>
    </checkout-form-one>
</div>
@endsection

@if(empty($cart->shipping_method_id))
    @push('gtm_data_layer')
        <script>
            dataLayer.push({ ads: null }); // Clear the previous ads object.
            dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
            dataLayer.push({
                event: 'begin_checkout',
                ecommerce: {
                    currency: 'BAM',
                    value: {{$cart->totalPrice()}},
                    items: [
                        @foreach($cart->products as $product)
                        {
                            item_id: {{$product->id}},
                            item_name: '{{$product->fullName()}}',
                            currency: 'BAM',
                            item_brand: '{{$product->brand->name}}',
                            location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                            price: {{$product->realPrice()}},
                            quantity: {{$product->pivot->quantity}},
                            index: {{$loop->index}},

                            @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                            @endif

                            @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                            @endif

                            @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                            @endif
                        },
                        @endforeach
                    ]
                }
            });
        </script>
    @endpush
@endif