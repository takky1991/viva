@extends('layouts.app')

@section('content')
<div class="checkout-page container-xl">
    <checkout-form-three 
        inline-template 
        @if(sizeof($errors)) :initial-errors="{{$errors}}" @endif
        @if(auth()->guest()) guest-id="{{session('guest_id')}}" @endif
    >
        <div class="row">
            <div class="checkout-content col-lg-7">
                @include('checkout/partials/header')
                @include('checkout/partials/cart-collapse')
                @include('checkout/partials/progress-bar')

                <div v-if="errors.terms_of_purchase" class="alert alert-danger" role="alert" v-cloak>
                    Potrebno je prihvatiti uvjete kupovine.
                </div>

                <br>

                <form id="paymentForm" :action="paymentMethod == 'online_payment' ? '{{config('app.monri_url')}}' : '{{route('checkout.three')}}'" method="POST">
                    @csrf()    

                    <h4 class="title">
                        Odaberite način plaćanja
                    </h4>

                    <br>

                    <div class="form-group">
                        <div class="in-panel">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cash_on_delivery" name="payment_method" v-model="paymentMethod" value="cash_on_delivery">
                                <label class="custom-control-label" for="cash_on_delivery" id="cash_on_delivery_label">
                                    <span>Plaćanje pouzećem</span>
                                </label>
                            </div>
                        </div>
                        <div class="in-panel">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="online_payment" name="payment_method" v-model="paymentMethod" value="online_payment">
                                <label class="custom-control-label" for="online_payment" id="online_payment_label">
                                    <img class="mr-2" src="{{asset('images/cards/monri.png')}}" alt="Monri" style="width: 80px">
                                    <br class="d-block d-md-none">
                                    <span>Plaćanje bankovnom karticom</span>
                                    <br class="d-block d-md-none">
                                    <a href="https://monri.com/ba/webpay/" target="_blank" class="float-md-right link mt-2">Šta je Monri WebPay?</a>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="terms-accepted" name="terms_of_purchase" v-model="termsOfPurchase" {{ old('terms_of_purchase') ? 'checked' : '' }}>
                            <label class="custom-control-label" :class="{'invalid': errors.terms_of_purchase}" for="terms-accepted"><span id="customRadioLabel1"><b>Prihvatam</b></span> <a href="{{route('terms_of_purchase')}}" target="_blank" class="link"><b>Uvjete kupovine</b></a></label>
                        </div>
                        <div v-if="errors.terms_of_purchase" class="invalid-feedback" style="display: block; font-size: 14px;" v-cloak>
                            Potrebno je prihvatiti uvjete kupovine.
                        </div>
                    </div>

                    <br>

                    <template v-if="paymentMethod == 'online_payment' && orderInfo">
                        <input type="hidden" name="ch_full_name" :value="orderInfo.full_name">
                        <input type="hidden" name="ch_phone" :value="orderInfo.phone">
                        <input type="hidden" name="ch_email" :value="orderInfo.email">

                        <input v-if="orderInfo.street_address" type="hidden" name="ch_address" :value="orderInfo.street_address">
                        <input v-if="orderInfo.city" type="hidden" name="ch_city" :value="orderInfo.city">
                        <input type="hidden" name="ch_country" value="BA">
                        <input v-if="orderInfo.postalcode" type="hidden" name="ch_zip" :value="orderInfo.postalcode">

                        <input type="hidden" name="order_info" :value="orderInfo.order_info">
                        <input type="hidden" name="order_number" :value="orderInfo.order_number">
                        <input type="hidden" name="amount" :value="orderInfo.amount">
                        <input type="hidden" name="currency" value="BAM">

                        <input type="hidden" name="language" value="hr">
                        <input type="hidden" name="transaction_type" value="authorize">
                        <input type="hidden" name="authenticity_token" :value="orderInfo.token">
                        <input type="hidden" name="digest" id="digest" :value="orderInfo.digest">
                    </template>

                    <div class="d-md-flex justify-content-between align-items-center">
                        <a href="{{route('checkout.two')}}" class="d-none d-md-inline-block link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad</a>
                        <button 
                            v-if="paymentMethod == 'online_payment'"
                            @click.prevent="createOrder"
                            id="checkout-three-submit-button" 
                            type="button" 
                            class="checkout-submit-button btn btn-primary"
                            style="min-width: 200px"
                            v-cloak
                        >
                            Naruči <i v-if="processing" class="fa fa-refresh fa-spin ml-2" aria-hidden="true"></i> <i v-else class="fa fa-chevron-right ml-2" aria-hidden="true"></i>
                        </button>
                        <button 
                            v-else
                            id="checkout-three-submit-button" 
                            type="submit" 
                            class="checkout-submit-button btn btn-primary"
                            style="min-width: 200px"
                        >
                            Naruči <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i>
                        </button>
                        <div class="d-md-none mt-5 text-center">
                            <a href="{{route('checkout.two')}}" class="link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad</a>
                        </div>
                    </div>
                </form>
                <div class="text-center mt-5">
                    <a href="https://www.mastercard.com" target="_blank">
                        <img class="m-2" src="{{asset('images/cards/mastercard.svg')}}" title="Mastercard" alt="Mastercard logo" style="height: 25px;">
                    </a>
                    <a href="https://brand.mastercard.com/brandcenter/more-about-our-brands.html" target="_blank">
                        <img class="m-2" src="{{asset('images/cards/maestro.svg')}}" title="Maestro" alt="Maestro logo" style="height: 25px;">
                    </a>
                    <a href="https://www.visa.co.uk/about-visa/visa-in-europe.html" target="_blank">
                        <img class="m-2" src="{{asset('images/cards/visa.svg')}}" title="Visa" alt="Visa logo" style="height: 25px;">
                    </a>
                    <a href="https://www.mastercard.ba/bs-ba/korisnici/podrska/sigurnost-i-zastita/identity-check.html" target="_blank">
                        <img class="m-2" src="{{asset('images/cards/mastercard_id_check.svg')}}" title="Mastercard ID Check" alt="Mastercard ID Check logo" style="height: 25px;">
                    </a>
                    <a href="https://www.visa.co.uk/pay-with-visa/featured-technologies/verified-by-visa.html" target="_blank" onclick="javascript:window.open('https://www.visaeurope.com/making-payments/verified-by-visa/','Verified by Visa','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=540, height=540'); return false;">
                        <img class="m-2" src="{{asset('images/cards/visa_secure.svg')}}" title="Visa Secure" alt="Visa Secure logo" style="height: 25px;">
                    </a>
                    <a href="https://monri.com" target="_blank">
                        <img class="m-2" src="{{asset('images/cards/monri_pay_web.png')}}" title="Monri" alt="Monri logo" style="height: 43px">
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-block col-lg-5">
                @include('checkout/partials/cart-info-two')
            </div>
        </div>
    </checkout-form-three>
</div>
@endsection

@if(url()->previous() == route('checkout.two'))
    @push('gtm_data_layer')
        <script>
            dataLayer.push({ ads: null }); // Clear the previous ads object.
            dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
            dataLayer.push({
                event: 'add_payment_info',
                ecommerce: {
                    currency: 'BAM',
                    value: {{$cart->totalPrice()}},
                    items: [
                        @foreach($cart->products as $product)
                        {
                            item_id: {{$product->id}},
                            item_name: '{{$product->fullName()}}',
                            currency: 'BAM',
                            item_brand: '{{$product->brand->name}}',
                            location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                            price: {{$product->realPrice()}},
                            quantity: {{$product->pivot->quantity}},
                            index: {{$loop->index}},

                            @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                            @endif

                            @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                            @endif

                            @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                            @endif
                        },
                        @endforeach
                    ]
                }
            });
        </script>
    @endpush
@endif