@extends('layouts.app')

@section('content')
<!-- Shoping Cart -->
<div class="cart-page container-xl my-5">    
    @if(empty($cart) || $cart->products->isEmpty())
        <div class="text-center py-5 my-5">
            <img src="{{asset('images/empty-cart.png')}}" alt="{{config('app.name')}}" width="100">
            <p class="my-3">Vaša korpa je prazna</p>
            <a href="{{route('home')}}" class="link font-weight-bold">Nazad na web shop ></a>
        </div>
    @else
        <h2 class="text-center">Korpa</h2>

        @if(isset($cartProductPriceChanged) && $cartProductPriceChanged)
            <br>
            <div class="alert alert-info" role="alert">
                Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.
            </div>
        @endif

        @if(isset($cartProductRemoved) && $cartProductRemoved)
            <br>
            <div class="alert alert-info" role="alert">
                Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.
            </div>
        @endif

        @if(session()->has('success'))
            <br>
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
        @endif

        <form action="{{route('cart.update')}}" method="POST">
            @csrf()
            <div class="table-shopping-cart">
                @foreach ($cart->products as $product)
                    <div class="table-row" id="product-{{$loop->index}}">
                        <input type="hidden" name="products[{{$loop->index}}][id]" value="{{$product->id}}">
                        <div class="column-1">
                            @if($product->mainPhoto())
                                <img src="{{$product->mainPhoto()->url('thumb')}}" alt="{{$product->fullName()}}" style="width: 65px;">
                            @else
                                <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" style="width: 65px;">
                            @endif
                        </div>
                        <div class="column-2">
                            <a href="{{$product->url()}}">
                                @if($product->brand)
                                    <div>{{$product->brand->name}}</div>
                                @endif
                                <b>{{$product->name()}}</b>
                                @if($product->package_description)
                                    <div>{{$product->package_description}}</div>
                                @endif
                            </a>
                        </div>
                        <div class="column-3">
                            <div class="{{$product->onSale() ? 'discount' : ''}}">{{formatPrice($product->price)}}</div>
                            @if($product->onSale())
                                <span class="discount-price">{{formatPrice($product->discount_price)}}</span>
                            @endif
                        </div>
                        <div class="column-4">
                            @if($product->pivot->variants)
                                <div>Br. {{$product->pivot->variants}}</div>
                            @else($product->pivot->variants)
                                <item-quantity classes="small" :value="{{$product->pivot->quantity}}" array="products[{{$loop->index}}]"></item-quantity>
                            @endif
                        </div>
                        <div class="column-5 font-weight-bold">{{formatPrice($product->pivot->quantity * $product->realPrice())}}</div>
                        <div class="column-6">
                            <a href="{{ route('cart.deleteProduct', ['product' => $product->id]) }}"
                                onclick="
                                    event.preventDefault();
                                    dataLayer.push({ ads: null }); // Clear the previous ads object.
                                    dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
                                    dataLayer.push({
                                        event: 'remove_from_cart',
                                        ecommerce: {
                                            currency: 'BAM',
                                            value: {{$product->realPrice() * $product->pivot->quantity}},
                                            items: [{
                                                item_id: {{$product->id}},
                                                item_name: '{{$product->fullName()}}',
                                                item_brand: '{{$product->brand->name}}',
                                                price: {{$product->realPrice()}},
                                                currency: 'BAM',
                                                quantity: {{$product->pivot->quantity}},
                                                location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                                                index: 0,

                                                @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                                    item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                                                @endif

                                                @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                                    item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                                                @endif

                                                @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                                    item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                                                @endif
                                            }]
                                        }
                                    });
                                    document.getElementById('delete-product-{{$loop->index}}-form').submit();"
                                >
                                <i class="fa fa-times remove-product" aria-hidden="true" id="delete-product-{{$loop->index}}-link"></i>
                                <span class="d-md-none">Ukloni</span>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

            <button id="update-cart-button" type="submit" class="btn btn-outline-primary">Ažuriraj korpu</button>
        </form>

        @foreach ($cart->products as $product)
            <form id="delete-product-{{$loop->index}}-form" action="{{ route('cart.deleteProduct', ['product' => $product->id]) }}" method="POST" style="display: none;">
                <input type="hidden" name="_method" value="DELETE">
                {{ csrf_field() }}
            </form>
        @endforeach

        <div class="my-4 text-right">
            <div class="pb-4">
                <div class="total-price font-weight-bold">
                    Cijena:
                    <span class="pl-2">{{formatPrice($cart->totalPrice())}}</span>
                </div>

                @if($cart->free_shipping)
                    <div class="free-shipping-success d-inline-block"><i class="fa fa-truck mr-1" aria-hidden="true"></i> Ostvarili ste besplatnu dostavu! 🎉🎊</div>
                @elseif((config('app.free_shipping_threshold') - $cart->totalPrice()) <= config('app.free_shipping_threshold_alert') && (config('app.free_shipping_threshold') - $cart->totalPrice()) > 0)
                    <div class="free-shipping-warning d-inline-block"><i class="fa fa-truck mr-1" aria-hidden="true"></i> {{formatPrice(config('app.free_shipping_threshold') - $cart->totalPrice())}} Vas dijeli do besplatne dostave!</div>
                @else
                    <div class="free-shipping-success d-inline-block"><i class="fa fa-truck mr-1" aria-hidden="true"></i> Dostava već od {{config('app.lowest_shipping_cost')}}KM!</div>
                @endif
            </div>
    
            <a href="{{route('checkout')}}" class="btn btn-primary checkout-button d-inline-block">Završi kupovinu <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></a>
        </div>
    @endIf
</div>
@endsection

@if(!empty($cart) && $cart->products->isNotEmpty())
    @push('gtm_data_layer')
        <script>
            dataLayer.push({ ads: null }); // Clear the previous ads object.
            dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
            dataLayer.push({
                event: 'view_cart',
                ecommerce: {
                    currency: 'BAM',
                    value: {{$cart->totalPrice()}},
                    items: [
                        @foreach($cart->products as $product)
                        {
                            item_id: {{$product->id}},
                            item_name: '{{$product->fullName()}}',
                            currency: 'BAM',
                            item_brand: '{{$product->brand->name}}',
                            location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                            price: {{$product->realPrice()}},
                            quantity: {{$product->pivot->quantity}},
                            index: {{$loop->index}},

                            @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                            @endif

                            @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                            @endif

                            @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                            @endif
                        },
                        @endforeach
                    ]
                }
            });
        </script>
    @endpush
@endif