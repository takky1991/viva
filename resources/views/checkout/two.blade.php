@extends('layouts.app')

@section('content')
<!-- Shoping Cart -->
<div class="checkout-page container-xl">
    <checkout-form-two :initial-register="{{old('register') ?? 'false'}}" inline-template>
        <div class="row">
            <div class="checkout-content col-lg-7">
                @include('checkout/partials/header')
                @include('checkout/partials/cart-collapse')
                @include('checkout/partials/progress-bar')

                @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif

                @if (sizeof($errors))
                    <div class="alert alert-danger" role="alert">
                        Unesite sva potrebna polje
                    </div>
                @endif

                <br>

                <form action="{{route('checkout.two')}}" method="POST">
                    @csrf()
        
                    <h4 class="title">
                        Unesite potrebne informacije
                    </h4>

                    <br>

                    <h5>Lični podaci</h5>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">Ime</label>
                                <input 
                                    id="first-name" 
                                    type="text" 
                                    class="input-field @if($errors->has('first_name')) invalid @endif" 
                                    name="first_name" 
                                    value="{{ old('first_name') ?? $cart->first_name ?? $user?->first_name }}" 
                                    placeholder="Unesite ime" 
                                    autofocus
                                >
                                @if ($errors->has('first_name'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Prezime</label>
                                <input 
                                    id="last-name" 
                                    type="text" 
                                    class="input-field @if($errors->has('last_name')) invalid @endif" 
                                    name="last_name" 
                                    value="{{ old('last_name') ?? $cart->last_name ?? $user?->last_name}}" 
                                    placeholder="Unesite prezime"
                                >
                                @if ($errors->has('last_name'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('last_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Broj telefona</label>
                                <phone-input phone-number="{{ old('phone') ?? $cart->phone ?? $user?->phone}}"></phone-input>
                                @if ($errors->has('phone'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('phone') }}
                                    </div>
                                @endif
                            </div>    
                        </div>
                        <div class="col-md-6">
                            @guest
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input 
                                    id="email" 
                                    type="email" 
                                    class="input-field @if($errors->has('email')) invalid @endif" 
                                    name="email" 
                                    value="{{ old('email') ?? $cart->email }}" 
                                    placeholder="Unesite e-mail"
                                >
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            @endguest
                        </div>
                    </div>

                    @guest
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="placeholder">placeholder</label>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="register" value="false">
                                        <input type="checkbox" class="custom-control-input" id="register" name="register" :value="register" v-model="register">
                                        <label class="custom-control-label" for="register">Želim se registrovati.</label><br>
                                        <small>* Otvaranjem računa moći ćete kupovati brže, biti u toku o statusu narudžbe i pratiti narudžbe koje ste prethodno napravili.</small>
                                    </div>
                                    @if ($errors->has('register'))
                                        <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                            {{ $errors->first('register') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endguest

                    <div v-if="register" class="row" v-cloak>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Šifra</label>
                                <input id="password" type="password" class="input-field @if($errors->has('password')) invalid @endif" name="password" placeholder="Unesite vašu šifru">
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_confirmation">Potvrda šifre</label>
                                <input id="password-confirm" type="password" class="input-field @if($errors->has('password_confirmation')) invalid @endif" name="password_confirmation" placeholder="Ponovno unesite šifru">
                                @if ($errors->has('password_confirmation'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="terms-accepted" name="terms_accepted" {{ old('terms_accepted') ? 'checked' : '' }}>
                                    <label class="custom-control-label @if($errors->has('terms_accepted')) invalid @endif" for="terms-accepted"><span id="customRadioLabel1">Prihvatam</span> <a href="{{route('privacy_policy')}}" target="_blank" class="link">Uvjete korištenja</a></label>
                                </div>
                                @if ($errors->has('terms_accepted'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('terms_accepted') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    @if(!$cart->shippingMethod->pickup)
                        <br>

                        <h5>Vaša adresa</h5>

                        <br>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="street_address">Ulica</label>
                                    <input 
                                        id="street-address" 
                                        type="text" 
                                        class="input-field @if($errors->has('street_address')) invalid @endif" 
                                        name="street_address" 
                                        value="{{ old('street_address') ?? $cart->street_address ?? $user?->street_address }}" 
                                        placeholder="Unesite naziv ulice"
                                    >
                                    @if ($errors->has('street_address'))
                                        <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                            {{ $errors->first('street_address') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="postalcode">Poštanski broj</label>
                                    <input 
                                        id="postalcode" 
                                        type="number" 
                                        class="input-field @if($errors->has('postalcode')) invalid @endif" 
                                        name="postalcode" 
                                        value="{{ old('postalcode') ?? $cart->postalcode ?? $user?->postalcode }}" 
                                        placeholder="Unesite poštanski broj"
                                    >
                                    @if ($errors->has('postalcode'))
                                        <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                            {{ $errors->first('postalcode') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="city">Grad</label>
                                    <input 
                                        id="city" 
                                        type="text" 
                                        class="input-field @if($errors->has('city')) invalid @endif" 
                                        name="city" 
                                        value="{{ old('city') ?? $cart->city ?? $user?->city }}" 
                                        placeholder="Unesite grad"
                                    >
                                    @if ($errors->has('city'))
                                        <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                            {{ $errors->first('city') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country_id">Država</label>
                                    <select id="country_id" name="country_id" class="custom-select input-field @if($errors->has('country_id')) invalid @endif">
                                        <option value="1" selected disabled>{{$country->name}}</option>
                                    </select>
                                    @if ($errors->has('country_id'))
                                        <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                            {{ $errors->first('country_id') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="additional_info">Napomene uz narudžbu (neobavezno)</label>
                                <textarea class="text-field" name="additional_info" id="additional_info" rows="2">{{ old('additional_info') ?? $cart->additional_info }}</textarea>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="d-md-flex justify-content-between align-items-center">
                        <a href="{{route('checkout.one')}}" class="d-none d-md-inline-block link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad</a>
                        <button 
                            id="checkout-two-submit-button" 
                            type="submit" 
                            class="checkout-submit-button btn btn-primary"
                            style="min-width: 200px"
                        >
                            Dalje 
                            <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i>
                        </button>
                        <div class="d-md-none mt-5 text-center">
                            <a href="{{route('checkout.one')}}" class="link"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i> Nazad</a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="d-none d-lg-block col-lg-5">
                @include('checkout/partials/cart-info-two')
            </div>
        </div>
    </checkout-form-two>
</div>
@endsection

@if(url()->previous() == route('checkout.one'))
    @push('gtm_data_layer')
        <script>
            dataLayer.push({ ads: null }); // Clear the previous ads object.
            dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
            dataLayer.push({
                event: 'add_shipping_info',
                ecommerce: {
                    currency: 'BAM',
                    value: {{$cart->totalPrice()}},
                    shipping_tier: '{{$cart->shippingMethod->key}}',
                    items: [
                        @foreach($cart->products as $product)
                        {
                            item_id: {{$product->id}},
                            item_name: '{{$product->fullName()}}',
                            currency: 'BAM',
                            item_brand: '{{$product->brand->name}}',
                            location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                            price: {{$product->realPrice()}},
                            quantity: {{$product->pivot->quantity}},
                            index: {{$loop->index}},

                            @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                            @endif

                            @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                            @endif

                            @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                            @endif
                        },
                        @endforeach
                    ]
                }
            });
        </script>
    @endpush
@endif