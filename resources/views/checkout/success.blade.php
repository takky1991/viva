@extends('layouts.app')

@section('content')
    <div class="checkout-page container-xl">
        <div class="row">
            <div class="checkout-content col-lg-7">
                @include('checkout/partials/header')
                @include('checkout/partials/cart-collapse')

                @if($firstTimeShowing)
                    <div class="alert alert-success my-3" role="alert">
                        <h4 class="alert-heading">Vaša narudžba je zaprimljena 🎉🎊</h4>
                        <br>
                        @if($order->shipping_pickup)
                            <p>
                                Narudžba će biti spremna u poslovnici <b>{{$order->shipping_name}}</b>.
                                Rok za preuzimanje je <b>{{config('app.pickup_days_limit')}} dana</b> od dana naručivanja tj. do <b>{{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}}</b> (do kraja radnog vremena poslovnice).
                                Kontakt informacije, lokaciju i radno vrijeme poslovnice možete pronaći <a href="{{route('locations')}}">ovdje</a>.
                            </p>
                            <hr>
                        @else
                            <p>
                                Zaprimili smo Vašu narudžbu. Naručeni proizvodi će biti poslani u najkraćem vremenskom roku na Vašu adresu. Sve narudžbe koje su zaprimljene petkom nakon 14h, subotom ili nedjeljom, šalju se sljedeći radni dan tj. u ponedjeljak.
                            </p>
                            <hr>
                        @endif
                        @if(auth()->check())
                            <p>U svakom trenutku možete pregledati sve narudžbe i njihov trenutni status, tako što otvorite "Narudžbe" u Vašem računu ili <a href="{{route('user.orders')}}">putem ovog linka</a>.</p>
                            <hr>
                        @endif
                        <p class="mb-0">Za sve dodatne informacije i pitanja kontaktirajte nas na broj telefona: <a href="tel:{{config('app.phone')}}">{{config('app.phone_short')}}</a> ili putem maila na adresu <a href="mailto:{{config('app.email')}}">{{config('app.email')}}</a></p>
                    </div>
                @endif

                <br>

                <div>
                    <a href="{{route('home')}}" class="link font-weight-bold">Nazad na webshop ></a>
                </div>

                <br>

                <h4 class="title">
                    Informacije o narudžbi
                </h4>

                <br>

                <div class="row">
                    <div class="col-md-6 mb-3 mb-md-0">
                        <p class="dusk-order-info mb-3">
                            <b>Podaci o kupcu</b> <br> 
                            {{$order->first_name}} {{$order->last_name}} <br>
                            @if(!$order->shipping_pickup)
                                {{$order->street_address}} <br>
                                {{$order->postalcode}} {{$order->city}} <br>
                                {{$order->country->name}}
                            @endif
                        </p>
                        <p>
                            <b>Kontakt</b> <br>
                            Tel: {{$order->phone}} <br>
                            @if($order->email)
                                {{$order->email}} <br>
                            @endif
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="mb-3">
                            <b>Način preuzimanja</b> <br>
                            @if($order->shipping_pickup)
                                Preuzimanje u {{$order->shipping_name}} <br>
                            @else 
                                Slanje na adresu <br> {{$order->shipping_name}} <br>
                            @endif
                        </p>
                        @if($order->shipping_pickup)
                            <p class="mb-3">
                                <b>Rok za preuzimanje</b> <br>
                                {{$order->created_at->addDays(config('app.pickup_days_limit'))->format('d.m.Y.')}} <br>
                            </p>
                        @endif
                        <p class="mb-3">
                            <b>Način plaćanja</b> <br>
                            @if($order->payment_method == 'online_payment')
                                Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif
                            @else
                                Pouzećem @if($order->shipping_pickup){{'(gotovina, kartično)'}}@else{{'(gotovina)'}}@endif
                            @endif
                        </p>
                        <p>
                            <b>Broj narudžbe</b> <br>
                            {{$order->order_number}}
                        </p>
                    </div>
                    @if($order->additional_info)
                        <div class="col-12">
                            <p class="mt-3">
                                <b>Napomene uz narudžbu</b> <br>
                                {{$order->additional_info}}
                            </p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="d-none d-lg-block col-lg-5">
                @include('checkout/partials/cart-info-success')
            </div>
        </div>
    </div>
    <checkout-success :order="{{$order->toSearchableJson()}}" :total-cart-price="{{$order->total_price}}"></checkout-success>
@endsection