<div class="checkout-cart-sidebar h-100">
    <div class="checkout-cart-content d-flex flex-wrap">
        <ul class="checkout-cart-wrapitem w-100">
            @foreach ($cart->products as $product)
                <li class="header-cart-item d-flex">
                    <div class="checkout-cart-item-img">
                        <a href="{{$product->url()}}" target="_blank">
                            @if($product->hasPhotos())
                                <img src="{{$product->mainPhoto()->url('thumb')}}" alt="{{$product->fullName()}}">
                            @else
                                <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}">
                            @endif
                        </a>
                    </div>

                    <div class="checkout-cart-item-text">
                        <a href="{{$product->url()}}" target="_blank" class="checkout-cart-item-name">
                            @if($product->brand)
                                <div>{{$product->brand->name}}</div>
                            @endif
                            {{$product->name()}}
                            @if($product->package_description)
                                <div>{{$product->package_description}}</div>
                            @endif
                            @if($product->pivot->variants)
                                <div>Br. {{$product->pivot->variants}}</div>
                            @endif
                            <span class="checkout-cart-item-info">
                                {{$product->pivot->quantity}} x {{formatPrice($product->realPrice())}}
                            </span>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        <div class="checkout-cart-total pb-3 w-100 d-flex">
            <span style="width:35%">Cijena:</span>
            <span>{{formatPrice($cart->totalPrice())}}</span>
        </div>

        @if($cart->shippingMethod)
            <div class="checkout-cart-total pb-3 w-100">
                <div class="d-flex">
                    <span style="width:35%">Dostava:</span>
                    <span>+ {{formatPrice($cart->free_shipping ? 0 : $cart->shippingMethod->price)}}</span>
                </div>

                <div class="d-flex">
                    @if($cart->shippingMethod->pickup)
                        <span style="font-size: 12px;margin-top: 10px;width:35%;">
                            U poslovnici:
                        </span>
                    @else
                        <span style="font-size: 12px;margin-top: 10px;width:35%;">
                            Na adresu:
                        </span>
                    @endif
    
                    <span style="font-size: 12px;margin-top: 10px;">
                        {{$cart->shippingMethod->name}}
                    </span>
                </div>
            </div>
        @endif

        <div class="checkout-cart-total total-price pt-3 w-100">
            <div class="d-flex font-weight-bold">
                <span style="width:35%">Ukupno:</span>
                <span>{{formatPrice($cart->totalPriceWithShipping())}}</span>
            </div>

            @if($cart->free_shipping)
                <div class="free-shipping-success"><i class="fa fa-truck mr-1" aria-hidden="true"></i> Ostvarili ste besplatnu dostavu! 🎉🎊</div>
                @elseif((config('app.free_shipping_threshold') - $cart->totalPrice()) <= config('app.free_shipping_threshold_alert') && (config('app.free_shipping_threshold') - $cart->totalPrice()) > 0)
                <div class="free-shipping-warning"><i class="fa fa-truck mr-1" aria-hidden="true"></i> {{formatPrice(config('app.free_shipping_threshold') - $cart->totalPrice())}} Vas dijeli do besplatne dostave!</div>
            @endif
        </div>
    </div>
</div>