<div class="top-bar d-lg-none">
    <header-alert 
        admin-link="{{route('backend.home')}}"
        locations-link="{{route('locations')}}"
        register-link="{{route('register')}}"
        settings-link="{{route('user.settings')}}"
        logout-link="{{route('logout')}}"
        login-link="{{route('login')}}"
        :show-admin-link="{{auth()->check() && auth()->user()->hasAnyRole('admin|employee|writer') ? 'true' : 'false'}}"
    ></header-alert>
</div>

<div class="checkout-header text-center">
    <a href="{{route('home')}}">
        <img src="{{asset('images/logos/horizontal-logo.svg')}}" alt="{{config('app.name')}}">
    </a>
</div>