<div class="d-lg-none collapse-block sticky-top" style="top: 25px">
    <div class="w-100 p-3 collapsed collapsed-handle d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#footerCollapseWhyToBuy" aria-expanded="false" aria-controls="footerCollapseWhyToBuy">
        <div class="font-weight-bold label">
            <i class="fa fa-shopping-cart mr-1" aria-hidden="true" style="font-size: 20px;"></i> Korpa
            <i class="fa fa-chevron-down ml-1" aria-hidden="true"></i>
            <i class="fa fa-chevron-up ml-1" aria-hidden="true"></i>
        </div>
        <div class="price font-weight-bold" v-cloak>
            @if(request()->route()->getName() == 'checkout.one')
                @{{totalPrice | formatPrice}}
            @elseif(request()->route()->getName() == 'checkout.two' || request()->route()->getName() == 'checkout.three')
                {{formatPrice($cart->totalPriceWithShipping())}}
            @elseif(request()->route()->getName() == 'checkout.success')
                {{formatPrice($order->totalPriceWithShipping())}}
            @endif
        </div>
    </div>
    <div class="collapse" id="footerCollapseWhyToBuy" style="overflow: auto;max-height: 500px;">
        <div class="px-3">
            @if(request()->route()->getName() == 'checkout.one')
                @include('checkout/partials/cart-info-one')
            @elseif(request()->route()->getName() == 'checkout.two' || request()->route()->getName() == 'checkout.three')
                @include('checkout/partials/cart-info-two')
            @elseif(request()->route()->getName() == 'checkout.success')
                @include('checkout/partials/cart-info-success')
            @endif
        </div>
    </div>
</div>