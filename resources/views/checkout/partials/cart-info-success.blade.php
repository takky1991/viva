<div class="checkout-cart-sidebar h-100">
    <div class="checkout-cart-content d-flex flex-wrap">
        <ul class="checkout-cart-wrapitem w-100">
            @foreach ($order->orderProducts as $orderProduct)
                <li class="header-cart-item d-flex">
                    <div class="checkout-cart-item-img">
                        <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank">
                            @if($orderProduct->product && $orderProduct->product->hasPhotos())
                                <img src="{{$orderProduct->product->mainPhoto()->url('thumb')}}" alt="{{$orderProduct->fullName()}}">
                            @else
                                <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}">
                            @endif
                        </a>
                    </div>

                    <div class="checkout-cart-item-text">
                        <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" target="_blank" class="checkout-cart-item-name">
                            @if($orderProduct->brand_name)
                                <div>{{$orderProduct->brand_name}}</div>
                            @endif
                            {{$orderProduct->name()}}
                            @if($orderProduct->package_description)
                                <div>{{$orderProduct->package_description}}</div>
                            @endif
                            @if($orderProduct->variants)
                                <div>Br. {{$orderProduct->variants}}</div>
                            @endif
                            <span class="checkout-cart-item-info">
                                {{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}}
                            </span>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        <div class="checkout-cart-total pb-3 w-100 d-flex">
            <span style="width:35%">Cijena:</span>
            <span>{{formatPrice($order->total_price)}}</span>
        </div>

        <div class="checkout-cart-total pb-3 w-100">
            <div class="d-flex">
                <span style="width:35%">Dostava:</span>
                <span>+ {{formatPrice($order->shipping_price)}}</span>
            </div>

            <div class="d-flex">
                @if($order->shipping_pickup)
                    <span style="font-size: 12px;margin-top: 10px;width:35%;">U poslovnici:</span>
                @else
                    <span style="font-size: 12px;margin-top: 10px;width:35%;">
                        Na adresu:
                    </span>
                @endif
    
                <span style="font-size: 12px;margin-top: 10px;">
                    {{$order->shipping_name}}
                </span>
            </div>
        </div>
        <div class="checkout-cart-total total-price pt-3 w-100 d-flex font-weight-bold">
            <span style="width:35%">Ukupno:</span>
            <span>{{formatPrice($order->total_price_with_shipping)}}</span>
        </div>
    </div>
</div>