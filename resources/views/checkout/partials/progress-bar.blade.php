<div class="progress-steps d-flex align-items-center justify-content-center">
    <div class="step {{request()->route()->getName() == 'checkout.one' ? 'active' : ''}}">
        <a href="{{route('checkout.one')}}" class="step-name">Dostava</a>
    </div>
    <i class="fa fa-chevron-right" aria-hidden="true"></i>
    <div class="step {{request()->route()->getName() == 'checkout.two' ? 'active' : ''}}">
        <a href="{{route('checkout.two')}}" class="step-name">Informacije</a>
    </div>
    <i class="fa fa-chevron-right" aria-hidden="true"></i>
    <div class="step {{request()->route()->getName() == 'checkout.three' ? 'active' : ''}}">
        <a href="{{route('checkout.three')}}" class="step-name">Plaćanje</a>
    </div>
</div>

<header-cart class="d-none" @if(!empty($cartJson)) :cart="{{$cartJson}}" @endif></header-cart>

@if(isset($cartProductPriceChanged) && $cartProductPriceChanged)
    <div class="alert alert-info" role="alert">
        Od Vaše zadnje posjete došlo je do promjene cijene jednog od proizvoda u Vašoj korpi.
    </div>
@endif

@if(isset($cartProductRemoved) && $cartProductRemoved)
    <div class="alert alert-info" role="alert">
        Od Vaše zadnje posjete uklonjen je jedan od proizvoda iz Vaše korpe zbog nedostupnosti.
    </div>
@endif

{{-- <div class="alert alert-success" role="alert">
    Sve narudžbe koje sadrže popuste za Black Friday šalju se na dan popusta.    
</div>

<div class="alert alert-info" role="alert">
    Ukoliko želite proizvode ranije, navedite u napomeni. U tom slučaju popust se ne obračunava.
</div> --}}