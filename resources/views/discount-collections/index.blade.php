@extends('layouts.app')

@section('content')

<div class="products-index mt-4">
	<div class="container-xl">
		<div class="row">
			<div class="d-none d-lg-block col-lg-3 product-filters">
				<span>{{$products->total()}} Proizvoda</span>
				<hr>
				@isset($mainMenuDiscounts)
					<div class="categories">
						<ul>
							@foreach($mainMenuDiscounts as $mainMenuDiscount)
								<li>
                                    @if(isset($currentDiscount) && ($mainMenuDiscount->id == $currentDiscount->id))
                                        <span class="font-weight-bold">{{$mainMenuDiscount->name}}</span>
                                    @else
                                        <a href="{{route('discount_collections.show', ['discountCollection' => $mainMenuDiscount->getSlug()])}}">
                                            {{$mainMenuDiscount->name}}
                                        </a>
                                    @endif
								</li>
							@endforeach
						</ul>
					</div>
				@endisset
			</div>
			<div class="col-lg-9" id="product-results">
				@isset($mainMenuDiscounts)
					<div class="d-lg-none mobile-categories-wrap">
						<div class="mobile-categories">
							@if(isset($currentDiscount))
								<a 
									href="{{route('discount_collections.show', ['discountCollection' => $currentDiscount->getSlug()])}}" 
									class="btn btn-primary btn-sm mr-2"
								>
									{{$currentDiscount->name}}
								</a>
							@endif
							@foreach ($mainMenuDiscounts as $mainMenuDiscount)
								@if(!isset($currentDiscount) || isset($currentDiscount) && ($mainMenuDiscount->id != $currentDiscount->id))
									<a 
										href="{{route('discount_collections.show', ['discountCollection' => $mainMenuDiscount->getSlug()])}}" 
										class="btn btn-outline-primary btn-sm mr-2"
									>
										{{$mainMenuDiscount->name}}
									</a>
								@endif
							@endforeach
							<div class="mr-5 d-inline-block"></div>
						</div>
					</div>
				@endisset
				<div class="d-lg-none py-4">{{$products->total()}} Proizvoda</div>
				@if($products->isNotEmpty())
					@include('products/partials/products-grid', [
						'skipLazyLoad' => 4,
						'dataLayerList' => 'Discount Products List'
					])
				@else
					<div class="alert alert-warning" role="alert">
						Proizvodi nisu pronađeni
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection