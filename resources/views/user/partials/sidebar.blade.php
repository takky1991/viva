<div class="side-menu">
    <h4>Moj račun</h4>

    <br>

    <ul>
        <li class="bor18 {{request()->route()->getName() == 'user.settings' ? 'active' : ''}}">
            <a href="{{route('user.settings')}}">
                Informacije o računu
            </a>
        </li>

        <li class="bor18 {{request()->route()->getName() == 'user.orders' ? 'active' : ''}}">
            <a href="{{route('user.orders')}}">
                Narudžbe
            </a>
        </li>

        <li class="bor18 {{request()->route()->getName() == 'user.password' ? 'active' : ''}}">
            <a href="{{route('user.password')}}">
                Promjena šifre
            </a>
        </li>
    </ul>
</div>