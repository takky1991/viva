@extends('layouts.app')

@section('content')
<section class="user-settings py-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-md-4 col-lg-3 pb-5">
                @include('user/partials/sidebar')
            </div>
            <div class="col-md-8 col-lg-9">
                <form class="form-horizontal" method="POST" action="{{ route('user.password') }}">
                    {{ csrf_field() }}

                    <h4>Promjena šifre</h4>

                    <br>

                    @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                        <br>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="old_password">Stara šifra</label>
                                <input id="old-password" type="password" class="input-field @if($errors->has('old_password')) invalid @endif" name="old_password" placeholder="Unesite staru šifru">
                                @if ($errors->has('old_password'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('old_password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Nova šifra</label>
                                <input id="password" type="password" class="input-field @if($errors->has('password')) invalid @endif" name="password" placeholder="Unesite novu šifru">
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_confirmation">Potvrda nove šifre</label>
                                <input id="password-confirm" type="password" class="input-field @if($errors->has('password_confirmation')) invalid @endif" name="password_confirmation" placeholder="Ponovno unesite novu šifru">
                                @if ($errors->has('password_confirmation'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
    
                    <br>

                    <button id="save-password-button" type="submit" class="btn btn-primary pull-right">Promjena šifre <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection