@extends('layouts.app')

@section('content')
<section class="user-settings py-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-md-4 col-lg-3 pb-5">
                @include('user/partials/sidebar')
            </div>
            <div class="col-md-8 col-lg-9">
                <h4>Moje narudžbe</h4>

                <br>

                <div class="accordion accordion-orders" id="accordionExample">
                    @forelse ($orders as $order)
                        <div class="card">
                            <div class="card-header" id="heading-{{$loop->index}}">
                                <h2 class="mb-0">
                                    <button class="btn btn-link link-text p-0" type="button" data-toggle="collapse" data-target="#collapse-{{$loop->index}}" aria-expanded="{{$loop->index ? 'false' : 'true'}}" aria-controls="collapse-{{$loop->index}}">
                                        Narudžba {{$order->order_number}}
                                    </button>
                                    <span class="order-date">{{$order->created_at->format('d.m.Y H:i')}}</span>
                                </h2>
                            </div>
                        
                            <div id="collapse-{{$loop->index}}" class="collapse {{$loop->index ? '' : 'show'}}" aria-labelledby="heading-{{$loop->index}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5 pb-3">
                                            <b>Status narudžbe:</b> <br>
                                            @switch($order->status)
                                                @case('created')
                                                    <span class="badge badge-warning">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('ready_for_pickup')
                                                    <span class="badge badge-info">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('ready_for_sending')
                                                    <span class="badge badge-info">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('shipped')
                                                    <span class="badge badge-primary">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('fulfilled')
                                                    <span class="badge badge-success">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('canceled')
                                                    <span class="badge badge-secondary">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                                @case('returned')
                                                    <span class="badge badge-danger">{{\Viva\Order::$statusWording[$order->status]}}</span>
                                                    @break
                                            @endswitch
                                            <br>
                                            <br>
                                            <b>Način preuzimanja:</b> <br> 
                                            @if($order->shipping_pickup) 
                                                Preuzimanje u {{$order->shipping_name}} 
                                            @else 
                                                Slanje na adresu <br> {{$order->shipping_name}}
                                            @endif <br>
                                            <br>
                                            <b>Način plaćanja:</b> <br> 
                                            @if($order->payment_method == 'online_payment')
                                                Kartično @if($order->card_number_masked && $order->card_type){{$order->card_number_masked}} - {{$order->card_type}}@endif<br>
                                            @else
                                                Pouzećem @if($order->shipping_pickup){{'(gotovina, kartično)'}}@else{{'(gotovina)'}}@endif<br>
                                            @endif
                                            <br>
                                            <b>Podaci o kupcu:</b> <br>
                                            {{$order->first_name}} {{$order->last_name}} <br>
                                            @if(!$order->shipping_pickup)
                                                {{$order->street_address}} <br>
                                                {{$order->postalcode}} {{$order->city}} <br>
                                                {{$order->country->name}} <br>
                                            @endif
                                            Tel: {{$order->phone}} <br>
                                            Email: {{$order->email}} <br>
                                            <br>
                                            <b>Broj narudžbe:</b> {{$order->order_number}}
                                        </div>
                                        <div class="col-md-7">
                                            <div class="table-responsive">
                                                <table class="table table-sm">
                                                    <tbody>
                                                        @foreach ($order->orderProducts as $orderProduct)
                                                            <tr>
                                                                <td>
                                                                    @if($orderProduct->product && $orderProduct->product->hasPhotos())
                                                                        <img src="{{$orderProduct->product->mainPhoto()->url('thumb')}}" alt="{{$orderProduct->fullName()}}" width="50px">
                                                                    @else
                                                                        <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" width="50px">
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <a href="{{$orderProduct->product ? $orderProduct->product->url() : ''}}" class="link-text" target="_blank">
                                                                        {{$orderProduct->fullName()}}
                                                                        @if($orderProduct->variants)
                                                                            <br>
                                                                            Br. {{$orderProduct->variants}}
                                                                        @endif
                                                                    </a>
                                                                </td>
                                                                <td>{{$orderProduct->quantity}} x {{formatPrice($orderProduct->realPrice())}}</td>
                                                                <td>{{formatPrice($orderProduct->quantity * $orderProduct->realPrice())}}</td>
                                                            </tr>
                                                        @endforeach
                                                      </tbody>
                                                </table>
                                                <hr>
                                                Proizvodi: {{formatPrice($order->total_price)}} <br>
                                                Dostava: {{formatPrice($order->shipping_price)}} <br>
                                                <b>Ukupno: {{formatPrice($order->total_price_with_shipping)}}</b>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            @if($order->additional_info)
                                                <b>Napomene uz narudžbu:</b> <br>
                                                {{$order->additional_info}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-info" role="alert">
                            Trenutno nemate narudžbi.
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection