@extends('layouts.app')

@section('content')
<section class="user-settings py-5">
    <div class="container-xl">
        <div class="row">
            <div class="col-md-4 col-lg-3 pb-5">
                @include('user/partials/sidebar')
            </div>
            <div class="col-md-8 col-lg-9">
                <form class="form-horizontal" method="POST" action="{{ route('user.settings') }}">
                    {{ csrf_field() }}

                    <h4>Informacije o računu</h4>
                    
                    @if(session()->has('success'))
                        <br>
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if (sizeof($errors))
                        <br>
                        <div class="alert alert-danger" role="alert">
                            Unesite sva potrebna polje
                        </div>
                    @endif

                    <br>

                    <h5>Vaši osobni podaci</h5>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">Ime *</label>
                                <input id="first-name" type="text" class="input-field @if($errors->has('first_name')) invalid @endif" name="first_name" value="{{ $user->first_name }}" placeholder="Unesite ime">
                                @if ($errors->has('first_name'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Prezime *</label>
                                <input id="last-name" type="text" class="input-field @if($errors->has('last_name')) invalid @endif" name="last_name" value="{{ $user->last_name }}" placeholder="Unesite prezime">
                                @if ($errors->has('last_name'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('last_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="day">Datum rođenja</label>
                                <div class="row">
                                    <div class="col">
                                        <select id="birth-day" name="birth_day" class="custom-select input-field @if($errors->has('birth_date')) invalid @endif">
                                            <option {{empty($user->birth_date) ? 'selected' : ''}}>Dan</option>
                                            @for ($day = 1; $day <= 31; $day++)
                                                <option value="{{$day}}" {{$user->birth_date?->day == $day ? 'selected' : ''}}>{{$day}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select id="birth-month" name="birth_month" class="custom-select input-field @if($errors->has('birth_date')) invalid @endif">
                                            <option {{empty($user->birth_date) ? 'selected' : ''}}>Mjesec</option>
                                            <option value="1" {{$user->birth_date?->month == 1 ? 'selected' : ''}}>Januar</option>
                                            <option value="2" {{$user->birth_date?->month == 2 ? 'selected' : ''}}>Februar</option>
                                            <option value="3" {{$user->birth_date?->month == 3 ? 'selected' : ''}}>Mart</option>
                                            <option value="4" {{$user->birth_date?->month == 4 ? 'selected' : ''}}>April</option>
                                            <option value="5" {{$user->birth_date?->month == 5 ? 'selected' : ''}}>Maj</option>
                                            <option value="6" {{$user->birth_date?->month == 6 ? 'selected' : ''}}>Juni</option>
                                            <option value="7" {{$user->birth_date?->month == 7 ? 'selected' : ''}}>Juli</option>
                                            <option value="8" {{$user->birth_date?->month == 8 ? 'selected' : ''}}>August</option>
                                            <option value="9" {{$user->birth_date?->month == 9 ? 'selected' : ''}}>Septembar</option>
                                            <option value="10" {{$user->birth_date?->month == 10 ? 'selected' : ''}}>Oktobar</option>
                                            <option value="11" {{$user->birth_date?->month == 11 ? 'selected' : ''}}>Novembar</option>
                                            <option value="12" {{$user->birth_date?->month == 12 ? 'selected' : ''}}>Decembar</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select id="birth-year" name="birth_year" class="custom-select input-field @if($errors->has('birth_date')) invalid @endif">
                                            <option {{empty($user->birth_date) ? 'selected' : ''}}>Godina</option>
                                            @for ($year = now()->year; $year > now()->year - 100; $year--)
                                                <option value="{{$year}}" {{$user->birth_date?->year == $year ? 'selected' : ''}}>{{$year}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                @if ($errors->has('birth_date'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('birth_date') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gender">Rod</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="customRadio" name="gender" value="1" {{ $user->gender == 1 ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customRadio" id="customRadioLabel">Muški</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="customRadio1" name="gender" value="2" {{ $user->gender == 2 ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customRadio1" id="customRadioLabel1">Ženski</label>
                                </div>
                                @if ($errors->has('gender'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('gender') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail *</label>
                                <input id="email" type="email" class="input-field @if($errors->has('email')) invalid @endif" name="email" value="{{ $user->email }}" placeholder="Unesite e-mail" disabled>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">Broj telefona</label>
                                <phone-input phone-number="{{$user->phone}}"></phone-input>
                                @if ($errors->has('phone'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('phone') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <br>
                    <br>

                    <h5>Vaša adresa</h5>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="street_address">Adresa ulice</label>
                                <input id="street-address" type="text" class="input-field @if($errors->has('street_address')) invalid @endif" name="street_address" value="{{ old('street_address') ?: $user->street_address }}" placeholder="Unesite adresu ulice">
                                @if ($errors->has('street_address'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('street_address') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="postalcode">Poštanski broj</label>
                                <input id="postalcode" type="number" class="input-field @if($errors->has('postalcode')) invalid @endif" name="postalcode" value="{{ old('postalcode') ?: $user->postalcode }}" placeholder="Unesite poštanski broj">
                                @if ($errors->has('postalcode'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('postalcode') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">Grad</label>
                                <input id="city" type="text" class="input-field @if($errors->has('city')) invalid @endif" name="city" value="{{ old('city') ?: $user->city }}" placeholder="Unesite grad">
                                @if ($errors->has('city'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('city') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country_id">Država</label>
                                <select id="country_id" name="country_id" class="custom-select input-field @if($errors->has('country_id')) invalid @endif">
                                    <option value="1" selected disabled>{{$country->name}}</option>
                                </select>
                                @if ($errors->has('country_id'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('country_id') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <br>
                    <br>

                    <h5>Newsletter</h5>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="hidden" name="newsletter" value="0">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="customCheckbox" name="newsletter" value="1" {{ $user->newsletterContact ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customCheckbox" id="customCheckboxLabel">Želim da me obavještavate o akcijama i novostima putem emaila</label>
                                </div>
                                @if ($errors->has('newsletter'))
                                    <div class="invalid-feedback" style="display: block; font-size: 14px;">
                                        {{ $errors->first('newsletter') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">

                        </div>
                    </div>

                    <br>

                    <button id="save-settings-button" type="submit" class="btn btn-primary pull-right">Spremi <i class="fa fa-chevron-right ml-2" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection