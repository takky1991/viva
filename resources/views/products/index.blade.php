@extends('layouts.app')

@section('content')

<div class="products-index mt-4">
	<div class="container-xl">
		<div class="row">
			<div class="d-none d-lg-block col-lg-3 product-filters">
				<span>{{$products->total()}} Proizvoda</span>
				<hr>
				{{-- <products-filter inline-template>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="on_sale" v-model="filters.onSale">
						<label class="custom-control-label" for="on_sale">Na popustu</label>
					</div>
				</products-filter> --}}
				@isset($mainMenuCategories)
					<div class="categories">
						<ul>
							@foreach($mainMenuCategories as $mainCategory)
								<li>
									<a href="{{route('categories.show', ['category' => $mainCategory->getSlug()])}}">{{$mainCategory->title}}</a>
								</li>
							@endforeach
						</ul>
					</div>
				@endisset
			</div>
			<div class="col-lg-9" id="product-results">
				<div class="d-lg-none mobile-categories-wrap">
					<div class="mobile-categories">
						@foreach ($mainMenuCategories as $item)
							<a href="{{route('categories.show', ['category' => $item->getSlug()])}}" class="btn btn-outline-primary btn-sm mr-2">{{$item->title}}</a>
						@endforeach
						<div class="mr-5 d-inline-block"></div>
					</div>
				</div>
				<div class="d-lg-none py-4">{{$products->total()}} Proizvoda</div>
				@if($products->isNotEmpty())
					@include('products/partials/products-grid', [
						'skipLazyLoad' => 4,
						'dataLayerList' => 'Search Products List'
					])
				@else
					<div class="alert alert-warning" role="alert">
						Proizvodi nisu pronađeni
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection