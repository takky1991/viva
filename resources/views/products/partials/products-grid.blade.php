<div class="products-grid" id="products-grid">
    <div class="row" itemscope itemtype="http://schema.org/ItemList">
        @foreach($products as $index => $product)
            <div class="product-wrap col-6 col-md-4 col-lg-4 col-xl-4">
                @if(isset($skipLazyLoad) && $loop->iteration <= $skipLazyLoad)
                    @include('products/partials/product', [
                        'lazyLoadPhoto' => false,
                        'dataLayerList' => $dataLayerList,
                        'dataLayerPosition' => $products instanceof \Illuminate\Pagination\LengthAwarePaginator ?
                            (($products->currentPage() - 1) * $products->perPage() + $index) : $index
                    ])
                @else
                    @include('products/partials/product', [
                        'dataLayerList' => $dataLayerList,
                        'dataLayerPosition' => $products instanceof \Illuminate\Pagination\LengthAwarePaginator ?
                            (($products->currentPage() - 1) * $products->perPage() + $index) : $index
                    ])
                @endif
            </div>
        @endforeach
    </div>

    @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator)
        <div class="d-flex justify-content-center w-100">
            {{$products->onEachSide(1)->appends(request()->all())->links()}}
        </div>
    @endif
</div>

@if(!isset($inCarousel) || !$inCarousel)
    @push('gtm_data_layer')
        <script>
            dataLayer.push({ ads: null }); // Clear the previous ads object.
            dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
            dataLayer.push({
                event: 'view_item_list',
                ecommerce: {
                    item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
                    item_list_name: '{{$dataLayerList}}',
                    items: [
                        @foreach($products as $index => $product)
                        {
                            item_id: {{$product->id}},
                            item_name: '{{$product->fullName()}}',
                            currency: 'BAM',
                            item_brand: '{{$product->brand->name}}',
                            index: {{$products instanceof \Illuminate\Pagination\LengthAwarePaginator ? (($products->currentPage() - 1) * $products->perPage() + $index) : $index}},
                            item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
                            item_list_name: '{{$dataLayerList}}',
                            location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                            price: {{$product->realPrice()}},
                            quantity: 1,

                            @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                                item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                            @endif

                            @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                                item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                            @endif

                            @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                                item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                            @endif
                        },
                        @endforeach
                    ]
                }
            });
        </script>
    @endpush
@endif