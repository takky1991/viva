<article id="product-{{$loop->iteration}}" class="product" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
	<meta itemprop="position" content="{{$loop->iteration}}"/>
	<div itemprop="item" itemtype="http://schema.org/Product" itemscope>
		<meta itemprop="url" content="{{$product->url()}}" />
		<div class="image-wrap">
			<a 
				href="{{$product->url()}}"
				onclick="
					dataLayer.push({ ads: null }); // Clear the previous ads object.
                    dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
					dataLayer.push({
						event: 'select_item',
						ecommerce: {
							@isset($dataLayerList)
								item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
								item_list_name: '{{$dataLayerList}}',
							@endisset

							items: [{
							 	item_id: {{$product->id}},
								item_name: '{{$product->fullName()}}',
								item_brand: '{{$product->brand->name}}',
								price: {{$product->realPrice()}},
								currency: 'BAM',
								quantity: 1,
								location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',

								@isset($dataLayerPosition)
									index: {{$dataLayerPosition}},
								@endisset

								@isset($dataLayerList)
									item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
									item_list_name: '{{$dataLayerList}}',
								@endisset

								@if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
									item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
								@endif

								@if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
									item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
								@endif

								@if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
									item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
								@endif
							}]
						}
					});
				"
			>
				<div class="img">
					@if($product->hasPhotos())
						<link itemprop="image" href="{{$product->mainPhoto()->url('product_profile')}}" />
						@if(isset($lazyLoadPhoto) && !$lazyLoadPhoto) 
							<img 
								src="{{$product->mainPhoto()->url('product_profile')}}" 
								alt="{{$product->fullName()}}" 
								width="250" 
								height="250"
							>
						@else
							<img 
								src="{{asset('images/spin_icon.jpg')}}"
								data-src="{{$product->mainPhoto()->url('product_profile')}}" 
								alt="{{$product->fullName()}}" 
								width="250" 
								height="250"
								v-lazy-img
							>
						@endif
					@else
						<img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" itemprop="image">
					@endif
					@if(
						(request()->route()->getName() == 'brands.show' && $product->best_seller_in_brand) || // for brands show page
						(request()->route()->getName() == 'categories.show' && $product->categories && $product->categories->first()->pivot->best_seller_in_category) || // for categories show page
						(!in_array(request()->route()->getName(), ['brands.show', 'categories.show']) && ($product->best_seller_in_brand || $product->best_seller_in_category)) // for products in other lists like related or homepage
					)
						<div class="popular-badge">
							<i class="fa fa-star" aria-hidden="true"></i> Popularno
						</div>
					@endif
					@if($product->on_sale)
						<div class="photo-badge-discount">-{{$product->getDiscountPercentage()}}%</div>
					@endif
					<div class="photo-badge-bottom">
						@if($product->featured_text)
						    <div class="free-shipping">{{$product->featured_text}}</div>
                        @endif
						@if(!$product->in_stock)
							<div class="not-available mt-1">Nije dostupno</div>
						@elseif($product->hasFreeShipping())
							<div class="free-shipping mt-1">Besplatna dostava</div>
						@endif
					</div>
				</div>
			</a>
		</div>

		<div class="product-info pt-2">
			<a 
				href="{{$product->url()}}" 
				onclick="
					dataLayer.push({ ads: null }); // Clear the previous ads object.
                    dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
					dataLayer.push({
						event: 'select_item',
						ecommerce: {
							@isset($dataLayerList)
								item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
								item_list_name: '{{$dataLayerList}}',
							@endisset

							items: [{
							 	item_id: {{$product->id}},
								item_name: '{{$product->fullName()}}',
								item_brand: '{{$product->brand->name}}',
								price: {{$product->realPrice()}},
								currency: 'BAM',
								quantity: 1,
								location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',

								@isset($dataLayerPosition)
									index: {{$dataLayerPosition}},
								@endisset

								@isset($dataLayerList)
									item_list_id: '{{\Illuminate\Support\Str::slug($dataLayerList, '_')}}',
									item_list_name: '{{$dataLayerList}}',
								@endisset

								@if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
									item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
								@endif

								@if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
									item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
								@endif

								@if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
									item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
								@endif
							}]
						}
					});
				">
				<div class="brand line-clamp-1 mb-1" itemprop="brand" itemtype="http://schema.org/Brand" itemscope>
					@if($product->brand)
						<meta itemprop="name" content="{{$product->brand->name}}" />
						{{$product->brand->name}}
					@endif
				</div>
				@if(isset($inCarousel) && $inCarousel)
					<h3 class="title mb-1 line-clamp-2">{{$product->name()}}</h3>
				@else
					<h2 class="title mb-1 line-clamp-2">{{$product->name()}}</h2>
				@endif
				<meta itemprop="name" content="{{$product->fullName()}}">
				<div class="package-description line-clamp-1" itemprop="description">
					@if($product->package_description)
						{{$product->package_description}}
					@endif
				</div>

				@if($product->rating && $product->rating_count)
                    <div class="product-rating mt-2" itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
						<meta itemprop="reviewCount" content="{{$product->rating_count}}" />
						<meta itemprop="ratingValue" content="{{$product->rating}}" />

                        @for($i = 0; $i < $product->rating; $i++)
                            <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                        @for($i = 0; $i < 5 - $product->rating; $i++)
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        @endfor
                        <span class="ml-1">({{$product->rating_count}})</span>
                    </div>
                @else
                    <div class="product-rating empty mt-2" itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
						<meta itemprop="reviewCount" content="{{rand(10, 100)}}" />
						<meta itemprop="ratingValue" content="{{rand(40, 50) / 10}}" />
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                @endif

				<div itemprop="review" itemtype="http://schema.org/Review" itemscope>
					<div itemprop="author" itemtype="http://schema.org/Person" itemscope>
						<meta itemprop="name" content="{{$randomReviewerNames[array_rand($randomReviewerNames)]}}" />
					</div>
					<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
						<meta itemprop="ratingValue" content="{{rand(40, 50) / 10}}" />
						<meta itemprop="bestRating" content="5" />
					</div>
				</div>

				<hr class="line">
				<div class="price-description" itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
					<link itemprop="url" href="{{$product->url()}}" />
					<meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
					<meta itemprop="price" content="{{$product->realPrice()}}" />
					<meta itemprop="pricecurrency" content="BAM">
					<meta itemprop="priceValidUntil" content="{{now()->addYears(3)->format('Y-m-d')}}" />
					<span class="price @if($product->onSale()) discount @endif">
						{{formatPrice($product->price)}}
					</span>
					@if($product->onSale())
						<span class="discount-price">
							{{formatPrice($product->discount_price)}}
						</span>
					@endif
					@if($product->in_stock)
						<meta itemprop="availability" content="https://schema.org/InStock" />
					@else
						<meta itemprop="availability" content="https://schema.org/OutOfStock" />
					@endif
				</div>

				<meta itemprop="sku" content="{{$product->sku}}" />
				<meta itemprop="mpn" content="{{$product->sku}}" />
			</a>
		</div>
	</div>
</article>