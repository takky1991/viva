@extends('layouts.app')

@section('content')

<!-- Product Detail -->
<section class="products-show">
    <article class="container-xl" itemscope itemtype="http://schema.org/Product">
        @if(session()->has('review-success'))
            <div class="alert alert-success mt-3">
                {{session()->get('review-success')}}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-6 text-center image-col">
                <div class="image-wrap sticky-top">
                    @if($product->mainPhoto())
                        <meta itemprop="image" content="{{$product->mainPhoto()->url('product_show')}}" />
                        <image-zoom 
                            alt="{{$product->fullName()}}"
                            regular="{{$product->mainPhoto()->url('product_show')}}" 
                            zoom="{{$product->mainPhoto()->url('original')}}"
                            :click-zoom="true"
                            close-pos="bottom-right"
                            click-message="<i class='fa fa-search-plus' aria-hidden='true'></i>"
                            hover-message="<i class='fa fa-search-plus' aria-hidden='true'></i>"
                            touch-message="<i class='fa fa-search-plus' aria-hidden='true'></i>"
                        >
                            <img src="{{$product->mainPhoto()->url('product_show')}}" alt="{{$product->fullName()}}" itemprop="image" width="500" height="500">
                        </image-zoom>
                    @else
                        <img src="{{asset('/images/logo_270_contrast.png')}}" alt="{{config('app.name')}}" itemprop="image">
                    @endif
                    @if($product->best_seller_in_brand || $product->best_seller_in_category)
						<div class="popular-badge big">
							<i class="fa fa-star" aria-hidden="true"></i> Popularno
						</div>
					@endif
                    @if($product->on_sale)
						<div class="photo-badge-discount big">-{{$product->getDiscountPercentage()}}%</div>
					@endif
                    <div class="photo-badge-bottom">
                        @if($product->featured_text)
						    <div class="free-shipping big">{{$product->featured_text}}</div>
                        @endif
						@if(!$product->in_stock)
                            <div class="not-available big mt-1">Nije dostupno</div>
                        @elseif($product->hasFreeShipping())
                            <div class="free-shipping big mt-1">Besplatna dostava</div>
                        @endif
					</div>
                </div>
            </div>

            <div class="col-lg-6">
                <h1 class="product-title" itemprop="name">
                    {{$product->nameWithBrand()}}
                    @if($product->package_description)
                        <span class="package-description" itemprop="description">{{$product->package_description}}</span>
                    @endif
                </h1>

                <br>

                @if($product->rating && $product->rating_count)
                    <a 
                        href="#reviews" 
                        class="product-rating text-center d-block" 
                        onclick="$('html, body').animate({scrollTop: $('#reviews').offset().top -100 }, 'slow');"
                        itemprop="aggregateRating" 
                        itemtype="http://schema.org/AggregateRating" 
                        itemscope
                    >
                        <meta itemprop="reviewCount" content="{{$product->rating_count}}" />
                        <meta itemprop="ratingValue" content="{{$product->rating}}" />

                        @for($i = 0; $i < $product->rating; $i++)
                            <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                        @for($i = 0; $i < 5 - $product->rating; $i++)
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        @endfor
                        <span class="ml-1">({{$product->rating_count}})</span>
                        <div class="review-cta">Šta drugi misle o proizvodu</div>
                    </a>

                    <div itemprop="review" itemtype="http://schema.org/Review" itemscope>
                        @foreach ($product->publishedReviews()->take(5)->get() as $review)
                            <div itemprop="author" itemtype="http://schema.org/Person" itemscope>
                                <meta itemprop="name" content="{{$review->name}}" />
                            </div>
                            <div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
                                <meta itemprop="ratingValue" content="{{$review->rating}}" />
                                <meta itemprop="bestRating" content="5" />
                            </div>
                        @endforeach
                    </div>
                @else
                    <a 
                        href="#reviews" 
                        class="product-rating empty text-center d-block" 
                        onclick="$('html, body').animate({scrollTop: $('#reviews').offset().top -100 }, 'slow');"
                        itemprop="aggregateRating" 
                        itemtype="http://schema.org/AggregateRating" 
                        itemscope
                    >
                        <meta itemprop="reviewCount" content="{{rand(10, 100)}}" />
                        <meta itemprop="ratingValue" content="{{rand(40, 50) / 10}}" />
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <div class="review-cta">Prvi recenzirajte ovaj proizvod</div>
                    </a>

                    <div itemprop="review" itemtype="http://schema.org/Review" itemscope>
    					<div itemprop="author" itemtype="http://schema.org/Person" itemscope>
    						<meta itemprop="name" content="{{$randomReviewerNames[array_rand($randomReviewerNames)]}}" />
    					</div>
    					<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
    						<meta itemprop="ratingValue" content="{{rand(40, 50) / 10}}" />
    						<meta itemprop="bestRating" content="5" />
    					</div>
    				</div>
                @endif

                <br>
                <hr class="m-0">
                <br>
                <p class="text-center">Šifra proizvoda: {{$product->sku}}</p>
                <br>

                <div itemprop="brand" itemtype="http://schema.org/Brand" itemscope>
                    <meta itemprop="name" content="{{$product->brand->name}}"/>
                </div>

                <div class="price-description {{$product->onSale() ? 'discount' : ''}}" itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
                    <link itemprop="url" href="{{$product->url()}}" />
                    <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
                    <div class="price-title">Cijena:</div>
                    <meta itemprop="price" content="{{$product->realPrice()}}" />
                    <meta itemprop="pricecurrency" content="BAM">
                    <meta itemprop="priceValidUntil" content="{{now()->addYears(3)->format('Y-m-d')}}" />
                    <span class="price">{{formatPrice($product->price)}}</span>
                    @if($product->onSale())
                        <span class="discount-price">
                            {{formatPrice($product->discount_price)}}
                        </span>
                    @endif
                    @if($product->in_stock)
                        <meta itemprop="availability" content="https://schema.org/InStock" />
                    @else
                        <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                    @endif
                </div>

                <meta itemprop="sku" content="{{$product->sku}}" />
                <meta itemprop="mpn" content="{{$product->sku}}" />
                <br>
                <hr class="m-0">
                <br>

                @if($product->buy_count_30_days >= 10)
                    <div class="free-shipping-success m-0">
                        Ovaj proizvod je kupljen {{$product->buy_count_30_days}} puta u zadnjih 7 dana!
                    </div>
                @endif

                <add-to-cart-button :product="{{$product->toSearchableJson()}}"></add-to-cart-button>
                <br>
                <br>
                @if($product->in_stock)
                    <shipping-info 
                        :shipping-methods="{{json_encode($shippingMethods)}}" 
                        :has-free-shipping="{{$product->hasFreeShipping() ? 'true' : 'false'}}"
                        fast-shipping-info-url="{{route('delivery')}}#hitna-dostava"
                    ></shipping-info>
                @else 
                    <back-to-stock-alert-form product-id="{{$product->id}}"></back-to-stock-alert-form>
                @endif

                @if($product->description)
                    <br>
                    <br>
                    <show-more inline-template>
                        <div>
                            <div class="font-weight-bold">Opis proizvoda</div>
                            <div class="show-more-wrapper my-2" :class="{'collapsed': collapsed}" :style="{height: height + 'px'}">
                                <div id="description" class="redactor-styles">
                                    {!!$product->description!!}
                                </div>
                                <div v-if="showCollapse" class="overlay"></div>
                            </div>
                            <span v-if="showCollapse && collapsed" class="link font-weight-bold" @click.prevent="toggleCollapse">
                                Prikaži više o proizvodu
                                <i aria-hidden="true" class="fa fa-chevron-down"></i>
                            </span>
                            <span v-else-if="showCollapse" class="link font-weight-bold" @click.prevent="toggleCollapse">
                                Prikaži manje
                                <i aria-hidden="true" class="fa fa-chevron-up"></i>
                            </span>
                        </div>
                    </show-more>
                @endif

                <br>
                <br>
                <div class="font-weight-bold mb-2">Česta pitanja</div>

                <div class="collapse-block">
                    <h2 class="w-100 p-3 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseWhyToBuy" aria-expanded="false" aria-controls="footerCollapseWhyToBuy">
                        Zašto kupovati na {{config('app.domain')}}?
                        <i class="fa fa-chevron-down right" aria-hidden="true"></i>
                        <i class="fa fa-chevron-up right" aria-hidden="true"></i>
                    </h2>
                    <div class="collapse" id="footerCollapseWhyToBuy">
                        <div class="px-3 pb-3">
                            <ul>
                                <li>Najjeftinija dostava od samo <b>{{config('app.lowest_shipping_cost')}}KM</b>.</li>
                                <li>Besplatna dostava za narudžbe iznad <b>{{config('app.free_shipping_threshold')}}KM</b>.</li>
                                <li>Šaljemo isti dan ukoliko naručite do <b>14:00h</b>.</li>
                                <li>Najbolje cijene.</li>
                                <li>Bez skrivenih troškova.</li>
                                <li>Testeri uz narudžbe.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="collapse-block">
                    <h2 class="w-100 p-3 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseDelivery" aria-expanded="false" aria-controls="footerCollapseDelivery">
                        Koja je cijena dostave?
                        <i class="fa fa-chevron-down right" aria-hidden="true"></i>
                        <i class="fa fa-chevron-up right" aria-hidden="true"></i>
                    </h2>
                    <div class="collapse" id="footerCollapseDelivery">
                        <div class="px-3 pb-3">
                            <ul>
                                <li>Cijena dostave je već od <b>{{config('app.lowest_shipping_cost')}}KM</b> u sve gradove i mjesta u BIH.</li>
                                <li>Da bi ostvarili besplatnu dostavu potrebno je da ukupna vrijednost naručenih proizvoda dostigne ili pređe iznos od <b>{{config('app.free_shipping_threshold')}}KM</b>.</li>
                                <li>Narudžbu je moguće preuzeti i u jednoj od naših <a href="{{route('locations')}}" class="link" target="_blank">lokacija</a> bez dodatnih troškova.</li>
                                <li>Ukoliko ste napravili narudžbu prije <b>14:00h</b>, narudžba se šalje isti dan (osim vikendom i praznicima). U suprotnom se šalje sljedeći radni dan.</li>
                            </ul>
                            <br>

                            Više informacija o dostavi možete pronaći <a href="{{route('delivery')}}" class="link" target="_blank">ovdje.</a>
                        </div>
                    </div>
                </div>
                <div class="collapse-block">
                    <h2 class="w-100 p-3 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapsePaying" aria-expanded="false" aria-controls="footerCollapsePaying">
                        Kako mogu da platim?
                        <i class="fa fa-chevron-down right" aria-hidden="true"></i>
                        <i class="fa fa-chevron-up right" aria-hidden="true"></i>
                    </h2>
                    <div class="collapse" id="footerCollapsePaying">
                        <div class="px-3 pb-3">
                            Vašu narudžbu možete platiti na jedan od sljedećih načina:
                            <ul>
                                <li>Plaćanje bankovnom karticom Mastercard®, Maestro® ili Visa</li>
                                <li>Plaćanje pouzećem dostavljaču (gotovina)</li>
                                <li>Plaćanje u poslovnici (gotovina i kartično)</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="collapse-block">
                    <h2 class="w-100 p-3 collapsed collapsed-handle" data-toggle="collapse" data-target="#footerCollapseChange" aria-expanded="false" aria-controls="footerCollapseChange">
                        Da li mogu izvršiti povrat?
                        <i class="fa fa-chevron-down right" aria-hidden="true"></i>
                        <i class="fa fa-chevron-up right" aria-hidden="true"></i>
                    </h2>
                    <div class="collapse" id="footerCollapseChange">
                        <div class="px-3 pb-3">
                            <ul>
                                <li>Svi kupci {{config('app.name')}} web shopa imaju pravo na povrat robe uz odgovarajući fiskalni račun.</li>
                                <li>Ako smo Vam poslali pogrešan proizvod, proizvod kojem je istekao rok ili oštećen proizvod, rado ćemo snositi troškove povrata robe. U svim drugim slučajevima, troškove povrata snose kupci.</li>
                                <li>Rok za povrat robe je 8 dana od dana primanja proizvoda.</li>
                                <li>Ako vraćena roba nije u odgovarajućem stanju to jest korištena i oštećena ili je originalna ambalaža oštećena ili nemate odgovarajući fiskalni račun, nismo u mogućnosti izvršiti povrat proizvoda.</li>
                            </ul>
                            <br>

                            Više informacija o povratu možete pronaći <a href="{{route('returns')}}" class="link" target="_blank">ovdje.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>

<section class="container-xl product-reviews">
    <reviews 
        :product-id="{{$product->id}}"
        @if(auth()->check())
            first-name="{{auth()->user()->first_name}}"
        @endif
    ></reviews>
</section>

@if(isset($relatedProductsByBrand) && $relatedProductsByBrand->isNotEmpty())
    <section class="products-index products-carousel related-by-brand">
        <div class="container-xl">
            <div class="row products-grid">
                <div class="product-wrap col-lg-12">
                    <div class="carousel-title">
                        <h2>A šta misliš o ovim proizvodima brenda <br class="d-md-none"> <a href="{{route('brands.show', ['brand' => $product->brand->getSlug()])}}" class="link">{{$product->brand->name}}</a>?</h2>
                    </div>
                    <products-carousel inline-template v-cloak>
                        <div v-if="isTabletOrSmaller" class="horizontal-slider">
                            <div class="horizontal-slider-items" ref="horizontalSliderItems">
                                @foreach ($relatedProductsByBrand as $relatedProductByBrand)
                                    @include('products/partials/product', [
                                        'product' => $relatedProductByBrand,
                                        'dataLayerList' => 'Recommended Products List By Brand',
                                        'dataLayerPosition' => $loop->index,
                                        'inCarousel' => true
                                    ])
                                @endforeach
                            </div>
                            <div v-if="hasScroll" class="horizontal-slider-slide">
                                <input type="range" min="1" step="1" :max="mobileMaxScroll" :value="mobileScrollValue" @input="updateScroll" class="slider" ref="mobileSlider">
                            </div>
                        </div>
                        <carousel v-else
                            :loop="true"
                            :per-page-custom="[[576, 3], [880, 4], [1200, 5]]" 
                            :scroll-per-page="true" 
                            :pagination-enabled="true"
                            pagination-active-color="#009900"
                            pagination-color="#e5e5e5"
                            :navigation-enabled="true"
                            navigation-prev-label="<i class='fa fa-chevron-left' aria-hidden='true'></i>"
                            navigation-next-label="<i class='fa fa-chevron-right' aria-hidden='true'></i>"
                        >
                            @foreach ($relatedProductsByBrand as $relatedProductByBrand)
                                <slide>
                                    @include('products/partials/product', [
                                        'product' => $relatedProductByBrand,
                                        'dataLayerList' => 'Recommended Products List By Brand',
                                        'dataLayerPosition' => $loop->index,
                                        'inCarousel' => true
                                    ])
                                </slide>
                            @endforeach
                        </carousel>
                    </products-carousel>
                </div>
            </div>
        </div>
    </section>
@endif
@if(isset($relatedProductsByCategory) && $relatedProductsByCategory->isNotEmpty())
    <section class="products-index products-carousel related-by-category">
        <div class="container-xl">
            <div class="row products-grid">
                <div class="product-wrap col-lg-12">
                    <div class="carousel-title">
                        <h2>Probaj i druge proizvode iz kategorije <br class="d-md-none"> <a href="{{route('categories.show', ['category' => $lastCategory->getSlug()])}}" class="link">{{$lastCategory->title}}</a>.</h2>
                    </div>
                    <products-carousel inline-template v-cloak>
                        <div v-if="isTabletOrSmaller" class="horizontal-slider">
                            <div class="horizontal-slider-items" ref="horizontalSliderItems">
                                @foreach ($relatedProductsByCategory as $relatedProductByCategory)
                                    @include('products/partials/product', [
                                        'product' => $relatedProductByCategory,
                                        'dataLayerList' => 'Recommended Products List By Category',
                                        'dataLayerPosition' => $loop->index,
                                        'inCarousel' => true
                                    ])
                                @endforeach
                            </div>
                            <div v-if="hasScroll" class="horizontal-slider-slide">
                                <input type="range" min="1" step="1" :max="mobileMaxScroll" :value="mobileScrollValue" @input="updateScroll" class="slider" ref="mobileSlider">
                            </div>
                        </div>
                        <carousel v-else
                            :loop="true"
                            :per-page-custom="[[576, 3], [880, 4], [1200, 5]]" 
                            :scroll-per-page="true" 
                            :pagination-enabled="true"
                            pagination-active-color="#009900"
                            pagination-color="#e5e5e5"
                            :navigation-enabled="true"
                            navigation-prev-label="<i class='fa fa-chevron-left' aria-hidden='true'></i>"
                            navigation-next-label="<i class='fa fa-chevron-right' aria-hidden='true'></i>"
                        >
                            @foreach ($relatedProductsByCategory as $relatedProductByCategory)
                                <slide>
                                    @include('products/partials/product', [
                                        'product' => $relatedProductByCategory,
                                        'dataLayerList' => 'Recommended Products List By Category',
                                        'dataLayerPosition' => $loop->index,
                                        'inCarousel' => true
                                    ])
                                </slide>
                            @endforeach
                        </carousel>
                    </products-carousel>
                </div>
            </div>
        </div>
    </section>
@endif
@endsection

@push('gtm_data_layer')
    <script>
        dataLayer.push({ ads: null }); // Clear the previous ads object.
        dataLayer.push({ ecommerce: null }); // Clear the previous ecommerce object.
        dataLayer.push({
            event: 'view_item',

            // Google Dynamic Remarketing values
            ads: {
                value: {{$product->realPrice()}},
                items: [{
                    id: {{$product->id}},
                    google_business_vertical: 'custom'
                }]
            },

            // GA4 values
            ecommerce: {
                currency: 'BAM',
                value: {{$product->realPrice()}},
                items: [{
                    item_id: {{$product->id}},
                    item_name: '{{$product->fullName()}}',
                    currency: 'BAM',
                    item_brand: '{{$product->brand->name}}',
                    location_id: 'ChIJTTMviO9IYUcRDYkZWEDmylQ',
                    price: {{$product->realPrice()}},
                    quantity: 1,
                    index: 0,

                    @if(array_key_exists(0, $product->categoriesHierarchyAsArray()))
                        item_category: '{{$product->categoriesHierarchyAsArray()[0]}}',
                    @endif

                    @if(array_key_exists(1, $product->categoriesHierarchyAsArray()))
                        item_category2: '{{$product->categoriesHierarchyAsArray()[1]}}',
                    @endif

                    @if(array_key_exists(2, $product->categoriesHierarchyAsArray()))
                        item_category3: '{{$product->categoriesHierarchyAsArray()[2]}}'
                    @endif
                }]
            }
        });
    </script>
@endpush