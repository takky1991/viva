@extends('layouts.app')

@section('content')

<section class="container-xl product-reviews mt-4">
    <h1>{{$product->fullName()}}</h1>
    <form class="py-4" method="POST" action="{{ route('products.review.save', ['product' => $product->getSlug()]) }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="rating">Ocjena *</label>
            <div class="rating-input" id="rating">
                <label>
                    <input type="radio" name="rating" value="1" {{ old('rating') == 1 ? 'checked' : '' }}/>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </label>
                <label>
                    <input type="radio" name="rating" value="2" {{ old('rating') == 2 ? 'checked' : '' }}/>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </label>
                <label>
                    <input type="radio" name="rating" value="3" {{ old('rating') == 3 ? 'checked' : '' }}/>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </label>
                <label>
                    <input type="radio" name="rating" value="4" {{ old('rating') == 4 ? 'checked' : '' }}/>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </label>
                <label>
                    <input type="radio" name="rating" value="5" {{ old('rating') == 5 ? 'checked' : '' }}/>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i id="_dusk_review_rating" class="fa fa-star" aria-hidden="true"></i>
                </label>
            </div>
            @if ($errors->has('rating'))
                <div class="invalid-feedback" style="display: block; font-size: 14px;">
                    Unesite ocjenu
                </div>
            @endif
        </div>
    
        <div class="form-group">
            <label for="name">Vaše Ime *</label>
            <input id="name" type="text" class="input-field @if($errors->has('name')) invalid @endif" name="name" value="{{ old('name') }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback" style="display: block; font-size: 14px;">
                    Unesite Vaše ime
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Recenzija *</label> 
            <textarea name="content" id="content" rows="5" class="text-field @if($errors->has('content')) invalid @endif">{{ old('content') }}</textarea>
            @if ($errors->has('content'))
                <div class="invalid-feedback" style="display: block; font-size: 14px;">
                    Unesite recenziju
                </div>
            @endif
        </div>

        @if(request()->filled('order_number'))
            <input type="hidden" value="{{request()->order_number}}" name="order_number">
        @endif

        <button class="btn btn-primary" id="_dusk_review_submit" type="submit">Pošalji</button>
    </form>
</section>
@endsection