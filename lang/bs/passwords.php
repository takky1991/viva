<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'password' => 'Potrebno je da šifra bude minimalno 8 karaktera duga i da je potvrdite.',
    'reset' => 'Kreirali ste novu šifru!',
    'sent' => 'Poslali smo Vam e-mail za kreiranje nove šifre!',
    'token' => 'Token za promjenu šifre nije ispravan.',
    'user' => 'Ne možemo pronaći korisnika sa datom e-mail adresom.',
];
