const mix = require('laravel-mix');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

require('laravel-mix-merge-manifest');

mix.options({ 
    terser: {
        terserOptions: {
            compress: {
               drop_console: true
            }
        }
    }
});

mix.webpackConfig({
    output: {
        publicPath: '/',
        chunkFilename: mix.inProduction() ? 'js/admin/[name].js?id=[hash]' : 'js/admin/[name].js'
    },
    plugins: [
        // This plugin should remove/clean the build folders before building
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [
                'css/admin',
                'js/admin',
                'mix-manifest.json'
            ]
        })
    ],
    stats: { children: true }
});

mix.js('resources/assets/js/backend.js', 'public/js/admin')
    .extract()
    .vue();

mix.sass('resources/assets/sass/backend.scss', 'public/css/admin');

if (mix.inProduction()) {
    mix.version();
}

mix.mergeManifest();