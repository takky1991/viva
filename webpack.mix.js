const mix = require('laravel-mix');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

require('laravel-mix-purgecss');
require('laravel-mix-merge-manifest');

mix.options({ 
    terser: {
        terserOptions: {
            compress: {
               drop_console: true
            }
        }
    }
});

mix.webpackConfig({
    output: {
        publicPath: '/',
        chunkFilename: mix.inProduction() ? 'js/app/[name].js?id=[hash]' : 'js/app/[name].js'
    },
    plugins: [
        // This plugin should remove/clean the build folders before building
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [
                'css/app',
                'js/app',
                'fonts',
                'images',
                'mix-manifest.json'
            ]
        })
    ],
    stats: { children: true }
});

mix.js('resources/assets/js/app.js', 'public/js/app')
    .extract()
    .vue();

mix.sass('resources/assets/sass/app.scss', 'public/css/app')
    .purgeCss({
        enabled: true, // runs also on dev
        fontFace: true, // remove unused font faces
        variables: true, // remove unsused variables
        safelist: {
        deep: [/vh--*/, /VueCarousel*/, /vue-phone-number-input*/]
        }
    });

mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/fonts/Poppins', 'public/fonts');

if (mix.inProduction()) {
    mix.version();
}

mix.mergeManifest();